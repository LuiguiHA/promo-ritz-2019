<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//$route[$this->config->item('base_api').'/users']['get'] ='api/user/index';


/* DEFAULT ROUTES */
$route['default_controller'] = 'inicio';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Web

$route['meme-generator'] = "frontend/inicio/memegenerator";
$route['ganadores'] = "frontend/inicio/winners";
$route['premios'] = "frontend/inicio/prizes";
$route['meme-list'] = "frontend/inicio/memelist";
$route['meme/:num'] = "frontend/inicio/meme";
$route['meme/(meme\-[0-9a-zA-Z]+)'] = "frontend/inicio/memebyname";
$route['perfil'] = "frontend/inicio/profile";
$route['home'] = "frontend/inicio/home";


$route['subasta'] = 'Web/subasta';
$route['subasta/(:num)'] = 'Web/subasta/$1';
$route['participa'] = 'Web/participa';
$route['login'] = 'Web/login';
$route['logout'] = 'Web/logout';
$route['concourses'] = 'Web/package';
$route['conquista-tu-momento'] = 'Web/index/conquista-tu-momento';
$route['participa-en-la-subasta'] = 'Web/index/participa-en-la-subasta';

//die($this->config->item('base_api').'/users');
/* API ROUTES*/
$route[$this->config->item('base_api').'/users']['post'] = 'api/User_Controller/store';
$route[$this->config->item('base_api').'/users/login']['post'] = 'api/User_Controller/login';
$route[$this->config->item('base_api').'/users/logout']['post'] = 'api/User_Controller/logout';
$route[$this->config->item('base_api').'/users/detail']['post'] = 'api/User_Controller/detail';
$route[$this->config->item('base_api').'/users/points']['post'] = 'api/User_Controller/points';
$route[$this->config->item('base_api').'/users/share']['post'] = 'api/User_Controller/share_facebook';
$route[$this->config->item('base_api').'/users/share_meme']['post'] = 'api/User_Controller/share_meme';
$route[$this->config->item('base_api').'/users/share_lote']['post'] = 'api/User_Controller/share_lote';
$route[$this->config->item('base_api').'/users/list_memes/(:num)']['get'] = 'api/User_Controller/list_memes/$1';
$route[$this->config->item('base_api').'/users/list_memes_by_user/(:num)']['post'] = 'api/User_Controller/list_memes_by_user/$1';
$route[$this->config->item('base_api').'/users/recover']['post'] = 'api/User_Controller/recover_account';
$route[$this->config->item('base_api').'/meme/save-meme']['post'] = "api/Meme_Controller/save_meme";
$route[$this->config->item('base_api').'/meme/save-meme-image']['post'] = "api/Meme_Controller/save_meme_image";
$route[$this->config->item('base_api').'/meme/validate-lote']['post'] = "api/Meme_Controller/validate_lote";

$route[$this->config->item('base_api').'/winners/list_winners/(:num)']['get'] = 'api/Winner_Controller/list_winners/$1';
$route[$this->config->item('base_api').'/winners/list_winners']['get'] = 'api/Winner_Controller/winners';

$route[$this->config->item('base_api').'/challenges/list']['post'] = 'api/Challenge_Controller/list';

$route[$this->config->item('base_api').'/photos/list/(:num)']['post'] = 'api/Photo_Controller/list/$1';

$route[$this->config->item('base_api').'/concourses']['post'] = 'api/Concourse_Controller/store';

$route[$this->config->item('base_api').'/prizes/register_winner']['post'] = 'api/Prize_Controller/register_winner';
$route[$this->config->item('base_api').'/prizes/list_prizes']['post'] = 'api/Prize_Controller/list_prizes_winner';

$route[$this->config->item('base_api').'/prizes/list_prizes_sale']['post'] = 'api/Prize_Controller/list_prizes_sale';

$route[$this->config->item('base_api').'/prizes/sale']['post'] = 'api/Prize_Controller/start_sale';
$route[$this->config->item('base_api').'/prizes/status']['get'] = 'api/Prize_Controller/prize_sale_status';
$route[$this->config->item('base_api').'/prizes/winners']['get'] = 'api/Prize_Controller/list_winners';

$route[$this->config->item('base_api').'/prizes/list_menu']['get'] = 'api/Prize_Controller/get_list_prize_menu';

$route[$this->config->item('base_api').'/terms_conditions/list']['get'] = 'api/Term_Condition_Controller/list_terms';

$route[$this->config->item('base_api').'/questions/list']['get'] = 'api/Question_Controller/list_questions';


/* CMS ROUTES */

$route[$this->config->item('base_cms')] = "/cms/Index_Controller";
$route[$this->config->item('base_cms').'/Index/login'] = "cms/Index_Controller/login";
$route[$this->config->item('base_cms').'/logout'] = "cms/Index_Controller/logout";

$route[$this->config->item('base_cms').'/Usuario'] = "cms/User_Controller/list_view";
$route[$this->config->item('base_cms').'/Usuario/(:num)'] = "cms/User_Controller/list_view/$1";
$route[$this->config->item('base_cms').'/Usuario/buscar'] = "cms/User_Controller/list_search_view";
$route[$this->config->item('base_cms').'/Usuario/buscar/(:num)'] = "cms/User_Controller/list_search_view/$1";
$route[$this->config->item('base_cms').'/Usuario/exportar_lista'] = "cms/User_Controller/export_list";
$route[$this->config->item('base_cms').'/Usuario/lista_ganadores'] = "cms/User_Controller/list_winners_view";
$route[$this->config->item('base_cms').'/Usuario/lista_ganadores/(:num)'] = "cms/User_Controller/list_winners_view/$1";
$route[$this->config->item('base_cms').'/Usuario/lista_ganadores/buscar'] = "cms/User_Controller/list_search_winners_view";
$route[$this->config->item('base_cms').'/Usuario/lista_ganadores/buscar/(:num)'] = "cms/User_Controller/list_search_winners_view/$1";
$route[$this->config->item('base_cms').'/Usuario/exportar_lista_ganadores'] = "cms/User_Controller/export_list_winners";

$route[$this->config->item('base_cms').'/Usuario/editar'] = "cms/User_Controller/edit_view";
$route[$this->config->item('base_cms').'/Usuario/editar/(:num)'] = "cms/User_Controller/edit_view/$1";

$route[$this->config->item('base_cms').'/Subasta'] = "cms/Subasta_Controller/list_view";
$route[$this->config->item('base_cms').'/Subasta/usuarios/(:num)'] = "cms/Subasta_Controller/list_users_view/$1";
$route[$this->config->item('base_cms').'/Subasta/premio/asignar/(:num)/(:num)/(:num)'] = "cms/Subasta_Controller/add_prize/$1/$2/$3";



$route[$this->config->item('base_cms').'/Premio'] = "cms/Prize_Controller/list_view";
$route[$this->config->item('base_cms').'/Premio/(:num)'] = "cms/Prize_Controller/list_view/$1";
$route[$this->config->item('base_cms').'/Premio/actualizar_estado'] = "cms/Prize_Controller/update_state";

$route[$this->config->item('base_cms').'/Administrador'] = "cms/Admin_Controller/list_view";
$route[$this->config->item('base_cms').'/Administrador/(:num)'] = "cms/Admin_Controller/list_view/$1";
$route[$this->config->item('base_cms').'/Administrador/buscar'] = "cms/Admin_Controller/list_search_view";
$route[$this->config->item('base_cms').'/Administrador/buscar/(:num)'] = "cms/Admin_Controller/list_search_view/$1";
$route[$this->config->item('base_cms').'/Administrador/exportar_lista'] = "cms/Admin_Controller/export_list";
$route[$this->config->item('base_cms').'/Administrador/editar/(:num)'] = "cms/Admin_Controller/update_view/$1";
$route[$this->config->item('base_cms').'/Administrador/actualizar/(:num)'] = "cms/Admin_Controller/update/$1";
$route[$this->config->item('base_cms').'/Administrador/agregar'] = "cms/Admin_Controller/add_view";
$route[$this->config->item('base_cms').'/Administrador/insertar'] = "cms/Admin_Controller/add";

$route[$this->config->item('base_cms').'/Sorteo/participantes'] = "cms/Raffle_Controller/list_view";
$route[$this->config->item('base_cms').'/Sorteo/participantes/(:num)'] = "cms/Raffle_Controller/list_view/$1";
$route[$this->config->item('base_cms').'/Sorteo/participantes/buscar'] = "cms/Raffle_Controller/list_search_view";
$route[$this->config->item('base_cms').'/Sorteo/participantes/buscar/(:num)'] = "cms/Raffle_Controller/list_search_view/$1";
$route[$this->config->item('base_cms').'/Sorteo/exportar_lista'] = "cms/Raffle_Controller/export_list";
$route[$this->config->item('base_cms').'/Sorteo'] = "cms/Raffle_Controller/generate_view";
$route[$this->config->item('base_cms').'/Sorteo/generar'] = "cms/Raffle_Controller/generate";
$route[$this->config->item('base_cms').'/Sorteo/ganadores'] = "cms/Raffle_Controller/list_winners_view";
$route[$this->config->item('base_cms').'/Sorteo/ganadores/(:num)'] = "cms/Raffle_Controller/list_winners_view/$1";
$route[$this->config->item('base_cms').'/Sorteo/ganadores/buscar'] = "cms/Raffle_Controller/list_search_winners_view";
$route[$this->config->item('base_cms').'/Sorteo/ganadores/buscar/(:num)'] = "cms/Raffle_Controller/list_search_winners_view/$1";
$route[$this->config->item('base_cms').'/Sorteo/ganadores/exportar_lista'] = "cms/Raffle_Controller/export_list_winners";

$route[$this->config->item('base_cms').'/Sorteo_Premios/lista'] = "cms/Raffle_Prize_Controller/list_prizes";

$route[$this->config->item('base_cms').'/Configuracion'] = "cms/Configuration_Controller/update_view";
$route[$this->config->item('base_cms').'/Configuracion/detalle'] = "cms/Configuration_Controller/get_configuration";
$route[$this->config->item('base_cms').'/Configuracion/actualizar'] = "cms/Configuration_Controller/update";


$route[$this->config->item('base_cms').'/Meme'] = "cms/Meme_Controller/list_view";
$route[$this->config->item('base_cms').'/Meme/(:num)'] = "cms/Meme_Controller/list_view/$1";
$route[$this->config->item('base_cms').'/Meme/editar/(:num)'] = "cms/Meme_Controller/update_view/$1";
$route[$this->config->item('base_cms').'/Meme/actualizar/(:num)'] = "cms/Meme_Controller/update/$1";
$route[$this->config->item('base_cms').'/Meme/exportar_lista'] = "cms/Meme_Controller/export_list";

$route[$this->config->item('base_cms').'/Meme/buscar'] = "cms/Meme_Controller/list_search_view";
$route[$this->config->item('base_cms').'/Meme/buscar/(:num)'] = "cms/Meme_Controller/list_search_view/$1";
