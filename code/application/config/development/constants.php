<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/* API CONSTANTS*/


define('RESULT_SUCCESS',1);
define('RESULT_ERROR',0);
define('INITIAL_POINTS',0);

define('ACTIVE',1);
define('INACTIVE',0);

define('STATUS_ACTIVE','1');
define('STATUS_INACTIVE','0');

define('STATUS_WIN','1');
define('STATUS_LOSE','0');


define('WIN',1);
define('LOSE',0);

define('MAX_WON_PER_CHALLENGE', 3);
define('MAX_WON_PER_DAY', 5);

define('MAX_RANDOM', 1);

define('POINTS_CONCOURSE', 1);
define('POINTS_SHARE', 5);

define('POINTS_SHARE_MEME', 1);
define('POINTS_SHARE_MEME_OWNER', 1);

define('SHARE_FACEBOOK', 1);

define('PAGINATION_CHALLENGE', '4');

define('PAGINATION_USERS', '9');

define('URL_HOST_IMAGES', 'http://www.promoritz.mccann.mediabytelab.com/gateway/');
define('DEFAULT_IMAGE', '/image');

define('RAFFLE_PRIZE_TEXT', 'Una experiencia por');

define('ACTIVE_LOG_API', '1');
define('INACTIVE_LOG_API', '0');

define('RANDOM_PHOTOS', '4');


define('NAME_METHOD_POST', 'post');
define('NAME_METHOD_PUT', 'put');
define('NAME_METHOD_GET', 'get');
define('NAME_METHOD_DELETE', 'delete');

define('API_USER_ACTION_LOGIN', 'login');
define('API_USER_ACTION_LOGOUT', 'logout');
define('API_USER_ACTION_DETAIL', 'detail');
define('API_USER_ACTION_POINTS', 'points');
define('API_USER_ACTION_REGISTER', 'register');
define('API_USER_ACTION_SHARE_FACEBOOK', 'share_facebook');
define('API_USER_ACTION_SHARE_INSTAGRAM', 'share_instagram');
define('API_USER_ACTION_SHARE_WHATSAPP', 'share_whatsapp');
define('API_USER_ACTION_RECOVER_ACCOUNT', 'recover_account');
define('API_USER_ACTION_REGISTER_LOTE', 'register_lote');

define('API_USER_TOTAL_MEMES_POR_LOTE', 12);

define('API_WINNER_ACTION_LIST_MEMES', 'list_memes');

define('API_WINNER_ACTION_LIST_WINNERS', 'list_winners');

define('API_CHALLENGE_ACTION_LIST', 'list');

define('API_TERM_CONDITION_ACTION_LIST', 'list');

define('API_QUESTION_ACTION_LIST', 'list');

define('API_PHOTO_ACTION_LIST', 'list');

define('API_CONCOURSE_ACTION_REGISTER', 'register');

define('API_PRIZE_ACTION_REGISTER_WINNER', 'register_winner');
define('API_PRIZE_ACTION_LIST_PRIZES_WINNER', 'list_prizes_winner');
define('API_PRIZE_ACTION_LIST_MENU', 'list');


/* CMS CONSTANST */

define('PAGINATION_LIST', '10');

define('SUPER_ADMIN', '1');



//countries
define('COUNTRY_PERU','PE');
define('COUNTRY_DOMINICAN_REPUBLIC','DO');
define('COUNTRY_ECUADOR','EC');
define('COUNTRY_PUERTO_RICO','PR');
define('COUNTRY_ADMIN','ADM');
define('COUNTRY_OTHERS','OT');

//DATABASES

define('USERS_TABLE','users');
define('USERS_ID',USERS_TABLE.'.id');
define('USERS_CAMPAIGN_ID',USERS_TABLE.'.campaign_id');
define('USERS_COUNTRY',USERS_TABLE.'.country');
define('USERS_NAME', USERS_TABLE.'.name');
define('USERS_DNI', USERS_TABLE.'.dni');
define('USERS_EMAIL', USERS_TABLE.'.email');
define('USERS_PHONE', USERS_TABLE.'.phone');
define('USERS_POINTS', USERS_TABLE.'.points');
define('USERS_SHARE', USERS_TABLE.'.share');
define('USERS_WIN', USERS_TABLE.'.win');
define('USERS_CREATED_AT',USERS_TABLE.'.created_at');
define('USERS_UPDATED_AT',USERS_TABLE.'.updated_at');
define('USERS_DELETED_AT',USERS_TABLE.'.deleted_at');
define('USERS_STATUS', USERS_TABLE.'.status');

define('TOKENS_TABLE','tokens');
define('TOKENS_ID',TOKENS_TABLE.'.id');
define('TOKENS_USER_ID',TOKENS_TABLE.'.user_id');
define('TOKENS_TOKEN',TOKENS_TABLE.'.token');
define('TOKENS_CREATED_AT',TOKENS_TABLE.'.created_at');
define('TOKENS_UPDATED_AT',TOKENS_TABLE.'.updated_at');
define('TOKENS_DELETED_AT',TOKENS_TABLE.'.deleted_at');
define('TOKENS_STATUS',TOKENS_TABLE.'.status');

define('CONCOURSES_TABLE','concourses');
define('CONCOURSES_ID',CONCOURSES_TABLE.'.id');
define('CONCOURSES_USER_ID',CONCOURSES_TABLE.'.user_id');
define('CONCOURSES_CHALLENGE_ID',CONCOURSES_TABLE.'.challenge_id');
define('CONCOURSES_DATE',CONCOURSES_TABLE.'.date');
define('CONCOURSES_TIME',CONCOURSES_TABLE.'.time');
define('CONCOURSES_WIN',CONCOURSES_TABLE.'.win');
define('CONCOURSES_CREATED_AT',CONCOURSES_TABLE.'.created_at');
define('CONCOURSES_UPDATED_AT',CONCOURSES_TABLE.'.updated_at');
define('CONCOURSES_DELETED_AT',CONCOURSES_TABLE.'.deleted_at');
define('CONCOURSES_STATUS',CHALLENGES_TABLE.'.status');

define('CHALLENGES_TABLE','challenges');
define('CHALLENGES_ID',CHALLENGES_TABLE.'.id');
define('CHALLENGES_CAMPAIGN_ID',CHALLENGES_TABLE.'.campaign_id');
define('CHALLENGES_COUNTRY',CHALLENGES_TABLE.'.country');
define('CHALLENGES_NAME',CHALLENGES_TABLE.'.name');
define('CHALLENGES_DESCRIPTION',CHALLENGES_TABLE.'.description');
define('CHALLENGES_PHOTO_LIST',CHALLENGES_TABLE.'.photo_list');
define('CHALLENGES_PHOTO_DETAIL',CHALLENGES_TABLE.'.photo_detail');
define('CHALLENGES_CREATED_AT',CHALLENGES_TABLE.'.created_at');
define('CHALLENGES_UPDATED_AT',CHALLENGES_TABLE.'.updated_at');
define('CHALLENGES_DELETED_AT',CHALLENGES_TABLE.'.deleted_at');
define('CHALLENGES_PHOTO',CHALLENGES_TABLE.'.photo');
define('CHALLENGES_STATUS',CHALLENGES_TABLE.'.status');

define('PHOTOS_TABLE','photos');
define('PHOTOS_ID',PHOTOS_TABLE.'.id');
define('PHOTOS_CHALLENGE_ID',PHOTOS_TABLE.'.challenge_id');
define('PHOTOS_URL',PHOTOS_TABLE.'.url');
define('PHOTOS_POSITION',PHOTOS_TABLE.'.position');
define('PHOTOS_COORDINATE_X',PHOTOS_TABLE.'.coordinate_x');
define('PHOTOS_COORDINATE_Y',PHOTOS_TABLE.'.coordinate_y');
define('PHOTOS_RADIUS',PHOTOS_TABLE.'.radius');
define('PHOTOS_LIMIT',PHOTOS_TABLE.'.limit');
define('PHOTOS_WEIGHT',PHOTOS_TABLE.'.weight');
define('PHOTOS_CREATED_AT',PHOTOS_TABLE.'.created_at');
define('PHOTOS_UPDATED_AT',PHOTOS_TABLE.'.updated_at');
define('PHOTOS_DELETED_AT',PHOTOS_TABLE.'.deleted_at');
define('PHOTOS_STATUS',PHOTOS_TABLE.'.status');

define('PRIZES_TABLE','prizes');
define('PRIZES_ID',PRIZES_TABLE.'.id');
define('PRIZES_CAMPAIGN_ID',PRIZES_TABLE.'.campaign_id');
define('PRIZES_NAME',PRIZES_TABLE.'.name');
define('PRIZES_PHOTO',PRIZES_TABLE.'.photo');
define('PRIZES_PHOTO_OFF',PRIZES_TABLE.'.photo_off');
define('PRIZES_PHOTO_MENU',PRIZES_TABLE.'.photo_menu');
define('PRIZES_DESCRIPTION',PRIZES_TABLE.'.description');
define('PRIZES_TYPE',PRIZES_TABLE.'.type');
define('PRIZES_CREATED_AT',PRIZES_TABLE.'.created_at');
define('PRIZES_UPDATED_AT',PRIZES_TABLE.'.updated_at');
define('PRIZES_DELETED_AT',PRIZES_TABLE.'.deleted_at');
define('PRIZES_STATUS',PRIZES_TABLE.'.status');

define('ADMIN_TABLE','admin');
define('ADMIN_ID',ADMIN_TABLE.'.id');
define('ADMIN_CAMPAIGN_ID',ADMIN_TABLE.'.campaign_id');
define('ADMIN_USERNAME',ADMIN_TABLE.'.username');
define('ADMIN_PASSWORD',ADMIN_TABLE.'.password');
define('ADMIN_COUNTRY',ADMIN_TABLE.'.country');
define('ADMIN_SUPER_ADMIN',ADMIN_TABLE.'.super_admin');
define('ADMIN_CREATED_AT',ADMIN_TABLE.'.created_at');
define('ADMIN_UPDATED_AT',ADMIN_TABLE.'.updated_at');
define('ADMIN_DELETED_AT',ADMIN_TABLE.'.deleted_at');
define('ADMIN_STATUS',ADMIN_TABLE.'.status');

define('TOKEN_ADMIN_TABLE','token_admin');
define('TOKEN_ADMIN_ID',TOKEN_ADMIN_TABLE.'.id');
define('TOKEN_ADMIN_ADMIN_ID',TOKEN_ADMIN_TABLE.'.admin_id');
define('TOKEN_ADMIN_TOKEN',TOKEN_ADMIN_TABLE.'.token');
define('TOKEN_ADMIN_CREATED_AT',TOKEN_ADMIN_TABLE.'.created_at');
define('TOKEN_ADMIN_UPDATED_AT',TOKEN_ADMIN_TABLE.'.updated_at');
define('TOKEN_ADMIN_DELETED_AT',TOKEN_ADMIN_TABLE.'.deleted_at');
define('TOKEN_ADMIN_STATUS',TOKEN_ADMIN_TABLE.'.status');


define('RAFFLE_TABLE','raffle');
define('RAFFLE_ID',RAFFLE_TABLE.'.id');
define('RAFFLE_USER_ID',RAFFLE_TABLE.'.user_id');
define('RAFFLE_CREATED_AT',RAFFLE_TABLE.'.created_at');
define('RAFFLE_UPDATED_AT',RAFFLE_TABLE.'.updated_at');
define('RAFFLE_DELETED_AT',RAFFLE_TABLE.'.deleted_at');
define('RAFFLE_STATUS',RAFFLE_TABLE.'.status');

define('RAFFLE_PRIZES_TABLE','raffle_prizes');
define('RAFFLE_PRIZES_ID',RAFFLE_PRIZES_TABLE.'.id');
define('RAFFLE_PRIZES_CAMPAIGN_ID',RAFFLE_PRIZES_TABLE.'.campaign_id');
define('RAFFLE_PRIZES_NAME',RAFFLE_PRIZES_TABLE.'.name');
define('RAFFLE_PRIZES_PHOTO',RAFFLE_PRIZES_TABLE.'.photo');
define('RAFFLE_PRIZES_DESCRIPTION',RAFFLE_PRIZES_TABLE.'.description');
define('RAFFLE_PRIZES_TITLE',RAFFLE_PRIZES_TABLE.'.title');
define('RAFFLE_PRIZES_CREATED_AT',RAFFLE_PRIZES_TABLE.'.created_at');
define('RAFFLE_PRIZES_UPDATED_AT',RAFFLE_PRIZES_TABLE.'.updated_at');
define('RAFFLE_PRIZES_DELETED_AT',RAFFLE_PRIZES_TABLE.'.deleted_at');
define('RAFFLE_PRIZES_STATUS',RAFFLE_PRIZES_TABLE.'.status');

define('CAMPAIGNS_TABLE','campaigns');
define('CAMPAIGNS_ID',CAMPAIGNS_TABLE.'.id');
define('CAMPAIGNS_NAME',CAMPAIGNS_TABLE.'.name');
define('CAMPAIGNS_COUNTRY_NAME',CAMPAIGNS_TABLE.'.country_name');
define('CAMPAIGNS_COUNTRY_CODE',CAMPAIGNS_TABLE.'.country_code');
define('CAMPAIGNS_PRIZE_DAY',CAMPAIGNS_TABLE.'.prize_day');
define('CAMPAIGNS_RANDOM',CAMPAIGNS_TABLE.'.random');
define('CAMPAIGNS_CREATED_AT',CAMPAIGNS_TABLE.'.created_at');
define('CAMPAIGNS_UPDATED_AT',CAMPAIGNS_TABLE.'.updated_at');
define('CAMPAIGNS_DELETED_AT',CAMPAIGNS_TABLE.'.deleted_at');
define('CAMPAIGNS_STATUS',CAMPAIGNS_TABLE.'.status');

define('WINNERS_TABLE','winners');
define('WINNERS_ID',WINNERS_TABLE.'.id');
define('WINNERS_CAMPAIGN_ID',WINNERS_TABLE.'.campaign_id');
define('WINNERS_USER_ID',WINNERS_TABLE.'.user_id');
define('WINNERS_PRIZE_ID',WINNERS_TABLE.'.prize_id');
define('WINNERS_CREATED_AT',WINNERS_TABLE.'.created_at');
define('WINNERS_UPDATED_AT',WINNERS_TABLE.'.updated_at');
define('WINNERS_DELETED_AT',WINNERS_TABLE.'.deleted_at');
define('WINNERS_STATUS',WINNERS_TABLE.'.status');

define('RAFFLE_WINNERS_TABLE','raffle_winners');
define('RAFFLE_WINNERS_ID',RAFFLE_WINNERS_TABLE.'.id');
define('RAFFLE_WINNERS_USER_ID',RAFFLE_WINNERS_TABLE.'.user_id');
define('RAFFLE_WINNERS_PRIZE_ID',RAFFLE_WINNERS_TABLE.'.raffle_prize_id');
define('RAFFLE_WINNERS_CAMPAIGN_ID',RAFFLE_WINNERS_TABLE.'.campaign_id');
define('RAFFLE_WINNERS_CREATED_AT',RAFFLE_WINNERS_TABLE.'.created_at');
define('RAFFLE_WINNERS_UPDATED_AT',RAFFLE_WINNERS_TABLE.'.updated_at');
define('RAFFLE_WINNERS_DELETED_AT',RAFFLE_WINNERS_TABLE.'.deleted_at');
define('RAFFLE_WINNERS_STATUS',RAFFLE_WINNERS_TABLE.'.status');

define('TERMS_CONDITIONS_TABLE','terms_conditions');
define('TERMS_CONDITIONS_ID',TERMS_CONDITIONS_TABLE.'.id');
define('TERMS_CONDITIONS_CAMPAIGN_ID',TERMS_CONDITIONS_TABLE.'.campaign_id');
define('TERMS_CONDITIONS_TITLE',TERMS_CONDITIONS_TABLE.'.title');
define('TERMS_CONDITIONS_DESCRIPTION',TERMS_CONDITIONS_TABLE.'.description');
define('TERMS_CONDITIONS_CREATED_AT',TERMS_CONDITIONS_TABLE.'.created_at');
define('TERMS_CONDITIONS_UPDATED_AT',TERMS_CONDITIONS_TABLE.'.updated_at');
define('TERMS_CONDITIONS_DELETED_AT',TERMS_CONDITIONS_TABLE.'.deleted_at');
define('TERMS_CONDITIONS_STATUS',TERMS_CONDITIONS_TABLE.'.status');

define('QUESTIONS_TABLE','questions');
define('QUESTIONS_ID',QUESTIONS_TABLE.'.id');
define('QUESTIONS_CAMPAIGN_ID',QUESTIONS_TABLE.'.campaign_id');
define('QUESTIONS_TITLE',QUESTIONS_TABLE.'.title');
define('QUESTIONS_DESCRIPTION',QUESTIONS_TABLE.'.description');
define('QUESTIONS_CREATED_AT',QUESTIONS_TABLE.'.created_at');
define('QUESTIONS_UPDATED_AT',QUESTIONS_TABLE.'.updated_at');
define('QUESTIONS_DELETED_AT',QUESTIONS_TABLE.'.deleted_at');
define('QUESTIONS_STATUS',QUESTIONS_TABLE.'.status');
