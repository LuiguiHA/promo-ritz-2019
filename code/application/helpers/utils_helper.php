<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('mostrar_puntos'))
{
    function mostrar_puntos($puntos)
    {
      $html = '';

      $arrayPuntos = str_split($puntos);

      if(count($arrayPuntos) < 8) {
        for($i=0;$i < (8 - count($arrayPuntos)); $i++){
          $html.= '<div class="numero">0</div>';
        }

      }

      foreach ($arrayPuntos as $punto) {
        $html.= '<div class="numero">'.$punto.'</div>';
      }

      return $html;
    }
}


if (!function_exists('get_error_form'))
{
    function get_error_form($errors)
    {
        $message = [];
        foreach($errors as $item => $value){
            $message[] = $value;
        }
        $response_error  = [
            'result' => 0,
            'message' => implode(', ', $message)
        ];
        return $response_error;
    }
}

if (!function_exists('validate_country'))
{
    function validate_country($code_country)
    {
        if ($code_country == COUNTRY_PERU || $code_country == COUNTRY_DOMINICAN_REPUBLIC ||
            $code_country == COUNTRY_PUERTO_RICO || $code_country == COUNTRY_ECUADOR)
        {
            return $code_country;
        }
        return null;
    }
}

if (!function_exists('get_geolocation')) {
    function get_geolocation($ip) {

        /*if ($_SERVER['HTTP_X_FORWARDED_FOR']){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }*/
        //$ip = '74.125.224.72'; //Google
      //  $CI = &get_instance();
       // $ip = $CI->input->ip_address();
        $request = file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key=16391d16397e23d586c1d65f1a645d18e9d4af4d5d0463f0d93cef015b0586e8&ip=$ip");

        return $request;

      /*  $request = file_get_contents("http://www.geoplugin.net/json.gp?ip=\".$ip");
        $response = json_decode($request);
        return $response;*/
    }
}

if (!function_exists('get_error_response'))
{
    function get_error_response($message)
    {
        $error = [
            'result' => RESULT_ERROR,
            'message' => $message
        ];
        return $error;
    }
}

if (!function_exists('get_success_response'))
{
    function get_success_response($message)
    {
        $response['result'] = RESULT_SUCCESS;
        $response['message'] = $message;
        return $response;
    }
}

if (!function_exists('validate_campaign'))
{
    function validate_campaign()
    {
        $CI = &get_instance();
        $CI->load->model("campaign_Model", "campaign_model");
        $CI->lang->load('api_error_messages_lang', 'spanish');

        $query = [
            'select' => CAMPAIGNS_ID.', '.
                CAMPAIGNS_COUNTRY_CODE.', '.
                CAMPAIGNS_COUNTRY_NAME.', '.
                CAMPAIGNS_STATUS,
            'where' => CAMPAIGNS_STATUS."=".ACTIVE
        ];

        $campaign =$CI->campaign_model->search($query);
        $result=array();

        if (isset($campaign))
        {
          if (sizeof($campaign) == 1 )
          {
              $result= array('result'=>1,'campaign'=>$campaign);
          } else
          {
              $result= array('result'=>0,'message'=>$CI->lang->line('campaign_error_get'));
          }
        } else
        {
            $result= array('result'=>0,'message'=>$CI->lang->line('campaign_error_get'));
        }

        return $result;
    }
}

if (!function_exists('save_log_api'))
{
    function save_log_api($params = array(), $email = "", $uri = "", $method = "", $action = "" )
    {
        $CI = &get_instance();

        if ($CI->db->table_exists('logs'))
        {
            $logApp = $CI->config->item('save_log_api');

            if ($logApp == ACTIVE_LOG_API)
            {
                $arrayData = array(
                    "email" => $email,
                    "uri" => $uri,
                    "method" => $method,
                    "action" => $action,
                    "ip" => $CI->input->ip_address(),
                    "user_agent" => $CI->input->user_agent(),
                    "created_at" => date("Y-m-d H:i:s"),
                );

                if (is_array($params))
                    $arrayData["params"] = json_encode($params);
                else
                    $arrayData["params"] = $params;

                $CI->db->insert('logs', $arrayData);
            }
        }

    }
}
