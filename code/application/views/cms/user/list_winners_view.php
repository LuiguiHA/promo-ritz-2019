<div class="row">
    <div class="col-lg-12">
        <div class="content-box">
            <div class="head clearfix head_background_color" style="padding-right: 0;!important;">
                <h5 class="content-title pull-left"><?php echo $this->lang->line('constant_form_title_user_winner')?></h5>
                <div class="pull-right" style="text-align: center;position: relative">

                    <button type="button"  id="div_search" class="btn btn-lg raised icon" >
                        <i id="img_search" class="zmdi zmdi-chevron-down"></i>
                    </button>
                </div>
            </div>

            <div class="content" id="div_content_search">
                <div class="row">
                    <div class="col-md-12">
                        <form enctype="multipart/form-data" method="POST" class="form-horizontal"  action="<?php echo site_url($this->config->item('base_cms').'/Usuario/lista_ganadores/buscar');?>">
                            <div class="form-group">
                                <div
                                  class = "col-md-4">
                                    <label><?php echo $this->lang->line('constant_form_search_dni_user')?></label>
                                    <input class="form-control m-b-20" type="text" id="search_user_winner_dni" maxlength="8" name="search_user_winner_dni" value="<?php echo $search_user_winner_dni?>">
                                </div>

                                <div class = "col-md-4">
                                    <label><?php echo $this->lang->line('constant_form_search_name_user')?></label>
                                    <input class="form-control m-b-20" type="text" id="search_user_winner_name" name="search_user_winner_name" value="<?php echo $search_user_winner_name?>">
                                </div>


                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('constant_form_search_country_user') ?></label>
                                    <select id="search_user_winner_country" class="form-control selectpicker"
                                            name="search_user_winner_country">
                                        <option value=""><?php echo $this->lang->line('constant_select') ?></option>
                                        <option
                                                value="<?php echo COUNTRY_PERU; ?>" <?php if ($search_user_winner_country == COUNTRY_PERU) echo "selected" ?>><?php echo $this->lang->line('constant_country_peru') ?></option>
                                        <option
                                                value="<?php echo COUNTRY_ECUADOR; ?>" <?php if ($search_user_winner_country == COUNTRY_ECUADOR) echo "selected" ?>><?php echo $this->lang->line('constant_country_ecuador') ?></option>
                                        <option
                                                value="<?php echo COUNTRY_DOMINICAN_REPUBLIC; ?>" <?php if ($search_user_winner_country == COUNTRY_DOMINICAN_REPUBLIC) echo "selected" ?>><?php echo $this->lang->line('constant_country_dominican_republic') ?></option>
                                        <option
                                                value="<?php echo COUNTRY_PUERTO_RICO; ?>" <?php if ($search_user_winner_country == COUNTRY_PUERTO_RICO) echo "selected" ?>><?php echo $this->lang->line('constant_country_puerto_rico') ?></option>
                                        <option
                                                value="<?php echo COUNTRY_OTHERS; ?>" <?php if ($search_user_winner_country == COUNTRY_OTHERS) echo "selected" ?>><?php echo $this->lang->line('constant_country_others') ?></option>
                                    </select>
                                </div>



                            </div>
                            <div class="form-group">
                                <?php
                                if ($this->session->userdata("super_admin") == SUPER_ADMIN)
                                {
                                    ?>
                                    <div class="col-md-4">
                                        <label><?php echo $this->lang->line('constant_form_search_campaign_user') ?></label>
                                        <select id="search_user_winner_campaign" class="form-control selectpicker"
                                                name="search_user_winner_campaign">
                                            <option value=""><?php echo $this->lang->line('constant_select') ?></option>
                                            <option
                                                    value="<?php echo COUNTRY_PERU; ?>" <?php if ($search_user_winner_campaign == COUNTRY_PERU) echo "selected" ?>><?php echo $this->lang->line('constant_country_peru') ?></option>
                                            <option
                                                    value="<?php echo COUNTRY_ECUADOR; ?>" <?php if ($search_user_winner_campaign == COUNTRY_ECUADOR) echo "selected" ?>><?php echo $this->lang->line('constant_country_ecuador') ?></option>
                                            <option
                                                    value="<?php echo COUNTRY_DOMINICAN_REPUBLIC; ?>" <?php if ($search_user_winner_campaign == COUNTRY_DOMINICAN_REPUBLIC) echo "selected" ?>><?php echo $this->lang->line('constant_country_dominican_republic') ?></option>
                                            <option
                                                    value="<?php echo COUNTRY_PUERTO_RICO; ?>" <?php if ($search_user_winner_campaign == COUNTRY_PUERTO_RICO) echo "selected" ?>><?php echo $this->lang->line('constant_country_puerto_rico') ?></option>

                                        </select>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div   <?php
                                if ($this->session->userdata("super_admin") != SUPER_ADMIN)
                                {
                                    echo 'class = "col-md-6"';
                                } else
                                {
                                    echo 'class = "col-md-4"';
                                }
                                ?>>
                                    <label><?php echo $this->lang->line('constant_form_search_prize_user')?></label>
                                    <select id="search_user_winner_prize" class="form-control selectpicker" name="search_user_winner_prize">
                                        <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                        <?php
                                            if (isset($prizes))
                                            {
                                                foreach ($prizes as $prize)
                                                {
                                                    ?>
                                        <option
                                                value="<?php echo $prize->type; ?>" <?php if ($prize->type == $search_user_winner_prize)
                                        {
                                            echo "selected";
                                        } ?>>

                                            <?php echo $prize->name; ?>
                                        </option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>

                                <div   <?php
                                if ($this->session->userdata("super_admin") != SUPER_ADMIN)
                                {
                                    echo 'class = "col-md-6"';
                                } else
                                {
                                    echo 'class = "col-md-4"';
                                }
                                ?>>
                                    <label><?php echo $this->lang->line('constant_form_search_status_user')?></label>
                                    <select id="search_user_winner_status" class="form-control selectpicker" name="search_user_winner_status">
                                        <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                        <option value="<?php echo STATUS_ACTIVE ; ?>" <?php if($search_user_winner_status == STATUS_ACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_active')?></option>
                                        <option value="<?php echo STATUS_INACTIVE ; ?>" <?php if($search_user_winner_status == STATUS_INACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_inactive')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">

                                <button type="input" class="btn btn-lg m-b-5 button_form"
                                        style="margin-top: 20px;!important;" id="search" value="search" name="search_user_winner"><?php echo $this->lang->line('constant_btn_search')?>
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-lg-12">
        <div class="content-box">
            <?php if (isset($users))
            {
                ?>
                <div class="head clearfix head_background_color" style="padding-left: 0;!important;">

                    <a title="<?php echo $this->lang->line('constant_export_excel_list') ?>" id="export_users_winners">
                        <button type="button" class="btn btn-lg raised icon color_buttons">
                            <i id="img_search" class="zmdi zmdi-file"></i>
                        </button>
                    </a>

                    <h7 class="total_list">
                        <?php echo $this->lang->line('constant_list_total_user') . $total_users ?>
                    </h7>
                </div>
                <?php
            }
            ?>
            <div class="table-responsive alt-table" style="padding: 10px">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Fecha de premio</th>
                        <th><?php echo $this->lang->line('constant_list_dni_user')?></th>
                        <th><?php echo $this->lang->line('constant_list_name_user')?></th>
                        <th><?php echo $this->lang->line('constant_list_campaign_user')?></th>
                        <th><?php echo $this->lang->line('constant_list_country_user')?></th>
                        <th>Ciudad</th>
                        <th><?php echo $this->lang->line('constant_list_email_user')?></th>
                        <th><?php echo $this->lang->line('constant_list_phone_user')?></th>
                        <th><?php echo $this->lang->line('constant_list_points_user')?></th>
                        <th><?php echo $this->lang->line('constant_list_prize_user')?></th>
                        <th><?php echo $this->lang->line('constant_list_status_user')?></th>
                    </tr>
                    </thead>

                    <tbody id="content_data_table">
                    <?php

                    if (isset($users) && count($users) > 0)
                    {
                        foreach ($users as $user)
                        {

                            ?>
                            <tr>

                                <td><?php echo date("d/m/Y H:i:s",strtotime($user->created_at));?></td>
                                <td><?php echo $user->dni;?></td>
                                <td><?php echo $user->user_name;?></td>
                                <td>
                                    <?php
                                    if ($user->country_code == COUNTRY_PERU)
                                    {
                                        echo  $this->lang->line('constant_country_peru');
                                    } elseif ($user->country_code == COUNTRY_ECUADOR)
                                    {
                                        echo  $this->lang->line('constant_country_ecuador');
                                    } elseif ($user->country_code == COUNTRY_PUERTO_RICO)
                                    {
                                        echo  $this->lang->line('constant_country_puerto_rico');
                                    }elseif ($user->country_code == COUNTRY_DOMINICAN_REPUBLIC)
                                    {
                                        echo  $this->lang->line('constant_country_dominican_republic');
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($user->country == COUNTRY_PERU)
                                    {
                                        echo  $this->lang->line('constant_country_peru');
                                    } elseif ($user->country == COUNTRY_ECUADOR)
                                    {
                                        echo  $this->lang->line('constant_country_ecuador');
                                    } elseif ($user->country == COUNTRY_PUERTO_RICO)
                                    {
                                        echo  $this->lang->line('constant_country_puerto_rico');
                                    }elseif ($user->country == COUNTRY_DOMINICAN_REPUBLIC)
                                    {
                                        echo  $this->lang->line('constant_country_dominican_republic');
                                    } else
                                    {
                                        echo $user->country;
                                    }
                                    ?>

                                </td>
                                <td><?php echo $user->city;?></td>
                                <td><?php echo $user->email;?></td>
                                <td><?php echo $user->phone;?></td>
                                <td><?php echo $user->points;?></td>
                                <td><?php echo $user->prize_name;?></td>
                                <td>

                                    <?php
                                    if ($user->status == ACTIVE)
                                    {

                                        ?>

                                        <span class="label label-form status_background_active" >

                                <?php echo $this->lang->line('constant_status_active')?>

                                                </span>

                                        <?php
                                    } elseif ($user->status == INACTIVE)
                                    {

                                        ?>
                                        <span class="label label-form status_background_inactive" >

                                             <?php echo $this->lang->line('constant_status_inactive')?>

                                                </span>

                                        <?php
                                    }
                                    ?>

                                </td>
                            </tr>
                            <?php
                        }

                    }
                    ?>

                    </tbody>
                </table>

                <div >
                    <?php
                    if (isset($paginador)) echo $paginador;
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>
<script>

    $('#export_users_winners').click(function () {

        var url = "<?php echo site_url($this->config->item('base_cms').'/Usuario/exportar_lista_ganadores');?>";
        var search_user_winner_dni = $("#search_user_winner_dni").val();
        var search_user_winner_name = $("#search_user_winner_name").val();
        var search_user_winner_prize = $("#search_user_winner_prize").val();
        var search_user_winner_country = $("#search_user_winner_country").val();
        var search_user_winner_campaign = $("#search_user_winner_campaign").val();
        var search_user_winner_status = $("#search_user_winner_status").val();

        window.location.href = url + "?search_user_winner_dni=" + search_user_winner_dni + "&search_user_winner_name=" + search_user_winner_name + "&search_user_winner_prize=" + search_user_winner_prize+ "&search_user_winner_country=" + search_user_winner_country + "&search_user_winner_campaign=" + search_user_winner_campaign + "&search_user_winner_status=" + search_user_winner_status;

    });

</script>
