<div class="row" >
    <div class="col-lg-6 col-lg-offset-2">
        <div class="content-box">

            <div class="head clearfix head_background_color content_center">
                <h5 class="box_content_title">Usuarios</h5>
            </div>

            <div class="content">
                <form  class="form-horizontal" id="form_update_admin" method="post" action="<?php echo site_url($this->config->item('base_cms').'/Usuario/editar/'.$user->id);?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name"  name="name" value="<?php echo $user->name?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">DNI</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="dni" name="dni" value="<?php echo $user->dni?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="email" name="email" value="<?php echo $user->email?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Télefono</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $user->phone?>">
                        </div>
                    </div>

                    <div class="form-group" style="display: <?php echo ($userAdmin->username == 'soda')? 'none':'block'; ?>">
                        <label class="col-sm-3 control-label">Puntos</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="points" name="points" value="<?php echo $user->points?>">
                        </div>
                    </div>





                    <div class="form-group">
                        <label class="col-sm-3 control-label">Estado</label>
                        <div class="col-sm-9">
                            <select id="status" name="status" class="form-control selectpicker">
                                <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                <option value="<?php echo STATUS_ACTIVE ; ?>" <?php if($user->status == STATUS_ACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_active')?></option>
                                <option value="<?php echo STATUS_INACTIVE ; ?>" <?php if($user->status  == STATUS_INACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_inactive')?></option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer content_center">
                        <div id="content_buttons">

                            <button type="submit" class="btn btn-lg m-b-5 button_form"
                                    id="btn_save_admin"><?php echo $this->lang->line('constant_btn_save')?>
                            </button>

                            <a href="<?php echo site_url($this->config->item('base_cms') . '/Administrador');?>">
                            <button type="button" class="btn btn-lg m-b-5 button_form"
                                        id="btn_cancel_admin"><?php echo $this->lang->line('constant_btn_cancel')?>
                            </button>
                            </a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
</div>

<script>
     <?php
            if($this->session->flashdata('mensaje') != null)
              echo "alert('".$this->session->flashdata('mensaje')."');"
                //echo "alert('".$this->session->flashdata('mensaje')."');";
            ?>
</script>
