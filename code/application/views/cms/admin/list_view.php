<div class="row">
    <div class="col-lg-12">
        <div class="content-box">
            <div class="head clearfix head_background_color" style="padding-right: 0;!important;">
                <h5 class="content-title pull-left"><?php echo $this->lang->line('constant_form_title_admin')?></h5>
                <div class="pull-right" style="text-align: center;position: relative">

                    <button type="button"  id="div_search" class="btn btn-lg raised icon" >
                        <i id="img_search" class="zmdi zmdi-chevron-down"></i>
                    </button>
                </div>
            </div>

            <div class="content" id="div_content_search">
                <div class="row">
                    <div class="col-md-12">
                        <form enctype="multipart/form-data" method="POST" class="form-horizontal"  action="<?php echo site_url($this->config->item('base_cms').'/Administrador/buscar');?>">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('constant_form_search_user_admin')?></label>
                                    <input class="form-control m-b-20" type="text" id="search_admin_user"  name="search_admin_user" value="<?php echo $search_admin_user?>">
                                </div>

                              <!--  <div class="col-md-4">
                                    <label><?php echo $this->lang->line('constant_form_search_country_admin')?></label>
                                    <select id="search_admin_country" class="form-control selectpicker" name="search_admin_country">
                                        <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                        <option value="<?php echo COUNTRY_PERU ; ?>" <?php if($search_admin_country == COUNTRY_PERU) echo "selected"?>><?php echo $this->lang->line('constant_country_peru')?></option>
                                        <option value="<?php echo COUNTRY_ECUADOR ; ?>" <?php if($search_admin_country == COUNTRY_ECUADOR) echo "selected"?>><?php echo $this->lang->line('constant_country_ecuador')?></option>
                                        <option value="<?php echo COUNTRY_DOMINICAN_REPUBLIC ; ?>" <?php if($search_admin_country == COUNTRY_DOMINICAN_REPUBLIC) echo "selected"?>><?php echo $this->lang->line('constant_country_dominican_republic')?></option>
                                        <option value="<?php echo COUNTRY_PUERTO_RICO ; ?>" <?php if($search_admin_country == COUNTRY_PUERTO_RICO) echo "selected"?>><?php echo $this->lang->line('constant_country_puerto_rico')?></option>
                                        <option value="<?php echo COUNTRY_ADMIN ; ?>" <?php if($search_admin_country == COUNTRY_ADMIN) echo "selected"?>><?php echo $this->lang->line('constant_country_admin')?></option>
                                    </select>
                                </div>-->

                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('constant_form_search_campaign_admin')?></label>
                                    <select id="search_admin_campaign" class="form-control selectpicker" name="search_admin_campaign">
                                        <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                        <option value="<?php echo COUNTRY_PERU ; ?>" <?php if($search_admin_campaign == COUNTRY_PERU) echo "selected"?>><?php echo $this->lang->line('constant_country_peru')?></option>
                                        <option value="<?php echo COUNTRY_ECUADOR ; ?>" <?php if($search_admin_campaign == COUNTRY_ECUADOR) echo "selected"?>><?php echo $this->lang->line('constant_country_ecuador')?></option>
                                        <option value="<?php echo COUNTRY_DOMINICAN_REPUBLIC ; ?>" <?php if($search_admin_campaign == COUNTRY_DOMINICAN_REPUBLIC) echo "selected"?>><?php echo $this->lang->line('constant_country_dominican_republic')?></option>
                                        <option value="<?php echo COUNTRY_PUERTO_RICO ; ?>" <?php if($search_admin_campaign== COUNTRY_PUERTO_RICO) echo "selected"?>><?php echo $this->lang->line('constant_country_puerto_rico')?></option>
                                        <option value="<?php echo COUNTRY_ADMIN ; ?>" <?php if($search_admin_campaign == COUNTRY_ADMIN) echo "selected"?>><?php echo $this->lang->line('constant_form_search_super_admin')?></option>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('constant_form_search_status_admin')?></label>
                                    <select id="search_admin_status" class="form-control selectpicker" name="search_admin_status">
                                        <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                        <option value="<?php echo STATUS_ACTIVE ; ?>" <?php if($search_admin_status == STATUS_ACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_active')?></option>
                                        <option value="<?php echo STATUS_INACTIVE ; ?>" <?php if($search_admin_status == STATUS_INACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_inactive')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">

                                <button type="input" class="btn btn-lg m-b-5 button_form"
                                        style="margin-top: 20px;!important;" id="search" value="search" name="search_admin"><?php echo $this->lang->line('constant_btn_search')?>
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-12">
        <div class="content-box">
            <div class="head clearfix head_background_color" style="padding-left: 0;!important;">
                <a title="<?php echo $this->lang->line('constant_export_excel_list')?>" id="export_admins" >
                    <button type="button" class="btn btn-lg raised icon color_buttons" >
                        <i id="img_search" class="zmdi zmdi-file"></i>
                    </button>
                </a>
                <h7 class="total_list">
                    <?php echo $this->lang->line('constant_list_total_admins').$total_admins ?>
                </h7>
                <a title="<?php echo $this->lang->line('constant_btn_add')?>"  href="<?php echo site_url($this->config->item('base_cms').'/Administrador/agregar');?>">
                    <button type="button" class="btn btn-lg raised icon color_buttons pull-left">
                        <i id="img_search" class="zmdi zmdi-account-add"></i>
                    </button>
                </a>
            </div>
            <div class="table-responsive alt-table" style="padding: 10px">
                <table class="table table-hover">
                    <thead>
                    <tr>

                        <th><?php echo $this->lang->line('constant_list_name_admin')?></th>
                        <th><?php echo $this->lang->line('constant_list_campaign_admin')?></th>
                        <th><?php echo $this->lang->line('constant_list_super_admin')?></th>
                        <th><?php echo $this->lang->line('constant_list_created_at_admin')?></th>
                        <th><?php echo $this->lang->line('constant_list_status_admin')?></th>
                        <th><?php echo $this->lang->line('constant_list_actions')?></th>
                    </tr>
                    </thead>

                    <tbody id="content_data_table">
                    <?php

                    if (isset($admins) && count($admins) > 0)
                    {
                        foreach ($admins as $admin)
                        {
                            ?>
                            <tr>
                                <td><?php echo $admin->username;?></td>
                                <td>
                                    <?php
                                    if ($admin->super_admin == SUPER_ADMIN)
                                    {
                                        echo $this->lang->line('constant_country_super_admin');
                                    }else
                                    {
                                        if ($admin->country_code == COUNTRY_PERU)
                                        {
                                            echo  $this->lang->line('constant_country_peru');
                                        } elseif ($admin->country_code == COUNTRY_ECUADOR)
                                        {
                                            echo  $this->lang->line('constant_country_ecuador');
                                        } elseif ($admin->country_code == COUNTRY_PUERTO_RICO)
                                        {
                                            echo  $this->lang->line('constant_country_puerto_rico');
                                        }elseif ($admin->country_code == COUNTRY_DOMINICAN_REPUBLIC)
                                        {
                                            echo  $this->lang->line('constant_country_dominican_republic');
                                        }
                                    }

                                    ?>

                                </td>
                                <td>
                                    <?php
                                    if ($admin->super_admin == SUPER_ADMIN)
                                    {
                                        ?>
                                        <span class="label label-form status_background_active" >
                                <?php echo $this->lang->line('constant_yes')?>
                                                </span>
                                        <?php
                                    } elseif ($admin->super_admin != SUPER_ADMIN)
                                    {
                                        ?>
                                        <span class="label label-form status_background_inactive" >
                                             <?php echo $this->lang->line('constant_no')?>
                                                </span>
                                        <?php
                                    }
                                    ?>
                                </td>

                                <td><?php echo date('Y-m-d', strtotime($admin->created_at));?></td>
                                <td>
                                    <?php
                                    if ($admin->status == ACTIVE)
                                    {
                                        ?>
                                        <span class="label label-form status_background_active" >
                                <?php echo $this->lang->line('constant_status_active')?>
                                                </span>
                                        <?php
                                    } elseif ($admin->status == INACTIVE)
                                    {
                                        ?>
                                        <span class="label label-form status_background_inactive" >
                                             <?php echo $this->lang->line('constant_status_inactive')?>
                                                </span>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a title="Ver detalle" href="<?php echo site_url($this->config->item('base_cms').'/Administrador/editar/'.$admin->id);?>">
                                        <button type="button" class="btn btn-default raised icon">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }

                    }
                    ?>

                    </tbody>
                </table>

                <div >
                    <?php
                    if (isset($paginador)) echo $paginador;
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>
<script>

    $('#export_admins').click(function () {

        var url = "<?php echo site_url($this->config->item('base_cms').'/Administrador/exportar_lista');?>";
        var search_admin_user = $("#search_admin_user").val();
        var search_admin_campaign = $("#search_admin_campaign").val();
        var search_admin_status = $("#search_admin_status").val();

        window.location.href = url + "?search_admin_user=" + search_admin_user + "&search_admin_campaign=" + search_admin_campaign + "&search_admin_status=" + search_admin_status ;

    });

</script>
