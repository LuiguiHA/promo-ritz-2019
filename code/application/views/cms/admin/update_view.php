<div class="row" >
    <div class="col-lg-4 col-lg-offset-4">
        <div class="content-box">

            <div class="head clearfix head_background_color content_center">
                <h5 class="box_content_title"><?php echo $this->lang->line('constant_form_title_admin')?></h5>
            </div>

            <div class="content">
                <form  class="form-horizontal" id="form_update_admin" >
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_user_admin')?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="user"  name="user" value="<?php echo $admin->username?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_password_admin')?></label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_super_admin')?></label>
                        <div class="col-sm-9">
                            <label>
                                <input class="toggle toggle-success" type="checkbox" id="super_admin" name="super_admin" <?php if ($admin->super_admin == SUPER_ADMIN) echo "checked"?>>
                                <i></i>
                            </label>
                        </div>
                    </div>

                        <div class="form-group" id="div_campaign">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_campaign_admin') ?></label>
                            <div class="col-sm-9">
                                <select id="campaign" name="campaign" class="form-control selectpicker">
                                    <option value=""><?php echo $this->lang->line('constant_select') ?></option>

                                    <?php
                                    if (isset($campaigns)) {
                                        foreach ($campaigns as $campaign)
                                        { ?>
                                            <option value="<?php echo $campaign->id; ?>" <?php if ($admin->campaign_id == $campaign->id) echo "selected"?> >
                                                <?php echo $campaign->country_name; ?>
                                            </option>
                                        <?php }
                                    }?>

                                </select>
                            </div>
                        </div>



                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_status_admin')?></label>
                        <div class="col-sm-9">
                            <select id="status" name="status" class="form-control selectpicker">
                                <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                <option value="<?php echo STATUS_ACTIVE ; ?>" <?php if($admin->status == STATUS_ACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_active')?></option>
                                <option value="<?php echo STATUS_INACTIVE ; ?>" <?php if($admin->status  == STATUS_INACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_inactive')?></option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer content_center">
                        <div id="content_buttons">

                            <button type="button" class="btn btn-lg m-b-5 button_form"
                                    id="btn_save_admin"><?php echo $this->lang->line('constant_btn_save')?>
                            </button>

                            <a href="<?php echo site_url($this->config->item('base_cms') . '/Administrador');?>">
                            <button type="button" class="btn btn-lg m-b-5 button_form"
                                        id="btn_cancel_admin"><?php echo $this->lang->line('constant_btn_cancel')?>
                            </button>
                            </a>
                        </div>
                        <div id="loader_form" class="loader_button"></div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
</div>

<script>
    show_campaign();

    $("#super_admin").click(function () {
        show_campaign();
    });


    $("#loader_form").hide();
    
    $("#btn_save_admin").click(function () {
        if (validation() == 0){
            $("#content_buttons").hide();
            $("#loader_form").show();

            $.ajax({
                url: "<?php echo site_url($this->config->item('base_cms') . '/Administrador/actualizar/'.$admin->id);?>",
                type: 'POST',
                data: $("#form_update_admin").serialize(),
                dataType: 'json',
                success: function (response) {

                    if (response.code == 1) {
                        $("#loader_form").hide();
                        $("#content_buttons").show();
                        swal({
                                title: "",
                                text: response.message,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: 'btn-form-ritz',
                                confirmButtonText: 'Aceptar'
                            },
                            function () {
                                    window.location.assign("<?php echo site_url($this->config->item('base_cms') . '/Administrador');?>")   
                            }
                        );

                    } else {
                        $("#loader_form").hide();
                        $("#content_buttons").show();
                        var text = response.message;
                        alter_error(text);
                    }
                },
                error: function (xhr, status) {
                    $("#loader_form").hide();
                    $("#content_buttons").show();
                    var text = "<?php echo $this->lang->line('constant_error_service')?>";
                    alter_error(text);
                }
            });

        }
    });

    function validation(){

        var message = new Array();
        var text = "";

        if ($("#user").val() == ""){
            text = "<?php echo $this->lang->line('constant_form_required_user_admin')?>";
            message.push(text);
        }

        if (!$('#super_admin').is(":checked"))
        {
            if ($("#campaign").val() == ""){
                text = "<?php echo $this->lang->line('constant_form_required_campaign_admin')?>";
                message.push(text);
            }
        }

        if ($("#status").val() == ""){
            text = "<?php echo $this->lang->line('constant_form_required_status_admin')?>";
            message.push(text);
        }

        for (var i =0 ; i< message.length ; i ++){
            alert_message_validation(message[i]);
        }

        return message.length;

    }

    function alert_message_validation(text) {
        var msg = text;
        var shortCutFunction = 'warning';

        var $toast = toastr[shortCutFunction](msg);

        toastr.options = {

            closeButton: false,
            debug: true,
            newestOnTop: false,
            progressBar: false,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            onclick: null,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "10000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "show",
            hideMethod: "fadeOut"

        };

    }

    function alter_error(text) {
        swal({
            title: "",
            text: text,
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: 'btn-form-ritz',
            confirmButtonText: 'Aceptar'
        });
    }

    function show_campaign()
    {
        if ($('#super_admin').is(":checked"))
        {
            $('#div_campaign').hide();
            $('#campaign').val("");
            $('#super_admin').val("1");
        }else
        {
            $('#div_campaign').show();
            $('#super_admin').val("0");
        }
    }
</script>