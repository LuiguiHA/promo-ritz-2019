<div class="wrapper warning-bg">
    <div class="table-wrapper text-center background_login">
        <div class="table-row">
            <div class="table-cell">
                <div class="login">
                    <div>
                        <img src="<?php echo base_url('assets/cms/'); ?>/img/logo_login.png">
                    </div>
                    <h4 class="text-center">B<font style="text-transform: lowercase;">ienvenido, ingresa tus
                            datos</font></h4>
                    <form method="post" id="form-login">
                        <div class="form-group">
                            <input type="text" class="form-control" id="user" name="user" placeholder="Usuario">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                        </div>
                        <button type="submit" class="btn btn-block btn-lg" id="btnlogin" value="Ingresar">Ingresar</button>
                        <div id="loader_login" class="loader_button"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $("#loader_login").hide();

    $("#form-login").submit(function ()
        {
                    if (validation() == 0)
                    {
                        $("#btnlogin").hide();
                        $("#loader_login").show();
                        $.post("<?php echo site_url($this->config->item('base_cms').'/Index/login/');?>", $(this).serialize(),
                            function (result)
                            {
                                switch (result.code)
                                {
                                    case 0:
                                        fn_mostrarError(result.message);
                                        break;
                                    case 1:

                                        location.href="<?php echo site_url($this->config->item('base_cms').'/Usuario');?>";
                                        break;
                                }
                            }, "json");
                    }
                    return false;
        }
    );

    function fn_mostrarError(mensaje)
    {
        $("#loader_login").hide();
        $("#btnlogin").show();

            swal({
                title: "Error",
                text: mensaje,
                type: "error",
                showCancelButton: false,
                cancelButtonClass: 'btn-success',
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Aceptar'

            });
    }

    function validation(){
        toastr.clear();

        var message = new Array();
        var text = "";
        if ($("#user").val() == ""){
            text = "<?php echo $this->lang->line('login_form_user_required')?>";
            message.push(text);
        }

        if ($("#password").val() == ""){
            text = "<?php echo $this->lang->line('login_form_password_required')?>";
            message.push(text);
        }

        for (var i =0 ; i< message.length ; i ++){
            alert_message_validation(message[i]);
        }

        return message.length;

    }

    function alert_message_validation(text) {

        var msg = text;
        var shortCutFunction = 'error';

        toastr.options = {
            "closeButton": false,
            "debug": true,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "10000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        toastr[shortCutFunction](msg);

    }


</script>