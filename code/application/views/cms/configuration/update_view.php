<div class="row" >
    <div class="col-lg-4 col-lg-offset-4">
        <div class="content-box">

            <div class="head clearfix head_background_color content_center">
                <h5 class="box_content_title"><?php echo $this->lang->line('constant_form_title_configuration')?></h5>
            </div>

            <div class="content">
                <form  class="form-horizontal" id="form_update_admin" >
                    <div class="form-group" id="div_campaign">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_campaign_admin') ?></label>
                        <div class="col-sm-9">
                            <select id="campaign" name="campaign" class="form-control selectpicker">
                                <option value=""><?php echo $this->lang->line('constant_select') ?></option>

                                <?php
                                if (isset($campaigns)) {
                                    foreach ($campaigns as $campaign)
                                    { ?>
                                        <option value="<?php echo $campaign->id; ?>">
                                            <?php echo $campaign->country_name; ?>
                                        </option>
                                    <?php }
                                }?>

                            </select>
                        </div>
                    </div>

                    <div id="div_form_configuration">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_prizes_day_configuration')?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="prize_day"  name="prize_day" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_random_configuration')?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="random" name="random">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_probability_configuration')?></label>
                            <div class="col-sm-9">
                                <label class="form-control" id="probability"></label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer content_center">
                        <div id="content_buttons">
                            <button type="button" class="btn btn-lg m-b-5 button_form"
                                    id="btn_save"><?php echo $this->lang->line('constant_btn_save')?>
                            </button>
                        </div>
                        <div id="loader_form" class="loader_button"></div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
</div>

<script>
    $("#loader_form").hide();
    $("#div_form_configuration").hide();


    $(document).ready(function() {
        if ("<?php echo $super_admin?>"=="")
        {
            change_campaign();
            $("#div_campaign").hide();
        }


    });

    $("#campaign").change(function () {
        change_campaign();
    });

    function change_campaign()
    {

        var campaign = "";

        if ("<?php echo $super_admin?>" == "1")
        {
            campaign = $("#campaign").val();
        }else
        {
            campaign = "<?php echo $campaign_id?>";
        }

        if (campaign != "")
        {
            $.ajax({
                url: "<?php echo site_url($this->config->item('base_cms') . '/Configuracion/detalle');?>",
                type: 'POST',
                data: {
                    'campaign' : campaign
                },
                dataType: 'json',
                success: function (response) {

                    if (response.code == 1) {
                        $("#prize_day").val(response["configuration"].prize_day);
                        $("#random").val(response["configuration"].random);
                        get_probability(response["configuration"].random);
                        $("#div_form_configuration").show();
                    } else {
                        var text = response.message;
                        alter_error(text);
                    }
                },
                error: function (xhr, status) {
                    var text = "<?php echo $this->lang->line('constant_error_service')?>";
                    alter_error(text);
                }
            });
        }
    }

    $("#btn_save").click(function () {
        if (validation() == 0){
             $("#btn_save").hide();
            $("#loader_form").show();

            $.ajax({
                url: "<?php echo site_url($this->config->item('base_cms') . '/Configuracion/actualizar');?>",
                type: 'POST',
                data: {
                    'campaign': $("#campaign").val(),
                    'prize_day': $("#prize_day").val(),
                    'random': $("#random").val()
                },
                dataType: 'json',
                success: function (response) {

                    if (response.code == 1) {

                        $("#loader_form").hide();
                        $("#btn_save").show();

                        var text = response.message;
                        swal({
                            title: "",
                            text: text,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: 'btn-form-ritz',
                            confirmButtonText: 'Aceptar'
                        });
                    } else {
                        $("#loader_form").hide();
                        $("#raffle").show();
                        var text = response.message;
                        alter_error(text);
                    }
                },
                error: function (xhr, status) {
                    $("#loader_form").hide();
                    $("#raffle").show();
                    var text = "<?php echo $this->lang->line('constant_error_service')?>";
                    alter_error(text);
                }
            });

        }
    });

    function validation(){

        var message = new Array();
        var text = "";

        if ("<?php echo $super_admin?>"!="")
        {
            if ($("#campaign").val() == ""){
                text = "<?php echo $this->lang->line('constant_form_required_campaign_configuration')?>";
                message.push(text);
            }
        }

        if ($("#prize_day").val() == ""){
            text = "<?php echo $this->lang->line('constant_form_required_prize_day_configuration')?>";
            message.push(text);
        }
        if ($("#random").val() == ""){
            text = "<?php echo $this->lang->line('constant_form_required_random_configuration')?>";
            message.push(text);
        }

        if ($("#random").val() != "" && $("#random").val() == 0){
            text = "<?php echo $this->lang->line('constant_form_required_random_cero_configuration')?>";
            message.push(text);
        }

        for (var i =0 ; i< message.length ; i ++){
            alert_message_validation(message[i]);
        }

        return message.length;

    }

    function alert_message_validation(text) {
        var msg = text;
        var shortCutFunction = 'warning';

        var $toast = toastr[shortCutFunction](msg);

        toastr.options = {

            closeButton: false,
            debug: true,
            newestOnTop: false,
            progressBar: false,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            onclick: null,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "10000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "show",
            hideMethod: "fadeOut"

        };

    }


    $("#random").on("change paste keyup", function() {
        var random = $("#random").val();
        get_probability(random)

    });

    function get_probability(random){
        if (random != 0) {
            if (random != "") {
                var probability = ((random / (random * random)) * 100);

                $("#probability").text(probability + " %");
            } else {
                $("#probability").text("%");
            }
        }else{
            $("#probability").text("%");
        }
    }


    function alter_error(text) {
        swal({
            title: "",
            text: text,
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: 'btn-form-ritz',
            confirmButtonText: 'Aceptar'
        });
    }

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    }

    $('#prize_day').keypress(validateNumber);
    $('#random').keypress(validateNumber);

</script>