<div class="row">
<div class="col-lg-12">
        <div class="content-box">
            <div class="head clearfix head_background_color" style="padding-right: 0;!important;">
                <h5 class="content-title pull-left"><?php echo $this->lang->line('constant_form_title_user')?></h5>
                <div class="pull-right" style="text-align: center;position: relative">

                    <button type="button"  id="div_search" class="btn btn-lg raised icon" >
                        <i id="img_search" class="zmdi zmdi-chevron-down"></i>
                    </button>
                </div>
            </div>

            <div class="content" id="div_content_search">
                <div class="row">
                    <div class="col-md-12">
                        <form enctype="multipart/form-data" method="POST" class="form-horizontal"  action="<?php echo site_url($this->config->item('base_cms').'/Meme/buscar');?>">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label><?php echo $this->lang->line('constant_form_search_dni_user')?></label>
                                    <input class="form-control m-b-20" type="text" id="search_user_dni" maxlength="8" name="search_user_dni" value="<?php echo $search_user_dni?>">
                                </div>

                                <div class="col-md-3">
                                    <label><?php echo $this->lang->line('constant_form_search_name_user')?></label>
                                    <input class="form-control m-b-20" type="text" id="search_user_name" name="search_user_name" value="<?php echo $search_user_name?>">
                                </div>

                                <div class="col-md-3">
                                    <label>Fecha inicio</label>
                                    <input class="form-control m-b-20" type="date" id="search_user_fecha_ini" min="2018-03-01"  min="2019-12-31"  name="search_user_fecha_ini" value="<?php echo $search_user_fecha_ini?>">
                                </div>

                             
                                <div class="col-md-3">
                                    <label>Fecha fin</label>
                                    <input class="form-control m-b-20" type="date" id="search_user_fecha_fin" min="2018-03-01"  min="2019-12-31"  name="search_user_fecha_fin" value="<?php echo $search_user_fecha_fin?>">
                                </div>
                                   
                                <!--

                                <div class="col-md-2">
                                    <label><?php echo $this->lang->line('constant_form_search_status_user')?></label>
                                    <select id="search_user_status" class="form-control selectpicker" name="search_user_status">
                                        <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                        <option value="<?php echo STATUS_ACTIVE ; ?>" <?php if($search_user_status == STATUS_ACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_active')?></option>
                                        <option value="<?php echo STATUS_INACTIVE ; ?>" <?php if($search_user_status == STATUS_INACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_inactive')?></option>
                                    </select>
                                </div> -->
                            </div>

                            <div class="modal-footer">

                                <button type="input" class="btn btn-lg m-b-5 button_form"
                                        style="margin-top: 20px;!important;" id="search" value="search" name="search_user"><?php echo $this->lang->line('constant_btn_search')?>
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-12">
        <div class="content-box">
            <?php if (isset($memes))
            {
                ?>
                <div class="head clearfix head_background_color" style="padding-left: 0;!important;">
                    <a title="<?php echo $this->lang->line('constant_export_excel_list') ?>" id="export_memes">
                        <button type="button" class="btn btn-lg raised icon color_buttons">
                            <i id="img_search" class="zmdi zmdi-file"></i>
                        </button>
                    </a>

                    <h7 class="total_list">
                        <?php echo 'Total de opciones: ' . $total_memes ?>
                    </h7>
                </div>
                <?php
            }
            ?>
            <div class="table-responsive alt-table" style="padding: 10px">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>DNI</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Foto</th>
                        <th>Lote</th>
                        <th>Fecha Registro</th>
                    </tr>
                    </thead>

                    <tbody id="content_data_table">
                    <?php

                    if (isset($memes) && count($memes) > 0)
                    {
                        foreach ($memes as $meme)
                        {

                            ?>
                            <tr>

                                <td><?php echo $meme->user_name;?></td>
                                <td><?php echo $meme->user_dni;?></td>
                                <td><?php echo $meme->user_email;?></td>
                                <td><?php echo $meme->user_phone;?></td>
                                <td>
                                    <?php 
                                    if($meme->photo != "" && $meme->photo != null){
                                    ?>
                                        <a class="gallery-item" href="<?php echo base_url($meme->photo)?>" title="<?php echo $comunidad->name; ?>" data-gallery>
                                            <img  src="<?php echo base_url($meme->photo)?>" width="50">
                                        </a>
                                    <?php
                                    }
                                    else{
                                    ?>
                                        <img  src="<?php echo base_url('assets/images/galleta-2x.png')?>" width="50">
                                    <?php
                                    }
                                    ?>
                                    
                                </td>
                                <td><?php echo $meme->lote;?></td>
                                <td><?php echo date('d/m/Y H:i:s',strtotime($meme->created_at));?></td>
                                
                            </tr>
                            <?php
                        }

                    }
                    ?>

                    </tbody>
                </table>

                <div >
                    <?php
                    if (isset($pagination)) echo $pagination;
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
   <div class="slides"></div>
   <h3 class="title"></h3>
   <a class="close">×</a>
</div>
<script>

    $('#export_memes').click(function () {

        var url = "<?php echo site_url($this->config->item('base_cms').'/Meme/exportar_lista');?>";
        var search_user_dni = $("#search_user_dni").val();
        var search_user_name = $("#search_user_name").val();
        var search_user_fecha_ini = $("#search_user_fecha_ini").val();
        var search_user_fecha_fin = $("#search_user_fecha_fin").val();
        var search_user_status = $("#search_user_status").val();

        window.location.href = url + "?search_user_dni=" + search_user_dni + "&search_user_name=" + search_user_name + "&search_user_fecha_ini=" + search_user_fecha_ini + "&search_user_fecha_fin=" + search_user_fecha_fin +"&search_user_status=";

    });

    

    }

</script>
