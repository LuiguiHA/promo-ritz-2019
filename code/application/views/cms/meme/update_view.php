<div class="row" >
    <div class="col-lg-4 col-lg-offset-4">
        <div class="content-box">

            <div class="head clearfix head_background_color content_center">
                <h5 class="box_content_title">Meme</h5>
            </div>

            <div class="content">
                <form  class="form-horizontal" id="form_update_admin" >
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name"  name="name" value="<?php echo $meme->name?>">
                        </div>
                    </div>
                    

                    <div class="form-group" id="div_campaign">
                        <label class="col-sm-3 control-label">Categoría</label>
                        <div class="col-sm-9">
                            <select id="campaign" name="campaign" class="form-control selectpicker">
                                <option value=""><?php echo $this->lang->line('constant_select') ?></option>

                                <?php
                                if (isset($categories)) {
                                    foreach ($categories as $category)
                                    { ?>
                                        <option value="<?php echo $category->id; ?>" <?php if ($meme->category_id == $category->id) echo "selected"?> >
                                            <?php echo $category->title; ?>
                                        </option>
                                    <?php }
                                }?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Photo</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control" id="photo"  name="photo" value="">
                            <a class="gallery-item" href="<?php echo base_url($meme->photo)?>" title="<?php echo $comunidad->name; ?>" data-gallery>
                                <img  src="<?php echo base_url($meme->photo)?>" width="50">
                            </a>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_status_admin')?></label>
                        <div class="col-sm-9">
                            <select id="status" name="status" class="form-control selectpicker">
                                <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                <option value="<?php echo STATUS_ACTIVE ; ?>" <?php if($meme->status == STATUS_ACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_active')?></option>
                                <option value="<?php echo STATUS_INACTIVE ; ?>" <?php if($meme->status  == STATUS_INACTIVE) echo "selected"?>><?php echo $this->lang->line('constant_status_inactive')?></option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer content_center">
                        <div id="content_buttons">

                            <button type="button" class="btn btn-lg m-b-5 button_form"
                                    id="btn_save_admin"><?php echo $this->lang->line('constant_btn_save')?>
                            </button>

                            <a href="<?php echo site_url($this->config->item('base_cms') . '/Meme');?>">
                            <button type="button" class="btn btn-lg m-b-5 button_form"
                                        id="btn_cancel_admin"><?php echo $this->lang->line('constant_btn_cancel')?>
                            </button>
                            </a>
                        </div>
                        <div id="loader_form" class="loader_button"></div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
   <div class="slides"></div>
   <h3 class="title"></h3>
   <a class="close">×</a>
</div>