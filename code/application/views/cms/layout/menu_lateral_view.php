<aside class="sidebar">
    <ul class="nav metismenu">
        <li
            <?php
            if($this->uri->segment(2)== "Usuario")
                echo  'class="active"';
            else
                echo  'class=""';
            ?>
        >
            <a href="#"><i class="zmdi zmdi-view-dashboard"></i><?php echo $this->lang->line('constant_menu_user')?><span class="zmdi arrow"></span></a>
            <ul class="nav nav-inside collapse">
                <li
                    <?php
                    if(($this->uri->segment(3)== "" && $this->uri->segment(2)== "Usuario") || ($this->uri->segment(3)== "buscar" && $this->uri->segment(2)== "Usuario"))
                        echo  'class="active"';
                    else
                        echo  'class=""';
                    ?>
                >
                    <a href="<?php echo site_url($this->config->item('base_cms').'/Usuario');?>"><?php echo $this->lang->line('constant_menu_user_list')?></a>
                </li>
                <li
                    <?php
                    if($this->uri->segment(2)== "Meme" )
                        echo  'class="active"';
                    else
                        echo  'class=""';
                    ?>
                >
                    <a href="<?php echo site_url($this->config->item('base_cms').'/Meme');?>">Lista de opciones</a>
                </li>
                <li
                    <?php
                    if($this->uri->segment(3)== "lista_ganadores" && $this->uri->segment(2)== "Usuario" )
                        echo  'class="active"';
                    else
                        echo  'class=""';
                    ?>
                >
                    <a href="<?php echo site_url($this->config->item('base_cms').'/Usuario/lista_ganadores');?>"><?php echo $this->lang->line('constant_menu_user_list_winners')?></a>
                </li>
            </ul>
        </li>
        
        <li
            <?php
            if($this->uri->segment(2)== "Sorteo")
                echo  'class="active"';
            else
                echo  'class=""';
            ?>
        >
            <a href="#"><i class="zmdi zmdi-view-dashboard"></i><?php echo $this->lang->line('constant_menu_raffle')?><span class="zmdi arrow"></span></a>
            <ul class="nav nav-inside collapse">
                <li
                    <?php
                    if($this->uri->segment(3)== "participantes" && $this->uri->segment(2)== "Sorteo")
                        echo  'class="active"';
                    else
                        echo  'class=""';
                    ?>
                >
                    <a href="<?php echo site_url($this->config->item('base_cms').'/Sorteo/participantes');?>"><?php echo $this->lang->line('constant_menu_raffle_list')?></a>
                </li>

                <li
                    <?php
                    if($this->uri->segment(3)== "ganadores" && $this->uri->segment(2)== "Sorteo")
                        echo  'class="active"';
                    else
                        echo  'class=""';
                    ?>
                >
                    <a href="<?php echo site_url($this->config->item('base_cms').'/Sorteo/ganadores');?>"><?php echo $this->lang->line('constant_menu_raffle_winners')?></a>
                </li>

                <li
                    <?php
                    if($this->uri->segment(3)== "" && $this->uri->segment(2)== "Sorteo")
                        echo  'class="active"';
                    else
                        echo  'class=""';
                    ?>
                >
                    <a href="<?php echo site_url($this->config->item('base_cms').'/Sorteo');?>"><?php echo $this->lang->line('constant_menu_raffle_generate')?></a>
                </li>

            </ul>
        </li>

        <li
            <?php
            if($this->uri->segment(2)== "premios")
                echo  'class="active"';
            else
                echo  'class=""';
            ?>
        >
            <a href="<?php echo site_url($this->config->item('base_cms').'/Premio');?>"><i class="zmdi zmdi-view-dashboard"></i>Premios Instantaneos</a>

        </li>

        <!--
        <li
            <?php
            if($this->uri->segment(2)== "Subasta")
                echo  'class="active"';
            else
                echo  'class=""';
            ?>
        >
            <a href="<?php echo site_url($this->config->item('base_cms').'/Subasta');?>"><i class="zmdi zmdi-view-dashboard"></i><?php echo $this->lang->line('constant_menu_subasta')?></a>

        </li> -->

        <?php

            if ($this->session->userdata("super_admin") == SUPER_ADMIN)
        {
        ?>

         <li
            <?php
            if($this->uri->segment(2)== "Administrador")
                echo  'class="active"';
            else
                echo  'class=""';
            ?>
        >
            <a href="<?php echo site_url($this->config->item('base_cms').'/Administrador');?>"><i class="zmdi zmdi-view-dashboard"></i><?php echo $this->lang->line('constant_menu_admin')?></a>

        </li> 

        <?php
        }
        ?>

        <li
            <?php
            if($this->uri->segment(2)== "Configuracion")
                echo  'class="active"';
            else
                echo  'class=""';
            ?>
        >
            <a href="<?php echo site_url($this->config->item('base_cms').'/Configuracion');?>"><i class="zmdi zmdi-view-dashboard"></i><?php echo $this->lang->line('constant_menu_configuration')?></a>

        </li>

    </ul>
    </aside>
