<nav class="navbar" id="navbar">
    <div class="navbar-header container">
        <a href="#" class="menu-toggle"><i class="zmdi zmdi-menu"></i></a>
        <a href="#" class="logo"><img src="<?php echo base_url('assets/cms/'); ?>img/logo.png" alt="Logo Pacificonis"></a>
        <a href="#" class="icon-logo"></a>
    </div>
    <div class="navbar-container clearfix">
        <div class="pull-left">
            <a href="#" class="page-title text-uppercase"><?php echo $this->lang->line('constant_title_default')?></a>
        </div>

        <div class="pull-right">
            <ul class="nav pull-right right-menu">
                <li class="more-options dropdown" id="nav_logout">
                    <a href="#" id="nav_logout"> <i class="zmdi zmdi-sign-in"></i></a>
                </li>
            </ul>
        </div>
    </div>
</nav>