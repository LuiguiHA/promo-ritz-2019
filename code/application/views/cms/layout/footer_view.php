<script type="text/javascript">

   $(window).resize(function(){
        if($(window).width() >= 1440){
            $(".side-panel").addClass("open");
            $(".sidepanel-toggle").parent().addClass("open");
            $("body").addClass("fixed-sidebar-example");
        }
        else{
            $(".side-panel").removeClass("open");
            $(".sidepanel-toggle").parent().removeClass("open");
            $("body").removeClass("fixed-sidebar-example");
        }
    });

  $('#nav_logout').on("click", function () {
      swal({
              title: "¿Está seguro que desea salir del sistema?",
              text: "Presione NO si desea continuar trabajando, presione SI para cerrar su sesión.",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-form-ritz",
              cancelButtonClass: "btn-form-ritz",
              confirmButtonText: "Si",
              cancelButtonText: "No",
              closeOnConfirm: false
          },
          function () {
              window.location.assign("<?php echo site_url($this->config->item('base_cms') . '/logout');?>");

          });
  });

</script>