<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- META SECTION -->
        <?php
        if (isset($tituloPagina) && $tituloPagina != ""):
            ?>
            <title><?php echo $tituloPagina; ?></title>
            <?php
        endif;
        ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no"/>


        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#49CEFF">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#49CEFF"/>
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/metisMenu/dist/metisMenu.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/Waves/dist/waves.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/toastr/toastr.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/bootstrap-select/dist/css/bootstrap-select.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/datatables/media/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>bower_components/bootstrap-sweetalert/lib/sweet-alert.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>css/ritz/style.css">


        <link rel="icon" href="<?php echo base_url('assets/cms/');?>img/favicon.ico" type="image/x-icon"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/cms/');?>css/style.css">




        <!--
          <link rel="icon" href="img/favicon.ico" type="image/x-icon" />
          <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        -->
        <!--[if lt IE 9]-->
        <script src="<?php echo base_url('assets/cms/');?>bower_components/html5shiv/dist/html5shiv.min.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/respondJs/dest/respond.src.js"></script>
        <!--[endif]-->

        <!--------------------JS-------------------->

        <script src="<?php echo base_url('assets/cms/');?>bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/Waves/dist/waves.min.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/toastr/toastr.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/datatables.net-responsive/js/dataTables.responsive.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/moment/min/moment.min.js"></script>

        <script src="<?php echo base_url('assets/cms/');?>bower_components/flot/jquery.flot.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/flot/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/flot.tooltip/js/jquery.flot.tooltip.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>bower_components/bootstrap-sweetalert/lib/sweet-alert.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>js/ritz/moment.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>js/ritz/es.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>js/ritz/datepicker/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php echo base_url('assets/cms/');?>js/common.js"></script>
        <!------------------------------------------>


        <!--------------->

        <!--------------->


    </head>
    <body class="user-page">
        <!--Preloader-->
        <div id="preloader">
            <div class="refresh-preloader">
                <div class="preloader"><i>.</i><i>.</i><i>.</i></div>
            </div>
        </div>

        <div class="wrapper warning-bg">
            <?php
            if (isset($view) && $view != "") $this->load->view($view);
            ?>
        </div>

        <script>
            $('#preloader').height($(window).height() + "px");
            $(window).on('load', function () {
                setTimeout(function () {
                    $('body').css("overflow-y", "visible");
                    $('#preloader').fadeOut(400);
                }, 800);
            });
        </script>
    </body>
</html>






