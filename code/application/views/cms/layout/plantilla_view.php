<!DOCTYPE html>
<html lang="en">
    <head> 
    <?php
        $this->load->view('cms/layout/header_view',TRUE);
    ?>      
    </head>

    <body class="fixed-sidebar-example">
    <!--Preloader-->
    <div id="preloader">
        <div class="refresh-preloader">
            <div class="preloader"><i>.</i><i>.</i><i>.</i></div>
        </div>
    </div>
    <div class="wrapper">


            <?php
            // if(isset($mostrarBarraTop) && $mostrarBarraTop === TRUE)
            $this->load->view('cms/layout/barra_navegacion_top_view');
            ?>


        <?php
        if(isset($mostrarMenuLateral) && $mostrarMenuLateral === TRUE){
            $this->load->view('cms/layout/menu_lateral_view');
        }
        ?>





        <div class="container-fluid">
            <?php
            if(isset($view) && $view !="")
                $this->load->view($view);
            ?>
        </div>



        <footer class="page-footer"> 
            <?php
            $this->load->view('cms/layout/footer_view',TRUE);
            ?>
        </footer>
        
       
        </div>
    </body>
</html>