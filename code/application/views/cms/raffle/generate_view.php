<div class="row">
    <div class="col-lg-12">
        <div class="content-box">
            <div class="head clearfix head_background_color" style="padding-right: 0;!important;">
                <h5 class="content-title pull-left"><?php echo $this->lang->line('constant_form_title_raffle')?></h5>
                <div class="pull-right" style="text-align: center;position: relative">

                </div>
            </div>

            <div class="content" id="div_content_search">
                <div class="row">
                    <div class="col-md-12">
                        <form enctype="multipart/form-data" method="POST" class="form-horizontal" >
                            <div class="form-group">
                                <?php
                                if ($this->session->userdata("super_admin") == SUPER_ADMIN) {
                                    ?>
                                    <div class="col-md-6">
                                        <label><?php echo $this->lang->line('constant_form_search_campaign_user') ?></label>
                                        <select id="raffle_campaign" class="form-control selectpicker"
                                                name="raffle_campaign">
                                            <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                            <?php
                                            if (isset($campaigns)) {
                                                foreach ($campaigns as $campaign)
                                                { ?>
                                                    <option value="<?php echo $campaign->id; ?>" >
                                                        <?php echo $campaign->country_name; ?>
                                                    </option>
                                                <?php }
                                            }?>
                                        </select>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div
                                    class = "col-md-6"
                                >
                                    <label><?php echo $this->lang->line('constant_form_prize_raffle')?></label>
                                    <select id="raffle_prize" class="form-control selectpicker" name="raffle_prize">
                                        <option value=""><?php echo $this->lang->line('constant_select')?></option>
                                    </select>
                                </div>


                            </div>

                            <div class="modal-footer" >

                                <button type="button" class="btn btn-lg m-b-5 button_form"
                                        style="margin-top: 20px;!important;" id="raffle" value="raffle" name="search_raffle"><?php echo $this->lang->line('constant_btn_raffle')?>
                                </button>

                                <div id="loader_form" class="loader_button_left"></div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
   
</div>



<div class="modal fade" id="modal-dafault" tabindex="-1" role="dialog" aria-hidden="true" >
    <div  class="modal-dialog" id="content_detail" >
        <div class="modal-content">
            <div class="modal-header content_center">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>

                <h4 class="modal-title"><?php echo $this->lang->line('constant_title_detail')?></h4>

            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_name_detail')?></label>
                                <div class="col-sm-9">
                                    <label class="form-control" id="name_winner"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_dni_detail')?></label>
                                <div class="col-sm-9">
                                    <label class="form-control" id="dni_winner"></label>
                                </div>
                            </div>

                            <div class="form-group" >
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_phone_detail')?></label>
                                <div class="col-sm-9">
                                    <label class="form-control" id="phone_winner"></label>
                                </div>
                            </div>

                            <div class="form-group" >
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_email_detail')?></label>
                                <div class="col-sm-9">
                                    <label class="form-control" id="email_winner"></label>
                                </div>
                            </div>

                            <div class="form-group" >
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_points_detail')?></label>
                                <div class="col-sm-9">
                                    <label class="form-control" id="points_winner"></label>
                                </div>
                            </div>

                            <div class="form-group" >
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_prize_detail')?></label>
                                <div class="col-sm-9">
                                    <label class="form-control" id="prize_winner"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('constant_form_status_detail')?></label>
                                <div class="col-sm-9">
                                    <label class="form-control" id="status_winner"></label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg m-b-5 button_form"  id="btn_accepted" data-dismiss="modal" ><?php echo $this->lang->line('constant_btn_accept')?>
                </button>
            </div>
        </div>
    </div>
    <div class="loader" id="loader_content_detail_client" style="visibility: hidden;display: none"></div>
</div>

<script>

    $(document).ready(function() {
        if ("<?php echo $super_admin?>"=="")
        {
            change_campaign();
        }

    });


    $("#raffle_campaign").change(function () {
        change_campaign();
    });


    function change_campaign()
    {

      var campaign = "";

        if ("<?php echo $super_admin?>" == "1")
        {
            campaign = $("#raffle_campaign").val();
        }else
        {
            campaign = "<?php echo $campaign_code?>";
        }


        $('#raffle_prize').empty();
        $("#raffle_prize").append('<option value=""> <?php echo $this->lang->line('constant_select')?>  </option>');
        if (campaign != "")
        {
            $.ajax({
                url: "<?php echo site_url($this->config->item('base_cms') . '/Sorteo_Premios/lista');?>",
                type: 'POST',
                data: {
                    'raffle_campaign' : campaign
                },
                dataType: 'json',
                success: function (response) {

                    if (response.code == 1) {

                        $.each(response.prizes, function (i, prizes) {
                            $("#raffle_prize").append('<option value="' + prizes.id + '">' + prizes.name + '</option>');
                        });

                    } else {
                        var text = response.message;
                        alter_error(text);
                    }
                },
                error: function (xhr, status) {
                    var text = "<?php echo $this->lang->line('constant_error_service')?>";
                    alter_error(text);
                }
            });
        }
    }

    $("#loader_form").hide();

    $("#raffle").click(function () {
        if (validation() == 0){
            $("#raffle").hide();
            $("#loader_form").show();

            $.ajax({
                url: "<?php echo site_url($this->config->item('base_cms') . '/Sorteo/generar');?>",
                type: 'POST',
                data: {
                        'raffle_campaign': $("#raffle_campaign").val(),
                        'raffle_prize': $("#raffle_prize").val()
                },
                dataType: 'json',
                success: function (response) {

                    if (response.code == 1) {

                        $("#loader_form").hide();
                        $("#raffle").show();
                      // var  name_prize = ("#raffle_prize option:selected").text();

                        var  name_prize =  $("#raffle_prize :selected").text();
                        $('#modal-dafault').modal('show');
                        $('#name_winner').text(response["user"].name);
                        $('#dni_winner').text(response["user"].dni);
                        $('#points_winner').text(response["user"].points);
                         $('#prize_winner').text(name_prize);
                        $('#phone_winner').text(response["user"].phone);
                        $('#email_winner').text(response["user"].email);

                        var status = "";

                        if(response["user"].status_user == "<?php echo STATUS_ACTIVE?>")
                        {
                            status = "<?php echo $this->lang->line('constant_status_active')?>";
                        }else
                        {
                            status = "<?php echo $this->lang->line('constant_status_inactive')?>";
                        }

                        $('#status_winner').text(status);

                        change_campaign();

                    } else {
                        $("#loader_form").hide();
                        $("#raffle").show();
                        var text = response.message;
                        alter_error(text);
                    }
                },
                error: function (xhr, status) {
                    $("#loader_form").hide();
                    $("#raffle").show();
                    var text = "<?php echo $this->lang->line('constant_error_service')?>";
                    alter_error(text);
                }
            });

        }
    });

    function validation(){

        var message = new Array();
        var text = "";

        if ($("#raffle_country").val() == ""){
            text = "<?php echo $this->lang->line('constant_form_required_country_raffle')?>";
            message.push(text);
        }
        if ($("#raffle_prize").val() == ""){
            text = "<?php echo $this->lang->line('constant_form_required_prize_raffle')?>";
            message.push(text);
        }

        for (var i =0 ; i< message.length ; i ++){
            alert_message_validation(message[i]);
        }

        return message.length;

    }

    function alert_message_validation(text) {
        var msg = text;
        var shortCutFunction = 'warning';

        var $toast = toastr[shortCutFunction](msg);

        toastr.options = {

            closeButton: false,
            debug: true,
            newestOnTop: false,
            progressBar: false,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            onclick: null,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "10000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "show",
            hideMethod: "fadeOut"

        };

    }

    function alter_error(text) {
        swal({
            title: "",
            text: text,
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: 'btn-form-ritz',
            confirmButtonText: 'Aceptar'
        });
    }
</script>
