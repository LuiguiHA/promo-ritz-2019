<div class="row">
    
    <div class="col-lg-12">
        <div class="content-box">
            
            <div class="table-responsive alt-table" style="padding: 10px">
                <table class="table table-hover">
                    <thead>
                    <tr>

                        <th>Nombre</th>
                        <th>Fecha de registro</th>
                        <th>Fecha de entrega</th>
                        <th>Estado</th>
                        
                    </tr>
                    </thead>

                    <tbody id="content_data_table">
                    <?php

                    if (isset($prizes) && count($prizes) > 0)
                    {
                        foreach ($prizes as $prize)
                        {

                            ?>
                            <tr>

                                <td><?php echo $prize->name;?></td>
                                <td><?php echo date("d/m/Y H:i:s", strtotime($prize->created_at));?></td>
                                <td><?php echo ($prize->updated_at != null)?date("d/m/Y H:i:s", strtotime($prize->updated_at)):"";?></td>
                                <td>
                                    <?php
                                    if ($prize->status == ACTIVE)
                                    {
                                        ?>
                                        <span class="label label-form status_background_active">
                                            Activo
                                        </span>
                                        <?php
                                    } elseif ($prize->status == INACTIVE)
                                    {
                                        if($prize->updated_at != null){
                                            ?>
                                            <span class="label label-form status_background_inactive" >
                                                ENTREGADO
                                            </span>
                                            <?php
                                        }
                                        else{
                                        ?>
                                        <span class="label label-form status_background_inactive" >
                                                INACTIVO
                                            </span>
                                        <?php
                                        }
                                        ?>
                                        
                                    <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if($prize->updated_at != null){
                                        echo "";
                                    }
                                    else{
                                    ?>
                                    <?php
                                        if ($prize->status == ACTIVE){
                                        ?>
                                            <a title="Desactivar" href="javascript:actualizarEstado(<?php echo $prize->id; ?>, 0);">
                                                <button type="button" class="btn btn-default raised icon">
                                                    <i class="zmdi zmdi-close"></i>
                                                </button>
                                            </a>
                                        <?php
                                        }
                                        else{
                                        ?>
                                            <a title="Activar" href="javascript:actualizarEstado(<?php echo $prize->id; ?>, 1);">
                                                <button type="button" class="btn btn-default raised icon">
                                                    <i class="zmdi zmdi-check"></i>
                                                </button>
                                            </a>
                                        <?php
                                        }
                                    }
                                    ?>
                                    
                                </td>
                            </tr>
                            <?php
                        }

                    }
                    ?>

                    </tbody>
                </table>

                <div >
                    <?php
                    if (isset($paginador)) echo $paginador;
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>
<script>

    $('#export_users').click(function () {

        var url = "<?php echo site_url($this->config->item('base_cms').'/Usuario/exportar_lista');?>";
        var search_user_dni = $("#search_user_dni").val();
        var search_user_name = $("#search_user_name").val();
        var search_user_fecha_ini = $("#search_user_fecha_ini").val();
        var search_user_fecha_fin = $("#search_user_fecha_fin").val();
        var search_user_status = $("#search_user_status").val();

        window.location.href = url + "?search_user_dni=" + search_user_dni + "&search_user_name=" + search_user_name + "&search_user_fecha_ini=" + search_user_fecha_ini + "&search_user_fecha_fin=" + search_user_fecha_fin +"&search_user_status=" + search_user_status;

    });


    function actualizarEstado(id_premio, estado) {
        $.ajax({
            url: "<?php echo site_url($this->config->item('base_cms') . '/Premio/actualizar_estado');?>",
            type: "POST",
            data : {prize_id : id_premio, status:estado},
            dataType: 'json',
            success: function (response) {
                location.reload();
                /*
                var message = "";
                var type = "";
                if(response.response == "ok"){
                    message = response.mensaje;
                    type =  "success";
                }else{
                    message = response.mensaje;
                    type =  "warning";
                }*/
               // $('#message-box-default').hide();
               // hidden_loader();
               // show_message(type,message)
            },
            error: function (xhr, status) {
                location.reload();
              //  $('#message-box-default').hide();
              //  hidden_loader();
              //  $('#content').show();
              //  var text = "No se ha podido conectar con el servidor";
              //  show_message('warning',text)
            }
        });
    }

</script>
