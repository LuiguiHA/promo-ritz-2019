<div class="row">
    <div class="col-lg-12">
        <div class="content-box">
            <div class="head clearfix head_background_color" style="padding-right: 0;!important;">
                <h5 class="content-title pull-left"><?php echo $this->lang->line('constant_form_title_admin')?></h5>
                <div class="pull-right" style="text-align: center;position: relative">

                    <button type="button"  id="div_search" class="btn btn-lg raised icon" >
                        <i id="img_search" class="zmdi zmdi-chevron-down"></i>
                    </button>
                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-12">
        <div class="content-box">            
            <div class="table-responsive alt-table" style="padding: 10px">
                <table class="table table-hover">
                    <thead>
                    <tr>

                        <th>Premio</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Fin</th>
                        <th>Ver puja</th>
                        
                    </tr>
                    </thead>

                    <tbody id="content_data_table">
                    <?php

                    if (isset($arrayPremiosEnSubasta) && count($arrayPremiosEnSubasta) > 0)
                    {
                        foreach ($arrayPremiosEnSubasta as $item)
                        {
                            ?>
                            <tr>
                                <td><?php echo $item->description;?></td>
                                <td><?php echo date("d/m/Y H:i:s",strtotime($item->start));?>
                                <td><?php echo date("d/m/Y H:i:s",strtotime($item->end));?>
                                <td>
                                    <a title="Ver puja" href="<?php echo site_url($this->config->item('base_cms').'/Subasta/usuarios/'.$item->id);?>">
                                        <button type="button" class="btn btn-default raised icon">
                                            <i class="zmdi zmdi-plus"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }

                    }
                    ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<script>


</script>
