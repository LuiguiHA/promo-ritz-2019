<div class="row">
    <div class="col-lg-12">
        <div class="content-box">
            <div class="head clearfix head_background_color" style="padding-right: 0;!important;">
                <h5 class="content-title pull-left"><?php echo $this->lang->line('constant_form_title_admin')?></h5>
                <div class="pull-right" style="text-align: center;position: relative">

                    <button type="button"  id="div_search" class="btn btn-lg raised icon" >
                        <i id="img_search" class="zmdi zmdi-chevron-down"></i>
                    </button>
                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-12">
        <div class="content-box">            
            <div class="table-responsive alt-table" style="padding: 10px">
                <table class="table table-hover">
                    <thead>
                    <tr>

                        <th>Premio</th>
                        <th>Usuario</th>
                        <th>DNI</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Puntos</th>
                        <th>Asignar premio</th>
                        
                    </tr>
                    </thead>

                    <tbody id="content_data_table">
                    <?php

                    if (isset($arrayUsuariosEnSubasta) && count($arrayUsuariosEnSubasta) > 0)
                    {
                        foreach ($arrayUsuariosEnSubasta as $item)
                        {
                            ?>
                            <tr>
                                <td><?php echo $item->description;?></td>
                                <td><?php echo $item->name;?></td>
                                <td><?php echo $item->dni;?></td>
                                <td><?php echo $item->email;?></td>
                                <td><?php echo $item->phone;?></td>
                                <td><?php echo $item->points;?></td>
                                <td>
                                <?php 
                                if($usuarioGanador->total_ganadores == 0)
                                {
                                ?>
                                    <a title="Asignar premio" href="javascript:confirmar_asignacion_premio('<?php echo $item->name;?>','<?php echo $item->user_id;?>','<?php echo $item->prize_sale_id;?>','<?php echo $item->points;?>')">
                                        <button type="button" class="btn btn-default raised icon">
                                            <i class="zmdi zmdi-badge-check"></i>
                                        </button>
                                    </a>
                                <?php
                                }
                                ?>
                                </td>
                            </tr>
                            <?php
                        }

                    }
                    ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<script>


    function confirmar_asignacion_premio(nombre, idUsuario, idSubasta, puntos) {
        var txt;
        var r = confirm('¿Estás seguro de asignar el premio a "'+nombre+'"  ?');
        if (r == true) {
            window.location.href = "<?php echo site_url($this->config->item('base_cms').'/Subasta/premio/asignar/');?>"+idUsuario+"/"+idSubasta+"/"+puntos;
        } else {
            return false;
        }
    }

    <?php 
        $mensaje = $this->session->flashdata('mensaje');
        if($mensaje != null && $mensaje!= "")
            echo "alert('".$mensaje."')";
    ?>

</script>
