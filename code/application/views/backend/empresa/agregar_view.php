<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend') . '/Empresa/agregar'); ?>"
                      id="agregar_empresa" class="form-horizontal" role="form" name="agregar_empresa" method="post"
                      enctype="multipart/form-data">

                    <div id="formEmpresa">
                        <div class="form-group">
                            <label class="col-md-1 control-label">Nombre</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="nombre" id="nombre"/>
                                <label id="error_nombre" style="visibility: hidden ; color: #b64645 ; font-weight : normal;font-size: 10px">Campo Obligatorio.</label>
                            </div>


                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Codigo</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="codigo" id="codigo"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-1 control-label">Descripción</label>
                            <div class="col-md-11">

                                <textarea type="text" class="form-control" value="" name="descripcion"
                                          id="descripcion"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Ruc</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="ruc" id="ruc"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Razón Social</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="razon_social" id="razon_social"/>
                                <label id="error_razon" style="visibility: hidden ; color: #b64645 ; font-weight : normal;font-size: 10px">Campo Obligatorio.</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Logo</label>
                            <div class="col-md-11">

                                <div class="input-group date col-md-6" id="dp-2">
                                    <input type="file" class="fileinput btn-success" name="logo" id="logo"
                                           title="Seleccione una Imagen" style="left: -83.9063px; top: 5px;"
                                           accept=".jpg,.png,.jpeg">
                                    <label id="error_img" style="padding-left:20px;visibility: hidden ; color: #b64645 ; font-weight : normal">La imagen es necesaria. </label>
                                </div>

                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Formula</label>
                            <div class="col-md-11">
                                <label id="error_formula" style="visibility: hidden ;display: none; color: #b64645 ; font-weight : normal;font-size: 10px">Campo Obligatorio.</label>

                                <select class="form-control select" name="formula" id="formula">
                                    <option value="">Seleccione</option>
                                    <?php foreach ($formulas as $formula) { ?>
                                        <option value="<?php echo $formula->id; ?>">
                                            <?php echo $formula->nombre; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Sitio Web</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="sitio_web" id="sitio_web"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-1 control-label">Estado</label>
                            <div class="col-md-11">

                                <label id="error_estado" style="visibility: hidden ;display: none; color: #b64645 ; font-weight : normal;font-size: 10px">Campo Obligatorio.</label>
                                <select class="form-control select" name="estado" id="estado">
                                    <option value="">Seleccione</option>
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>


                        <div class="btn-group pull-right">
                            <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                               href="<?php echo site_url($this->config->item('path_backend') . '/Empresa'); ?>">Cancelar</a>
                            <a type="button" class="btn btn-primary" style="margin-right: .5em;" onclick="formLocal()">Siguiente</a>
                        </div>

                    </div>

                    <div id="formLocal" style="visibility: hidden;display: none">

                        <div class="form-group">
                            <label class="col-md-1 control-label" style="font-size: 25px">Local</label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Nombre</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="nombre_local" id="nombre_local"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Dirección</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="direccion" id="direccion"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Latitud</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="latitud" id="latitud"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Longitud</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="longitud" id="longitud"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Correo</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="correo" id="correo"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Telefono</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="telefono" id="telefono"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Localidad</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control" value="" name="localidad" id="localidad"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Meses en Reserva</label>
                            <div class="col-md-11">
                                <input type="text" class="form-control spinner_default" value="1" id="meses"
                                       name="meses" min="1" readonly/>
                            </div>
                        </div>

                        <div class="btn-group pull-right">
                            <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                               onclick="formEmpresa()">Anterior</a>
                            <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">

    function formLocal() {
/*
      var validateForm = $("#agregar_empresa").validate({
            ignore: [],
            rules: {
                nombre: {
                    required: true
                },
                razon_social: {
                    required: true
                },
                formula: {
                    required: true
                },
                estado: {
                    required: true
                }
            }
        }).form();
*/


        var logo_val = $("#logo").val();
        var nombre_val = $("#nombre").val();
        var razon_social_val = $("#razon_social").val();
        var estado_val = $("#estado").val();
        var formula_val = $("#formula").val();
        var validacionLogo;

        if(nombre_val.trim() == "" ) {
            error_nombre.style.visibility='visible';
            $("#nombre").css("border-color", "#b64645");
            validacionNombre = false;
        }else {
            error_nombre.style.visibility='hidden';
            $("#nombre").css("border-color", "");
            validacionNombre =  true;
        }

        if(razon_social_val.trim() == "") {
            error_razon.style.visibility='visible';
            $("#razon_social").css("border-color", "#b64645");
            validacionRazon = false;
        }else {
            error_razon.style.visibility='hidden';
            $("#razon_social").css("border-color", "");
            validacionRazon =  true;
        }
        if(logo_val == "") {
            error_img.style.visibility='visible';
            validacionLogo = false;
        }else {
            error_img.style.visibility='hidden';
            validacionLogo =  true;
        }

        if(formula_val == "") {
            error_formula.style.visibility='visible';
            error_formula.style.display='block';
            $("#formula").css("border-color", "#b64645");
            validacionFormula = false;
        }else {
            error_formula.style.visibility='hidden';
            error_formula.style.display='none';
            $("#formula").css("border-color", "");
            validacionFormula =  true;
        }

        if(estado_val == "") {
            error_estado.style.visibility='visible';
            error_estado.style.display='block';
            validacionEstado = false;
        }else {
            error_estado.style.visibility='hidden';
            error_formula.style.display='none';
            validacionEstado =  true;
        }



        if (validacionNombre && validacionRazon && validacionLogo && validacionFormula && validacionEstado) {
            hiddenForm('#formEmpresa');
            showForm('#formLocal');

        }
    }

    function formEmpresa() {
        hiddenForm('#formLocal');
        showForm('#formEmpresa');

    }

        $("#agregar_empresa").validate({
            ignore: [],
            rules: {
                nombre_local: {
                    required: true,
                },
                direccion: {
                    required: true,
                },
                latitud: {
                    required: true,
                },

                correo: {
                    email: true,
                    required: true
                },
                cantidad: {
                    required: true,
                },

                localidad: {
                    required: true,
                },

                telefono: {
                    required: true,
                },

                longitud: {
                    required: true,
                },

                estado: {
                    required: true
                }
            }
        });

    $(document).ready(function () {
        $('#ruc').keypress(validateNumber);
    });

    $(function () {
        $(".spinner_default").spinner()
    });


    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if (key < 48 || key > 57) {
            return false;
        }
        else return true;
    }


    function showForm(id) {
        $(id).css({"visibility": "visible"});
        $(id).css({"display": "block"});
    }

    function hiddenForm(id) {
        $(id).css({"visibility": "hidden"});
        $(id).css({"display": "none"});
    }


</script>
