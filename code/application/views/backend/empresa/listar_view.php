<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap" style="padding: 15px">
    <div class="container-fluid" style="padding-left: 0px !important;padding-right: 0px !important;">
        <div class="panel panel-default" style="border-top-color: #DAD4D4 ; border-top-width : 2px">
            <div class="panel-body">
                <div class="panel-body">


                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php echo site_url($this->config->item('path_backend').'/Empresa/buscar');?>">
                        <div class="col-md-12 form-group">


                            <div class="col-md-3">
                                <label class="control-label"> Nombre</label>
                                <input id="nombre" class="form-control input" type="text" name="nombre"
                                       placeholder="Nombre" value="<?php echo $nombre;?>" class="input-small"/>
                            </div>

                            <div class="col-md-3">
                                <label class="control-label"> Código</label>
                                <input id="codigo" class="form-control input" type="text" name="codigo"
                                       placeholder="Codigo" value="<?php echo $codigo;?>" class="input-small"/>
                            </div>

                            <div class="col-md-3">
                                <label class="control-label"> RUC</label>
                                <input id="ruc" class="form-control input" type="text" name="ruc"
                                       placeholder="RUC" value="<?php echo $ruc;?>" class="input-small"/>
                            </div>



                            <div class="col-md-3" >
                                <label class="control-label">Estado</label>
                                <select class="form-control select" name="estado" id="estado">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($estado == "1") echo "selected";?>>Activo</option>
                                    <option value="0" <?php if ($estado == "0") echo "selected";?>>Inactivo</option>
                                </select>

                            </div>
                            <input type="hidden" name="buscar" id="buscar"
                                   value=""/>

                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-8">
                                <input type="submit" class="btn btn-primary pull-right" value="Buscar"/>
                            </div>
                        </div>

                    </form>


                </div>

            </div>
        </div>
    </div>
    <div class="panel panel-default" style="border-top-color: #DAD4D4 ; border-top-width : 2px">
        <div class="panel-heading">
            <h3 class="panel-title">Empresas</h3>
            <a   id="export_empresa" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>
            <a href="<?php echo site_url($this->config->item('path_backend').'/Empresa/agregar');?>" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>


        </div><?php

    
     
        ?>
        <div class="panel-body">
            <table class="table table-hover table-cms">
                <thead>
                <tr>
                    <td colspan="8" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Nombre</th>
                    <th>Código</th>
                    <th>Descripción</th>
                    <th>RUC</th>
                    <th>Razón Social</th>
                    <th>Sitio Web</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>

                <?php
                if (isset($empresas) && count($empresas) > 0) {

                    foreach ($empresas as $empresa) {
                        ?>
                        <tr>
                            <td><?php echo $empresa->nombre; ?></td>
                            <td><?php echo $empresa->codigo; ?></td>
                            <td><?php echo $empresa->descripcion; ?></td>
                            <td><?php echo $empresa->ruc; ?></td>
                            <td><?php echo $empresa->razon_social; ?></td>
                            <td><?php echo $empresa->sitio_web; ?></td>
                            <td>
                                <?php
                                if ($empresa->estado == 1)
                                     echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>';
                                else
                                     echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>';
                                ?>
                            </td>
                            <td>

                                <a href="<?php echo site_url($this->config->item('path_backend') . '/Empresa/editar/' . $empresa->id); ?>"
                                   class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>
                            </td>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php


            if (isset($paginador))
                echo $paginador;
            ?>

        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(document).on('click','#paginador ul li a',function(c){
            c.preventDefault();
            var inicio = jQuery(this).attr('href');
            jQuery("#form_busqueda").attr("action",inicio);
            jQuery("#form_busqueda").submit();
        });
    });

    $('#export_empresa').click(function(){
        // e.preventDefault();

        var url = "<?php echo site_url($this->config->item('path_backend').'/Empresa/export');?>";
        var nombre=$("#nombre").val();
        var codigo=$("#codigo").val();
        var ruc=$("#ruc").val();
        var estado=$("#estado").val();
        window.location.href= url+"?nombre="+nombre+"&codigo="+codigo+"&ruc="+ruc+"&estado="+estado;


    });
</script>


<!-- END PAGE CONTENT WRAPPER -->