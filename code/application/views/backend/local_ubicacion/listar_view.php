<!-- PAGE CONTENT WRAPPER -->


<div class="panel-body">
    <table class="table table-hover table-cms">
        <thead>
        <tr>
            <td colspan="4" align="center" valign="top"></td>
        </tr>
        <tr>
            <th>Nombre</th>
            <th>Fecha de Registro</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>


        <?php


        if (isset($local_ubicacion) && count($local_ubicacion) > 0)
        {

            foreach ($local_ubicacion as $ubicacion)
            {
                ?>
                <tr>

                    <td><?php echo $ubicacion->nombre; ?></td>
                    <td><?php echo date('Y-m-d g:i a', strtotime($ubicacion->fecha_registro)); ?></td>
                    <td>
                        <?php
                        if ($ubicacion->estado == 1) echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>'; else
                            echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>'
                        ?>
                    </td>
                    <td>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Local_ubicacion/editar_view/' .$empresa.'/'. $local . '/' . $ubicacion->id); ?>"
                           class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>
                    </td>

                </tr>
                <?php
            }

        }
        ?>

        </tbody>
    </table>
    <?php

    if (isset($paginador)) echo $paginador;
    ?>
</div>


<script>

    $(document).on("click", "#paginador ul li a", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");

        // alert(href);
        $.post(href, {
                id_local: <?php echo $local;?> ,
                id_empresa: <?php echo $empresa;?> ,
                nombre: $("#nombre_local_ubicacion").val(),
                estado: $("#estado_local_ubicacion").val()
            },
            function (result) {

                $('#tabla_local_ubicacion').html(result);


            }, "html");

        return true;
    });

</script>
<!-- END PAGE CONTENT WRAPPER -->