<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Local_ubicacion/editar/'.$empresa.'/'.$local.'/'.$local_ubicacion->id); ?>"
                    id="editar_local_ubicacion" class="form-horizontal" role="form" name="editar_local_ubicacion" method="post"
                    enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $local_ubicacion->nombre;?>" name="nombre" id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if($local_ubicacion->estado == "1") echo "selected";?>>Activo</option>
                                <option value="0" <?php if($local_ubicacion->estado == "0") echo "selected";?>>Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend').'/Local_Empresa/editar/'.$empresa.'/'.$local.'/U'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">

  

    var jvalidate = $("#editar_local_ubicacion").validate({
        ignore: [],
        rules: {
          
            nombre: {
                required: true,
            },

            estado: {
                required: true
            }
        }
    });

  

</script>
