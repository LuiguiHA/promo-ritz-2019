<!-- PAGE CONTENT WRAPPER -->

<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Promoción</a></li>
        <li><a href="#tab2" data-toggle="tab" id="lista_promocion_foto">Foto</a></li>
        <li><a href="#tab3" data-toggle="tab" id="lista_promocion_info">Información Adicional</a></li>
        <?php if ($promocion->locales != 2){?>
        <li><a href="#tab4" data-toggle="tab" id="lista_promocion_local">Locales validos</a></li>
        <?php }?>
    </ul>


    <div class="tab-content">
        <div class="tab-pane panel-body active" id="tab1">


            <form action="<?php echo site_url('/backend/Promocion/editar/' . $promocion->id) ?>" id="editar_promocion"
                  class="form-horizontal" role="form" name="editar_promocion" method="post"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label class="col-md-2 control-label">Categoría</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="categoria" id="categoria" >
                            <option value="">Seleccione</option>
                            <?php foreach ($categorias as $categoria) { ?>
                                <option value="<?php echo $categoria->id;?>" <?php if ($promocion->id_categoria == $categoria->id) {
                                    echo "selected";
                                } ?>>
                                    <?php echo $categoria->nombre; ?>
                                </option>
                            <?php } ?>

                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">Empresa</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="empresa" id="empresa" <?php if ($empresas !=null && sizeof($empresas) == 1) echo "disabled"?>>
                            <option value="">Seleccione</option>
                            <?php foreach ($empresas as $empresa) { ?>
                                <option value="<?php echo $empresa->id;?>" <?php if ($promocion->id_empresa == $empresa->id) {
                                    echo "selected";
                                } ?>>
                                    <?php echo $empresa->nombre; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <?php if ($empresas != null && sizeof($empresas) == 1){
                    ?>
                    <input type="hidden" name="empresa" value="<?php  echo $empresas[0]->id?>" />
                <?php }
                ?>

                <div class="form-group">
                    <label class="col-md-2 control-label">Tipo</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="tipo" id="tipo" >
                            <option value="">Seleccione</option>
                            <option value="1" <?php if($promocion->tipo == "1") echo "selected";?>>%</option>
                            <option value="2" <?php if($promocion->tipo == "2") echo "selected";?>>Nominal</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Nombre</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $promocion->nombre;?>" name="nombre"  id="nombre" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Descripción</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $promocion->descripcion;?>" name="descripcion"  id="descripcion" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Descuento</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $promocion->descuento;?>" name="descuento" id="descuento" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Restricción</label>
                    <div class="col-md-10">

                        <textarea type="text" class="form-control"  name="restriccion" id="restriccion"><?php echo $promocion->restriccion;?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Orden</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $promocion->orden;?>" name="orden" id="orden" maxlength="4"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Disponible en todos los locales </label>
                    <div class="col-md-10">
                        <select class="form-control select" name="locales" id="locales" required>
                            <option value="">Seleccione</option>
                            <!--
                            <option value="1" <?php if ($promocion->locales == "1") echo "selected";?>>Si</option>
                            <option value="0" <?php if ($promocion->locales == "0") echo "selected";?>>Algunos</option>
                            <option value="2" <?php if ($promocion->locales == "2") echo "selected";?>>Delivery</option>
                            -->
                            <option value="<?php echo STATUS_ALL_LOCALS?>" <?php if ($beneficio->tipo_locales == STATUS_ALL_LOCALS) echo "selected"?>>Si</option>
                            <option value="<?php echo STATUS_SOME_LOCALS?>" <?php if ($beneficio->tipo_locales == STATUS_SOME_LOCALS) echo "selected"?>>Algunos</option>
                            <option value="<?php echo STATUS_DELIVERY?>" <?php if ($beneficio->tipo_locales == STATUS_DELIVERY) echo "selected"?>>Delivery</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">Fecha de inicio</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo date('Y-m-d', strtotime($promocion->fecha_inicio)); ?>" name="fecha_inicio" id="fecha_inicio" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Fecha de finalización</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo date('Y-m-d', strtotime($promocion->fecha_fin)); ?>" name="fecha_fin" id="fecha_fin" />
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">Estado</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="estado" id="estado" >
                            <option value="">Seleccione</option>
                            <option value="1"  <?php if($promocion->estado == "1") echo "selected";?>>Activo</option>
                            <option value="0"  <?php if($promocion->estado == "0") echo "selected";?>>Inactivo</option>
                        </select>
                    </div>
                </div>



                <div class="btn-group pull-right">
                    <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/Promocion');?>">Cancelar</a>
                    <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                </div>


            </form>

        </div>
        <div class="tab-pane panel-body " id="tab2">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">


                                <form name="form_busqueda_foto" id="form_busqueda_foto" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-4">
                                            <label class="control-label">Tipo</label>
                                            <select class="form-control select" name="tipo_foto" id="tipo_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($tipo == "1") echo "selected"; ?>>Principal
                                                </option>
                                                <option value="2" <?php if ($tipo == "2") echo "selected"; ?>>
                                                    Detalle
                                                </option>
                                            </select>

                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Plataforma</label>
                                            <select class="form-control select" name="plataforma_foto" id="plataforma_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($plataforma == "1") echo "selected"; ?>>Web
                                                </option>
                                                <option value="2" <?php if ($plataforma == "2") echo "selected"; ?>>
                                                    Móvil
                                                </option>
                                            </select>

                                        </div>


                                        <div class="col-md-4">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_foto" id="estado_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_promocion_foto">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fotos</h3>
                        <a   id="export_foto_promocion" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Foto_Promocion/agregar/'.$promocion->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_promocion_foto"></div>

                </div>


            </div>
        </div>
        <div class="tab-pane panel-body " id="tab3">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">
                                <form name="form_busqueda_info" id="form_busqueda_info" method="post" action="">
                                    <div class="col-md-12 form-group">


                                        <div class="col-md-4">
                                            <label class="control-label"> Campo</label>
                                            <input id="campo_info" class="form-control input" type="text" name="campo_info"
                                                   placeholder="Campo" value="<?php echo $campo; ?>"
                                                   class="input-small"/>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label"> Valor</label>
                                            <input id="valor_info" class="form-control input" type="text" name="valor_info"
                                                   placeholder="Valor" value="<?php echo $valor; ?>"
                                                   class="input-small"/>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_info" id="estado_info">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_promocion_info">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Infomación Adicional</h3>
                        <a   id="export_info_promocion" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Info_Adicional_Promocion/agregar/'.$promocion->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_promocion_info"></div>

                </div>


            </div>
        </div>
        <div class="tab-pane panel-body " id="tab4">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">
                                <form name="form_busqueda_local" id="form_busqueda_local" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-6">
                                            <label class="control-label"> Local</label>
                                            <input id="local_nombre" class="form-control input" type="text" name="local_nombre"
                                                   placeholder="Local" value="<?php echo $local; ?>"
                                                   class="input-small"/>
                                        </div>


                                        <div class="col-md-6">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_local" id="estado_local">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Seleccionar
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Quitar
                                                </option>
                                            </select>

                                        </div>


                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_promocion_local">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Locales validos</h3>
                        <a   id="export_local_promocion" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Local_Promocion/agregar/' . $promocion->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Seleccionar</a>

                    </div>
                    <div id="tabla_promocion_local"></div>

                </div>


            </div>
        </div>
    </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    <script type="text/javascript">

        var jvalidate = $("#editar_promocion").validate({
            ignore: [],
            rules: {
                categoria: {
                    required: true
                },
                empresa: {
                    required: true

                },
                nombre: {
                    required: true
                },
                descuento: {
                    required: true
                },
                correo: {

                    email: true
                },
                tipo: {
                    required: true

                },
                puntos: {
                    required: true

                },

                fecha_inicio: {
                    required: true
                },
                fecha_fin: {
                    required: true
                },
                estado: {
                    required: true
                }
            }
        });

        $(document).ready(function(){


            $('#orden').keypress(validateNumber);
            $('#descuento').keypress(validateNumberDouble);

        });

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;

            if (event.keyCode === 8 || event.keyCode === 46
                || event.keyCode === 37 || event.keyCode === 39) {
                return true;
            }
            else if ( key < 48 || key > 57 ) {
                return false;
            }
            else return true;
        }

        function validateNumberDouble(event) {
            var code = (event.which) ? event.which : event.keyCode;
            if(code==8 || code ==45 || code == 46)
            {
                //backspace
                return true;
            }
            else if(code>=48 && code<=57)
            {
                //is a number
                return true;
            }
            else
            {
                return false;
            }
        }


        $("#lista_promocion_foto").click(function () {

            $.post("<?php echo site_url('backend/Foto_promocion/listar');?>",{id_promocion : <?php echo $promocion->id ;?>},
                function (result) {

                    $('#tabla_promocion_foto').html(result);


                }, "html");

            return true;
        });

        $("#buscar_promocion_foto").click(function () {


            $.post("<?php echo site_url('backend/Foto_promocion/buscar');?>",
                {id_promocion : <?php echo $promocion->id;?>, tipo : $("#tipo_foto").val(), estado : $("#estado_foto").val(),plataforma : $("#plataforma_foto").val()},
                function (result) {
                    $('#tabla_promocion_foto').html(result);

                }, "html");

            return true;
        });


        $("#lista_promocion_info").click(function () {

            $.post("<?php echo site_url('backend/Info_adicional_promocion/listar');?>",{id_promocion : <?php echo $promocion->id ;?>},
                function (result) {

                    $('#tabla_promocion_info').html(result);


                }, "html");

            return true;
        });

        $("#buscar_promocion_info").click(function () {


            $.post("<?php echo site_url('backend/Info_adicional_promocion/buscar');?>",
                {id_promocion : <?php echo $promocion->id;?>, campo : $("#campo_info").val(), valor : $("#valor_info").val()
                    , estado : $("#estado_info").val()},
                function (result) {
                    $('#tabla_promocion_info').html(result);

                }, "html");

            return true;
        });

        $("#lista_promocion_local").click(function () {

            $.post("<?php echo site_url('backend/Local_promocion/listar');?>", {id_promocion : <?php echo $promocion->id ;?>},
                function (result) {

                    $('#tabla_promocion_local').html(result);


                }, "html");

            return true;
        });

        $("#buscar_promocion_local").click(function () {


            $.post("<?php echo site_url('backend/Local_promocion/buscar');?>",
                {id_promocion : <?php echo $promocion->id;?>, local : $("#local_nombre").val()
                    , estado : $("#estado_local").val()},
                function (result) {
                    $('#tabla_promocion_local').html(result);

                }, "html");

            return true;
        });





        <?php
        switch($this->uri->segment(5)){
        case 'F' :
        ?>
        $(document).ready(function () {
            $('#lista_promocion_foto').trigger('click');
        });
        <?php  break;
        case 'I' :
        ?>
        $(document).ready(function () {
            $('#lista_promocion_info').trigger('click');
        });
        <?php  break;
        case 'L' :
        ?>
        $(document).ready(function () {
            $('#lista_promocion_local').trigger('click');
        });
        <?php
        break;
        }
        ?>
        $('#export_foto_promocion').click(function(){
            // e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Foto_Promocion/export');?>";
            var id_promocion = <?php echo $promocion->id;?>;
            var tipo=$("#tipo_foto").val();
            var estado=$("#estado_foto").val();
            var plataforma=$("#plataforma_foto").val();

            window.location.href= url+"?id_promocion="+id_promocion+"&tipo="+tipo+"&estado="+estado+"&plataforma="+plataforma;


        });

        $('#export_info_promocion').click(function(){
            // e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Info_Adicional_Promocion/export');?>";
            var id_promocion = <?php echo $promocion->id;?>;
            var campo=$("#campo_info").val();
            var valor=$("#valor_info").val();
            var estado=$("#estado_info").val();

            window.location.href= url+"?id_promocion="+id_promocion+"&campo="+campo+"&valor="+valor+"&estado="+estado;


        });

        $('#export_local_promocion').click(function(){
            // e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Local_Promocion/export');?>";
            var id_promocion = <?php echo $promocion->id;?>;
            var local=$("#local_nombre").val();
            var estado=$("#estado_local").val();

            window.location.href= url+"?id_promocion="+id_promocion+"&local="+local+"&estado="+estado;


        });

        $("#fecha_inicio").datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        });

        $("#fecha_fin").datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        });
    </script>
