<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Banner/agregar/'.$empresa); ?>"
                    id="agregar_banner" class="form-horizontal" role="form" name="agregar_banner" method="post"
                    enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Recurso</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="recurso" id="recurso" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="nombre" id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Descripción</label>
                        <div class="col-md-11">

                            <textarea type="text" class="form-control" value="" name="descripcion" id="descripcion"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Enlace</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="enlace" id="enlace" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Target</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="target" id="target" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Tipo</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="tipo" id="tipo">
                                <option value="">Seleccione</option>
                                <option value="1">Principal</option>
                                <option value="2">Detalle</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend').'/Empresa/editar/'.$empresa.'/B'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">



    var jvalidate = $("#agregar_banner").validate({
        ignore: [],
        rules: {
            recurso: {
                required: true,
            },
            nombre: {
                required: true,
            },
            
            estado: {
                required: true
            }
        }
    });

    

</script>
