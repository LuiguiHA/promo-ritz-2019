<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "/controllers/frontend/FrontendBaseController.php";

class Inicio extends FrontendBaseController {
  public function __construct()
  {
      parent::__construct();
      $this->load->helper('url');
      $this->load->model('meme_Model','meme_model');
  }



  public function index()
  {
    $this->minify
      ->js(['home.js'], "extra");
    //TODO: Juanka agrega aquí si hay winners o no
    $this->render('frontend/home', []);
    
  }

  public function winners()
  {
    $this->minify
      ->js(['winners.js'], "extra");
    $this->render('frontend/winners', []);
  }

  public function prizes()
  {
    $this->minify
      ->js(['prizes.js'], "extra");
    $this->render('frontend/prizes', []);
  }

  public function meme()
  {
    $meme_model = new meme_Model();

    $id = $this->uri->segment(2);

    $this->render('frontend/meme', [
      "meme" => $meme_model->getUrlById($id),
      "server" => substr(site_url() , 0, -1)
    ]);
  }

  public function memeGenerator()
  {

    $this->minify
      ->js(['meme-generator.js'], "extra");
    
    $this->render('frontend/meme-generator', [
      "test" => 1,
      "site_url" => site_url()
    ]);
  }
  public function memeList()
  {

    $this->minify
      ->js(['meme-list.js'], "extra");
    
    $this->render('frontend/meme-list', [
      "test" => 1,
      "site_url" => site_url()
    ]);
  }
  public function profile()
  {

    $this->minify
      ->js(['profile.js'], "extra");
    
    $this->render('frontend/profile', [
      "test" => 1,
    ]);
  }

}
