<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontendBaseController extends CI_Controller {
  public function __construct()
  {
      parent::__construct();

      $this->load->helper('utils_helper');
      $this->load->helper('form');
      $this->load->helper('url');
      $this->load->library('minify');
      $this->load->library('twig');

      $this->minify->js([
        'main.js',
        'menu.js',
        'footer.js'
      ]);

      $this->minify->css([
        'reset.css',
        'style.css',
        'responsive.css'
      ]);

      $this->minify->versioning = TRUE;
  }

  public function render($template, $template_vars) {
    $js_minified = $this->minify->deploy_js(true, 'auto');
    $css_minified = $this->minify->deploy_css(true, 'auto');

    $default_vars = array(
        "css_minified" => $css_minified,
        "js_minified" => $js_minified,
    );    

    $vars = array_merge($template_vars, $default_vars);

    $this->twig->display($template, $vars);
  }
}
