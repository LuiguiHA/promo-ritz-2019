<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');

        $this->load->helper('utils_helper');
        $this->load->model('admin_Model', 'admin_model');
        $this->load->model('token_Admin_Model', 'token_admin_model');
	}

	public function index()
	{
		if( $this->session->userdata('logged_in') == true )
		{
			redirect($this->config->item('base_cms').'/Usuario');
		}
		else
		{
			$this->arrayVista['view'] = 'cms/login/login_view';
			$this->cargarVistaLoginCMS();
		}

	}

	public function login()
    {
        $user = trim($this->input->post("user",TRUE));
        $password = trim($this->input->post("password",TRUE));
        $error = array();

        $admin_model = new Admin_Model();

        $login = $admin_model->login($user);

            if (isset($login->id))
            {
                if($login->status == ACTIVE)
                {
                    if(md5($password) != $login->password)
                    {
                        $error = array("message"=>$this->lang->line('login_error_password_incorrect'),"code"=>0);
                    } else
                    {

                        $token_admin_model = new Token_Admin_Model();
                        $token = $token_admin_model->generate_token();
                        $id_token  = $token_admin_model->save_token($token,$login->id);

                        if (isset($id_token)){
                            $this->session->sess_destroy();
                            $this->session->__construct();

                            unset($login->password);

                            $this->session->set_userdata(array('user' => $login,
                                                               'token'  => $token,
                                                               'logged_in' => true,
                                                               'username'=> $login->username,
                                                                'campaign_code'=> $login->country_code,
                                                                'country_name'=> $login->country_name,
                                                                'super_admin'=> $login->super_admin,
                                                                'campaign_id'=> $login->campaign_id)
                                                                );

                            $error = array("message"=>$this->lang->line('login_success'),"code"=>1);
                        }else{
                            $error = array("message"=>$this->lang->line('login_error_token'),"code"=>0);
                        }

                    }
                } else
                {
                    $error = array("message"=>$this->lang->line('login_error_user_inactive'),"code"=>0);
                }
            }
            else
            {
                $error = array("message"=>$this->lang->line('login_error_user_incorrect'),"code"=>0);
            }

        echo json_encode($error);
    }

    public function logout(){

        $token = $this->session->userdata('token');

        $token_admin_model = new Token_Admin_Model();

         $token_admin_model->update_token_close_session($token);

            $this->session->sess_destroy();
            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('token');
            $this->session->unset_userdata('user');
            $this->session->unset_userdata('username');
            redirect($this->config->item('base_cms'));


    }

}
