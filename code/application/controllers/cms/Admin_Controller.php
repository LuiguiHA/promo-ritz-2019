<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');
        $this->lang->load('cms_constans_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->model('admin_Model', 'admin_model');
        $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('token_Admin_Model', 'token_admin_model');

        if ( $this->session->userdata('logged_in') == false )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ($this->session->userdata("token") == "" || $this->session->userdata("token") == null)
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ( $this->session->userdata("super_admin") != SUPER_ADMIN )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
	}

	public function list_view($start = 0)
	{
        $this->session->mark_as_flash('pagination_list_admins');
        $this->session->set_flashdata('pagination_list_admins', $start);

        $admin_model = new Admin_Model();

        $admins = $admin_model->get_list_admins($start);

        $total_admins = $admins['total_admins'];
        $data_admins = $admins['data_admins'];


        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Administrador'), $total_admins);
        $this->arrayVista['admins'] = $data_admins;
        $this->arrayVista['total_admins'] = $total_admins;
        $this->arrayVista['view'] = 'cms/admin/list_view';
        $this->cargarVistaCMS();
	}

	public function list_search_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_list_search_admins');
        $this->session->set_flashdata('pagination_list_search_admins', $start);

        $search_admin_user = "";
        $search_admin_campaign = "";
        $search_admin_status = "";


        if (trim($this->input->post("search_admin_user", TRUE)) != null)
        {
            $search_admin_user = trim($this->input->post("search_admin_user", TRUE));
            $this->session->mark_as_flash('search_admin_user');
            $this->session->set_flashdata('search_admin_user', $search_admin_user);
        } else
        {
            if ($this->session->flashdata('search_admin_user') != null)
            {
                if ($this->input->post("search_admin") != "")
                {
                    $search_admin_user = trim($this->input->post("search_admin_user", TRUE));
                    $this->session->mark_as_flash('search_admin_user');
                    $this->session->set_flashdata('search_admin_user', $search_admin_user);
                } else
                {
                    $search_admin_user = $this->session->flashdata('search_admin_user');
                    $this->session->mark_as_flash('search_admin_user');
                    $this->session->set_flashdata('search_admin_user', $search_admin_user);
                }
            }
        }

        if ($this->input->post("search_admin_campaign", TRUE) != null)
        {
            $search_admin_campaign = $this->input->post("search_admin_campaign", TRUE);
            $this->session->mark_as_flash('search_admin_campaign');
            $this->session->set_flashdata('search_admin_campaign', $search_admin_campaign);
        } else
        {
            if ($this->session->flashdata('search_admin_campaign') != null)
            {
                if ($this->input->post("search_admin") != "")
                {
                    $search_admin_campaign = $this->input->post("search_admin_campaign", TRUE);
                    $this->session->mark_as_flash('search_admin_campaign');
                    $this->session->set_flashdata('search_admin_campaign', $search_admin_campaign);
                } else
                {
                    $search_admin_campaign = $this->session->flashdata('search_admin_campaign');
                    $this->session->mark_as_flash('search_admin_campaign');
                    $this->session->set_flashdata('search_admin_campaign', $search_admin_campaign);
                }
            }
        }

        if ($this->input->post("search_admin_status", TRUE) != null)
        {
            $search_admin_status = $this->input->post("search_admin_status", TRUE);
            $this->session->mark_as_flash('search_admin_status');
            $this->session->set_flashdata('search_admin_status', $search_admin_status);
        } else
        {
            if ($this->session->flashdata('search_admin_status') != null)
            {
                if ($this->input->post("search_admin") != "")
                {
                    $search_admin_status = $this->input->post("search_admin_status", TRUE);
                    $this->session->mark_as_flash('search_admin_status');
                    $this->session->set_flashdata('search_admin_status', $search_admin_status);
                } else
                {
                    $search_admin_status = $this->session->flashdata('search_admin_status');
                    $this->session->mark_as_flash('search_admin_status');
                    $this->session->set_flashdata('search_admin_status', $search_admin_status);
                }
            }
        }

        $admin_model = new Admin_Model();

        $admins = $admin_model->get_search_admins($start,$search_admin_user,$search_admin_campaign,$search_admin_status);

        if ($search_admin_user != "")
        {
            $this->arrayVista['search_admin_user'] = $search_admin_user;
        }

        if ($search_admin_campaign != "")
        {
            $this->arrayVista['search_admin_campaign'] = $search_admin_campaign;
        }

        if ($search_admin_status != "")
        {
            $this->arrayVista['search_admin_status'] = $search_admin_status;
        }


        $total_admins = $admins['total_admins'];
        $data_admins = $admins['data_admins'];


        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Administrador/buscar'), $total_admins);
        $this->arrayVista['admins'] = $data_admins;
        $this->arrayVista['total_admins'] = $total_admins;
        $this->arrayVista['view'] = 'cms/admin/list_view';
        $this->cargarVistaCMS();
    }

    public function export_list()
    {

        $search_admin_user = $this->input->get('search_admin_user');
        $search_admin_campaign = $this->input->get('search_admin_campaign');
        $search_admin_status = $this->input->get('search_admin_status');

        $admin_model = new Admin_Model();

        $admins = $admin_model->get_export_admins($search_admin_user,$search_admin_campaign,$search_admin_status);

            $data = array();

            if (isset($admins)){

                foreach ($admins as $admin) {

                    $campaign = "";
                    $status = "";
                    $share = "";
                    $super_admin = "";


                    if ($admin->super_admin == SUPER_ADMIN){
                        $campaign = $this->lang->line('constant_country_super_admin');
                    }else{
                        if ($admin->country_code == COUNTRY_PERU)
                        {
                            $campaign =  $this->lang->line('constant_country_peru');
                        } elseif ($admin->country_code == COUNTRY_ECUADOR)
                        {
                            $campaign =   $this->lang->line('constant_country_ecuador');
                        } elseif ($admin->country_code == COUNTRY_PUERTO_RICO)
                        {
                            $campaign =   $this->lang->line('constant_country_puerto_rico');
                        }elseif ($admin->country_code == COUNTRY_DOMINICAN_REPUBLIC)
                        {
                            $campaign =  $this->lang->line('constant_country_dominican_republic');
                        }elseif ($admin->country_code == COUNTRY_ADMIN)
                        {
                            $campaign =  $this->lang->line('constant_country_admin');
                        }
                    }


                    if ($admin->status == STATUS_ACTIVE){
                        $status = $this->lang->line('constant_status_active');
                    }else{
                        $status = $this->lang->line('constant_status_inactive');
                    }

                    if ($admin->super_admin == SUPER_ADMIN){
                        $super_admin = $this->lang->line('constant_yes');
                    }else{
                        $super_admin = $this->lang->line('constant_no');
                    }



                    array_push($data, array(
                        utf8_decode($this->lang->line('constant_list_name_admin')) => isset($admin->username)?utf8_decode($admin->username):"-",
                        utf8_decode($this->lang->line('constant_list_campaign_admin')) => isset($campaign)?utf8_decode($campaign):"-",
                        utf8_decode($this->lang->line('constant_list_super_admin')) => isset($super_admin)?utf8_decode($super_admin):"-",
                        utf8_decode($this->lang->line('constant_list_created_at_admin')) => isset($admin->created_at)?date("d/m/Y h:i:s a", strtotime($admin->created_at)):"-",
                        utf8_decode($this->lang->line('constant_list_status_admin')) => isset($status)?utf8_decode($status):"-",
                    ));
                }

                function filterData(&$str)
                {
                    $str = preg_replace("/\t/", "\\t", $str);
                    $str = preg_replace("/\r?\n/", "\\n", $str);
                    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
                }

                $fileName = $this->lang->line('constant_excel_admin'). ".xls";

                header("Content-Disposition: attachment; filename=\"$fileName\"");
                header("Content-Type: application/vnd.ms-excel, charset=utf-8");

                $flag = false;

                foreach($data as $row) {

                    if(!$flag) {
                        echo implode("\t", array_keys($row)) . "\n";
                        $flag = true;
                    }

                    array_walk($row, 'filterData');
                    echo implode("\t", array_values($row)) . "\n";
                }

                exit;
            }

           // redirect(site_url($this->config->item('base_cms').'/Usuario/buscar'));
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    
    public function add_view()
    {
        $campaign_model = new Campaign_Model();

        $campaigns = $campaign_model->get_list_campaign();

        $this->arrayVista['campaigns'] =  $campaigns;
        $this->arrayVista['view'] = 'cms/admin/add_view';
        $this->cargarVistaCMS();
    }
    
    public function add()
    {
        $user = trim($this->input->post("user",TRUE));
        $password = trim($this->input->post("password",TRUE));
        $campaign= $this->input->post("campaign",TRUE);
        $super_admin= $this->input->post("super_admin",TRUE);
        $status = $this->input->post("status",TRUE);
        
        $password = md5($password);
        
        $admin_model = new Admin_Model();

        $id_admin = $admin_model->add_admin($user,$password,$campaign,$super_admin,$status);

        if (isset($id_admin))
        {
            $error = array("message"=>$this->lang->line('admin_success_add'),"code"=>1);
        } else
        {
            $error = array("message"=>$this->lang->line('admin_error_add'),"code"=>0);
        }

        echo json_encode($error);
    }

    public function update_view($id_admin)
    {
        if (isset($id_admin))
        {
            $admin_model = new Admin_Model();

            $admin = $admin_model->get_data_admin($id_admin);

            $campaign_model = new Campaign_Model();

            $campaigns = $campaign_model->get_list_campaign();

            if (isset($admin))
            {
                $this->arrayVista['admin'] = $admin;
                $this->arrayVista['campaigns'] =   $campaigns;
                $this->arrayVista['view'] = 'cms/admin/update_view';
                $this->cargarVistaCMS();
            } else {
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        } else
        {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

    public function update($id_admin)
    {
        if (isset($id_admin))
        {

            $user = trim($this->input->post("user",TRUE));
            $password = trim($this->input->post("password",TRUE));
            $campaign = $this->input->post("campaign",TRUE);
            $super_admin = $this->input->post("super_admin",TRUE);
            $status = $this->input->post("status",TRUE);


            if ($password != "")
            {
            $password = md5($password);    
            }

            $admin_model = new Admin_Model();

            $admin = $admin_model->update_admin($id_admin,$user,$password,$campaign,$super_admin,$status);

            if (isset($admin))
            {
                $error = array("message"=>$this->lang->line('admin_success_update'),"code"=>1);
            } else 
            {
                $error = array("message"=>$this->lang->line('admin_error_update'),"code"=>0);
            }
           
            echo json_encode($error);
        } else
        {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

}
