<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meme_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');
        $this->lang->load('cms_constans_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->model('raffle_Model', 'raffle_model');
        $this->load->model('user_Model', 'user_model');
        $this->load->model('meme_Model', 'meme_model');
        $this->load->model('meme_Category_Model', 'meme_category_model');

        if ( $this->session->userdata('logged_in') == false )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ($this->session->userdata("token") == "" || $this->session->userdata("token") == null)
        {
            redirect($this->config->item('base_cms').'/logout');
        }
	}

	public function list_view($start = 0)
	{
        $this->session->mark_as_flash('pagination_memes');
        $this->session->set_flashdata('pagination_memes', $start);


        /*
        $query = array(
            'select' => 'meme.*,meme_category.title as category, COALESCE(users.name,\'\') as user, COALESCE(admin.username,\'\') as admin',
            'join'=> array(
                'meme_category, meme.category_id = meme_category.id'
            ),
            'left'=> array(
                'users, meme.user_id = users.id,left',
                'admin, meme.admin_id = admin.id,left'
            ),
            'order'=>  'id desc '
        );

        $total_memes = $this->meme_model->total_records($query);

        $data = $this->meme_model->search_data($query, $start,20);
        */

        $arrayMeme = $this->raffle_model->get_list_all_memes($start);
        $total_memes = $arrayMeme["users_total"];
        $data = $arrayMeme["data_users"];

        

        $this->arrayVista['pagination'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Meme'), $total_memes);
        $this->arrayVista['memes'] = $data;
        $this->arrayVista['total_memes'] = $total_memes;
        $this->arrayVista['view'] = 'cms/meme/list_view';

        /*echo "<pre>";
        print_r($this->arrayVista);
        exit();*/
        $this->cargarVistaCMS();
    }

    public function list_search_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_list_search_users');
        $this->session->set_flashdata('pagination_list_search_users', $start);

        $search_user_dni = "";
        $search_user_name = "";
        $search_user_fecha_ini = "";
        $search_user_fecha_fin = "";
        $search_user_status = "";



        if (trim($this->input->post("search_user_dni", TRUE)) != null)
        {
            $search_user_dni = trim($this->input->post("search_user_dni", TRUE));
            $this->session->mark_as_flash('search_user_dni');
            $this->session->set_flashdata('search_user_dni', $search_user_dni);
        } else
        {
            if ($this->session->flashdata('search_user_dni') != null)
            {
                if ($this->input->post("search_user") != "")
                {
                    $search_user_dni = trim($this->input->post("search_user_dni", TRUE));
                    $this->session->mark_as_flash('search_user_dni');
                    $this->session->set_flashdata('search_user_dni', $search_user_dni);
                } else
                {
                    $search_user_dni = $this->session->flashdata('search_user_dni');
                    $this->session->mark_as_flash('search_user_dni');
                    $this->session->set_flashdata('search_user_dni', $search_user_dni);
                }
            }
        }

        if (trim($this->input->post("search_user_name", TRUE)) != null)
        {
            $search_user_name = trim($this->input->post("search_user_name", TRUE));
            $this->session->mark_as_flash('search_user_name');
            $this->session->set_flashdata('search_user_name', $search_user_name);
        } else
        {
            if ($this->session->flashdata('search_user_name') != null)
            {
                if ($this->input->post("search_user") != "")
                {
                    $search_user_name = trim($this->input->post("search_user_name", TRUE));
                    $this->session->mark_as_flash('search_user_name');
                    $this->session->set_flashdata('search_user_name', $search_user_name);
                } else
                {
                    $search_user_name = $this->session->flashdata('search_user_name');
                    $this->session->mark_as_flash('search_user_name');
                    $this->session->set_flashdata('search_user_name', $search_user_name);
                }
            }
        }

        if (trim($this->input->post("search_user_fecha_ini", TRUE)) != null)
        {
            $search_user_fecha_ini = trim($this->input->post("search_user_fecha_ini", TRUE));
            $this->session->mark_as_flash('search_user_fecha_ini');
            $this->session->set_flashdata('search_user_fecha_ini', $search_user_fecha_ini);
        } else
        {
            if ($this->session->flashdata('search_user_fecha_ini') != null)
            {
                if ($this->input->post("search_user_fecha_ini") != "")
                {
                    $search_user_fecha_ini = trim($this->input->post("search_user_fecha_ini", TRUE));
                    $this->session->mark_as_flash('search_user_fecha_ini');
                    $this->session->set_flashdata('search_user_fecha_ini', $search_user_fecha_ini);
                } else
                {
                    $search_user_fecha_ini = $this->session->flashdata('search_user_fecha_ini');
                    $this->session->mark_as_flash('search_user_fecha_ini');
                    $this->session->set_flashdata('search_user_fecha_ini', $search_user_fecha_ini);
                }
            }
        }

        if (trim($this->input->post("search_user_fecha_fin", TRUE)) != null)
        {
            $search_user_fecha_fin = trim($this->input->post("search_user_fecha_fin", TRUE));
            $this->session->mark_as_flash('search_user_fecha_fin');
            $this->session->set_flashdata('search_user_fecha_fin', $search_user_fecha_ini);
        } else
        {
            if ($this->session->flashdata('search_user_fecha_fin') != null)
            {
                if ($this->input->post("search_user_fecha_fin") != "")
                {
                    $search_user_fecha_fin = trim($this->input->post("search_user_fecha_fin", TRUE));
                    $this->session->mark_as_flash('search_user_fecha_fin');
                    $this->session->set_flashdata('search_user_fecha_fin', $search_user_fecha_ini);
                } else
                {
                    $search_user_fecha_fin = $this->session->flashdata('search_user_fecha_fin');
                    $this->session->mark_as_flash('search_user_fecha_fin');
                    $this->session->set_flashdata('search_user_fecha_fin', $search_user_fecha_ini);
                }
            }
        }

        

        if ($this->input->post("search_user_status", TRUE) != null)
        {
            $search_user_status = $this->input->post("search_user_status", TRUE);
            $this->session->mark_as_flash('search_user_status');
            $this->session->set_flashdata('search_user_status', $search_user_status);
        } else
        {
            if ($this->session->flashdata('search_user_status') != null)
            {
                if ($this->input->post("search_user") != "")
                {
                    $search_user_status = $this->input->post("search_user_status", TRUE);
                    $this->session->mark_as_flash('search_user_status');
                    $this->session->set_flashdata('search_user_status', $search_user_status);
                } else
                {
                    $search_user_status = $this->session->flashdata('search_user_status');
                    $this->session->mark_as_flash('search_user_status');
                    $this->session->set_flashdata('search_user_status', $search_user_status);
                }
            }
        }

            $super_admin = $this->session->userdata("super_admin");

            /*
            $user_model = new User_Model();
            $users = $user_model->get_search_users($start,$search_user_dni,$search_user_name,$search_user_fecha_ini,$search_user_fecha_fin,$search_user_status);
            */

            $arrayMeme = $this->raffle_model->get_list_search_memes($start,PAGINATION_LIST,$search_user_dni,$search_user_name,$search_user_fecha_ini,$search_user_fecha_fin,$search_user_status);
            $total_memes = $arrayMeme["users_total"];
            $data = $arrayMeme["data_users"];

            if ($search_user_dni != "")
            {
                $this->arrayVista['search_user_dni'] = $search_user_dni;
            }

            if ($search_user_name != "")
            {
                $this->arrayVista['search_user_name'] = $search_user_name;
            }

            if ($search_user_fecha_ini != "")
            {
                $this->arrayVista['search_user_fecha_ini'] = $search_user_fecha_ini;
            }

            if ($search_user_fecha_fin != "")
            {
                $this->arrayVista['search_user_fecha_fin'] = $search_user_fecha_fin;
            }

            if ($search_user_status != "")
            {
                $this->arrayVista['search_user_status'] = $search_user_status;
            }


            /*
            $total_users = $users['total_users'];
            $data_users = $users['data_users'];


            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Usuario/buscar'), $total_users);
            $this->arrayVista['users'] = $data_users;
            $this->arrayVista['total_users'] = $total_users;
            $this->arrayVista['view'] = 'cms/user/list_view';
            */

            $this->arrayVista['pagination'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Meme/buscar'), $total_memes);
            $this->arrayVista['memes'] = $data;
            $this->arrayVista['total_memes'] = $total_memes;
            $this->arrayVista['view'] = 'cms/meme/list_view';

            $this->cargarVistaCMS();
    }


    public function export_list()
    {

        $search_user_dni = $this->input->get('search_user_dni');
        $search_user_name = $this->input->get('search_user_name');
        $search_user_fecha_ini = $this->input->get('search_user_fecha_ini');
        $search_user_fecha_fin = $this->input->get('search_user_fecha_fin');
        $search_user_status = $this->input->get('search_user_status');


        $total_memes = $this->raffle_model->get_total_search_memes($start,$search_user_dni,$search_user_name,$search_user_fecha_ini,$search_user_fecha_fin,$search_user_status);
        
        $fileName = $this->lang->line('constant_excel_user'). ".xls";

        header("Content-Disposition: attachment; filename=\"$fileName\"");
        header("Content-Type: application/vnd.ms-excel, charset=utf-8");
        


            

            $total_por_pagina = 30;
            $total_pagina = ceil($total_memes / $total_por_pagina);
            
            $flag = false;

            //echo $total_pagina;

            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }
            

            for($i=0;$i<$total_pagina;$i++)
            {
                $data = array();

                $start = $i*$total_por_pagina;
                $arrayMeme = $this->raffle_model->get_list_search_memes($start,$total_por_pagina,$search_user_dni,$search_user_name,$search_user_fecha_ini,$search_user_fecha_fin,$search_user_status);
                // $total_memes = $arrayMeme["users_total"];
                $users = $arrayMeme["data_users"];

                //echo "<pre>";
                //print_r($arrayMeme);
                

                foreach ($users as $user) {

                
                    $status = "";
                    
    
                    if ($user->status == STATUS_ACTIVE){
                        $status = $this->lang->line('constant_status_active');
                    }else{
                        $status = $this->lang->line('constant_status_inactive');
                    }
    
                    
    
                    array_push($data, array(

                        utf8_decode($this->lang->line('constant_list_name_user')) => isset($user->user_name)?utf8_decode($user->user_name):"-",
                        utf8_decode($this->lang->line('constant_list_dni_user')) => isset($user->user_dni)?utf8_decode($user->user_dni):"-",
                        utf8_decode($this->lang->line('constant_list_email_user')) => isset($user->user_email)?utf8_decode($user->user_email):"-",
                        utf8_decode($this->lang->line('constant_list_phone_user')) => isset($user->user_phone)?utf8_decode($user->user_phone):"-",
                        utf8_decode("Lote") => isset($user->lote)?utf8_decode($user->lote):"-",
                        utf8_decode($this->lang->line('constant_list_created_at_user')) => isset($user->created_at)?date("d/m/Y h:i:s a", strtotime($user->created_at)):"-",
                        //utf8_decode($this->lang->line('constant_list_status_user')) => isset($status)?utf8_decode($status):"-",
                    ));
                }
    
                
    
                
    
    
                foreach($data as $row) {
    
                    if(!$flag) {
                        echo implode("\t", array_keys($row)) . "\n";
                        $flag = true;
                    }
    
                    array_walk($row, 'filterData');
                    echo implode("\t", array_values($row)) . "\n";
                }
            }

            //exit();

            exit;
       // }

           // redirect(site_url($this->config->item('base_cms').'/Usuario/buscar'));
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    
    public function update_view($id)
    {
        if (isset($id))
        {
            //$admin_model = new Admin_Model();

            $query = array(
                'select' => 'meme.*,meme_category.title as category, COALESCE(users.name,\'\') as user, COALESCE(admin.username,\'\') as admin',
                'join'=> array(
                    'meme_category, meme.category_id = meme_category.id'
                ),
                'left'=> array(
                    'users, meme.user_id = users.id,left',
                    'admin, meme.admin_id = admin.id,left'
                ),
                'where' => 'meme.id = '.$id.''
            );
            
            $meme = $this->meme_model->get_search_row($query);
            
            $query_category = array(
                'select'=>'*',
                'where'=>'status = 1'
            );

            $categories = $this->meme_category_model->search($query_category);

            if (isset($meme))
            {
                $this->arrayVista['meme'] = $meme;
                $this->arrayVista['categories'] =   $categories;
                $this->arrayVista['view'] = 'cms/meme/update_view';
                $this->cargarVistaCMS();
            } else {
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        } else
        {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

    public function update($id_admin)
    {
        if (isset($id_admin))
        {

            $user = trim($this->input->post("user",TRUE));
            $password = trim($this->input->post("password",TRUE));
            $campaign = $this->input->post("campaign",TRUE);
            $super_admin = $this->input->post("super_admin",TRUE);
            $status = $this->input->post("status",TRUE);


            if ($password != "")
            {
            $password = md5($password);    
            }

            $admin_model = new Admin_Model();

            $admin = $admin_model->update_admin($id_admin,$user,$password,$campaign,$super_admin,$status);

            if (isset($admin))
            {
                $error = array("message"=>$this->lang->line('admin_success_update'),"code"=>1);
            } else 
            {
                $error = array("message"=>$this->lang->line('admin_error_update'),"code"=>0);
            }
           
            echo json_encode($error);
        } else
        {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

	

}
