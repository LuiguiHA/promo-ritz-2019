<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');
        $this->lang->load('cms_constans_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->model('user_Model', 'user_model');
        $this->load->model('winner_Model', 'winner_model');
        $this->load->model('prize_Model', 'prize_model');
        $this->load->model('token_Admin_Model', 'token_admin_model');

        if ( $this->session->userdata('logged_in') == false )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ($this->session->userdata("token") == "" || $this->session->userdata("token") == null)
        {
            redirect($this->config->item('base_cms').'/logout');
        }
	}

	public function list_view($start = 0)
	{
        $this->session->mark_as_flash('pagination_list_users');
        $this->session->set_flashdata('pagination_list_users', $start);

        $user_model = new User_Model();

        $campaign = $this->session->userdata("campaign_code");
        $super_admin = $this->session->userdata("super_admin");

            $users = $user_model->get_list_users($start,$campaign,$super_admin);

            $total_users = $users['total_users'];
            $data_users = $users['data_users'];
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Usuario'), $total_users);
            $this->arrayVista['users'] = $data_users;
            $this->arrayVista['total_users'] = $total_users;

        $this->arrayVista['view'] = 'cms/user/list_view';
        $this->cargarVistaCMS();
	}

    public function edit_view($userId=0){

        //var_dump($this->input->post());
        if ($this->input->post()) {
            $arrayDatos = array(
                "name"=>$this->input->post("name",TRUE),
                "dni"=>$this->input->post("dni",TRUE),
                "email"=>$this->input->post("email",TRUE),
                "points"=>$this->input->post("points",TRUE),
                "phone"=>$this->input->post("phone",TRUE),
                "points"=>$this->input->post("points",TRUE)
                );

            $this->user_model->update($userId, $arrayDatos);

            //redirect(site_url($this->config->item('base_cms').'/Usuario/buscar'));
            $this->session->set_flashdata('mensaje', 'Se actualizo correctamente la información del usuario.');
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        else
        {
            if(is_numeric($userId)){

                $arrayQuerySearchUser = array(
                "select"=>"*",
                "where"=>"id='".$userId."'"
                );

                $user = $this->user_model->get_search_row($arrayQuerySearchUser);

								$userAdmin= $this->session->userdata('user');

                $this->arrayVista['user'] = $user;
                $this->arrayVista['userAdmin'] = $userAdmin;
                $this->arrayVista['view'] = 'cms/user/edit_view';
                $this->cargarVistaCMS();
            }

        }
    }

	public function list_search_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_list_search_users');
        $this->session->set_flashdata('pagination_list_search_users', $start);

        $search_user_dni = "";
        $search_user_name = "";
        $search_user_fecha_ini = "";
        $search_user_fecha_fin = "";
        $search_user_status = "";



        if (trim($this->input->post("search_user_dni", TRUE)) != null)
        {
            $search_user_dni = trim($this->input->post("search_user_dni", TRUE));
            $this->session->mark_as_flash('search_user_dni');
            $this->session->set_flashdata('search_user_dni', $search_user_dni);
        } else
        {
            if ($this->session->flashdata('search_user_dni') != null)
            {
                if ($this->input->post("search_user") != "")
                {
                    $search_user_dni = trim($this->input->post("search_user_dni", TRUE));
                    $this->session->mark_as_flash('search_user_dni');
                    $this->session->set_flashdata('search_user_dni', $search_user_dni);
                } else
                {
                    $search_user_dni = $this->session->flashdata('search_user_dni');
                    $this->session->mark_as_flash('search_user_dni');
                    $this->session->set_flashdata('search_user_dni', $search_user_dni);
                }
            }
        }

        if (trim($this->input->post("search_user_name", TRUE)) != null)
        {
            $search_user_name = trim($this->input->post("search_user_name", TRUE));
            $this->session->mark_as_flash('search_user_name');
            $this->session->set_flashdata('search_user_name', $search_user_name);
        } else
        {
            if ($this->session->flashdata('search_user_name') != null)
            {
                if ($this->input->post("search_user") != "")
                {
                    $search_user_name = trim($this->input->post("search_user_name", TRUE));
                    $this->session->mark_as_flash('search_user_name');
                    $this->session->set_flashdata('search_user_name', $search_user_name);
                } else
                {
                    $search_user_name = $this->session->flashdata('search_user_name');
                    $this->session->mark_as_flash('search_user_name');
                    $this->session->set_flashdata('search_user_name', $search_user_name);
                }
            }
        }

        if (trim($this->input->post("search_user_fecha_ini", TRUE)) != null)
        {
            $search_user_fecha_ini = trim($this->input->post("search_user_fecha_ini", TRUE));
            $this->session->mark_as_flash('search_user_fecha_ini');
            $this->session->set_flashdata('search_user_fecha_ini', $search_user_fecha_ini);
        } else
        {
            if ($this->session->flashdata('search_user_fecha_ini') != null)
            {
                if ($this->input->post("search_user_fecha_ini") != "")
                {
                    $search_user_fecha_ini = trim($this->input->post("search_user_fecha_ini", TRUE));
                    $this->session->mark_as_flash('search_user_fecha_ini');
                    $this->session->set_flashdata('search_user_fecha_ini', $search_user_fecha_ini);
                } else
                {
                    $search_user_fecha_ini = $this->session->flashdata('search_user_fecha_ini');
                    $this->session->mark_as_flash('search_user_fecha_ini');
                    $this->session->set_flashdata('search_user_fecha_ini', $search_user_fecha_ini);
                }
            }
        }

        if (trim($this->input->post("search_user_fecha_fin", TRUE)) != null)
        {
            $search_user_fecha_fin = trim($this->input->post("search_user_fecha_fin", TRUE));
            $this->session->mark_as_flash('search_user_fecha_fin');
            $this->session->set_flashdata('search_user_fecha_fin', $search_user_fecha_ini);
        } else
        {
            if ($this->session->flashdata('search_user_fecha_fin') != null)
            {
                if ($this->input->post("search_user_fecha_fin") != "")
                {
                    $search_user_fecha_fin = trim($this->input->post("search_user_fecha_fin", TRUE));
                    $this->session->mark_as_flash('search_user_fecha_fin');
                    $this->session->set_flashdata('search_user_fecha_fin', $search_user_fecha_ini);
                } else
                {
                    $search_user_fecha_fin = $this->session->flashdata('search_user_fecha_fin');
                    $this->session->mark_as_flash('search_user_fecha_fin');
                    $this->session->set_flashdata('search_user_fecha_fin', $search_user_fecha_ini);
                }
            }
        }

        

        if ($this->input->post("search_user_status", TRUE) != null)
        {
            $search_user_status = $this->input->post("search_user_status", TRUE);
            $this->session->mark_as_flash('search_user_status');
            $this->session->set_flashdata('search_user_status', $search_user_status);
        } else
        {
            if ($this->session->flashdata('search_user_status') != null)
            {
                if ($this->input->post("search_user") != "")
                {
                    $search_user_status = $this->input->post("search_user_status", TRUE);
                    $this->session->mark_as_flash('search_user_status');
                    $this->session->set_flashdata('search_user_status', $search_user_status);
                } else
                {
                    $search_user_status = $this->session->flashdata('search_user_status');
                    $this->session->mark_as_flash('search_user_status');
                    $this->session->set_flashdata('search_user_status', $search_user_status);
                }
            }
        }

            $super_admin = $this->session->userdata("super_admin");

            $user_model = new User_Model();

            

            $users = $user_model->get_search_users($start,$search_user_dni,$search_user_name,$search_user_fecha_ini,$search_user_fecha_fin,$search_user_status);

            if ($search_user_dni != "")
            {
                $this->arrayVista['search_user_dni'] = $search_user_dni;
            }

            if ($search_user_name != "")
            {
                $this->arrayVista['search_user_name'] = $search_user_name;
            }

            if ($search_user_fecha_ini != "")
            {
                $this->arrayVista['search_user_fecha_ini'] = $search_user_fecha_ini;
            }

            if ($search_user_fecha_fin != "")
            {
                $this->arrayVista['search_user_fecha_fin'] = $search_user_fecha_fin;
            }

            if ($search_user_status != "")
            {
                $this->arrayVista['search_user_status'] = $search_user_status;
            }

            $total_users = $users['total_users'];
            $data_users = $users['data_users'];


            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Usuario/buscar'), $total_users);
            $this->arrayVista['users'] = $data_users;
            $this->arrayVista['total_users'] = $total_users;
            $this->arrayVista['view'] = 'cms/user/list_view';
            $this->cargarVistaCMS();
    }

    public function list_winners_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_list_users_winners');
        $this->session->set_flashdata('pagination_list_users_winners', $start);

        $winner_model = new Winner_Model();

            $campaign = $this->session->userdata("campaign_code");
            $super_admin = $this->session->userdata("super_admin");



            $users = $winner_model->get_list_users_winners($start,$campaign,$super_admin);

            $prize_model = new Prize_Model();

            $prizes = $prize_model->get_list_prizes_by_type();

            $total_users_winners = $users['total_users_winners'];
            $data_users_winners = $users['data_users_winners'];

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Usuario/lista_ganadores'), $total_users_winners);
            $this->arrayVista['users'] = $data_users_winners;
            $this->arrayVista['prizes'] = $prizes;
            $this->arrayVista['campaign'] = $campaign;
            $this->arrayVista['total_users'] = $total_users_winners;

        $this->arrayVista['view'] = 'cms/user/list_winners_view';
        $this->cargarVistaCMS();
    }

    public function list_search_winners_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_search_users_winners');
        $this->session->set_flashdata('pagination_search_users_winners', $start);

        $search_user_winner_dni = "";
        $search_user_winner_name = "";
        $search_user_winner_prize = "";
        $search_user_winner_country = "";
        $search_user_winner_campaign = "";
        $search_user_winner_status = "";


            if (trim($this->input->post("search_user_winner_dni", TRUE)) != null)
            {
                $search_user_winner_dni = trim($this->input->post("search_user_winner_dni", TRUE));
                $this->session->mark_as_flash('search_user_winner_dni');
                $this->session->set_flashdata('search_user_winner_dni', $search_user_winner_dni);
            } else
            {
                if ($this->session->flashdata('search_user_winner_dni') != null)
                {
                    if ($this->input->post("search_user_winner") != "")
                    {
                        $search_user_winner_dni = trim($this->input->post("search_user_winner_dni", TRUE));
                        $this->session->mark_as_flash('search_user_winner_dni');
                        $this->session->set_flashdata('search_user_winner_dni', $search_user_winner_dni);
                    } else
                    {
                        $search_user_winner_dni = $this->session->flashdata('search_user_winner_dni');
                        $this->session->mark_as_flash('search_user_winner_dni');
                        $this->session->set_flashdata('search_user_winner_dni', $search_user_winner_dni);
                    }
                }
            }

            if (trim($this->input->post("search_user_winner_name", TRUE)) != null)
            {
                $search_user_winner_name = trim($this->input->post("search_user_winner_name", TRUE));
                $this->session->mark_as_flash('search_user_winner_name');
                $this->session->set_flashdata('search_user_winner_name', $search_user_winner_name);
            } else
            {
                if ($this->session->flashdata('search_user_winner_name') != null)
                {
                    if ($this->input->post("search_user_winner") != "")
                    {
                        $search_user_winner_name = trim($this->input->post("search_user_winner_name", TRUE));
                        $this->session->mark_as_flash('search_user_winner_name');
                        $this->session->set_flashdata('search_user_winner_name', $search_user_winner_name);
                    } else
                    {
                        $search_user_winner_name = $this->session->flashdata('search_user_winner_name');
                        $this->session->mark_as_flash('search_user_winner_name');
                        $this->session->set_flashdata('search_user_winner_name', $search_user_winner_name);
                    }
                }
            }

            if (trim($this->input->post("search_user_winner_prize", TRUE)) != null)
            {
                $search_user_winner_prize = $this->input->post("search_user_winner_prize", TRUE);
                $this->session->mark_as_flash('search_user_winner_prize');
                $this->session->set_flashdata('search_user_winner_prize', $search_user_winner_prize);
            } else
            {
                if ($this->session->flashdata('search_user_winner_prize') != null)
                {
                    if ($this->input->post("search_user_winner") != "")
                    {
                        $search_user_winner_prize = trim($this->input->post("search_user_winner_prize", TRUE));
                        $this->session->mark_as_flash('search_user_winner_prize');
                        $this->session->set_flashdata('search_user_winner_prize', $search_user_winner_prize);
                    } else
                    {
                        $search_user_winner_prize = $this->session->flashdata('search_user_winner_prize');
                        $this->session->mark_as_flash('search_user_winner_prize');
                        $this->session->set_flashdata('search_user_winner_prize', $search_user_winner_prize);
                    }
                }
            }

        if ($this->input->post("search_user_winner_country", TRUE) != null)
        {
            $search_user_winner_country = $this->input->post("search_user_winner_country", TRUE);
            $this->session->mark_as_flash('search_user_winner_country');
            $this->session->set_flashdata('search_user_winner_country', $search_user_winner_country);
        } else
        {
            if ($this->session->flashdata('search_user_winner_country') != null)
            {
                if ($this->input->post("search_user_winner") != "")
                {
                    $search_user_winner_country = $this->input->post("search_user_winner_country", TRUE);
                    $this->session->mark_as_flash('search_user_winner_country');
                    $this->session->set_flashdata('search_user_winner_country', $search_user_winner_country);
                } else
                {
                    $search_user_winner_country = $this->session->flashdata('search_user_winner_country');
                    $this->session->mark_as_flash('search_user_winner_country');
                    $this->session->set_flashdata('search_user_winner_country', $search_user_winner_country);
                }
            }
        }

            if ( $this->session->userdata('super_admin')== SUPER_ADMIN)
            {
                if ($this->input->post("search_user_winner_campaign", TRUE) != null)
                {
                    $search_user_winner_campaign = $this->input->post("search_user_winner_campaign", TRUE);
                    $this->session->mark_as_flash('search_user_winner_campaign');
                    $this->session->set_flashdata('search_user_winner_campaign', $search_user_winner_campaign);
                } else
                {
                    if ($this->session->flashdata('search_user_winner_campaign') != null)
                    {
                        if ($this->input->post("search_user_winner") != "")
                        {
                            $search_user_winner_campaign = $this->input->post("search_user_winner_campaign", TRUE);
                            $this->session->mark_as_flash('search_user_winner_campaign');
                            $this->session->set_flashdata('search_user_winner_campaign', $search_user_winner_campaign);
                        } else
                        {
                            $search_user_winner_campaign = $this->session->flashdata('search_user_winner_campaign');
                            $this->session->mark_as_flash('search_user_winner_campaign');
                            $this->session->set_flashdata('search_user_winner_campaign', $search_user_winner_campaign);
                        }
                    }
                }
            } else
            {
                $search_user_winner_campaign = $this->session->userdata("campaign_code");
            }

            if ($this->input->post("search_user_winner_status", TRUE) != null)
            {
                $search_user_winner_status = $this->input->post("search_user_winner_status", TRUE);
                $this->session->mark_as_flash('search_user_winner_status');
                $this->session->set_flashdata('search_user_winner_status', $search_user_winner_status);
            } else
            {
                if ($this->session->flashdata('search_user_winner_status') != null)
                {
                    if ($this->input->post("search_user_winner") != "")
                    {
                        $search_user_winner_status = $this->input->post("search_user_winner_status", TRUE);
                        $this->session->mark_as_flash('search_user_winner_status');
                        $this->session->set_flashdata('search_user_winner_status', $search_user_winner_status);
                    } else
                    {
                        $search_user_winner_status = $this->session->flashdata('search_user_winner_status');
                        $this->session->mark_as_flash('search_user_winner_status');
                        $this->session->set_flashdata('search_user_winner_status', $search_user_winner_status);
                    }
                }
            }

            $winner_model = new Winner_Model();

            $users = $winner_model->get_search_users_winners($start,$search_user_winner_dni,$search_user_winner_name,$search_user_winner_prize,$search_user_winner_country,$search_user_winner_campaign,$search_user_winner_status);

            $prize_model = new Prize_Model();

            $prizes = $prize_model->get_list_prizes_by_type();

            if ($search_user_winner_dni != "")
            {
                $this->arrayVista['search_user_winner_dni'] = $search_user_winner_dni;
            }

            if ($search_user_winner_name != "")
            {
                $this->arrayVista['search_user_winner_name'] = $search_user_winner_name;
            }

            if ($search_user_winner_prize != "")
            {
                $this->arrayVista['search_user_winner_prize'] = $search_user_winner_prize;
            }

            if ($search_user_winner_country != "")
            {
                $this->arrayVista['search_user_winner_country'] = $search_user_winner_country;
            }

            if ($search_user_winner_campaign != "")
            {
                $this->arrayVista['search_user_winner_campaign'] = $search_user_winner_campaign;
            }

            if ($search_user_winner_status != "")
            {
                $this->arrayVista['search_user_winner_status'] = $search_user_winner_status;
            }

            $total_users = $users['total_users'];
            $data_users = $users['data_users'];

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Usuario/lista_ganadores/buscar'), $total_users);
            $this->arrayVista['users'] = $data_users;
            $this->arrayVista['prizes'] = $prizes;
            $this->arrayVista['total_users'] = $total_users;


        $this->arrayVista['view'] = 'cms/user/list_winners_view';
        $this->cargarVistaCMS();
    }

    public function export_list()
    {

        $search_user_dni = $this->input->get('search_user_dni');
        $search_user_name = $this->input->get('search_user_name');
        $search_user_fecha_ini = $this->input->get('search_user_fecha_ini');
        $search_user_fecha_fin = $this->input->get('search_user_fecha_fin');
        $search_user_status = $this->input->get('search_user_status');



        if ($this->session->userdata("super_admin")!= SUPER_ADMIN)
        {
            $search_user_campaign =  $this->session->userdata("campaign_code");
        }

        $user_model = new User_Model();

        $users = $user_model->get_export_users($search_user_dni,$search_user_name,$search_user_fecha_ini,$search_user_fecha_fin,$search_user_status);

            $data = array();

            if (isset($users)){

                foreach ($users as $user) {

                    
                    $status = "";
                    

                    if ($user->status == STATUS_ACTIVE){
                        $status = $this->lang->line('constant_status_active');
                    }else{
                        $status = $this->lang->line('constant_status_inactive');
                    }

                    

                    array_push($data, array(
                        utf8_decode($this->lang->line('constant_list_dni_user')) => isset($user->dni)?utf8_decode($user->dni):"-",
                        utf8_decode($this->lang->line('constant_list_name_user')) => isset($user->name)?utf8_decode($user->name):"-",
                        utf8_decode($this->lang->line('constant_list_email_user')) => isset($user->email)?utf8_decode($user->email):"-",
                        utf8_decode($this->lang->line('constant_list_phone_user')) => isset($user->phone)?utf8_decode($user->phone):"-",
                        utf8_decode($this->lang->line('constant_list_points_user')) => isset($user->points)?utf8_decode($user->points):"-",
                        utf8_decode($this->lang->line('constant_list_created_at_user')) => isset($user->created_at)?date("d/m/Y h:i:s a", strtotime($user->created_at)):"-",
                        utf8_decode($this->lang->line('constant_list_status_user')) => isset($status)?utf8_decode($status):"-",
                    ));
                }

                function filterData(&$str)
                {
                    $str = preg_replace("/\t/", "\\t", $str);
                    $str = preg_replace("/\r?\n/", "\\n", $str);
                    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
                }

                $fileName = $this->lang->line('constant_excel_user'). ".xls";

                header("Content-Disposition: attachment; filename=\"$fileName\"");
                header("Content-Type: application/vnd.ms-excel, charset=utf-8");

                $flag = false;

                foreach($data as $row) {

                    if(!$flag) {
                        echo implode("\t", array_keys($row)) . "\n";
                        $flag = true;
                    }

                    array_walk($row, 'filterData');
                    echo implode("\t", array_values($row)) . "\n";
                }

                exit;
            }

           // redirect(site_url($this->config->item('base_cms').'/Usuario/buscar'));
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function export_list_winners()
    {

        $search_user_winner_dni = $this->input->get('search_user_winner_dni');
        $search_user_winner_name = $this->input->get('search_user_winner_name');
        $search_user_winner_prize = $this->input->get('search_user_winner_prize');
        $search_user_winner_country = $this->input->get('search_user_winner_country');
        $search_user_winner_campaign = $this->input->get('search_user_winner_campaign');
        $search_user_winner_status = $this->input->get('search_user_winner_status');

        if ($this->session->userdata("super_admin")!= SUPER_ADMIN)
        {
            $search_user_winner_campaign =  $this->session->userdata("campaign_code");
        }


        $winner_model = new Winner_Model();

        $users = $winner_model->get_export_users_winners($search_user_winner_dni,$search_user_winner_name,$search_user_winner_prize,$search_user_winner_country,$search_user_winner_campaign,$search_user_winner_status);

        $data = array();

        if (isset($users)){

            foreach ($users as $user) {

                $campaign = "";
                $country = "";
                $status = "";
                $share = "";

                if ($user->country_code == COUNTRY_PERU)
                {
                    $campaign =  $this->lang->line('constant_country_peru');
                } elseif ($user->country_code == COUNTRY_ECUADOR)
                {
                    $campaign =   $this->lang->line('constant_country_ecuador');
                } elseif ($user->country_code == COUNTRY_PUERTO_RICO)
                {
                    $campaign =   $this->lang->line('constant_country_puerto_rico');
                }elseif ($user->country_code == COUNTRY_DOMINICAN_REPUBLIC)
                {
                    $campaign =  $this->lang->line('constant_country_dominican_republic');
                }

                if ($user->country == COUNTRY_PERU)
                {
                    $country =  $this->lang->line('constant_country_peru');
                } elseif ($user->country == COUNTRY_ECUADOR)
                {
                    $country =   $this->lang->line('constant_country_ecuador');
                } elseif ($user->country == COUNTRY_PUERTO_RICO)
                {
                    $country =   $this->lang->line('constant_country_puerto_rico');
                }elseif ($user->country == COUNTRY_DOMINICAN_REPUBLIC)
                {
                    $country =  $this->lang->line('constant_country_dominican_republic');
                } else
                {
                    $country = $user->country ;
                }

                if ($user->status == STATUS_ACTIVE){
                    $status = $this->lang->line('constant_status_active');
                }else{
                    $status = $this->lang->line('constant_status_inactive');
                }

                if ($user->share == SHARE_FACEBOOK){
                    $share = $this->lang->line('constant_yes');
                }else{
                    $share = $this->lang->line('constant_no');
                }

                array_push($data, array(
                    utf8_decode($this->lang->line('constant_list_dni_user')) => isset($user->dni)?utf8_decode($user->dni):"-",
                    utf8_decode($this->lang->line('constant_list_name_user')) => isset($user->user_name)?utf8_decode($user->user_name):"-",
                    utf8_decode($this->lang->line('constant_list_country_user')) => isset($country)?utf8_decode($country):"-",
                    utf8_decode($this->lang->line('constant_list_campaign_user')) => isset($campaign)?utf8_decode($campaign):"-",
                    utf8_decode($this->lang->line('constant_list_email_user')) => isset($user->email)?utf8_decode($user->email):"-",
                    utf8_decode($this->lang->line('constant_list_phone_user')) => isset($user->phone)?utf8_decode($user->phone):"-",
                    utf8_decode($this->lang->line('constant_list_points_user')) => isset($user->points)?utf8_decode($user->points):"-",
                    utf8_decode($this->lang->line('constant_list_share_user')) => isset($share)?utf8_decode($share):"-",
                    utf8_decode($this->lang->line('constant_list_prize_user')) => isset($user->prize_name)?utf8_decode($user->prize_name):"-",
                    utf8_decode($this->lang->line('constant_list_winner_at_user')) => isset($user->created_at)?date("d/m/Y h:i:s a", strtotime($user->created_at)):"-",
                    utf8_decode($this->lang->line('constant_list_status_user')) => isset($status)?utf8_decode($status):"-",
                ));
            }

            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }

            $fileName = $this->lang->line('constant_excel_user_winner'). ".xls";

            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");

            $flag = false;

            foreach($data as $row) {

                if(!$flag) {
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }

                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }

            exit;
        }

        // redirect(site_url($this->config->item('base_cms').'/Usuario/buscar'));
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}
