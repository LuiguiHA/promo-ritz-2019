<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subasta_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');
        $this->lang->load('cms_constans_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->model('admin_Model', 'admin_model');
       // $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('Prizes_Sale_Model', 'prize_sale_model');

        $this->load->model('User_Prizes_Sale_Model', 'user_prizes_sale_model');

        $this->load->model('token_Admin_Model', 'token_admin_model');
        $this->load->model('Winner_Model', 'winner_Model');
        $this->load->model('User_Model', 'user_Model');

        if ( $this->session->userdata('logged_in') == false )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ($this->session->userdata("token") == "" || $this->session->userdata("token") == null)
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ( $this->session->userdata("super_admin") != SUPER_ADMIN )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
	}

	public function list_view($start = 0)
	{



        $arrayQueryPremiosEnSubasta = array(
            "select"=>"*",
            "order"=>"start desc, status desc"
            );

        $arrayPremiosEnSubasta = $this->prize_sale_model->search($arrayQueryPremiosEnSubasta);
       // $admins = $this->prizes_sale_model->get_list_admins($start);


        $this->arrayVista['arrayPremiosEnSubasta'] = $arrayPremiosEnSubasta;
        $this->arrayVista['view'] = 'cms/subasta/list_view';
        $this->cargarVistaCMS();
	}

    public function list_users_view($idPrizeSale = 0)
    {



        $arrayQueryPremiosEnSubastaUsuario = array(
            "select"=>"prizes_sale.id as prize_sale_id, prizes_sale.description, users.id as user_id, users.name, users.dni, users.email, users.phone, MAX(user_prizes_sale.points) as points",
            "join"=>array(
                "prizes_sale, prizes_sale.id = user_prizes_sale.prizes_sales_id",
                "users, users.id = user_prizes_sale.user_id"
                ),
            "where"=>"prizes_sale.id = '".$idPrizeSale."'",
            "group"=>"prizes_sale.id, prizes_sale.description, users.id, users.dni, users.email, users.phone, users.name",
            "order"=>"MAX(user_prizes_sale.points) DESC",
            "limit"=>"20"
        );

        $arrayQueryGanador = array(
            "select"=>"count(*) as total_ganadores",
           // "group"=>"winner",
            "where"=>"prizes_sales_id = '".$idPrizeSale."' and winner = '1'"
        );

        $usuarioGanador = $this->user_prizes_sale_model->get_search_row($arrayQueryGanador);

        $arrayUsuariosEnSubasta = $this->user_prizes_sale_model->search($arrayQueryPremiosEnSubastaUsuario);


       // $admins = $this->prizes_sale_model->get_list_admins($start);


        $this->arrayVista['usuarioGanador'] = $usuarioGanador;
        $this->arrayVista['arrayUsuariosEnSubasta'] = $arrayUsuariosEnSubasta;
        $this->arrayVista['view'] = 'cms/subasta/list_usuarios_puja_view';
        $this->cargarVistaCMS();
    }

    public function add_prize($idUsuario=null, $idPrizeSale=null, $puntos=null)
    {
        if(is_numeric($idUsuario) && is_numeric($idPrizeSale) && is_numeric($puntos))
        {
            $arrayQueryPremiosEnSubastaUsuario = array(
                "select"=>"user_prizes_sale.id, prizes_sale.id as prize_sale_id, prizes_sale.prize_id, prizes_sale.description, users.id as user_id, users.name, users.dni, users.email, users.phone, MAX(user_prizes_sale.points) as points",
                "join"=>array(
                    "prizes_sale, prizes_sale.id = user_prizes_sale.prizes_sales_id",
                    "users, users.id = user_prizes_sale.user_id"
                    ),
                "where"=>"prizes_sale.id = '".$idPrizeSale."' and users.id='".$idUsuario."'",
                //"group"=>"prizes_sale.id, prizes_sale.description, users.id, users.dni, users.email, users.phone, users.name",
                "order"=>"user_prizes_sale.points DESC",
                "limit"=>"20"
            );

            $arrayUsuariosEnSubasta = $this->user_prizes_sale_model->get_search_row($arrayQueryPremiosEnSubastaUsuario);

             $arrayQuerySubastaGanadora = array(
                "select"=>"*",
                "where"=>"user_id='".$idUsuario."' and prizes_sales_id='".$idPrizeSale."' and points='".$puntos."'"
             );
             $subastaGanadora = $this->user_prizes_sale_model->get_search_row($arrayQuerySubastaGanadora);

            if(isset($arrayUsuariosEnSubasta->user_id) && isset($subastaGanadora->id))
            {
                $fechaGanador = date("Y-m-d H:i:s");

                $arrayInsertWinner = array(
                    "user_id"=>$arrayUsuariosEnSubasta->user_id,
                    "prize_id"=>$arrayUsuariosEnSubasta->prize_id,
                    "campaign_id"=>"1",
                    "status"=>"1",
                    "created_at"=>$fechaGanador
                    );

                $idWinner = $this->winner_Model->insert($arrayInsertWinner);

                $arrayUpdateUser = array(
                    "win"=>"1",
                    "winner_at"=>$fechaGanador
                    );

                $this->user_Model->update($arrayUsuariosEnSubasta->user_id, $arrayUpdateUser);

				$this->prize_sale_model->update($idPrizeSale, array("status"=>"0"));

                $this->user_prizes_sale_model->update($subastaGanadora->id, array("winner"=>"1"));

                if($idWinner > 0)
                {
                    $this->session->set_flashdata('mensaje', 'Se asigno correctamente el premio.');
                }
                else
                {
                    $this->session->set_flashdata('mensaje', 'No se pudo asignar el premio, por favor vuelve a intentarlo.');
                }


            }
            else
            {
                $this->session->set_flashdata('mensaje', 'No se pudo asignar el premio, por favor vuelve a intentarlo.');
            }
        }
        else
        {
            $this->session->set_flashdata('mensaje', 'No se pudo asignar el premio, por favor vuelve a intentarlo.');
        }

        if(isset($arrayUsuariosEnSubasta->prize_sale_id))
            redirect(site_url($this->config->item('base_cms').'/Subasta/usuarios/'.$arrayUsuariosEnSubasta->prize_sale_id));
        else
            redirect(site_url($this->config->item('base_cms').'/Subasta/'));

    }
}
