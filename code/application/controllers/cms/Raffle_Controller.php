<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Raffle_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');
        $this->lang->load('cms_constans_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->model('raffle_Model', 'raffle_model');
        $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('user_Model', 'user_model');
        $this->load->model('raffle_Prize_Model', 'raffle_prize_model');
        $this->load->model('raffle_Winner_Model', 'raffle_winner_model');
        $this->load->model('token_Admin_Model', 'token_admin_model');

        if ( $this->session->userdata('logged_in') == false )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ($this->session->userdata("token") == "" || $this->session->userdata("token") == null)
        {
            redirect($this->config->item('base_cms').'/logout');
        }

	}

	public function list_view($start = 0)
	{
        $this->session->mark_as_flash('pagination_list_raffle');
        $this->session->set_flashdata('pagination_list_raffle', $start);

        $raffle_model = new Raffle_Model();

        $campaign = $this->session->userdata("campaign_code");
        $super_admin = $this->session->userdata("super_admin");

        $users = $raffle_model->get_list_users($start,$campaign,$super_admin);

        $total_users = $users['total_users'];
        $data_users = $users['data_users'];
        
        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Sorteo/participantes'), $total_users);
        $this->arrayVista['users'] = $data_users;
        $this->arrayVista['total_users'] = $total_users;
        $this->arrayVista['view'] = 'cms/raffle/list_view';
        $this->cargarVistaCMS();
	}

	public function list_search_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_list_search_user_raffle');
        $this->session->set_flashdata('pagination_list_search_user_raffle', $start);

        $search_raffle_user = "";
        $search_raffle_dni= "";
        $search_raffle_country = "";
        $search_raffle_campaign = "";
        $search_raffle_status = "";


        if (trim($this->input->post("search_raffle_user", TRUE)) != null)
        {
            $search_raffle_user = trim($this->input->post("search_raffle_user", TRUE));
            $this->session->mark_as_flash('search_raffle_user');
            $this->session->set_flashdata('search_raffle_user', $search_raffle_user);
        } else
        {
            if ($this->session->flashdata('search_raffle_user') != null)
            {
                if ($this->input->post("search_raffle") != "")
                {
                    $search_raffle_user = trim($this->input->post("search_raffle_user", TRUE));
                    $this->session->mark_as_flash('search_raffle_user');
                    $this->session->set_flashdata('search_raffle_user', $search_raffle_user);
                } else
                {
                    $search_raffle_user = $this->session->flashdata('search_raffle_user');
                    $this->session->mark_as_flash('search_raffle_user');
                    $this->session->set_flashdata('search_raffle_user', $search_raffle_user);
                }
            }
        }

        if (trim($this->input->post("search_raffle_dni", TRUE)) != null)
        {
            $search_raffle_dni = trim($this->input->post("search_raffle_dni", TRUE));
            $this->session->mark_as_flash('search_raffle_dni');
            $this->session->set_flashdata('search_raffle_dni', $search_raffle_dni);
        } else
        {
            if ($this->session->flashdata('search_raffle_dni') != null)
            {
                if ($this->input->post("search_raffle") != "")
                {
                    $search_raffle_dni = trim($this->input->post("search_raffle_dni", TRUE));
                    $this->session->mark_as_flash('search_raffle_dni');
                    $this->session->set_flashdata('search_raffle_dni', $search_raffle_dni);
                } else
                {
                    $search_raffle_dni = $this->session->flashdata('search_raffle_dni');
                    $this->session->mark_as_flash('search_raffle_dni');
                    $this->session->set_flashdata('search_raffle_dni', $search_raffle_dni);
                }
            }
        }

        if ($this->input->post("search_raffle_country", TRUE) != null)
        {
            $search_raffle_country = $this->input->post("search_raffle_country", TRUE);
            $this->session->mark_as_flash('search_raffle_country');
            $this->session->set_flashdata('search_raffle_country', $search_raffle_country);
        } else
        {
            if ($this->session->flashdata('search_raffle_country') != null)
            {
                if ($this->input->post("search_raffle") != "")
                {
                    $search_raffle_country = $this->input->post("search_raffle_country", TRUE);
                    $this->session->mark_as_flash('search_raffle_country');
                    $this->session->set_flashdata('search_raffle_country', $search_raffle_country);
                } else
                {
                    $search_raffle_country = $this->session->flashdata('search_raffle_country');
                    $this->session->mark_as_flash('search_raffle_country');
                    $this->session->set_flashdata('search_raffle_country', $search_raffle_country);
                }
            }
        }

        if ($this->session->userdata("super_admin") == SUPER_ADMIN)
        {
            if ($this->input->post("search_raffle_campaign", TRUE) != null)
            {
                $search_raffle_campaign= $this->input->post("search_raffle_campaign", TRUE);
                $this->session->mark_as_flash('search_raffle_campaign');
                $this->session->set_flashdata('search_raffle_campaign', $search_raffle_campaign);
            } else
            {
                if ($this->session->flashdata('search_raffle_campaign') != null)
                {
                    if ($this->input->post("search_raffle") != "")
                    {
                        $search_raffle_campaign = $this->input->post("search_raffle_campaign", TRUE);
                        $this->session->mark_as_flash('search_raffle_campaign');
                        $this->session->set_flashdata('search_raffle_campaign', $search_raffle_campaign);
                    } else
                    {
                        $search_raffle_campaign = $this->session->flashdata('search_raffle_campaign');
                        $this->session->mark_as_flash('search_raffle_campaign');
                        $this->session->set_flashdata('search_raffle_campaign', $search_raffle_campaign);
                    }
                }
            }
        } else
        {
            $search_raffle_campaign = $this->session->userdata("campaign_code");
        }

        if ($this->input->post("search_raffle_status", TRUE) != null)
        {
            $search_raffle_status = $this->input->post("search_raffle_status", TRUE);
            $this->session->mark_as_flash('search_raffle_status');
            $this->session->set_flashdata('search_raffle_status', $search_raffle_status);
        } else
        {
            if ($this->session->flashdata('search_raffle_status') != null)
            {
                if ($this->input->post("search_raffle") != "")
                {
                    $search_raffle_status = $this->input->post("search_raffle_status", TRUE);
                    $this->session->mark_as_flash('search_raffle_status');
                    $this->session->set_flashdata('search_raffle_status', $search_raffle_status);
                } else
                {
                    $search_raffle_status = $this->session->flashdata('search_raffle_status');
                    $this->session->mark_as_flash('search_raffle_status');
                    $this->session->set_flashdata('search_raffle_status', $search_raffle_status);
                }
            }
        }

        $raffle_model = new Raffle_Model();

        $users = $raffle_model->get_search_users_raffle($start,$search_raffle_user,$search_raffle_dni,$search_raffle_country,$search_raffle_campaign,$search_raffle_status);

        if ($search_raffle_user != "")
        {
            $this->arrayVista['search_raffle_user'] = $search_raffle_user;
        }

        if ($search_raffle_dni != "")
        {
            $this->arrayVista['search_raffle_dni'] = $search_raffle_dni;
        }

        if ($search_raffle_country != "")
        {
            $this->arrayVista['search_raffle_country'] = $search_raffle_country;
        }

        if ($search_raffle_campaign != "")
        {
            $this->arrayVista['search_raffle_campaign'] = $search_raffle_campaign;
        }

        if ($search_raffle_status != "")
        {
            $this->arrayVista['search_raffle_status'] = $search_raffle_status;
        }
        
        $total_users = $users['total_users'];
        $data_users = $users['data_users'];


        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Sorteo/participantes/buscar'), $total_users);
        $this->arrayVista['users'] = $data_users;
        $this->arrayVista['total_users'] = $total_users;
        $this->arrayVista['view'] = 'cms/raffle/list_view';
        $this->cargarVistaCMS();
    }

    public function list_winners_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_list_raffle_winners');
        $this->session->set_flashdata('pagination_list_raffle_winners', $start);

        $raffle_winner_model = new Raffle_Winner_Model();

        $campaign = $this->session->userdata("campaign_code");
        $super_admin = $this->session->userdata("super_admin");

        $users = $raffle_winner_model->get_list_user_winners($start,$campaign,$super_admin);

        $total_users = $users['total_users'];
        $data_users = $users['data_users'];

        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Sorteo/ganadores'), $total_users);
        $this->arrayVista['users'] = $data_users;
        $this->arrayVista['total_users'] = $total_users;
        $this->arrayVista['view'] = 'cms/raffle/list_winners_view';
        $this->cargarVistaCMS();
    }

    public function list_search_winners_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_list_search_user_raffle_winners');
        $this->session->set_flashdata('pagination_list_search_user_raffle_winners', $start);

        $search_raffle_winner_user = "";
        $search_raffle_winner_dni= "";
        $search_raffle_winner_country = "";
        $search_raffle_winner_campaign = "";
        $search_raffle_winner_status = "";

        if (trim($this->input->post("search_raffle_winner_user", TRUE)) != null)
        {
            $search_raffle_winner_user = trim($this->input->post("search_raffle_winner_user", TRUE));
            $this->session->mark_as_flash('search_raffle_winner_user');
            $this->session->set_flashdata('search_raffle_winner_user', $search_raffle_winner_user);
        } else
        {
            if ($this->session->flashdata('search_raffle_winner_user') != null)
            {
                if ($this->input->post("search_raffle_winner") != "")
                {
                    $search_raffle_winner_user = trim($this->input->post("search_raffle_winner_user", TRUE));
                    $this->session->mark_as_flash('search_raffle_winner_user');
                    $this->session->set_flashdata('search_raffle_winner_user', $search_raffle_winner_user);
                } else
                {
                    $search_raffle_winner_user = $this->session->flashdata('search_raffle_winner_user');
                    $this->session->mark_as_flash('search_raffle_winner_user');
                    $this->session->set_flashdata('search_raffle_winner_user', $search_raffle_winner_user);
                }
            }
        }

        if (trim($this->input->post("search_raffle_winner_dni", TRUE)) != null)
        {
            $search_raffle_winner_dni = trim($this->input->post("search_raffle_winner_dni", TRUE));
            $this->session->mark_as_flash('search_raffle_winner_dni');
            $this->session->set_flashdata('search_raffle_winner_dni', $search_raffle_winner_dni);
        } else
        {
            if ($this->session->flashdata('search_raffle_winner_dni') != null)
            {
                if ($this->input->post("search_raffle_winner") != "")
                {
                    $search_raffle_winner_dni = trim($this->input->post("search_raffle_winner_dni", TRUE));
                    $this->session->mark_as_flash('search_raffle_winner_dni');
                    $this->session->set_flashdata('search_raffle_winner_dni', $search_raffle_winner_dni);
                } else
                {
                    $search_raffle_winner_dni = $this->session->flashdata('search_raffle_winner_dni');
                    $this->session->mark_as_flash('search_raffle_winner_dni');
                    $this->session->set_flashdata('search_raffle_winner_dni', $search_raffle_winner_dni);
                }
            }
        }

        if ($this->input->post("search_raffle_winner_country", TRUE) != null)
        {
            $search_raffle_winner_country = $this->input->post("search_raffle_winner_country", TRUE);
            $this->session->mark_as_flash('search_raffle_winner_country');
            $this->session->set_flashdata('search_raffle_winner_country', $search_raffle_winner_country);
        } else
        {
            if ($this->session->flashdata('search_raffle_winner_country') != null)
            {
                if ($this->input->post("search_raffle_winner") != "")
                {
                    $search_raffle_winner_country = $this->input->post("search_raffle_winner_country", TRUE);
                    $this->session->mark_as_flash('search_raffle_winner_country');
                    $this->session->set_flashdata('search_raffle_winner_country', $search_raffle_winner_country);
                } else
                {
                    $search_raffle_winner_country = $this->session->flashdata('search_raffle_winner_country');
                    $this->session->mark_as_flash('search_raffle_winner_country');
                    $this->session->set_flashdata('search_raffle_winner_country', $search_raffle_winner_country);
                }
            }
        }

        if ($this->session->userdata("super_admin") == SUPER_ADMIN)
        {
            if ($this->input->post("search_raffle_winner_campaign", TRUE) != null)
            {
                $search_raffle_winner_campaign= $this->input->post("search_raffle_winner_campaign", TRUE);
                $this->session->mark_as_flash('search_raffle_winner_campaign');
                $this->session->set_flashdata('search_raffle_winner_campaign', $search_raffle_winner_campaign);
            } else
            {
                if ($this->session->flashdata('search_raffle_winner_campaign') != null)
                {
                    if ($this->input->post("search_raffle_winner") != "")
                    {
                        $search_raffle_winner_campaign = $this->input->post("search_raffle_winner_campaign", TRUE);
                        $this->session->mark_as_flash('search_raffle_winner_campaign');
                        $this->session->set_flashdata('search_raffle_winner_campaign', $search_raffle_winner_campaign);
                    } else
                    {
                        $search_raffle_winner_campaign = $this->session->flashdata('search_raffle_winner_campaign');
                        $this->session->mark_as_flash('search_raffle_winner_campaign');
                        $this->session->set_flashdata('search_raffle_winner_campaign', $search_raffle_winner_campaign);
                    }
                }
            }
        } else
        {
            $search_raffle_winner_campaign = $this->session->userdata("campaign_code");
        }

        if ($this->input->post("search_raffle_winner_status", TRUE) != null)
        {
            $search_raffle_winner_status = $this->input->post("search_raffle_winner_status", TRUE);
            $this->session->mark_as_flash('search_raffle_winner_status');
            $this->session->set_flashdata('search_raffle_winner_status', $search_raffle_winner_status);
        } else
        {
            if ($this->session->flashdata('search_raffle_winner_status') != null)
            {
                if ($this->input->post("search_raffle_winner") != "")
                {
                    $search_raffle_winner_status = $this->input->post("search_raffle_winner_status", TRUE);
                    $this->session->mark_as_flash('search_raffle_winner_status');
                    $this->session->set_flashdata('search_raffle_winner_status', $search_raffle_winner_status);
                } else
                {
                    $search_raffle_winner_status = $this->session->flashdata('search_raffle_winner_status');
                    $this->session->mark_as_flash('search_raffle_winner_status');
                    $this->session->set_flashdata('search_raffle_winner_status', $search_raffle_winner_status);
                }
            }
        }

        $raffle_winner_model = new Raffle_Winner_Model();

        $users = $raffle_winner_model->get_search_users_winners_raffle($start,$search_raffle_winner_user,$search_raffle_winner_dni,$search_raffle_winner_country,$search_raffle_winner_campaign,$search_raffle_winner_status);

        if ($search_raffle_winner_user != "")
        {
            $this->arrayVista['search_raffle_winner_user'] = $search_raffle_winner_user;
        }

        if ($search_raffle_winner_dni != "")
        {
            $this->arrayVista['search_raffle_winner_dni'] = $search_raffle_winner_dni;
        }

        if ($search_raffle_winner_country != "")
        {
            $this->arrayVista['search_raffle_winner_country'] = $search_raffle_winner_country;
        }

        if ($search_raffle_winner_campaign != "")
        {
            $this->arrayVista['search_raffle_winner_campaign'] = $search_raffle_winner_campaign;
        }

        if ($search_raffle_winner_status != "")
        {
            $this->arrayVista['search_raffle_winner_status'] = $search_raffle_winner_status;
        }

        $total_users = $users['total_users'];
        $data_users = $users['data_users'];


        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Sorteo/ganadores/buscar'), $total_users);
        $this->arrayVista['users'] = $data_users;
        $this->arrayVista['total_users'] = $total_users;
        $this->arrayVista['view'] = 'cms/raffle/list_winners_view';
        $this->cargarVistaCMS();
    }

    public function export_list_winners()
    {

        $search_raffle_winner_user = $this->input->get('search_raffle_winner_user');
        $search_raffle_winner_dni = $this->input->get('search_raffle_winner_dni');
        $search_raffle_winner_country = $this->input->get('search_raffle_winner_country');
        $search_raffle_winner_campaign = $this->input->get('search_raffle_winner_campaign');
        $search_raffle_winner_status = $this->input->get('search_raffle_winner_status');



        if ($this->session->userdata("super_admin") != SUPER_ADMIN)
        {
            $search_raffle_winner_campaign = $this->session->userdata("campaign_code");
        }

        $raffle_winner_model = new Raffle_Winner_Model();

        $users = $raffle_winner_model->export_list_user_winners_raffle($search_raffle_winner_user,$search_raffle_winner_dni,$search_raffle_winner_country,$search_raffle_winner_campaign,$search_raffle_winner_status);

        $data = array();

        if (isset($users)){

            foreach ($users as $user) {

                $campaign = "";
                $country = "";
                $status = "";

                if ($user->country_code == COUNTRY_PERU)
                {
                    $campaign =  $this->lang->line('constant_country_peru');
                } elseif ($user->country_code == COUNTRY_ECUADOR)
                {
                    $campaign =   $this->lang->line('constant_country_ecuador');
                } elseif ($user->country_code == COUNTRY_PUERTO_RICO)
                {
                    $campaign =   $this->lang->line('constant_country_puerto_rico');
                }elseif ($user->country_code == COUNTRY_DOMINICAN_REPUBLIC)
                {
                    $campaign =  $this->lang->line('constant_country_dominican_republic');
                }

                if ($user->country == COUNTRY_PERU)
                {
                    $country =  $this->lang->line('constant_country_peru');
                } elseif ($user->country == COUNTRY_ECUADOR)
                {
                    $country =   $this->lang->line('constant_country_ecuador');
                } elseif ($user->country == COUNTRY_PUERTO_RICO)
                {
                    $country =   $this->lang->line('constant_country_puerto_rico');
                }elseif ($user->country == COUNTRY_DOMINICAN_REPUBLIC)
                {
                    $country =  $this->lang->line('constant_country_dominican_republic');
                }else
                {
                    $country = $user->country;
                }

                if ($user->status == STATUS_ACTIVE){
                    $status = $this->lang->line('constant_status_active');
                }else{
                    $status = $this->lang->line('constant_status_inactive');
                }

                array_push($data, array(
                    utf8_decode($this->lang->line('constant_list_dni_user')) => isset($user->dni)?utf8_decode($user->dni):"-",
                    utf8_decode($this->lang->line('constant_list_name_user')) => isset($user->name)?utf8_decode($user->name):"-",
                    utf8_decode($this->lang->line('constant_list_campaign_user')) => isset($campaign)?utf8_decode($campaign):"-",
                    utf8_decode($this->lang->line('constant_list_country_user')) => isset($country)?utf8_decode($country):"-",
                    utf8_decode($this->lang->line('constant_list_email_user')) => isset($user->email)?utf8_decode($user->email):"-",
                    utf8_decode($this->lang->line('constant_list_phone_user')) => isset($user->phone)?utf8_decode($user->phone):"-",
                    utf8_decode($this->lang->line('constant_list_points_user')) => isset($user->points)?utf8_decode($user->points):"-",
                    utf8_decode($this->lang->line('constant_list_prize_user')) => isset($user->prize_name)?utf8_decode($user->prize_name):"-",
                    utf8_decode($this->lang->line('constant_list_status_user')) => isset($status)?utf8_decode($status):"-",
                ));
            }

            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }

            $fileName = $this->lang->line('constant_excel_raffle'). ".xls";

            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");

            $flag = false;

            foreach($data as $row) {

                if(!$flag) {
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }

                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }

            exit;
        }

        // redirect(site_url($this->config->item('base_cms').'/Usuario/buscar'));
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    
    public function export_list()
    {
        
        $search_raffle_user = $this->input->get('search_raffle_user');
        $search_raffle_dni = $this->input->get('search_raffle_dni');
        $search_raffle_country = $this->input->get('search_raffle_country');
        $search_raffle_campaign = $this->input->get('search_raffle_campaign');
        $search_raffle_status = $this->input->get('search_raffle_status');


        if ($this->session->userdata("super_admin") != SUPER_ADMIN)
        {
            $search_raffle_campaign = $this->session->userdata("campaign_code");
        }

        $raffle_model = new Raffle_Model();

        $users = $raffle_model->export_list_user_raffle($search_raffle_user,$search_raffle_dni,$search_raffle_country,$search_raffle_campaign,$search_raffle_status);

        $data = array();

        if (isset($users)){

            foreach ($users as $user) {

                $campaign = "";
                $country = "";
                $status = "";

                if ($user->country_code == COUNTRY_PERU)
                {
                    $campaign =  $this->lang->line('constant_country_peru');
                } elseif ($user->country_code == COUNTRY_ECUADOR)
                {
                    $campaign =   $this->lang->line('constant_country_ecuador');
                } elseif ($user->country_code == COUNTRY_PUERTO_RICO)
                {
                    $campaign =   $this->lang->line('constant_country_puerto_rico');
                }elseif ($user->country_code == COUNTRY_DOMINICAN_REPUBLIC)
                {
                    $campaign =  $this->lang->line('constant_country_dominican_republic');
                }

                if ($user->country == COUNTRY_PERU)
                {
                    $country =  $this->lang->line('constant_country_peru');
                } elseif ($user->country == COUNTRY_ECUADOR)
                {
                    $country =   $this->lang->line('constant_country_ecuador');
                } elseif ($user->country == COUNTRY_PUERTO_RICO)
                {
                    $country =   $this->lang->line('constant_country_puerto_rico');
                }elseif ($user->country == COUNTRY_DOMINICAN_REPUBLIC)
                {
                    $country =  $this->lang->line('constant_country_dominican_republic');
                }else
                {
                    $country = $user->country;
                }

                if ($user->status == STATUS_ACTIVE){
                    $status = $this->lang->line('constant_status_active');
                }else{
                    $status = $this->lang->line('constant_status_inactive');
                }

                array_push($data, array(
                    utf8_decode($this->lang->line('constant_list_dni_user')) => isset($user->dni)?utf8_decode($user->dni):"-",
                    utf8_decode($this->lang->line('constant_list_name_user')) => isset($user->name)?utf8_decode($user->name):"-",
                    utf8_decode($this->lang->line('constant_list_campaign_user')) => isset($campaign)?utf8_decode($campaign):"-",
                    utf8_decode($this->lang->line('constant_list_country_user')) => isset($country)?utf8_decode($country):"-",
                    utf8_decode($this->lang->line('constant_list_email_user')) => isset($user->email)?utf8_decode($user->email):"-",
                    utf8_decode($this->lang->line('constant_list_phone_user')) => isset($user->phone)?utf8_decode($user->phone):"-",
                    utf8_decode($this->lang->line('constant_list_points_user')) => isset($user->points)?utf8_decode($user->points):"-",
                    utf8_decode($this->lang->line('constant_list_status_user')) => isset($status)?utf8_decode($status):"-",
                ));
            }

            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }

            $fileName = $this->lang->line('constant_excel_raffle'). ".xls";

            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");

            $flag = false;

            foreach($data as $row) {

                if(!$flag) {
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }

                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }

            exit;
        }

        // redirect(site_url($this->config->item('base_cms').'/Usuario/buscar'));
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    
    public function generate_view()
    {
        $super_admin = $this->session->userdata("super_admin");
        $campaign_code = $this->session->userdata("campaign_code");

        $campaign_model = new Campaign_Model();

        $campaigns = $campaign_model->get_list_campaign();

        $this->arrayVista['super_admin'] = $super_admin;
        $this->arrayVista['campaign_code'] = $campaign_code;
        $this->arrayVista['campaigns'] = $campaigns;
        $this->arrayVista['view'] = 'cms/raffle/generate_view';
        $this->cargarVistaCMS();
    }

    public function generate()
    {
        $campaign =  $this->input->post("raffle_campaign");
        $prize =  $this->input->post("raffle_prize");

        $raffle_model = new Raffle_Model();

        $raffle_winner = new Raffle_Winner_Model();
        $prize_user = $raffle_winner->get_prize_by_raffle($prize,$campaign);

        if (!isset($prize_user))
        {
            $total_participants = $raffle_model->get_total_participants($campaign);

            if (isset($total_participants))
            {
                if ($total_participants > 0)
                {

                    $user = $raffle_model->get_user_winner_raffle($campaign);
                    if (isset($user))
                    {
                                $id_user = $user->id;

                                $raffle = $raffle_model->update_user_winner_raffle($id_user);

                                if (isset($raffle)) {

                                    $raffle_winner_model = new Raffle_Winner_Model();

                                    $winner = $raffle_winner_model->register_user_raffle_winner($prize, $id_user, $campaign);

                                    if (isset($winner)) {

                                        $raffle_prize_model = new Raffle_Prize_Model();

                                        $raffle_prize = $raffle_prize_model->update_status_prize($prize);

                                        if (isset($raffle_prize)) {
                                            $error = array("user" => $user, "code" => 1);
                                        } else {
                                            $error = array("message" => $this->lang->line('raffle_error_update_raffle_prize'), "code" => 0);
                                        }

                                    } else {
                                        $error = array("message" => $this->lang->line('raffle_error_register_winner'), "code" => 0);
                                    }
                                } else
                                {
                                    $error = array("message" => $this->lang->line('raffle_error_update_user'), "code" => 0);
                                }
                    }else
                    {
                        $error = array("message"=>$this->lang->line('raffle_error_user'),"code"=>0);
                    }
                } else
                {
                    $error = array("message"=>$this->lang->line('raffle_error_total_user_cero'),"code"=>0);
                }

            } else
            {
                $error = array("message"=>$this->lang->line('raffle_error_total_user'),"code"=>0);
            }
        } else
        {
            $error = array("message"=>$this->lang->line('raffle_error_prize_cero'),"code"=>0);
        }

        echo json_encode($error);
    }
    
}
