<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Raffle_Prize_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');
        $this->lang->load('cms_constans_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->model('raffle_Prize_Model', 'raffle_prize_model');
        $this->load->model('token_Admin_Model', 'token_admin_model');

        if ( $this->session->userdata('logged_in') == false )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ($this->session->userdata("token") == "" || $this->session->userdata("token") == null)
        {
            redirect($this->config->item('base_cms').'/logout');
        }

	}

	public function list_prizes()
	{


        if ($this->session->userdata("super_admin")== SUPER_ADMIN)
        {
            $campaign =  $this->input->post("raffle_campaign");
        }else
        {
            $campaign =  $this->session->userdata("campaign_id");
        }


        $raffle_prizes_model = new Raffle_Prize_Model();

        $prizes = $raffle_prizes_model->get_list_prizes($campaign);

        if (isset($prizes))
        {
            $error = array("prizes"=>$prizes,"code"=>1);
        } else
        {
            $error = array("message"=>$this->lang->line('raffle_prizes_error_list'),"code"=>0);
        }

        echo json_encode($error);

	}




   
    
}
