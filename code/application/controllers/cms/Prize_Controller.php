<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prize_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');
        $this->lang->load('cms_constans_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->model('user_Model', 'user_model');
        $this->load->model('prize_Model', 'prize_model');

        if ( $this->session->userdata('logged_in') == false )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ($this->session->userdata("token") == "" || $this->session->userdata("token") == null)
        {
            redirect($this->config->item('base_cms').'/logout');
        }
	}

	public function list_view($start = 0)
	{
        $this->session->mark_as_flash('pagination_prizes');
        $this->session->set_flashdata('pagination_prizes', $start);

        $prize_model = new Prize_Model();

        $prizes = $prize_model->get_all_list_prizes($start);

        $total_prizes = $prizes['total_prizes'];
        $data_prizes = $prizes['data_prizes'];

        //$this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Premio'), $total_prizes);
        //$this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Usuario'), $total_users);
        
        
        
        $this->arrayVista['prizes'] = $data_prizes;
        $this->arrayVista['view'] = 'cms/prize/list_view';
        $this->cargarVistaCMS();
	}

	public function search_view($start = 0)
    {
        $this->session->mark_as_flash('pagination_list_search_prizes');
        $this->session->set_flashdata('pagination_list_search_prizes', $start);

        $type = "";
        $name = "";
        $country = "";
        $status = "";

        if ($this->input->post("type", TRUE) != null)
        {
            $type = $this->input->post("type", TRUE);
            $this->session->mark_as_flash('search_prize_type');
            $this->session->set_flashdata('search_prize_type', $type);
        } else
        {
            if ($this->session->flashdata('search_prize_type') != null)
            {
                if ($this->input->post("search_prize") != "")
                {
                    $type = $this->input->post("type", TRUE);
                    $this->session->mark_as_flash('search_prize_type');
                    $this->session->set_flashdata('search_prize_type', $type);
                } else
                {
                    $type = $this->session->flashdata('search_prize_type');
                    $this->session->mark_as_flash('search_prize_type');
                    $this->session->set_flashdata('search_prize_type', $type);
                }
            }
        }

        if (trim($this->input->post("name", TRUE)) != null)
        {
            $name = $this->input->post("name", TRUE);
            $this->session->mark_as_flash('search_prize_name');
            $this->session->set_flashdata('search_prize_name', $name);
        } else
        {
            if ($this->session->flashdata('search_prize_name') != null)
            {
                if ($this->input->post("search_prize") != "")
                {
                    $name = trim($this->input->post("name", TRUE));
                    $this->session->mark_as_flash('search_prize_name');
                    $this->session->set_flashdata('search_prize_name', $name);
                } else
                {
                    $name = $this->session->flashdata('search_prize_name');
                    $this->session->mark_as_flash('search_prize_name');
                    $this->session->set_flashdata('search_prize_name', $name);
                }
            }
        }

        if ($this->input->post("country", TRUE) != null)
        {
            $country = $this->input->post("country", TRUE);
            $this->session->mark_as_flash('search_prize_country');
            $this->session->set_flashdata('search_prize_country', $country);
        } else
        {
            if ($this->session->flashdata('search_prize_country') != null)
            {
                if ($this->input->post("search_prize") != "")
                {
                    $country = $this->input->post("country", TRUE);
                    $this->session->mark_as_flash('search_prize_country');
                    $this->session->set_flashdata('search_prize_country', $country);
                } else
                {
                    $country = $this->session->flashdata('search_prize_country');
                    $this->session->mark_as_flash('search_prize_country');
                    $this->session->set_flashdata('search_prize_country', $country);
                }
            }
        }

        if ($this->input->post("status", TRUE) != null)
        {
            $status = $this->input->post("status", TRUE);
            $this->session->mark_as_flash('search_prize_status');
            $this->session->set_flashdata('search_prize_status', $status);
        } else
        {
            if ($this->session->flashdata('search_prize_status') != null)
            {
                if ($this->input->post("search_prize") != "")
                {
                    $status = $this->input->post("status", TRUE);
                    $this->session->mark_as_flash('search_prize_status');
                    $this->session->set_flashdata('search_prize_status', $status);
                } else
                {
                    $status = $this->session->flashdata('search_prize_status');
                    $this->session->mark_as_flash('search_prize_status');
                    $this->session->set_flashdata('search_prize_status', $status);
                }
            }
        }

        $prize_model = new Prize_Model();

        $prizes = $prize_model->get_all_search_prizes($start,$type,$name,$country,$status);

        if ($type != "")
        {
            $this->arrayVista['type'] = $type;
        }

        if ($name != "")
        {
            $this->arrayVista['name'] = $name;
        }

        if ($country != "")
        {
            $this->arrayVista['country'] = $country;
        }

        if ($status != "")
        {
            $this->arrayVista['status'] = $status;
        }

        $total_users = $users['total_users'];
        $data_users = $users['data_users'];

        $this->arrayVista['pagination'] = $this->obtenerPaginadoListado(site_url($this->config->item('base_cms') . '/Usuario'), $total_users);
        $this->arrayVista['users'] = $data_users;
        $this->arrayVista['view'] = 'cms/user/list_view';
        $this->cargarVistaCMS();
    }

    public function update_state(){
        
        $prize_id = trim($this->input->post("prize_id",TRUE));
        $status = trim($this->input->post("status",TRUE));

        $this->prize_model->update_status($prize_id, $status);

        header('Content-Type: application/json');
        echo json_encode(array("response"=>"ok"));

    }

}
