<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('cms_messages_lang', 'spanish');
		$this->lang->load('cms_error_messages_lang', 'spanish');
        $this->lang->load('cms_constans_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->model('admin_Model', 'admin_model');
        $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('token_Admin_Model', 'token_admin_model');

        if ( $this->session->userdata('logged_in') == false )
        {
            redirect($this->config->item('base_cms').'/logout');
        }
        if ($this->session->userdata("token") == "" || $this->session->userdata("token") == null)
        {
            redirect($this->config->item('base_cms').'/logout');
        }
	}

    public function update_view()
    {
        $super_admin = $this->session->userdata("super_admin");
        $campaign_code = $this->session->userdata("campaign_code");
        $campaign_id = $this->session->userdata("campaign_id");

        $campaign_model = new Campaign_Model();
        $campaigns = $campaign_model->get_list_campaign();

        $this->arrayVista['super_admin'] = $super_admin;
        $this->arrayVista['campaign_code'] = $campaign_code;
        $this->arrayVista['campaign_id'] = $campaign_id;
        $this->arrayVista['campaigns'] =  $campaigns;
        $this->arrayVista['view'] = 'cms/configuration/update_view';
        $this->cargarVistaCMS();
    }

    public function get_configuration()
    {

        if ($this->session->userdata("super_admin")== SUPER_ADMIN)
        {
            $campaign =  $this->input->post("campaign");
        }else
        {
            $campaign =  $this->session->userdata("campaign_id");
        }


        $campaign_model = new Campaign_Model();

        $configuration= $campaign_model->get_configuration($campaign);

        if (isset($configuration))
        {
            $error = array("configuration"=>$configuration,"code"=>1);
        } else
        {
            $error = array("message"=>$this->lang->line('configuration_error_get_detail'),"code"=>0);
        }

        echo json_encode($error);

    }


    public function update()
    {
        if ($this->session->userdata("super_admin")== SUPER_ADMIN)
        {
            $campaign =  $this->input->post("campaign");
        }else
        {
            $campaign =  $this->session->userdata("campaign_id");
        }

        $prize_day=  $this->input->post("prize_day");
        $random =  $this->input->post("random");

        $campaign_model = new Campaign_Model();

        $configuration = $campaign_model->update_configuration($campaign,$prize_day,$random);

        if (isset($configuration))
        {
            $error = array("message"=>$this->lang->line('configuration_success_update'),"code"=>1);
        } else
        {
            $error = array("message"=>$this->lang->line('configuration_error_update'),"code"=>0);
        }

        echo json_encode($error);
    }


}
