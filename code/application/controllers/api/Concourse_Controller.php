<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";

class Concourse_Controller extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->lang->load('api_error_messages_lang', 'spanish');
        $this->lang->load('api_messages_lang', 'spanish');
        $this->load->model('concourse_Model', 'concourse_model');
        $this->load->model('challenge_Model', 'challenge_model');
        $this->load->model('user_Model', 'user_model');
        $this->load->model('prize_Model', 'prize_model');
        $this->load->model('raffle_Model', 'raffle_model');
        $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('winner_Model', 'winner_model');
        $this->load->model('token_Model', 'token_model');
        $this->load->model('package_Model', 'package_Model');
    }

    public function store_post()
    {
        $token_get = $this->input->post('token',true);

        $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action =    API_CONCOURSE_ACTION_REGISTER;

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {

            $rules = [
                [
                    'field' => 'point',
                    'rules' => 'required|numeric|is_natural',
                    'errors' => [
                        'required' => $this->lang->line('concourse_error_challenge_required'),
                        'numeric' =>  $this->lang->line('concourse_error_challenge_numeric'),
                        'is_natural' =>  $this->lang->line('concourse_error_challenge_natural')
                    ]
                ]

            ];

            $this->form_validation->set_rules($rules);

            $package_id = $this->post('point');

            $params = array(
                'token'=>$token_get,
                'points'=>$point
            );

            save_log_api($params,$email,$uri,$method,$action);

            if ($this->form_validation->run() == FALSE)
            {
                $errors_message = $this->form_validation->error_array();
                $error_response = get_error_form($errors_message);
                $this->response($error_response, 400);
            } else
            {


                $user = $token_model->get_user_country_with_token($token_get);
                if (isset($user))
                {
                    $id_campaign = $user->campaign_id;
                    $campaign_model = new Campaign_Model();

                    $campaign  = $campaign_model->get_campaign($id_campaign);

                    if (isset($campaign)) {
                        if ($campaign->status == STATUS_ACTIVE) {

                            $id_user = $user->user_id;

                            $package_model = new Package_Model();
                            $package = $this->package_Model->get_points($package_id);

                            if(isset($package)){
                              $user_model = new User_Model();
                              $user = $user_model->find_prize_by_user($id_user);

                              $points = $user->points + ($package->points * 4);
                              $accumulate_points = $user_model->accumulate_points_user($id_user, $points);

                              if (isset($accumulate_points)) {

                                $winner_model = new Winner_Model();
                                $campaign_id = $campaign->id;
                                $total_prizes_day_campaign = $winner_model->total_winner_by_day($campaign->id);
                                //die('$total_prizes_day_campaign: '.$total_prizes_day_campaign);

                                if ($total_prizes_day_campaign < $campaign->prize_day) {


                                  $prize_model = new Prize_Model();
                                  $prizes = $prize_model->get_total_prizes_instantwin($id_campaign);
                                  // var_dump($prizes);
                                  // die('$total_prizes_day_campaign: '.$total_prizes_day_campaign);

                                  if(isset($prizes)){

                                    $winner = $winner_model->find_winner_by_user($id_user);

                                    // var_dump($winner);
                                    // die();
                                    if (!isset($winner)) {


                                      $number_random_1 = rand(1, $campaign->random);
                                      $number_random_2 = rand(1, $campaign->random);

                                      $campaign = validate_campaign();

                                      if ($campaign['result'] == 1) {


                                        //die('ssss');
                                          $campaign = $campaign['campaign'];
                                          $country_code = $campaign[0]->country_code;

                                          if ($country_code == $user->country) {

                                              if ($number_random_1 == $number_random_2) {
                                                  $win = WIN;
                                                  $prize_win = $prize_model->update_status_prize($prizes->id);
                                                  $winner_win = $winner_model->register_winner($campaign_id,$id_user,$prizes->id);
                                                  $user_win = $user_model->update_instant_winner_user($id_user);
                                                  $response['win_prize'] = $win;
                                                  $response['prize'] = $prizes;
                                                  //$response['win_prize'] = $win;
                                              } /*else {
                                                  $win = LOSE;
                                              }*/

                                              // $response['win_prize'] = $win;

                                          }

                                      }

                                    }
                                  }

                                }






                                $this->register_concourse($id_user, 1, $package->points * 3, 1);







                                $response['points'] = $package->points * 3;
                                $this->response($response, 200);
                              }else{

                              }
                            }else{

                            }

                        }else
                        {
                            $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                        }
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                    }
                } else
                {
                    $this->response(get_error_response($this->lang->line('user_error_found')),404);
                }

            }
        } else
        {
            save_log_api($params,$email,$uri,$method,$action);
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }
    }

    public function register_concourse($id_user, $id_challenge, $time, $win)
    {
        $concourse_model = new Concourse_Model();
        $concourse = $concourse_model->create($id_user, $id_challenge, $time, $win);

        $response = null;
        if (isset($concourse))
        {
            $response = get_success_response($this->lang->line('concourse_success_create'));

        } else
        {
            $this->response(get_error_response($this->lang->line('concourse_error_create')),404);
        }

        return $response;
    }

}
