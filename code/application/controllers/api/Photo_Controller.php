<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";

class Photo_Controller extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->lang->load('api_error_messages_lang', 'spanish');
        $this->lang->load('api_messages_lang', 'spanish');
        $this->load->model('photo_Model', 'photo_model');
        $this->load->model('concourse_Model', 'concourse_model');
        $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('challenge_Model', 'challenge_model');
        $this->load->model('token_Model', 'token_model');
    }

    public function list_post($id_challenge)
    {
        $token_get = $this->input->post('token',true);

        $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_PHOTO_ACTION_LIST;

        save_log_api($params,$email,$uri,$method,$action);

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {
            $user = $token_model->get_user_country_with_token($token_get);

            if (isset($user)) {

                $id_campaign = $user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {

                        $id_user = $user->user_id;
                        if (isset($id_challenge)) {
                            $challenge_model = new Challenge_Model();

                            $challenge_validation_user_win = $challenge_model->validate_user_win_challenge($id_user, $id_challenge);

                            if (isset($challenge_validation) || sizeof($challenge_validation_user_win) >= 0) {

                                if ($challenge_validation_user_win->allow < MAX_WON_PER_CHALLENGE) {
                                    $photo_model = new Photo_Model();
                                    $photo_list = $photo_model->get_list_photos($id_challenge);

                                    if (isset($photo_list)) {

                                        for ($i=0;$i<sizeof($photo_list);$i++){
                                            if ($photo_list[$i]->limit != null || $photo_list[$i]->limit != ""){
                                               $limits = explode(",",$photo_list[$i]->limit );
                                               for ($j=0;$j<sizeof($limits);$j++){
                                                 for ($k=0;$k<sizeof($photo_list);$k++){
                                                     if ($limits[$j] == $photo_list[$k]->id){
                                                         $photo = $photo_model->get_photos($id_challenge,$limits,$photo_list);
                                                         if (isset($photo)){
                                                             $photo_list[$k]=$photo;
                                                         }else{
                                                             $this->response(get_error_response($this->lang->line('photo_error_list')),404);
                                                         }
                                                     }
                                                 }
                                               }
                                            }
                                        }

                                        $challenge = $challenge_model->get_photo_detail($id_challenge);

                                        if (isset($challenge)) {

                                            $challenge_validation_user = $challenge_model->validate_user_challenge($id_user, $id_challenge);

                                            $concourse_model = new Concourse_Model();
                                            $won_per_day = $concourse_model->won_per_day($id_user);
                                            //if ($won_per_day < MAX_WON_PER_DAY) {
                                            if ($won_per_day < MAX_WON_PER_DAY){
                                                $win_day = STATUS_ACTIVE;
                                            }else{
                                                $win_day = STATUS_INACTIVE;
                                            }

                                            if (isset($challenge_validation_user) || sizeof($challenge_validation_user) >= 0) {

                                                if ($challenge_validation_user->challenge_play== null){
                                                    $play = STATUS_INACTIVE;
                                                }else{
                                                    $play = $challenge_validation_user->challenge_play;
                                                }

                                                if (sizeof($photo_list) > 0) {

                                                    $challenge->play = $play;

                                                    $response = get_success_response($this->lang->line('photo_success_list'));
                                                    $response['photos'] = $photo_list;
                                                    $response['challenge'] = $challenge;
                                                    $response['win_day'] = $win_day;

                                                    $this->response($response, 200);
                                                } else {
                                                    $this->response(get_error_response($this->lang->line('photo_error_list_empty')),404);
                                                }
                                            } else {
                                                $this->response(get_error_response($this->lang->line('challenge_error_validation_user')),404);
                                            }
                                        } else {
                                            $this->response(get_error_response($this->lang->line('challenge_error_detail')),404);
                                        }
                                    } else {
                                        $this->response(get_error_response($this->lang->line('photo_error_list')),404);
                                    }
                                } else {
                                    $this->response(get_error_response($this->lang->line('challenge_error_user_challenge')),404);
                                }
                            } else {
                                $this->response(get_error_response($this->lang->line('challenge_error_validation_user_win')),404);
                            }
                        } else {
                            $this->response(get_error_response($this->lang->line('photo_error_list_challenge_empty')),404);
                        }
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }

            } else
                {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }

        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }

    }

}