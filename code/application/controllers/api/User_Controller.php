<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class User_Controller extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->lang->load('api_error_messages_lang', 'spanish');
        $this->lang->load('api_messages_lang', 'spanish');
        $this->load->model('user_Model','user_model');
        $this->load->model('campaign_Model','campaign_model');
        $this->load->model('prize_Model', 'prize_model');
        $this->load->model('raffle_Model', 'raffle_model');
        $this->load->model('winner_Model', 'winner_model');
        $this->load->model('token_Model','token_model');
        $this->load->model('meme_Model','meme_model');
        $this->load->model('meme_Category_Model','meme_category_model');
    }

    public function login_post()
    {
        $rules = [
            [
                'field' => 'dni',
                'rules' => 'required|exact_length[8]|numeric',
                'errors' => [
                    'required' => $this->lang->line('user_form_dni_required'),
                    'numeric' => $this->lang->line('user_form_dni_numeric'),
                    'exact_length' => $this->lang->line('user_form_dni_length')
                ]
            ]
            /*,
            [
                'field' => 'email',
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => $this->lang->line('user_form_email_required'),
                    'valid_email' => $this->lang->line('user_form_email_valid')
                ]
            ]*/
        ];
        $this->form_validation->set_rules($rules);
        $params = array();
        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_USER_ACTION_LOGIN;

        $email = $this->post('email');
        $dni = $this->post('dni');

        $params = array(
            'email'=>$email,
            'dni'=>$dni,
        );

        save_log_api($params,$email,$uri,$method,$action);

        if ($this->form_validation->run() == FALSE)
        {
            $errors_message = $this->form_validation->error_array();
            $error_response = get_error_form($errors_message);
            $this->response($error_response, 400);
        } else
        {
            $user_model = new User_Model();
            $user = $user_model->find($email,$dni);
           

            if (isset($user->id))
            {
                $id_campaign = $user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {
                        $token_model = new Token_Model();
                        $token = $token_model->generate_token();
                        $id_token = $token_model->save_token($token, $user->id);

                        if (isset($id_token)) {
                            $response = get_success_response($this->lang->line('user_success_login'));
                            $response['token'] = $token;
                            $response['user'] = $user;
                            $this->response($response, 200);
                        }
                        $this->response(get_error_response($this->lang->line('user_error_found')), 404);
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }
        }

    }

    public function logout_post()
    {
        $token_get = $this->input->post('token',true);

        $params = array('token'=>$token_get);
        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_USER_ACTION_LOGOUT;

        save_log_api($params,$email,$uri,$method,$action);
        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);

        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {
            $id_user =  $token_model->get_user_with_token($token_get);
            if(isset($id_user))
            {
                $close_session = $token_model->update_token_close_session($token_get);

                if ($close_session)
                {
                    $this->response(get_success_response($this->lang->line('user_success_close_session')),200);
                } else
                {
                    $this->response(get_error_response($this->lang->line('user_error_close_session')),404);
                }
            } else
            {
                 $this->response(get_error_response($this->lang->line('user_error_found')), 404);
            }
        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')), 404);
        }
    }

    public function detail_post()
    {
        $token_get = $this->input->post('token',true);

        $params = array('token'=>$token_get);
        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_USER_ACTION_DETAIL;

        save_log_api($params,$email,$uri,$method,$action);

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);

        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {
            $user =  $token_model->get_user_country_with_token($token_get);

            if(isset($user))
            {
                $id_campaign = $user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {
                        $user_model = new User_Model();
                        $user_detail = $user_model->get_detail($user->user_id);

                        if (isset($user_detail)) {
                            $response = get_success_response($this->lang->line('user_success_detail'));
                            $response['user'] = $user_detail;
                            $this->response($response, 200);
                        } else {
                            $this->response(get_error_response($this->lang->line('user_error_get_detail')),404);
                        }
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }
        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }

    }

    public function points_post(){
        $token_get = $this->input->post('token',true);

        $params = array('token'=>$token_get);
        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_USER_ACTION_POINTS;

        save_log_api($params,$email,$uri,$method,$action);

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token) )
        {
            $user =  $token_model->get_user_country_with_token($token_get);

            if(isset($user))
            {
                $id_campaign = $user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {

                        $user_model = new User_Model();
                        $points = $user_model->get_points($user->user_id);

                        if (isset($points)) {
                            $response = get_success_response($this->lang->line('user_success_points'));
                            $response['points'] = $points->points;
                            $this->response($response, 200);
                        } else {
                            $this->response(get_error_response($this->lang->line('user_error_get_points')),404);
                        }
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }
        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }


    }

    public function store_post()
    {

        $rules = [
            [
                'field' => 'name',
                'rules' => 'required',
                'errors' => [
                    'required' => $this->lang->line('user_form_name_required')
                ]
            ],

            [
                'field' => 'email',
                'rules' => 'required|valid_email|is_unique[users.email]',
                'errors' => [
                    'required' => $this->lang->line('user_form_email_required'),
                    'valid_email' => $this->lang->line('user_form_email_valid'),
                    'is_unique' => 'El email ya se encuentra registrado'
                ]
            ],
            [
                'field' => 'dni',
                'rules' => 'required|exact_length[8]|numeric|is_unique[users.dni]',
                'errors' => [
                    'required' => $this->lang->line('user_form_dni_required'),
                    'numeric' => $this->lang->line('user_form_dni_numeric'),
                    'exact_length' => $this->lang->line('user_form_dni_length'),
                    'is_unique' => 'El dni ya se encuentra registrado'
                ]
            ],
            [
                'field' => 'phone',
                'rules' => 'required',
                'errors' => [
                    'required' => $this->lang->line('user_form_phone_required')

                ]
            ]
        ];
        $this->form_validation->set_rules($rules);

        $name = $this->input->post('name',TRUE);
        $email = $this->input->post('email',TRUE);
        $dni = $this->input->post('dni',TRUE);
        $phone = $this->input->post('phone',TRUE);
        //$fecha_compra= $this->input->post('fecha_compra',TRUE);
        //$tienda_compra= $this->input->post('tienda_compra',TRUE);
        $personal_data= 1; //$this->input->post('datos',TRUE);

        if($personal_data == 'on') $personal_data = 1;


        $user_model = new User_Model();
        if ($_SERVER['HTTP_X_FORWARDED_FOR']){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        
        /*$geolocation = get_geolocation($ip);
        $location = explode(";",$geolocation);
        $code_country = "-";

        if ($location[0] == "OK")
        {
            $code_country = $location[3];
        }*/

        //borrar luego y cambiar por el dato remoto
        $code_country = 'PE';

        $params = array(
            'name'=>$name,
            'email'=>$email,
            'dni'=>$dni,
            'phone'=>$phone,
            //'fecha_compra' => $fecha_compra,
            //'tienda_compra' => $tienda_compra,
            'personal_data' => $personal_data,
            'country'=>$code_country,
            'ip'=>$ip
        );

        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_USER_ACTION_REGISTER;

        save_log_api($params,$email,$uri,$method,$action);

        if ($this->form_validation->run() == FALSE)
        {
            $errors_message = $this->form_validation->error_array();
            $error_response = get_error_form($errors_message);
            $this->response($error_response, 200);
        }
        else
        {

            $campaign = validate_campaign();

            if ($campaign['result'] == 1) {
                $campaign = $campaign['campaign'];

                    $country = $campaign[0]->country_code;
                    $id_campaign = $campaign[0]->id;

                    if ($code_country == $country) {
                        $user_dni = $user_model->find_by_dni($id_campaign, $email);

                        if (!isset($user_dni->id)) {

                            $user_email = $user_model->find_by_email($id_campaign, $email);

                            if (!isset($user_email->id)) {
                              // public function create($id_campaign, $name , $email, $password, $phone, $fecha_compra, $tienda_compra, $country, $personal_data)
                              //  public function create($id_campaign,$email, $name, $dni, $phone, $country, $personal_data
                                $user = $user_model->create($id_campaign, $email , $name, $dni, $phone, $code_country, $personal_data);
                                $id_user = $user->id;
                                if (isset($id_user)) {
                                    $token_model = new Token_Model();
                                    $token = $token_model->generate_token();
                                    $id_token = $token_model->save_token($token, $user->id);

                                    if (isset($id_token)) {
                                        $response = get_success_response($this->lang->line('user_success_created'));
                                        $response['token'] = $token;
                                        $response['user'] = $user;
                                        $this->response($response, 200);
                                    }
                                    $this->response(get_error_response($this->lang->line('user_error_created')),200);
                                }
                                $this->response(get_error_response($this->lang->line('user_error_created')),200);
                            } else {
                                $this->response(get_error_response($this->lang->line('user_error_exist_email')),200);
                            }
                        } else {
                            $this->response(get_error_response('El email ya ha sido registrado anteriormente.'),200);
                        }
                    } else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),200);
                    }
            } else
            {
                $this->response(get_error_response($campaign['message']),200);
            }
        }
    }

    public function share_facebook_post(){
        $token_get = $this->input->post('token',true);

        $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_USER_ACTION_SHARE_FACEBOOK;

        save_log_api($params,$email,$uri,$method,$action);

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {
            $id_user =  $token_model->get_user_country_with_token($token_get);
            if(isset($id_user))
            {
                $id_campaign = $id_user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {

                        $user_model = new User_Model();
                        $user = $user_model->check_share_facebook($id_user->user_id);
                        if (isset($user)) {
                            if ($user->share != SHARE_FACEBOOK) {
                                $raffle_model = new Raffle_Model();
                                $raffle = $raffle_model->register_user_raffle(POINTS_SHARE, $id_user->user_id);
                                if (isset($raffle)) {
                                    $points = $user->points + POINTS_SHARE;
                                    $share_facebook = $user_model->accumulate_points_user_share($id_user->user_id, $points);
                                    if (isset($share_facebook)) {
                                        $response = get_success_response($this->lang->line('user_success_accumulated'));
                                        $response['points'] = $points;
                                        $this->response($response, 200);
                                    } else {
                                        $this->response(get_error_response($this->lang->line('concourse_error_points')),404);
                                    }
                                } else {
                                    $this->response(get_error_response($this->lang->line('concourse_error_raffle')),404);
                                }
                            } else {
                                $this->response(get_error_response($this->lang->line('user_error_shared_facebook')),404);
                            }
                        } else {
                            $this->response(get_error_response($this->lang->line('user_error_get_points')),404);
                        }
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }
        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }
    }


    public function share_meme_post(){
        $token_get = $this->input->post('token',true);
        $share_option = $this->input->post('option',true); //1:facebook 2:instagram 3:whatsapp
        $lote = $this->input->post('lote',true);
        $meme_id = $this->input->post('meme',true);

        $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        if($share_option == '1')
            $action = API_USER_ACTION_SHARE_FACEBOOK;
        else if($share_option == '2')
            $action = API_USER_ACTION_SHARE_INSTAGRAM;
        else
            $action = API_USER_ACTION_SHARE_WHATSAPP;

        save_log_api($params,$email,$uri,$method,$action);

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {
            $id_user =  $token_model->get_user_country_with_token($token_get);
            if(isset($id_user))
            {
                $id_campaign = $id_user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {


                        if($lote != ""){

                            $user_model = new User_Model();
                            $total_lote = $user_model->get_total_share_meme($id_user->user_id, $lote);

                            
                            //Total por Lote
                            if ($total_lote <= API_USER_TOTAL_MEMES_POR_LOTE) {

                                    $raffle_model = new Raffle_Model();

                                    $queryMeme = array(
                                        "select"=>"*",
                                        "where"=>"id='".$meme_id."'"
                                    );

                                    $meme = $this->meme_model->get_search_row($queryMeme);

                                    if(isset($meme->id)){

                                        $points = POINTS_SHARE_MEME;
                                        if($meme->user_id == $id_user->user_id)
                                            $points = $points + POINTS_SHARE_MEME_OWNER;

                                        $raffle = $raffle_model->register_user_raffle_meme($points, $id_user->user_id, $meme_id, $lote);
                                        if (isset($raffle)) {
                                            $total_points = $id_user->points + $points;
                                            $share_meme = $user_model->accumulate_points_user_share($id_user->user_id, $total_points);
                                            if (isset($share_meme)) {

                                                $response = get_success_response($this->lang->line('user_success_accumulated'));
                                                $response['points'] = $total_points;

                                                $winner_model = new Winner_Model();

                                                $total_prizes_day_campaign = $winner_model->total_winner_by_day($campaign->id);

                                                
                                                if ($total_prizes_day_campaign < $campaign->prize_day) {

                                                    $prize_model = new Prize_Model();
                                                    $prizes = $prize_model->get_total_prizes_instantwin($id_campaign);
                                                    
                                                        

                                                    if(isset($prizes)){

                                                        $winner = $winner_model->find_winner_by_user($id_user->user_id);
                                                        

                                                        if (!isset($winner)) {
                                                            $number_random_1 = rand(1, $campaign->random);
                                                            $number_random_2 = rand(1, $campaign->random);

                                                            

                                                            if ($number_random_1 == $number_random_2) {
                                                                $win = WIN;
                                                                $prize_win = $prize_model->update_status_prize($prizes->id);
                                                                $winner_win = $winner_model->register_winner($id_campaign,$id_user->user_id,$prizes->id);
                                                                $user_win = $user_model->update_instant_winner_user($id_user->user_id);
                                                                $response['win_prize'] = $win;
                                                                $response['prize'] = $prizes;
                                                            } else {
                                                                $win = LOSE;
                                                            }
                                                            $response['win_prize'] = $win;
                                                            $this->response($response, 200);

                                                        } else {
                                                            $response['win_prize'] = LOSE;
                                                            $this->response($response, 200);
                                                        }
                                                    } else {
                                                        $response['win_prize'] = LOSE;
                                                        $this->response($response, 200);
                                                    }
                                                } else {
                                                    $response['win_prize'] = LOSE;
                                                    $this->response($response, 200);
                                                }

                                            } else {
                                                $this->response(get_error_response($this->lang->line('concourse_error_points')),404);
                                            }
                                        } else {
                                            $this->response(get_error_response($this->lang->line('concourse_error_raffle')),404);
                                        }

                                    }
                                    else{
                                        $this->response(get_error_response('El meme no se encuentra registrado'),404);
                                    }
                                    
                            
                            } else {
                                $this->response(get_error_response("Este lote ya ha sido registrado muchas veces, vuelve a intentarlo con otro lote"),404);
                            }

                        }
                        else{
                            $this->response(get_error_response("El lote no puede ser vacío, ingresa un lote válido."),404);
                        }

                        
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }
        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }
    }


    public function share_lote_post(){
        $token_get = $this->input->post('token',true);
        //$share_option = $this->input->post('option',true); //1:facebook 2:instagram 3:whatsapp
        $lote = $this->input->post('lote',true);
        //$meme_id = $this->input->post('meme',true);

        $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        
        //if($share_option == '1')
        //    $action = API_USER_ACTION_SHARE_FACEBOOK;
        //else if($share_option == '2')
        //    $action = API_USER_ACTION_SHARE_INSTAGRAM;
        //else
        //    $action = API_USER_ACTION_SHARE_WHATSAPP;
        $action = API_USER_ACTION_REGISTER_LOTE;
        save_log_api($params,$email,$uri,$method,$action);

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {
            $id_user =  $token_model->get_user_country_with_token($token_get);
            if(isset($id_user))
            {
                $id_campaign = $id_user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {

                        if($lote != ""){

                            $user_model = new User_Model();
                            $total_lote = $user_model->get_total_share_meme($id_user->user_id, $lote);

                            
                            //Total por Lote
                            if ($total_lote <= API_USER_TOTAL_MEMES_POR_LOTE) {

                                    $raffle_model = new Raffle_Model();

                                    $queryMeme = array(
                                        "select"=>"*",
                                        "where"=>"id='".$meme_id."'"
                                    );

                                    //$meme = $this->meme_model->get_search_row($queryMeme);

                                    //if(isset($meme->id)){

                                        $points = POINTS_SHARE_MEME;
                                        //if($meme->user_id == $id_user->user_id)
                                        //  $points = $points + POINTS_SHARE_MEME_OWNER;

                                        $raffle = $raffle_model->register_user_raffle_meme($points, $id_user->user_id, null, $lote);
                                        if (isset($raffle)) {
                                            $total_points = $id_user->points + $points;
                                            $share_meme = $user_model->accumulate_points_user_share($id_user->user_id, $total_points);
                                            if (isset($share_meme)) {

                                                $response = get_success_response($this->lang->line('user_success_accumulated'));
                                                $response['points'] = $total_points;

                                                $winner_model = new Winner_Model();

                                                $total_prizes_day_campaign = $winner_model->total_winner_by_day($campaign->id);

                                                
                                                if ($total_prizes_day_campaign < $campaign->prize_day) {

                                                    $prize_model = new Prize_Model();
                                                    $prizes = $prize_model->get_total_prizes_instantwin($id_campaign);
                                                    
                                                        

                                                    if(isset($prizes)){

                                                        $winner = $winner_model->find_winner_by_user($id_user->user_id);
                                                        

                                                        if (!isset($winner)) {
                                                            $number_random_1 = rand(1, $campaign->random);
                                                            $number_random_2 = rand(1, $campaign->random);

                                                            

                                                            if ($number_random_1 == $number_random_2) {
                                                                $win = WIN;
                                                                $prize_win = $prize_model->update_status_prize($prizes->id);
                                                                $winner_win = $winner_model->register_winner($id_campaign,$id_user->user_id,$prizes->id);
                                                                $user_win = $user_model->update_instant_winner_user($id_user->user_id);
                                                                $response['win_prize'] = $win;
                                                                $response['prize'] = $prizes;
                                                            } else {
                                                                $win = LOSE;
                                                            }
                                                            $response['win_prize'] = $win;
                                                            $this->response($response, 200);

                                                        } else {
                                                            $response['win_prize'] = LOSE;
                                                            $this->response($response, 200);
                                                        }
                                                    } else {
                                                        $response['win_prize'] = LOSE;
                                                        $this->response($response, 200);
                                                    }
                                                } else {
                                                    $response['win_prize'] = LOSE;
                                                    $this->response($response, 200);
                                                }

                                            } else {
                                                $this->response(get_error_response($this->lang->line('concourse_error_points')),404);
                                            }
                                        } else {
                                            $this->response(get_error_response($this->lang->line('concourse_error_raffle')),404);
                                        }

                                    //}
                                    //else{
                                    //    $this->response(get_error_response('El meme no se encuentra registrado'),404);
                                    //}
                                    
                            
                            } else {
                                $this->response(get_error_response("Este lote ya ha sido registrado muchas veces, vuelve a intentarlo con otro lote"),404);
                            }


                        }
                        else{
                            $this->response(get_error_response("El lote no puede ser vacío, ingresa un lote válido."),404);
                        }

                        
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }
        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }
    }


    public function list_memes_get($pagination = 1){

        $campaign = validate_campaign();

        if ($campaign['result'] == 1) {
            $campaign = $campaign['campaign'];
            $id_campaign = $campaign[0]->id;

            $params = array(
                'pagination'=>$pagination
            );

            $email = "";
            $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
            $method =  NAME_METHOD_GET;
            $action = API_WINNER_ACTION_LIST_MEMES;

            save_log_api($params,$email,$uri,$method,$action);

            //$winner_model = new Winner_Model();
            $query_category = array(
                "select"=>"*",
                "where"=>"status='1'"
            );

            $memeCategories = $this->meme_category_model->search($query_category);

            $arrayTotalMemes = array();

            foreach($memeCategories as $itemCategory){
                $arrayMemes =  $this->meme_model->get_list_memes_by_category($itemCategory->id);

                $arrayTotalMemes[] = array(
                    "category"=>$itemCategory->title,
                    "memes"=>$arrayMemes
                );
            }

            $response = get_success_response($this->lang->line('user_success_list_memes'));
            $response['memes']=$arrayTotalMemes;
            $this->response($response,200);

            /*
            $users =  $this->meme_model->get_list_memes($pagination,$id_campaign);
            if (isset($users))
            {
                if (sizeof($users['data_users']) > 0)
                {
                        $response = get_success_response($this->lang->line('user_success_list_memes'));
                        $response['memes']=$users['data_users'];
                        $response['pagination_next']=$users['pagination_next'];
                        $response['pagination_total']=$users['users_total'];
                        $this->response($response,200);
                } else
                {
                    $this->response(get_error_response($this->lang->line('user_error_list_memes_empty')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_list_memes')),404);
            }
            */
        } else
        {
            $this->response(get_error_response($campaign['message']),404);
        }
    }


    public function list_memes_by_user_post($pagination = 1){

        $token_get = $this->input->post('token',true);

        $campaign = validate_campaign();

        if ($campaign['result'] == 1) {
            $campaign = $campaign['campaign'];
            $id_campaign = $campaign[0]->id;

            $params = array(
                'pagination'=>$pagination
            );

            $email = "";
            $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
            $method =  NAME_METHOD_GET;
            $action = API_WINNER_ACTION_LIST_MEMES;

            save_log_api($params,$email,$uri,$method,$action);

            $token_model = new Token_Model();
            $token = $token_model->validation_token($token_get);
            if (isset($token_get) && strlen($token_get) > 0 && isset($token))
            {
                $id_user =  $token_model->get_user_country_with_token($token_get);
                if(isset($id_user))
                {
                    /*
                    $query_category = array(
                        "select"=>"*",
                        "where"=>"status='1'"
                    );
        
                    $memeCategories = $this->meme_category_model->search($query_category);
        
                    $arrayTotalMemes = array();
        
                    foreach($memeCategories as $itemCategory){
                        //$arrayMemes =  $this->meme_model->get_list_memes_by_category($itemCategory->id);
                        $arrayMemes =  $this->meme_model->get_list_memes_by_user_and_category($pagination, $id_user->user_id, $itemCategory->id);
        
                        $arrayTotalMemes[] = array(
                            "category"=>$itemCategory->title,
                            "memes"=>$arrayMemes
                        );
                    }
                    */

                    //$users =  $this->meme_model->get_list_memes_by_user($pagination,$id_campaign, $id_user->user_id);
                    
                    $arrayMemes =  $this->raffle_model->get_list_memes_by_user($pagination, $id_user->user_id);


                    if (isset($arrayMemes))
                    {
                        ////if (sizeof($users['data_users']) > 0)
                        ////{
                        $response = get_success_response($this->lang->line('user_success_list_memes'));
                        $response['memes']=$arrayMemes['data_users'];
                        $response['pagination_next']=$arrayMemes['pagination_next'];
                        $response['pagination_total']=$arrayMemes['users_total'];
                        $this->response($response,200);
                        ////} else
                        ////{
                        ////    $this->response(get_error_response($this->lang->line('user_error_list_memes_empty')),404);
                        ////}
                    } else
                    {
                        $this->response(get_error_response($this->lang->line('user_error_list_memes')),404);
                    }
                }
                else
                {
                    $this->response(get_error_response($this->lang->line('user_error_found')),404);
                }
            }else
            {
                $this->response(get_error_response($this->lang->line('user_error_token')),401);
            }





            //$winner_model = new Winner_Model();
            
        } else
        {
            $this->response(get_error_response($campaign['message']),404);
        }
    }

    public function recover_account_post()
    {
        $rules = [
            [
                'field' => 'email',
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => $this->lang->line('user_form_email_required'),
                    'valid_email' => $this->lang->line('user_form_email_valid')
                ]
            ]
        ];

        $this->form_validation->set_rules($rules);
        $params=array();
        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_USER_ACTION_RECOVER_ACCOUNT;

        $email = $this->post('email');

        $params = array(
            'email'=>$email
        );

        save_log_api($params,$email,$uri,$method,$action);

        if ($this->form_validation->run() == FALSE)
        {
            $errors_message = $this->form_validation->error_array();
            $error_response = get_error_form($errors_message);
            $this->response($error_response, 400);
        } else
        {
            $user_model = new User_Model();
            $user = $user_model->find_email_by_user($email);

            if (isset($user->id))
            {
                $response = get_success_response($this->lang->line('photo_success_list'));
                $response['user'] = $user;
                $this->response($response, 200);
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }
        }
    }

}
