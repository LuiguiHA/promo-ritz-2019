<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";

class Challenge_Controller extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->lang->load('api_error_messages_lang', 'spanish');
        $this->lang->load('api_messages_lang', 'spanish');
        $this->load->model('challenge_Model', 'challenge_model');
        $this->load->model('concourse_Model', 'concourse_model');
        $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('token_Model', 'token_model');
    }

    public function list_post()
    {
        $token_get = $this->input->post('token',true);

        $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_CHALLENGE_ACTION_LIST;

        save_log_api($params,$email,$uri,$method,$action);

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {
            $user = $token_model->get_user_country_with_token($token_get);
            if (isset($user))
            {
                $id_campaign = $user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {

                        $id_user = $user->user_id;
                        $campaign = validate_campaign();

                        $concourse_model = new Concourse_Model();
                        $won_per_day = $concourse_model->won_per_day($id_user);
                        //if ($won_per_day < MAX_WON_PER_DAY) {
                            if ($won_per_day < MAX_WON_PER_DAY){
                                $win_day = STATUS_ACTIVE;
                            }else{
                                $win_day = STATUS_INACTIVE;
                            }

                            if ($campaign['result'] == 1) {
                                $campaign = $campaign['campaign'];
                                $id_campaign = $campaign[0]->id;
                                $challenge_model = new Challenge_Model();
                                $challenge_list = $challenge_model->get_list_Challenge($id_campaign, $id_user);

                                if (isset($challenge_list)) {
                                    if (sizeof($challenge_list) > 0) {
                                        $response = get_success_response($this->lang->line('challenge_success_list'));
                                        $response['challenges'] = $challenge_list;
                                        $response['win_day'] = $win_day;
                                        $this->response($response, 200);
                                    } else {
                                        $this->response(get_error_response($this->lang->line('challenge_error_list_empty')),404);
                                    }
                                } else {
                                    $this->response(get_error_response($this->lang->line('challenge_error_list')),404);
                                }

                            } else {
                                $this->response(get_error_response($campaign['message']),404);
                            }
                       /* } else {
                            $this->response(get_error_response($this->lang->line('concourse_error_max_won_per_day'), 404));
                        }*/
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }

        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }

    }

}