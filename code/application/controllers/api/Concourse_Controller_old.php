<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";

class Concourse_Controller extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->lang->load('api_error_messages_lang', 'spanish');
        $this->lang->load('api_messages_lang', 'spanish');
        $this->load->model('concourse_Model', 'concourse_model');
        $this->load->model('challenge_Model', 'challenge_model');
        $this->load->model('user_Model', 'user_model');
        $this->load->model('prize_Model', 'prize_model');
        $this->load->model('raffle_Model', 'raffle_model');
        $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('winner_Model', 'winner_model');
        $this->load->model('token_Model', 'token_model');
    }

    public function store_post()
    {
        $token_get = $this->input->post('token',true);

        $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action =    API_CONCOURSE_ACTION_REGISTER;

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {

            $rules = [
                [
                    'field' => 'id_challenge',
                    'rules' => 'required|numeric|is_natural',
                    'errors' => [
                        'required' => $this->lang->line('concourse_error_challenge_required'),
                        'numeric' =>  $this->lang->line('concourse_error_challenge_numeric'),
                        'is_natural' =>  $this->lang->line('concourse_error_challenge_natural')
                    ]
                ],
                [
                    'field' => 'time',
                    'rules' => 'required|numeric|is_natural',
                    'errors' => [
                        'required' => $this->lang->line('concourse_error_time_required'),
                        'numeric' =>  $this->lang->line('concourse_error_time_numeric'),
                        'is_natural' =>  $this->lang->line('concourse_error_time_natural')
                    ]
                ],
                [
                    'field' => 'win',
                    'rules' => 'required|numeric|is_natural',
                    'errors' => [
                        'required' => $this->lang->line('concourse_error_win_required'),
                        'numeric' =>  $this->lang->line('concourse_error_win_numeric'),
                        'is_natural' =>  $this->lang->line('concourse_error_win_natural')
                    ]
                ]

            ];

            $this->form_validation->set_rules($rules);

            $id_challenge = $this->post('id_challenge');
            $time = $this->post('time');
            $win = $this->post('win');

            $params = array(
                'token'=>$token_get,
                'id_challenge'=>$id_challenge,
                'time'=>$time,
                'win'=>$win
            );

            save_log_api($params,$email,$uri,$method,$action);

            if ($this->form_validation->run() == FALSE)
            {
                $errors_message = $this->form_validation->error_array();
                $error_response = get_error_form($errors_message);
                $this->response($error_response, 400);
            } else
            {


                $user = $token_model->get_user_country_with_token($token_get);
                if (isset($user))
                {
                    $id_campaign = $user->campaign_id;
                    $campaign_model = new Campaign_Model();

                    $campaign  = $campaign_model->get_campaign($id_campaign);

                    if (isset($campaign)) {
                        if ($campaign->status == STATUS_ACTIVE) {

                            $id_user = $user->user_id;

                            $concourse_model = new Concourse_Model();

                            if ($win == WIN && $time > 0) {
                                $challenge_model = new Challenge_Model();

                                $challenge_validation_user_win = $challenge_model->validate_user_win_challenge($id_user, $id_challenge);

                                if (isset($challenge_validation) || sizeof($challenge_validation_user_win) >= 0) {

                                    if ($challenge_validation_user_win->allow < MAX_WON_PER_CHALLENGE) {
                                        $won_per_day = $concourse_model->won_per_day($id_user);
                                        if ($won_per_day < MAX_WON_PER_DAY) {
                                            $user_model = new User_Model();
                                            $user = $user_model->find_prize_by_user($id_user);

                                            if (isset($user)) {
                                                $raffle_model = new Raffle_Model();
                                                $raffle = $raffle_model->register_user_raffle(POINTS_CONCOURSE, $id_user);

                                                if (isset($raffle)) {
                                                    $points = $user->points + POINTS_CONCOURSE;
                                                    $accumulate_points = $user_model->accumulate_points_user($id_user, $points);

                                                    if (isset($accumulate_points)) {
                                                        $winner_model = new Winner_Model();

                                                        $total_prizes_day_campaign = $winner_model->total_winner_by_day($campaign->id);

                                                        if ($total_prizes_day_campaign < $campaign->prize_day) {

                                                            $prize_model = new Prize_Model();
                                                            $prizes = $prize_model->get_total_prizes_active($id_campaign);

                                                            if ($prizes > 0) {

                                                                $winner = $winner_model->find_winner_by_user($id_user);
                                                                if (!isset($winner)) {
                                                                    $number_random_1 = rand(1, $campaign->random);
                                                                    $number_random_2 = rand(1, $campaign->random);

                                                                    $response = $this->register_concourse($id_user, $id_challenge, $time, $win);

                                                                    $campaign = validate_campaign();

                                                                    if ($campaign['result'] == 1) {
                                                                        $campaign = $campaign['campaign'];
                                                                        $country_code = $campaign[0]->country_code;

                                                                        if ($country_code == $user->country) {

                                                                            if ($number_random_1 == $number_random_2) {
                                                                                $win = WIN;

                                                                                $user_win = $user_model->update_win_user($id_user);

                                                                                if (!isset($user_win)) {
                                                                                    $this->response(get_error_response($this->lang->line('concourse_error_user_win')), 404);
                                                                                }
                                                                            } else {
                                                                                $win = LOSE;
                                                                            }
                                                                            $response['win_prize'] = $win;
                                                                            $this->response($response, 200);
                                                                        } else {
                                                                            $this->response(get_success_response($this->lang->line('concourse_success_create')), 200);
                                                                        }

                                                                    } else {
                                                                        $this->response(get_error_response($campaign['message']), 404);
                                                                    }
                                                                } else {
                                                                    // $this->response(get_error_response($this->lang->line('concourse_error_user_prize')),404);

                                                                    $response = $this->register_concourse($id_user, $id_challenge, $time, $win);
                                                                    $this->response($response, 200);
                                                                }
                                                            } else {
                                                                $response = $this->register_concourse($id_user, $id_challenge, $time, $win);
                                                                $this->response($response, 200);
                                                            }
                                                        } else {
                                                            $response = $this->register_concourse($id_user, $id_challenge, $time, $win);
                                                            $this->response($response, 200);
                                                        }
                                                    } else {
                                                        $this->response(get_error_response($this->lang->line('concourse_error_points')), 404);
                                                    }
                                                } else {
                                                    $this->response(get_error_response($this->lang->line('concourse_error_raffle')), 404);
                                                }
                                            } else {
                                                $this->response(get_error_response($this->lang->line('concourse_error_user')), 404);
                                            }
                                        } else {
                                            $this->response(get_error_response($this->lang->line('concourse_error_max_won_per_day')), 404);
                                        }
                                    } else {
                                        $this->response(get_error_response($this->lang->line('challenge_error_user_challenge')),404);
                                    }
                                } else {
                                    $this->response(get_error_response($this->lang->line('challenge_error_validation_user_win')),404);
                                }
                            } else {
                                $response = $this->register_concourse($id_user, $id_challenge, $time, $win);
                                $this->response($response, 200);
                            }
                        }else
                        {
                            $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                        }
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                    }
                } else
                {
                    $this->response(get_error_response($this->lang->line('user_error_found')),404);
                }

            }
        } else
        {
            save_log_api($params,$email,$uri,$method,$action);
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }
    }

    public function register_concourse($id_user, $id_challenge, $time, $win)
    {
        $concourse_model = new Concourse_Model();
        $concourse = $concourse_model->create($id_user, $id_challenge, $time, $win);

        $response = null;
        if (isset($concourse))
        {
            $response = get_success_response($this->lang->line('concourse_success_create'));

        } else
        {
            $this->response(get_error_response($this->lang->line('concourse_error_create')),404);
        }

        return $response;
    }

}
