<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "/libraries/REST_Controller.php";

class Meme_Controller extends REST_Controller {
  public function __construct()
  {
      parent::__construct();
      $this->load->model('token_Model','token_model');
      $this->load->model('meme_Model','meme_model');
      $this->load->model('User_Model','user_Model');
  }

  public function validate_lote_post(){
    $token_get = $this->input->post('token',true);
    $lote = $this->input->post('lote',true);

    $token_model = new Token_Model();
    $token = $token_model->validation_token($token_get);
    if (isset($token_get) && strlen($token_get) > 0 && isset($token))
    {
      
      $id_user =  $token_model->get_user_with_token($token_get);

      $user_model = new User_Model();
      $total_lote = $user_model->get_total_share_meme($id_user->user_id, $lote);
      
      $this->response(array("valid" => $total_lote <= API_USER_TOTAL_MEMES_POR_LOTE), 200); 

    }
  }


  public function save_meme_image_post()
  {
    $token_get = $this->input->post('token',true);

    $token_model = new Token_Model();
    $token = $token_model->validation_token($token_get);
    if (isset($token_get) && strlen($token_get) > 0 && isset($token))
    {
      
      

        $config['upload_path']          = './assets/';
       // $config['encrypt_name']          = true;
        $config['allowed_types']        = '*';
        $config['max_size']             = 1143540;
        $uniqid = uniqid();
        $config['file_name']            = "meme-".$uniqid.".png";

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('image'))
        {
          

          $result = array("success" => false, "file" => $this->upload->display_errors());
          $this->response($result, 400);
        }
        else
        {

          $dataUpload = $this->upload->data();
          
          $configImage['image_library'] = 'gd2';
          $configImage['source_image'] = './assets/'.$this->upload->data('raw_name').$this->upload->data('file_ext');  ///'./assets/'.$config['file_name'];
          $configImage['maintain_ratio'] = TRUE;
          //$configImage['create_thumb'] = TRUE;
          $configImage['quality'] = "30%";
          $configImage['width']         = 600;
          $configImage['height']       = 600;
          $configImage['new_image'] = './assets/'.$this->upload->data('raw_name').$this->upload->data('file_ext');
          
          $this->load->library('image_lib', $configImage);
          
          $this->image_lib->resize();
          


          $result = array("success" => true, "name" => substr($this->upload->data('raw_name').$this->upload->data('file_ext'), 0, -4));
          $file_path = '/assets/'.$this->upload->data('raw_name').$this->upload->data('file_ext');  //substr($config['upload_path'].$config['file_name'], 1);

          $result["path"] = $file_path;

          $this->response($result, 200);
        }  

      

    } 
    else 
    {
      $this->response(get_error_response($this->lang->line('user_error_token')),401);
    }

  }

  public function save_meme_post()
  {
    $token_get = $this->input->post('token',true);
    $lote = $this->input->post('lote',true);

    $token_model = new Token_Model();
    $token = $token_model->validation_token($token_get);
    if (isset($token_get) && strlen($token_get) > 0 && isset($token))
    {
      
      

          $file_path = $this->input->post('path',true);

          $meme_model = new meme_Model();

          $user =  $token_model->get_user_country_with_token($token_get);

          $meme_data["category_id"] = 4;
          $meme_data["admin_id"] = 0;
          $meme_data["user_id"] = $user->user_id;
          $meme_data["name"] = "user-generated";
          $meme_data["register_at"] = date("Y-m-d H:i:s");
          $meme_data["photo"] = $file_path;
          $meme_data["status"] = 1;

          $meme_id = $meme_model->save($meme_data);

          $result["meme"] = $meme_id;

          $this->response($result, 200);

    } 
    else 
    {
      $this->response(get_error_response($this->lang->line('user_error_token')),401);
    }
  }
}
