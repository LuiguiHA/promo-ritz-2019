<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";

class Question_Controller extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->lang->load('api_error_messages_lang', 'spanish');
        $this->lang->load('api_messages_lang', 'spanish');
        $this->load->model('question_Model','question_model');
    }

    public function list_questions_get()
    {
        $params = array(
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_GET;
        $action = API_QUESTION_ACTION_LIST;

        save_log_api($params,$email,$uri,$method,$action);

                $campaign = validate_campaign();

                if ($campaign['result'] == 1) {
                        $campaign = $campaign['campaign'];
                        $id_campaign = $campaign[0]->id;
                        $question_model = new Question_Model();
                         $questions = $question_model->get_questions_by_campaign($id_campaign);
                
                        if (isset($questions))
                        {
                                $response = get_success_response($this->lang->line('question_success_list'));
                                $response['questions'] = $questions;
                                $this->response($response, 200);

                        } else
                        {
                            $this->response(get_error_response($this->lang->line('question_error_list')),404);
                        }

                } else
                {
                    $this->response(get_error_response($campaign['message']),404);
                }
    }

}