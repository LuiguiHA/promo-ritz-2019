<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";

class Prize_Controller extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->lang->load('api_error_messages_lang', 'spanish');
        $this->lang->load('api_messages_lang', 'spanish');
        $this->load->model('concourse_Model', 'concourse_model');
        $this->load->model('user_Model', 'user_model');
        $this->load->model('winner_Model', 'winner_model');
        $this->load->model('campaign_Model', 'campaign_model');
        $this->load->model('prize_Model', 'prize_model');
        $this->load->model('Prizes_Sale_Model', 'prize_sale_model');
        $this->load->model('raffle_Prize_Model', 'raffle_prize_model');
        $this->load->model('token_Model', 'token_model');
        $this->load->model('Sale_Model', 'user_prizes_sale_model');//
        //$this->load->model('User_Prizes_Sale_Model', 'user_prizes_sale_model');
    }

    public function list_winners_get()
    {
        $arrayQueryWinner = array(
          "select"=>"users.name, users.dni, prizes_sale.description as premio, user_prizes_sale.points as points, DATE_FORMAT(user_prizes_sale.register_at, '%d/%m/%Y') as fecha_registro",
          "join"=>array(
            "user_prizes_sale, user_prizes_sale.user_id = users.id",
            "prizes_sale, prizes_sale.id = user_prizes_sale.prizes_sales_id"
            ),
          "where"=>"users.win = '1' AND user_prizes_sale.winner = '1'"
        );

        $arrayWinners = $this->user_model->search($arrayQueryWinner);

        $arrayResponse = array(
            "result"=>"1",
            "mensaje"=>"",
            "usuarios"=>$arrayWinners
            );

        $this->response($arrayResponse, 200);
    }

    public function prize_sale_status_get()
    {

        $prizeSaleId = $this->get("prize_sales_id",TRUE);
        $arrayQueryPremiosEnSubastaUsuario = array(
            "select"=>"users.name, MAX(user_prizes_sale.points) as points",
            "join"=>array(
                "prizes_sale, prizes_sale.id = user_prizes_sale.prizes_sales_id",
                "users, users.id = user_prizes_sale.user_id"
                ),
            "where"=>"prizes_sale.id = '".$prizeSaleId."'",
            "group"=>"prizes_sale.id, prizes_sale.description, users.id, users.dni, users.email, users.phone, users.name",
            "order"=>"MAX(user_prizes_sale.points) DESC",
            "limit"=>"10"
        );

        $arrayUsuariosEnSubasta = $this->user_prizes_sale_model->search($arrayQueryPremiosEnSubastaUsuario);

        $arrayResponse = array(
            "result"=>"1",
            "mensaje"=>"",
            "usuarios"=>$arrayUsuariosEnSubasta
            );

        $this->response($arrayResponse, 200);
    }

    public function start_sale_post()
    {
        $token_get = $this->input->post('token',true);
        $token_model = new Token_Model();
        
        $arrayResponse = array(
            "result"=>"1",
            "mensaje"=>""
            );

        $fechaHora = date("Y-m-d H:i:s");

        $token = $token_model->validation_token($token_get);
          if (isset($token_get) && strlen($token_get) > 0 && isset($token))
          {
            $user = $token_model->get_user_country_with_token($token_get);

            if (isset($user))
            {
                

                if($user->win == null || $user->win == '0')
                {


                    $prizeSaleId = $this->post("prize_sales_id",TRUE);
                    $puntos = $this->post("puntos",TRUE);
                    //$prizeSaleId = $this->post("prize_sales_id",TRUE);
                    $puntosUsuario = ($user->points != null && is_numeric($user->points))?$user->points:0;
                    $puntosUsuarioEnSubasta = ($user->points_sale != null &&  is_numeric($user->points_sale))?$user->points_sale:0;
                    $puntosRestantes = $puntosUsuario - $puntosUsuarioEnSubasta;

                    $fechaHoraActual=date("Y-m-d H:i:s");
                    $arrayQueryPremiosEnSubasta = array(
                        "select"=>"*",
                        "where"=>"start <= '".$fechaHoraActual."' AND end >= '".$fechaHoraActual."' AND status = 1"
                        );

                    $arrayPremiosEnSubasta = $this->prize_sale_model->search($arrayQueryPremiosEnSubasta);

                    

                    if(is_array($arrayPremiosEnSubasta) && count($arrayPremiosEnSubasta) > 0)
                    {
                        $subastaEncontrada = false;
                        //$arraySubastasDisponibles = array();

                        foreach($arrayPremiosEnSubasta as $itemPremioEnSubasta)
                        {
                           
                            if($itemPremioEnSubasta->id == $prizeSaleId)
                            {
                                $subastaEncontrada = true;
                                
                            }
                        }

                        

                        if($subastaEncontrada)
                        {
                            $arrayQueryPremiosEnSubastaUsuario = array(
                                "select"=>"prizes_sale.id as prizes_sales_id, MAX(points) as points",
                                "join"=>array("prizes_sale, prizes_sale.id = user_prizes_sale.prizes_sales_id"),
                                "where"=>"user_prizes_sale.user_id='".$user->id."' and prizes_sale.status = 1",
                                "group"=>"prizes_sale.id, user_prizes_sale.user_id"
                                );

                            $premiosEnSubastaUsuario = $this->user_prizes_sale_model->get_search_row($arrayQueryPremiosEnSubastaUsuario);

                            if(isset($premiosEnSubastaUsuario->points))
                            {
                                if($premiosEnSubastaUsuario->prizes_sales_id == $prizeSaleId)
                                {
                                    //validar logica
                                    if($premiosEnSubastaUsuario->points < $puntos)
                                    {                                        
                                        if($puntosUsuario >= $puntos)
                                        {
                                            $arrayData = array(
                                                "user_id"=>$user->id,
                                                "prizes_sales_id"=>$prizeSaleId,
                                                "points"=>$puntos,
                                                "status"=>"1",
                                                "register_at"=>$fechaHora
                                                );
                                            $idInsert = $this->user_prizes_sale_model->insert($arrayData);
                                            if($idInsert > 0)
                                            {
                                                $arrayResponse["result"] = "1";
                                                $arrayResponse["mensaje"] = "Tu subasta fue registrada con éxito.";
                                                $this->response($arrayResponse, 200);
                                            }
                                            else
                                            {
                                                $arrayResponse["result"] = "0";
                                                $arrayResponse["mensaje"] = "Tú subasta no se pudo registrar por favor vuelve a intentarlo.";
                                                $this->response($arrayResponse, 200);
                                            }
                                        }
                                        else
                                        {
                                            $arrayResponse["result"] = "0";
                                            $arrayResponse["mensaje"] = "No tienes los puntos necesarios para realizar esta subasta, intenta de nuevo ingresando un monto menor o igual a tus puntos acumulados.";
                                            $this->response($arrayResponse, 200);
                                        }
                                    }
                                    else
                                    {
                                        $arrayResponse["result"] = "0";
                                        $arrayResponse["mensaje"] = "El monto a subastar debe de ser mayor al monto de ".$premiosEnSubastaUsuario->points." puntos que ya tienes subastado.";
                                        $this->response($arrayResponse, 200);
                                    }
                                }
                                else
                                {
                                    $arrayResponse["result"] = "0";
                                    $arrayResponse["mensaje"] = "No puedes participar de esta subasta hasta que termine la otra subasta en la que estas participando.";
                                    $this->response($arrayResponse, 200);
                                }
                            }
                            else
                            {
                                if($puntosUsuario >= $puntos)
                                {
                                    $arrayData = array(
                                        "user_id"=>$user->id,
                                        "prizes_sales_id"=>$prizeSaleId,
                                        "points"=>$puntos,
                                        "status"=>"1",
                                        "register_at"=>$fechaHora
                                        );
                                    $idInsert = $this->user_prizes_sale_model->insert($arrayData);
                                    if($idInsert > 0)
                                    {
                                        $arrayResponse["result"] = "1";
                                        $arrayResponse["mensaje"] = "Tu subasta fue registrada con éxito.";
                                        $this->response($arrayResponse, 200);
                                    }
                                    else
                                    {
                                        $arrayResponse["result"] = "0";
                                        $arrayResponse["mensaje"] = "Tú subasta no se pudo registrar por favor vuelve a intentarlo.";
                                        $this->response($arrayResponse, 200);
                                    }
                                }
                                else
                                {
                                    $arrayResponse["result"] = "0";
                                    $arrayResponse["mensaje"] = "No tienes los puntos necesarios para realizar esta subasta, intenta de nuevo ingresando un monto menor o igual a tus puntos acumulados.";
                                    $this->response($arrayResponse, 200);
                                }
                            }
                        }
                        else
                        {
                            $arrayResponse["result"] = "0";
                            $arrayResponse["mensaje"] = "Subasta no encontrada.";
                            $this->response($arrayResponse, 200);
                        }
                    }
                    else
                    {
                        $arrayResponse["result"] = "0";
                        $arrayResponse["mensaje"] = "No hay subasta disponible.";
                        $this->response($arrayResponse, 200);
                    }

                }
                else 
                {
                    $arrayResponse["result"] = "0";
                    $arrayResponse["mensaje"] = "Ya has ganado un premio, no puedes volver a participar de otra subasta.";
                    $this->response($arrayResponse, 200);
                }
                
            }
            else 
            {
                $arrayResponse["result"] = "0";
                $arrayResponse["mensaje"] = "Usuario no encontrado.";
                $this->response($arrayResponse, 200);
            }
          }
          else
          {
            $arrayResponse["result"] = "0";
            $arrayResponse["mensaje"] = "Acceso no autorizado.";
            $this->response($arrayResponse, 200);
          }
    }

    public function list_prizes_sale_post()
    {
      $token_get = $this->input->post('token',true);

      $params = array(
      'token'=>$token_get
      );

      $token_model = new Token_Model();
      $token = $token_model->validation_token($token_get);
      if (isset($token_get) && strlen($token_get) > 0 && isset($token))
      {
        $user = $token_model->get_user_country_with_token($token_get);

        if (isset($user))
        {
            $id_campaign = $user->campaign_id;
            $campaign_model = new Campaign_Model();

            $campaign  = $campaign_model->get_campaign($id_campaign);

            if (isset($campaign)) {
                if ($campaign->status == STATUS_ACTIVE) {


                  $prizes_sale_model = new Prizes_Sale_Model();
                  $prizes_sale = $this->prize_sale_model->get_prizes_sale();

                  //die($prizes_sale);


                  if(isset($prizes_sale)){

                      $response['result'] = 1;
                      $response['prizes_sale'] = $prizes_sale;
                      $response['cantidad'] = count($prizes_sale);
                      $this->response($response, 200);

                  }else{

                  }
                }else
                  {
                      $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                  }
              }else
              {
                  $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
              }

          } else
          {
              $this->response(get_error_response($this->lang->line('user_error_found')),404);
          }
      }
      else
      {
          $this->response(get_error_response($this->lang->line('user_error_token')),401);
      }

    }

    public function list_prizes_winner_post()
    {
        $token_get = $this->input->post('token',true);

       $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action =    API_PRIZE_ACTION_LIST_PRIZES_WINNER;

        save_log_api($params,$email,$uri,$method,$action);

        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {
            $user = $token_model->get_user_country_with_token($token_get);

            if (isset($user))
            {
                $id_campaign = $user->campaign_id;
                $campaign_model = new Campaign_Model();

                $campaign  = $campaign_model->get_campaign($id_campaign);

                if (isset($campaign)) {
                    if ($campaign->status == STATUS_ACTIVE) {
                     /*   $winner_model = new Winner_Model();
                        $total_prizes_day_campaign = $winner_model->total_winner_by_day($campaign->id);
                        if ($total_prizes_day_campaign < $campaign->prize_day) {*/

                            $id_user = $user->user_id;
                            $user_model = new User_Model();
                            $user_win = $user_model->get_user_win($id_user);

                            if (isset($user_win)) {
                                if ($user_win->win == WIN) {
                                    $winner_model = new Winner_Model();
                                    $winner = $winner_model->find_winner_by_user($id_user);
                                    if (!isset($winner)) {
                                        $campaign = validate_campaign();

                                        if ($campaign['result'] == 1) {
                                            $campaign = $campaign['campaign'];
                                            $id_campaign = $campaign[0]->id;

                                            $prize_model = new Prize_Model();
                                            $prizes = $prize_model->get_list_prizes($id_campaign);

                                            if (isset($prizes)) {
                                                $name = 'type';
                                                usort($prizes, function ($a, $b) use (&$name) {
                                                    return $a[$name] - $b[$name];
                                                });

                                                $response = get_success_response($this->lang->line('prize_success_list_winner'));
                                                $response['prizes'] = $prizes;
                                                $this->response($response, 200);
                                            } else {
                                                $this->response(get_error_response($this->lang->line('concourse_error_list_prize')), 404);
                                            }
                                        } else {
                                            $this->response(get_error_response($campaign['message']), 404);
                                        }


                                    } else {
                                        $this->response(get_error_response($this->lang->line('prize_error_user_win_prize')), 404);
                                    }
                                } else {
                                    $this->response(get_error_response($this->lang->line('prize_error_user_win')), 404);
                                }

                            } else {
                                $this->response(get_error_response($this->lang->line('user_error_found')), 404);
                            }
                        /*}else{
                             $this->response(get_error_response($this->lang->line('user_error_found')), 404);
                        }*/
                    }else
                    {
                        $this->response(get_error_response($this->lang->line('campaign_error_inactive')),404);
                    }
                }else
                {
                    $this->response(get_error_response($this->lang->line('campaign_error_get_campaign')),404);
                }

            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_found')),404);
            }
        } else
        {
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }




    }

    public function register_winner_post()
    {
        $token_get = $this->input->post('token',true);

        $params = array(
            'token'=>$token_get
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_POST;
        $action = API_PRIZE_ACTION_REGISTER_WINNER;


        $token_model = new Token_Model();
        $token = $token_model->validation_token($token_get);
        if (isset($token_get) && strlen($token_get) > 0 && isset($token))
        {

            $rules = [
                [
                    'field' => 'type_prize',
                    'rules' => 'required',
                    'errors' => [
                        'required' => $this->lang->line('prize_error_type_prize_required')
                    ]
                ]
            ];

            $this->form_validation->set_rules($rules);


            $type = $this->post('type_prize');

            $params =  array(
                'token'=>$token_get,
                'type_prize'=>$type
            );

            save_log_api($params,$email,$uri,$method,$action);

            if ($this->form_validation->run() == FALSE)
            {
                $errors_message = $this->form_validation->error_array();
                $error_response = get_error_form($errors_message);
                $this->response($error_response, 400);
            } else
            {
                $user = $token_model->get_user_country_with_token($token_get);
                if (isset($user)) {
                    $id_user = $user->user_id;
                    $campaign = validate_campaign();

                    if ($campaign['result'] == 1) {
                        $campaign = $campaign['campaign'];
                        $id_campaign = $campaign[0]->id;

                        $prize_model = new Prize_Model();
                        $amount_prize = $prize_model->get_amount_prize($type, $id_campaign);
                        if (isset($amount_prize)) {
                            if (sizeof($amount_prize) > 0) {
                                $id_prize = $amount_prize[0]['id'];

                                $winner_model = new Winner_Model();

                                $winner = $winner_model->find_prize_by_user($id_prize);

                                if (!isset($winner->prize_id)) {
                                    $prize = $prize_model->update_status_prize($id_prize);
                                    if (isset($prize)) {
                                        $user = $winner_model->register_winner($id_campaign, $id_user, $id_prize);

                                        if (isset($user)) {
                                            $this->response(get_success_response($this->lang->line('prize_success_register_prize_user')),200);
                                        } else {
                                            $this->response(get_error_response($this->lang->line('prize_error_register_prize_user')),404);
                                        }
                                    } else {
                                        $this->response(get_error_response($this->lang->line('prize_error_update_prize_user')),404);
                                    }
                                } else {
                                    $this->response(get_error_response($this->lang->line('prize_error_amount_prize_cero')),404);
                                }
                            } else {
                                $this->response(get_error_response($this->lang->line('prize_error_amount_prize_cero')),404);
                            }
                        } else {
                            $this->response(get_error_response($this->lang->line('prize_error_amount_prize')),404);
                        }

                    } else {
                        $this->response(get_error_response($campaign['message']),404);
                    }
                } else
                {
                    $this->response(get_error_response($this->lang->line('user_error_found')),404);
                }
            }
        } else
        {
            save_log_api($params,$email,$uri,$method,$action);
            $this->response(get_error_response($this->lang->line('user_error_token')),401);
        }
    }

    public function get_list_prize_menu_get()
    {
        $params = array(
        );

        $email = "";
        $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
        $method =  NAME_METHOD_GET;
        $action = API_PRIZE_ACTION_LIST_MENU;

        save_log_api($params,$email,$uri,$method,$action);

            $campaign = validate_campaign();

            if ($campaign['result'] == 1)
            {
                $campaign = $campaign['campaign'];
                $id_campaign = $campaign[0]->id;

                $prize_model = new Prize_Model();

                $prizes = $prize_model->get_list_prizes_menu($id_campaign);

                if (isset($prizes))
                {
                    $raffle_prize_model = new Raffle_Prize_Model();

                    $raffle_prizes = $raffle_prize_model->get_list_raffle_prizes_menu($id_campaign);

                    if (isset($raffle_prizes))
                    {
                        $response = get_success_response($this->lang->line('prize_success_list_menu'));
                        $response['prizes'] = $prizes;
                        $response['raffle_prizes'] = $raffle_prizes;

                        $this->response($response, 200);
                    } else
                    {
                        $this->response(get_error_response($this->lang->line('prize_error_get_list_raffle_menu')),404);
                    }
                } else
                {
                    $this->response(get_error_response($this->lang->line('prize_error_get_list_menu')),404);
                }
            } else
            {
                $this->response(get_error_response($campaign['message']),404);
            }
    }
}
