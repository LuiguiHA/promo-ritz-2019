<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Winner_Controller extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->lang->load('api_error_messages_lang', 'spanish');
        $this->lang->load('api_messages_lang', 'spanish');
        $this->load->model('user_Model','user_model');
        $this->load->model('campaign_Model','campaign_model');
        $this->load->model('raffle_Model','raffle_model');
        $this->load->model('winner_Model','winner_model');
        $this->load->model('token_Model','token_model');
    }

    public function list_winners_get($pagination = 1){

        $campaign = validate_campaign();

        if ($campaign['result'] == 1) {
            $campaign = $campaign['campaign'];
            $id_campaign = $campaign[0]->id;

            $params = array(
                'pagination'=>$pagination
            );

            $email = "";
            $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
            $method =  NAME_METHOD_GET;
            $action = API_WINNER_ACTION_LIST_WINNERS;

            save_log_api($params,$email,$uri,$method,$action);

            $winner_model = new Winner_Model();
            $users =  $winner_model->get_list_winners($pagination,$id_campaign);
            if (isset($users))
            {
                if (sizeof($users['data_users']) > 0)
                {
                        $response = get_success_response($this->lang->line('user_success_list_winners'));
                        $response['users']=$users['data_users'];
                        $response['pagination_next']=$users['pagination_next'];
                        $response['pagination_total']=$users['users_total'];
                        $this->response($response,200);
                } else
                {
                    $this->response(get_error_response($this->lang->line('user_error_list_winners_empty')),404);
                }
            } else
            {
                $this->response(get_error_response($this->lang->line('user_error_list_winners')),404);
            }
        } else
        {
            $this->response(get_error_response($campaign['message']),404);
        }
    }

    public function winners_get(){

        $campaign = validate_campaign();

        if ($campaign['result'] == 1) {
            $campaign = $campaign['campaign'];
            $id_campaign = $campaign[0]->id;

            $params = array(
            );

            $email = "";
            $uri = $this->uri->segment(2) .'/'.$this->uri->segment(3);
            $method =  NAME_METHOD_GET;
            $action = API_WINNER_ACTION_LIST_WINNERS;

            save_log_api($params,$email,$uri,$method,$action);

            $winner_model = new Winner_Model();
            $prizes =  $winner_model->get_prizes($id_campaign);

           if (isset($prizes)) {

               $prizes_data = array();
               for ($i = 0 ;  $i< sizeof($prizes) ;$i++){
                   $users = $winner_model->get_winners($id_campaign,$prizes[$i]->type);
                   if (isset($users)){
                       $prizes_data[$i] = array(
                           'name'=> $prizes[$i]->name,
                           'users'=>$users
                       );
                   }else{
                       $this->response(get_error_response($this->lang->line('winner_error_get_users')),404);
                   }
               }


                       $response = get_success_response($this->lang->line('user_success_list_winners'));
                       $response['prizes'] = $prizes_data;
                       $this->response($response, 200);
           }else{
               $this->response(get_error_response($this->lang->line('prize_error_get_list_id_prizes')),404);
           }
        } else
        {
            $this->response(get_error_response($campaign['message']),404);
        }
    }

}
