<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Winner_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = WINNERS_TABLE;
        $this->table_id = WINNERS_ID;
    }


    public function find_winner_by_user($id_user)
    {
        $query = [
            'select' =>
                WINNERS_ID.', '.
                WINNERS_PRIZE_ID.', '.
                WINNERS_USER_ID,
            'where' => WINNERS_USER_ID."= ".$id_user." and ".
                WINNERS_STATUS."=".ACTIVE
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function find_prize_by_user($id_prize)
    {
        $query = [
            'select' =>
                WINNERS_ID,
            'where' => WINNERS_PRIZE_ID."= '".$id_prize."' ",

        ];

        $winner = $this->get_search_row($query);
        return $winner;
    }

    public function register_winner($id_campaign,$id_user,$id_prize)
    {
        $parameters = [
            'campaign_id' => $id_campaign,
            'user_id' => $id_user,
            'prize_id'=>$id_prize,
            'status' => ACTIVE,
            'created_at' => date('Y-m-d H:i:s')
        ];
        $id = $this->insert($parameters, TRUE);
        return $id;
    }

    public function get_list_winners($pagination,$id_campaign)
    {
        $query = [
            'select' =>
                USERS_ID.', '.
                USERS_NAME.' as name_user, '.
                PRIZES_NAME.' as name_prize, '.
                PRIZES_TYPE,
            'join'   => array(
                USERS_TABLE .', '. USERS_ID ."=". WINNERS_USER_ID,
                PRIZES_TABLE .', '. PRIZES_ID ."=". WINNERS_PRIZE_ID
            ),
            'where' => WINNERS_CAMPAIGN_ID .'='.$id_campaign .' and '.
                WINNERS_STATUS.'='.ACTIVE
        ];

        // var_dump($query);
        // die('--->');

        $paginationByPage = 9;

        $pagination_next="0";
        $users_amount="0";

        if (is_numeric($pagination) && $pagination > 0)
        {
            $start = $paginationByPage * ($pagination - 1);
            $data_users = $this->search_data_array($query, $start, $paginationByPage);

            $users_amount = $this->total_records($query);
            if($users_amount/$paginationByPage>$pagination)
                $pagination_next="1";
        } else
        {
            $data_users = $this->search_array($query);
        }
        $users = array(
            "data_users" => $data_users,
            "pagination_next" => $pagination_next,
            "users_total" => (string)ceil($users_amount/$paginationByPage)
        );

        return $users;


    }

    // public function get_list_winners($pagination,$id_campaign)
    // {
    //     $query = [
    //         'select' =>
    //             USERS_ID.', '.
    //             USERS_NAME.' as name_user, '.
    //             PRIZES_NAME.' as name_prize',
    //         'join'   => array(
    //             USERS_TABLE .', '. USERS_ID ."=". WINNERS_USER_ID,
    //             PRIZES_TABLE .', '. PRIZES_ID ."=". WINNERS_PRIZE_ID
    //         ),
    //         'where' => WINNERS_CAMPAIGN_ID .'='.$id_campaign .' and '.
    //             WINNERS_STATUS.'='.ACTIVE
    //     ];
    //
    //     $pagination_next="0";
    //     $users_amount="0";
    //
    //     if (is_numeric($pagination) && $pagination > 0)
    //     {
    //         $start = PAGINATION_USERS * ($pagination - 1);
    //         $data_users = $this->search_data_array($query, $start, PAGINATION_USERS);
    //
    //         $users_amount = $this->total_records($query);
    //         if($users_amount/PAGINATION_USERS>$pagination)
    //             $pagination_next="1";
    //     } else
    //     {
    //         $data_users = $this->search_array($query);
    //     }
    //     $users = array(
    //         "data_users" => $data_users,
    //         "pagination_next" => $pagination_next,
    //         "users_total" => (string)ceil($users_amount/PAGINATION_USERS)
    //     );
    //
    //     return $users;
    //
    //
    // }

    public function get_winners($id_campaign,$type_prize)
    {
        $query = [
            'select' =>
                USERS_ID.', '.
                USERS_NAME,
            'join'   => array(
                USERS_TABLE .', '. USERS_ID ."=". WINNERS_USER_ID,
                PRIZES_TABLE .', '. PRIZES_ID ."=". WINNERS_PRIZE_ID
            ),
            'where' => WINNERS_CAMPAIGN_ID .'='.$id_campaign .' and '.
                WINNERS_STATUS.'='.ACTIVE . " and ". PRIZES_TYPE ."=".$type_prize
        ];

        $users = $this->search_array($query);

        return $users;
    }

    public function get_prizes($id_campaign)
    {
        $query = [
            'select' =>
                PRIZES_ID.','.
                PRIZES_NAME.','.
                PRIZES_TYPE,
            'join'   => array(
                USERS_TABLE .', '. USERS_ID ."=". WINNERS_USER_ID,
                PRIZES_TABLE .', '. PRIZES_ID ."=". WINNERS_PRIZE_ID
            ),
            'where' => WINNERS_CAMPAIGN_ID .'='.$id_campaign .' and '.
                WINNERS_STATUS.'='.ACTIVE,
            'group'=>PRIZES_TYPE.','.PRIZES_NAME.','.PRIZES_ID
        ];

        $users = $this->search($query);

        return $users;
    }

    public function get_list_users_winners($start,$campaign,$super_admin)
    {
        if ($super_admin != SUPER_ADMIN)
        {
            $query = [
                'select' =>
                    WINNERS_CREATED_AT.', '.
                    USERS_ID.', '.
                    USERS_NAME.' as user_name ,'.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    PRIZES_NAME.' as prize_name ,'.
                    USERS_POINTS.' ,'.
                    CAMPAIGNS_COUNTRY_CODE.' , city, '.
                    USERS_STATUS,
                'join'  =>
                    array(
                        USERS_TABLE .' , '. USERS_ID ."=".WINNERS_USER_ID,
                        CAMPAIGNS_TABLE .' , '.CAMPAIGNS_ID ."=".WINNERS_CAMPAIGN_ID,
                        PRIZES_TABLE .' , '.PRIZES_ID ."=".WINNERS_PRIZE_ID
                    ),
                'where'=> CAMPAIGNS_COUNTRY_CODE ."='".$campaign."'",
                'order'=> WINNERS_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    WINNERS_CREATED_AT.', '.
                    USERS_ID.', '.
                    USERS_NAME.' as user_name ,'.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    PRIZES_NAME.' as prize_name ,'.
                    USERS_POINTS.' ,'.
                    CAMPAIGNS_COUNTRY_CODE.' , city, '.
                    USERS_STATUS,
                'join'  =>
                    array(
                        USERS_TABLE .' , '. USERS_ID ."=".WINNERS_USER_ID,
                        CAMPAIGNS_TABLE .' , '.CAMPAIGNS_ID ."=".WINNERS_CAMPAIGN_ID,
                        PRIZES_TABLE .' , '.PRIZES_ID ."=".WINNERS_PRIZE_ID
                    ),

                'order'=> WINNERS_CREATED_AT .' desc '
            ];
        }


        $total_users_winners = $this->total_records($query);

        $data_users_winners = $this->search_data($query, $start,PAGINATION_LIST);

        $users = array(
            'total_users_winners'=>$total_users_winners,
            'data_users_winners'=>$data_users_winners
        );

        return $users;

    }

    public function get_search_users_winners($start,$dni,$name,$prize,$country,$campaign,$status)
    {
        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI." LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%' ";
            }
        }

        if ($name != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $name . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $name . "%' ";
            }
        }

        if ($prize != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".PRIZES_TYPE ." = '" . $prize . "' ";
            } else
            {
                $where = PRIZES_TYPE." LIKE '" . $prize . "' ";
            }
        }

        if ($campaign != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            } else
            {
                $where = CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            }
        }

        if ($country != "" )
        {

            if ($where != "")
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  " AND ". USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where .= " AND ".USERS_COUNTRY." = '" . $country . "' ";
                }

            } else
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where = USERS_COUNTRY." = '" . $country . "' ";
                }

            }
        }

        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_STATUS." = " . $status . " ";
            } else
            {
                $where = USERS_STATUS." = " . $status . "";
            }
        }

        if ($where != "")
        {
            $query = [
                'select' =>
                    WINNERS_CREATED_AT.', '.
                    USERS_ID.', '.
                    USERS_NAME.' as user_name ,'.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    PRIZES_NAME.' as prize_name ,'.
                    USERS_POINTS.' ,'.
                    CAMPAIGNS_COUNTRY_CODE.' , city, '.
                    USERS_STATUS,
                'join'  =>
                    array(
                        USERS_TABLE .' , '. USERS_ID ."=".WINNERS_USER_ID,
                        CAMPAIGNS_TABLE .' , '.CAMPAIGNS_ID ."=".WINNERS_CAMPAIGN_ID,
                        PRIZES_TABLE .' , '.PRIZES_ID ."=".WINNERS_PRIZE_ID
                    ),
                'where' => $where,
                'order'=> WINNERS_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    WINNERS_CREATED_AT.', '.
                    USERS_ID.', '.
                    USERS_NAME.' as user_name ,'.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    PRIZES_NAME.' as prize_name ,'.
                    USERS_POINTS.' ,'.
                    CAMPAIGNS_COUNTRY_CODE.' , city, '.
                    USERS_STATUS,
                'join'  =>
                    array(
                        USERS_TABLE .' , '. USERS_ID ."=".WINNERS_USER_ID,
                        CAMPAIGNS_TABLE .' , '.CAMPAIGNS_ID ."=".WINNERS_CAMPAIGN_ID,
                        PRIZES_TABLE .' , '.PRIZES_ID ."=".WINNERS_PRIZE_ID
                    ),
                'order'=> WINNERS_CREATED_AT .' desc '
            ];
        }



        $total_users = $this->total_records($query);

        $data_users = $this->search_data($query, $start,PAGINATION_LIST);

        $users = array(
            'total_users'=>$total_users,
            'data_users'=>$data_users
        );

        return $users;
    }

    public function get_export_users_winners($dni,$name,$prize,$country,$campaign,$status)
    {
        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI." LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%' ";
            }
        }

        if ($name != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $name . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $name . "%' ";
            }
        }

        if ($prize != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".PRIZES_TYPE ." = '" . $prize . "' ";
            } else
            {
                $where = PRIZES_TYPE." LIKE '" . $prize . "' ";
            }
        }

        if ($campaign != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            } else
            {
                $where = CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            }
        }

        if ($country != "" )
        {

            if ($where != "")
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  " AND ". USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where .= " AND ".USERS_COUNTRY." = '" . $country . "' ";
                }

            } else
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where = USERS_COUNTRY." = '" . $country . "' ";
                }

            }
        }

        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_STATUS." = " . $status . " ";
            } else
            {
                $where = USERS_STATUS." = " . $status . "";
            }
        }





        if ($where != "")
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.' as user_name ,'.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_SHARE.', '.
                    WINNERS_CREATED_AT.', '.
                    PRIZES_NAME.' as prize_name ,'.
                    USERS_STATUS,
                'join'  =>
                    array(
                        USERS_TABLE .' , '. USERS_ID ."=".WINNERS_USER_ID,
                        CAMPAIGNS_TABLE .' , '.CAMPAIGNS_ID ."=".WINNERS_CAMPAIGN_ID,
                        PRIZES_TABLE .' , '.PRIZES_ID ."=".WINNERS_PRIZE_ID
                    ),
                'where' => $where ,
                'order'=> WINNERS_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.' as user_name ,'.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_SHARE.', '.
                    WINNERS_CREATED_AT.' , '.
                    PRIZES_NAME.' as prize_name ,'.
                    USERS_STATUS,
                'join'  =>
                    array(
                        USERS_TABLE .' , '. USERS_ID ."=".WINNERS_USER_ID,
                        CAMPAIGNS_TABLE .' , '.CAMPAIGNS_ID ."=".WINNERS_CAMPAIGN_ID,
                        PRIZES_TABLE .' , '.PRIZES_ID ."=".WINNERS_PRIZE_ID
                    ),

                'order'=> WINNERS_CREATED_AT .' desc '
            ];
        }

        $users = $this->search($query);

        return $users;

    }

    public function total_winner_by_day($campaign){
        $query = [
            'select' =>
                WINNERS_ID,
            'where'=> WINNERS_CAMPAIGN_ID ."=".$campaign ." and DATE_FORMAT(".WINNERS_CREATED_AT.",'%Y-%m-%d') = '".date('Y-m-d')."'"
        ];
        $total_users = $this->total_records($query);
        return $total_users;
    }




}
