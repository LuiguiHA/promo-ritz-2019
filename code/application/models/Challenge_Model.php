<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Challenge_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = CHALLENGES_TABLE;
        $this->table_id = CHALLENGES_ID;
    }

    public function  get_list_Challenge($id_campaign,$id_user)
    {
        $query_challenge_won = [
            'select' => CHALLENGES_ID .','.
                        CHALLENGES_NAME .', '.
                        CHALLENGES_DESCRIPTION .', '.
                        'CONCAT("'.URL_HOST_IMAGES.'",ifnull('.CHALLENGES_PHOTO_LIST.', "'.DEFAULT_IMAGE.'")) as photo ,'.
                        'IF(COUNT('.CONCOURSES_CHALLENGE_ID.')>='.MAX_WON_PER_CHALLENGE.',"0","1") as allow',
            'left'=> array(
                    CONCOURSES_TABLE .' , '. CONCOURSES_CHALLENGE_ID ."=".CHALLENGES_ID.', left',
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".CHALLENGES_CAMPAIGN_ID.', left'
            ),
            'where'=> CHALLENGES_CAMPAIGN_ID . "= ".$id_campaign." and ".
                    CHALLENGES_STATUS ."=".ACTIVE ." and ".CONCOURSES_USER_ID ."=".$id_user." and  ".
                    CONCOURSES_WIN."=".WIN,
            'group'=> CONCOURSES_CHALLENGE_ID

        ];

        $data_challenges_won = $this->search_array($query_challenge_won);

        $query = [
            'select' => CHALLENGES_ID .', '.
                CHALLENGES_NAME .', '.
                CHALLENGES_DESCRIPTION .', '.
                'CONCAT("'.URL_HOST_IMAGES.'",ifnull('.CHALLENGES_PHOTO_LIST.', "'.DEFAULT_IMAGE.'")) as photo ,'.
                '1 as allow',
            'left'=> array(
                CONCOURSES_TABLE .' , '. CONCOURSES_CHALLENGE_ID ."=".CHALLENGES_ID.', left',
                CAMPAIGNS_TABLE .' , '. CHALLENGES_ID ."=".CHALLENGES_CAMPAIGN_ID.', left'
            ),
            'where'=> CHALLENGES_CAMPAIGN_ID . "= ".$id_campaign." and ".
                CHALLENGES_STATUS ."=".ACTIVE ." and ".
                CHALLENGES_ID .' not in (
                select '.CHALLENGES_ID .
                ' from '.CHALLENGES_TABLE .
                ' left join '.CONCOURSES_TABLE. ' on '.CONCOURSES_CHALLENGE_ID .'='. CHALLENGES_ID .
                ' left join '.CAMPAIGNS_TABLE. ' on '.CHALLENGES_ID .'='. CHALLENGES_CAMPAIGN_ID .
                ' where '. CHALLENGES_CAMPAIGN_ID . "= ".$id_campaign." and ".
                CHALLENGES_STATUS ."=".ACTIVE ." and ".CONCOURSES_USER_ID ."=".$id_user." and  ".
                CONCOURSES_WIN."=".WIN." group by '".PRIZES_TYPE ."')",
            'group'=> CHALLENGES_ID

        ];

        $data = $this->search_array($query);

        $data_challenges =  null;

        if (isset($data_challenges_won) && isset($data))
    {
        $data_challenges = array_merge($data_challenges_won,$data);
    } else
    {
        if (isset($data_challenges_won))
        {
            $data_challenges = $data_challenges_won;
        } else if(isset($data))
        {
            $data_challenges = $data;
        }
    }
        return $data_challenges;
    }

    public function get_photo_detail($id_challenge)
    {
        $query = [
            'select' =>
                'CONCAT("'.URL_HOST_IMAGES.'",ifnull('.CHALLENGES_PHOTO_DETAIL.', "'.DEFAULT_IMAGE.'")) as photo ,',
            'where'=> CHALLENGES_ID .'= '.$id_challenge

        ];

        $challenge = $this->get_search_row($query);
        return $challenge;
    }

    public function validate_user_win_challenge($id_user,$id_challenge)
    {
        $query = [
            'select' => CHALLENGES_ID .','.
                CHALLENGES_NAME .', '.
                'COUNT('.CHALLENGES_ID.') as allow',
            'left'=> array(
                CONCOURSES_TABLE .' , '. CONCOURSES_CHALLENGE_ID ."=".CHALLENGES_ID.', left'
            ),
            'where'=>  CHALLENGES_ID."=".$id_challenge ." and ".
                CHALLENGES_STATUS ."=".ACTIVE ." and ".CONCOURSES_USER_ID ."=".$id_user." and  ".
                CONCOURSES_WIN."=".WIN,
            'group'=> CHALLENGES_ID

        ];

        $challenge = $this->get_search_row($query);

        return $challenge;
    }

    public function validate_user_challenge($id_user,$id_challenge)
    {
        $query = [
            'select' => CHALLENGES_ID .', '.
                CHALLENGES_NAME .', '.
                'IF(COUNT('.CHALLENGES_ID.') > 0,"1","0") as challenge_play',
            'left'=> array(
                CONCOURSES_TABLE .' , '. CONCOURSES_CHALLENGE_ID ."=".CHALLENGES_ID.', left'
            ),
            'where'=>  CHALLENGES_ID."=".$id_challenge ." and ".
                CHALLENGES_STATUS ."=".ACTIVE ." and ".CONCOURSES_USER_ID ."=".$id_user,
            'group'=> CHALLENGES_ID

        ];

        $challenge = $this->get_search_row($query);

        return $challenge;
    }
}
