<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Subasta_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = 'subasta';
        $this->table_id = 'Id';
    }

    public function create($user_id,$prizes_id, $puntos)
    {
        $subasta = [
            'users_id' => $user_id,
            'prizes_id' => $prizes_id,
            'puntos' => $puntos,
            'status' => 1,
            'fecha_subasta' => date('Y-m-d H:i:s')
        ];
        $id = $this->insert($subasta, TRUE);

        return $id;
    }

}
