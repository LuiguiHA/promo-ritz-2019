<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Concourse_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = CONCOURSES_TABLE;
        $this->table_id = CONCOURSES_ID;
    }

    public function create($id_user,$id_challenge,$time,$win)
    {
        $parameters = [
            'user_id'=>$id_user,
            'challenge_id'=>$id_challenge,
            'date'=>date('Y-m-d H:i:s'),
            'created_at'=>date('Y-m-d H:i:s'),
            'time'=>$time,
            'win'=>$win
        ];

       $id_concourse = $this->insert($parameters, TRUE);
       return $id_concourse;
    }

    public function won_per_day($id_user)
    {
        $query = [
            'select'=>CONCOURSES_USER_ID,
            'where'=>CONCOURSES_USER_ID ."=".$id_user." and ".
            CONCOURSES_WIN ."=".WIN . " and
            DATE_FORMAT(".CONCOURSES_DATE.",'%Y-%m-%d') = '".date('Y-m-d')."'"
        ];

        $times_won_day = $this->total_records($query);
        return $times_won_day;
    }

    public function won_per_challenge($id_user,$id_challenge)
    {
        $query = [
            'select'=>CONCOURSES_USER_ID,
            'where'=>CONCOURSES_USER_ID ."=".$id_user." and ".
                CONCOURSES_WIN ."=".WIN . " and ".
                CONCOURSES_CHALLENGE_ID ."=".$id_challenge
        ];

        $times_won_challenge = $this->total_records($query);
        return $times_won_challenge;
    }


}
