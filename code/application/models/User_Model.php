<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class User_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = USERS_TABLE;
        $this->table_id = USERS_ID;
    }

     public function create($id_campaign,$email, $name, $dni, $phone, $country, $personal_data)
    //public function create($id_campaign, $name , $email, $dni, $phone, $fecha_compra, $tienda_compra, $country, $personal_data)
    {
        $user = [
            'campaign_id' => $id_campaign,
            'name' => "$name",
            'email' => "$email",
            'dni' => "$dni",
            'phone' => $phone,
            //'fecha_compra' => $fecha_compra,
            //'tienda_compra' => $tienda_compra,
            'country' => "$country",
            'personal_data' => $personal_data,
            'created_at' => date('Y-m-d H:i:s')
        ];
        $id = $this->insert($user, TRUE);
        $user['points'] = 0;
        $user['id'] = $id;
        return (object)$user;
    }

    public function find($email, $dni)
    {
        $query = [
            'select' => USERS_ID.', '.
                USERS_NAME.', '.
                USERS_COUNTRY.', '.
                USERS_CAMPAIGN_ID.', '.
                USERS_POINTS,
            'where' => "dni='".$dni. "' and ".
               // USERS_EMAIL."='".$email. "' and ".
                USERS_STATUS.'='.'1'
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function find_by_dni($id_campaign,$dni)
    {
        $query = [
            'select' => USERS_ID,
            'where' => USERS_EMAIL."='".$dni. "' and ".
                USERS_CAMPAIGN_ID."=".$id_campaign. " and ".
                USERS_STATUS."=".ACTIVE
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function find_by_email($id_campaign,$email)
    {
        $query = [
            'select' => USERS_ID,
            'where' => USERS_EMAIL."='".$email. "' and ".
                USERS_CAMPAIGN_ID."=".$id_campaign. " and ".
                USERS_STATUS."=".ACTIVE
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function find_by_campaign($id_campaign,$dni,$email)
    {
        $query = [
            'select' => USERS_ID,
            'where' => USERS_DNI."=".$dni. " and ".
                USERS_EMAIL."='".$email. "' and ".
                USERS_CAMPAIGN_ID."=".$id_campaign. " and ".
                USERS_STATUS."=".ACTIVE
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function find_email_by_user($email)
    {
        $query = [
            'select' => USERS_ID.', '.
                USERS_NAME.', '.
                USERS_DNI.', '.
                USERS_EMAIL.', '.
                USERS_PHONE.', '.
                USERS_NAME,
            'where' =>
                USERS_EMAIL."='".$email. "'"
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function get_detail($id_user)
    {
        $query = [
            'select' => USERS_ID.', '.
                USERS_NAME.',
                SUBSTRING_INDEX('.USERS_NAME.'," ", 2)  as name_short , '.
                USERS_EMAIL.', '.
                USERS_PHONE.', '.
                USERS_DNI.', '.
                USERS_POINTS.' ',
                //'MAX(DATE_FORMAT('.CONCOURSES_DATE.',"%Y-%m-%d" )) as  last_participation'.', '.
                //'ROUND(AVG('.CONCOURSES_TIME.'),2) as  average_time'.', '.
                //'COUNT('.CONCOURSES_USER_ID.') as  tries'.', '.
                //'MAX('.CONCOURSES_TIME.') as  best_time'.', ',
            'left' => array(
             CONCOURSES_TABLE.",".CONCOURSES_USER_ID."=".USERS_ID.","."left"
            ),
            'where' => USERS_ID."= '".$id_user."' and ".
                USERS_STATUS.'='.ACTIVE
        ];

        $user = $this->get_search_row($query);

        $query_last_participation = [
            'select' =>
                CONCOURSES_TIME .' as  last_participation',
            'left' => array(
                CONCOURSES_TABLE.",".CONCOURSES_USER_ID."=".USERS_ID.","."left"
            ),
            'where' => USERS_ID."= '".$id_user."' and ".
                USERS_STATUS.'='.ACTIVE ." and ".
                CONCOURSES_ID .' = (select MAX('.CONCOURSES_ID.') from '.USERS_TABLE.
                ' left join '.CONCOURSES_TABLE.' on '.CONCOURSES_USER_ID ."=".USERS_ID .' where '.
                USERS_ID."='".$id_user."' and ". USERS_STATUS ."=".ACTIVE.')'

        ];

        $last_participation = $this->get_search_row($query_last_participation);

       // $user->last_participation = $last_participation->last_participation;

        return $user;
    }

    public function get_points($id_user)
    {
        $query = [
            'select' =>
                USERS_POINTS,
            'where' => USERS_ID."= '".$id_user."' and ".
                USERS_STATUS.'='.ACTIVE
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function find_prize_by_user($id_user)
    {
        $query = [
            'select' =>
                USERS_COUNTRY.', '.
                USERS_POINTS,
            'where' => USERS_ID."= ".$id_user." and ".
                USERS_STATUS."=".ACTIVE
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function accumulate_points_user($id_user,$points)
    {
        $parameters = [
            'points' => $points
        ];

        $accumulate = $this->update($id_user,$parameters);
        return $accumulate;
    }

    public function accumulate_points_user_share($id_user,$points)
    {

        $parameters = [
            'points' => $points ,
            'share'=> SHARE_FACEBOOK
        ];

        $accumulate = $this->update($id_user,$parameters);
        return $accumulate;
    }

    public function check_share_facebook($id_user)
    {

        $query = [
            'select' =>
                USERS_SHARE.', '.
                USERS_POINTS,
            'where' => USERS_ID."= '".$id_user."' and ".
                USERS_STATUS.'='.ACTIVE
        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function get_total_share_meme($id_user, $numero_lote)
    {

        $query = array(
            "select"=>"raffle.*",
            "join"=>array("raffle, raffle.user_id = users.id"),
            "where"=>"raffle.user_id='".$id_user."' and raffle.numero_lote = '".$numero_lote."'"
        );

        $total = $this->total_records($query);
        return $total;
    }

    public function register_prize_user($id_user,$id_prize)
    {

        $parameters = [
            'prize_id' => $id_prize ,
            'winner_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];

        $user = $this->update($id_user,$parameters);
        return $user;
    }

    public function find_user_by_prize($id_prize)
    {
        $query = [
            'select' =>
                USERS_PRIZE,
            'where' => USERS_PRIZE."= '".$id_prize."' ",

        ];

        $user = $this->get_search_row($query);
        return $user;
    }

    public function get_list_users($start,$campaign,$super_admin)
    {
        if ($super_admin != SUPER_ADMIN)
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', `users`.created_at, '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    USERS_STATUS.', city',
                'join'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".USERS_CAMPAIGN_ID
                ),
                'where'=> CAMPAIGNS_COUNTRY_CODE ."='".$campaign."'",
                'order'=> USERS_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', `users`.created_at, '.
                    CAMPAIGNS_COUNTRY_CODE.', city, '.
                    USERS_STATUS,
                'join'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".USERS_CAMPAIGN_ID
                ),
                'order'=> USERS_CREATED_AT .' desc '
            ];
        }


        $total_users = $this->total_records($query);

        $data_users = $this->search_data($query, $start,PAGINATION_LIST);

        $users = array(
            'total_users'=>$total_users,
            'data_users'=>$data_users
        );

        return $users;

    }

    public function get_search_users($start,$dni,$name,$fecha_ini,$fecha_fin,$status)
    {
        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI."  LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%'";
            }
        }

        if ($name != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $name . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $name . "%' ";
            }
        }

        if ($fecha_ini != "" )
        {
            if ($where != "")
            {
                $where .= " AND "." `users`.created_at >= '".$fecha_ini."' ";
            } else
            {
                $where = " `users`.created_at >= '".$fecha_ini."' ";
            }
        }


        if ($fecha_fin != "" )
        {
            if ($where != "")
            {
                $where .= " AND "." `users`.created_at <= '".$fecha_fin."' ";
            } else
            {
                $where = " `users`.created_at <= '".$fecha_fin."' ";
            }
        }


        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_STATUS." = " . $status . " ";
            } else
            {
                $where = USERS_STATUS." = " . $status . "";
            }
        }

        if ($where !="")
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    CAMPAIGNS_COUNTRY_CODE.', city, `users`.created_at, '.
                    USERS_STATUS,
                'join'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".USERS_CAMPAIGN_ID
                ),
                'where'=> $where,
                'order'=> USERS_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    CAMPAIGNS_COUNTRY_CODE.', city, '.
                    USERS_STATUS,
                'join'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".USERS_CAMPAIGN_ID
                ),
                'order'=> USERS_CREATED_AT .' desc '
            ];
        }


        $total_users = $this->total_records($query);

        $data_users = $this->search_data($query, $start,PAGINATION_LIST);

        $users = array(
            'total_users'=>$total_users,
            'data_users'=>$data_users
        );

        return $users;
    }

    public function get_export_users($dni,$name,$fecha_ini,$fecha_fin,$status)
    {
        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI."  LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%'";
            }
        }

        if ($name != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $name . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $name . "%' ";
            }
        }

        if ($fecha_ini != "" )
        {
            if ($where != "")
            {
                $where .= " AND "." `users`.created_at >= '".$fecha_ini."' ";
            } else
            {
                $where = " `users`.created_at >= '".$fecha_ini."' ";
            }
        }


        if ($fecha_fin != "" )
        {
            if ($where != "")
            {
                $where .= " AND "." `users`.created_at <= '".$fecha_fin."' ";
            } else
            {
                $where = " `users`.created_at <= '".$fecha_fin."' ";
            }
        }

        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_STATUS." = " . $status . " ";
            } else
            {
                $where = USERS_STATUS." = " . $status . "";
            }
        }


        if ($where !="")
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    USERS_SHARE.', '.
                    USERS_CREATED_AT.', '.
                    USERS_STATUS,
                'join'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".USERS_CAMPAIGN_ID
                ),
                'where'=> $where,
                'order'=> USERS_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_COUNTRY.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    USERS_SHARE.', '.
                    USERS_CREATED_AT.', '.
                    USERS_STATUS,
                'join'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".USERS_CAMPAIGN_ID
                ),
                'order'=> USERS_CREATED_AT .' desc '
            ];
        }

        $users = $this->search($query);

        return $users;

    }

    public function update_win_user($id_user)
    {
        $query = [
            'win' => WIN,
            'updated_at' =>date("Y-m-d H:i:s")
        ];

        return $this->update($id_user ,$query);

    }

    public function update_instant_winner_user($id_user)
    {
        $query = [
            'instant_winner' => WIN,
            'updated_at' =>date("Y-m-d H:i:s")
        ];

        return $this->update($id_user ,$query);

    }

    public function get_user_win($id_user)
    {
        $query = [
            'select' =>
                USERS_WIN,
            'where' => USERS_ID."= ".$id_user."",

        ];

        $user = $this->get_search_row($query);
        return $user;
    }



}
