<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Raffle_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = RAFFLE_TABLE;
        $this->table_id = RAFFLE_ID;
    }

    public function register_user_raffle($points,$id_user)
    {
        $query = [
            'user_id'=>$id_user,
            'created_at'=>date('Y-m-d H:i:s'),
            'status'=>ACTIVE
        ];
        
        $data = array();
        
        for ( $i=0 ; $i<$points ; $i++ )
        {
            array_push($data,$query);
        }
        
        $raffle = $this->insert_lote($data);

        return $raffle;
    }

    public function get_list_memes_by_user($pagination, $id_user)
    {
        $totalPorPagina = 4;

        $query = [
            'select' => "COALESCE(meme.id,0) as id, COALESCE(meme.name,'') as name, COALESCE(CONCAT('".base_url()."',meme.photo),'') as photo, COALESCE(meme_category.title,'') as category, raffle.numero_lote as lote, DATE_FORMAT(raffle.created_at, '%d/%m/%Y') as date",
            'left' => array(
                "meme, raffle.meme_id = meme.id, left",
                "meme_category, meme_category.id = meme.category_id, left"
            ),
            'where' => "raffle.user_id='".$id_user."'",
            "order" => "raffle.id desc"
        ];

         

        $pagination_next="0";
        $users_amount="0";

        if (is_numeric($pagination) && $pagination > 0)
        {
            $start = $totalPorPagina * ($pagination - 1);
            $data_users = $this->search_data_array($query, $start, $totalPorPagina);

            $users_amount = $this->total_records($query);
            if($users_amount/$totalPorPagina>$pagination)
                $pagination_next="1";
        } else
        {
            $data_users = $this->search_array($query);
        }
        $users = array(
            "data_users" => $data_users,
            "pagination_next" => $pagination_next,
            "users_total" => (string)ceil($users_amount/$totalPorPagina)
        );

        return $users;
    }


    public function get_list_all_memes($start)
    {
        $totalPorPagina = 20;

        

        $query = [
            'select' => "users.id as user_id, users.dni as user_dni, users.name as user_name, users.email as user_email, users.phone as user_phone, COALESCE(meme.id,0) as id, COALESCE(meme.name,'') as name, COALESCE(meme.photo,'') as photo, COALESCE(meme_category.title,'') as category, raffle.numero_lote as lote, raffle.created_at ",
            'left' => array(
                "meme, raffle.meme_id = meme.id, left",
                "meme_category, meme_category.id = meme.category_id, left",
            ),
            'join' => array(
                "users, users.id = raffle.user_id"
            ),
            "order" => "raffle.id desc"
        ];

         

        $pagination_next="0";
        $users_amount="0";

       /* if (is_numeric($pagination) && $pagination > 0)
        {*/
            //$start = $totalPorPagina * ($pagination - 1);
            $data_users = $this->search_data($query, $start, PAGINATION_LIST);

            $users_amount = $this->total_records($query);
           // if($users_amount/$totalPorPagina>$pagination)
             //   $pagination_next="1";
        /*} else
        {
            $data_users = $this->search($query);
        }*/
        $users = array(
            "data_users" => $data_users,
            //"pagination_next" => $pagination_next,
            "users_total" => ($users_amount)
        );

        return $users;
    }

    public function get_list_search_memes($start,$total_por_pagina,$dni,$name,$fecha_ini,$fecha_fin,$status)
    //($start,$dni,$name,$fecha_ini,$fecha_fin,$status)
    {
        //$totalPorPagina = 20;

        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI."  LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%'";
            }
        }

        if ($name != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $name . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $name . "%' ";
            }
        }

        if ($fecha_ini != "" )
        {
            if ($where != "")
            {
                $where .= " AND "." `raffle`.created_at >= '".$fecha_ini."' ";
            } else
            {
                $where = " `raffle`.created_at >= '".$fecha_ini."' ";
            }
        }


        if ($fecha_fin != "" )
        {
            if ($where != "")
            {
                $where .= " AND "." `raffle`.created_at <= '".$fecha_fin."' ";
            } else
            {
                $where = " `raffle`.created_at <= '".$fecha_fin."' ";
            }
        }


        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_STATUS." = " . $status . " ";
            } else
            {
                $where = USERS_STATUS." = " . $status . "";
            }
        }

        

        $query = [
            'select' => "users.id as user_id, users.dni as user_dni, users.name as user_name, users.email as user_email, users.phone as user_phone, COALESCE(meme.id,0) as id, COALESCE(meme.name,'') as name, COALESCE(meme.photo,'') as photo, COALESCE(meme_category.title,'') as category, raffle.numero_lote as lote, raffle.created_at ",
            'left' => array(
                "meme, raffle.meme_id = meme.id, left",
                "meme_category, meme_category.id = meme.category_id, left",
            ),
            'join' => array(
                "users, users.id = raffle.user_id"
            ),
            "order" => "raffle.id desc"
        ];

        if($where != "")
            $query["where"] = $where;

         

        $pagination_next="0";
        $users_amount="0";

       /* if (is_numeric($pagination) && $pagination > 0)
        {*/
            //$start = $totalPorPagina * ($pagination - 1);
            $data_users = $this->search_data($query, $start, $total_por_pagina);

            $users_amount = $this->total_records($query);
           // if($users_amount/$totalPorPagina>$pagination)
             //   $pagination_next="1";
        /*} else
        {
            $data_users = $this->search($query);
        }*/
        $users = array(
            "data_users" => $data_users,
            //"pagination_next" => $pagination_next,
            "users_total" => ($users_amount)
        );

        return $users;
    }


    public function get_total_search_memes($start,$dni,$name,$fecha_ini,$fecha_fin,$status)
    {
        $totalPorPagina = 20;

        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI."  LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%'";
            }
        }

        if ($name != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $name . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $name . "%' ";
            }
        }

        if ($fecha_ini != "" )
        {
            if ($where != "")
            {
                $where .= " AND "." `raffle`.created_at >= '".$fecha_ini."' ";
            } else
            {
                $where = " `raffle`.created_at >= '".$fecha_ini."' ";
            }
        }


        if ($fecha_fin != "" )
        {
            if ($where != "")
            {
                $where .= " AND "." `raffle`.created_at <= '".$fecha_fin."' ";
            } else
            {
                $where = " `raffle`.created_at <= '".$fecha_fin."' ";
            }
        }


        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_STATUS." = " . $status . " ";
            } else
            {
                $where = USERS_STATUS." = " . $status . "";
            }
        }

        

        $query = [
            'select' => "users.id as user_id, users.dni as user_dni, users.name as user_name, users.email as user_email, users.phone as user_phone, COALESCE(meme.id,0) as id, COALESCE(meme.name,'') as name, COALESCE(meme.photo,'') as photo, COALESCE(meme_category.title,'') as category, raffle.numero_lote as lote, raffle.created_at ",
            'left' => array(
                "meme, raffle.meme_id = meme.id, left",
                "meme_category, meme_category.id = meme.category_id, left",
            ),
            'join' => array(
                "users, users.id = raffle.user_id"
            ),
            "order" => "raffle.id desc"
        ];

        if($where != "")
            $query["where"] = $where;

         
        $users_amount = $this->total_records($query);
           
        return $users_amount;
    }

    public function register_user_raffle_meme($points,$id_user, $id_meme, $lote)
    {
        $query = [
            'user_id'=>$id_user,
            'meme_id'=>$id_meme,
            'numero_lote'=>$lote,
            'created_at'=>date('Y-m-d H:i:s'),
            'status'=>ACTIVE
        ];
        
        $data = array();
        
        for ( $i=0 ; $i<$points ; $i++ )
        {
            array_push($data,$query);
        }
        
        $raffle = $this->insert_lote($data);

        return $raffle;
    }

    public function get_list_users($start,$campaign,$super_admin)
    {
        if ($super_admin != SUPER_ADMIN)
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". USERS_CAMPAIGN_ID
                ),
                'where'=> CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'",
                'order'=> RAFFLE_CREATED_AT .' desc '
            ];
        }else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". USERS_CAMPAIGN_ID
                ),

                'order'=> RAFFLE_CREATED_AT .' desc '
            ];
        }


        $total_users = $this->total_records($query);

        $data_users = $this->search_data($query, $start,PAGINATION_LIST);

        $users = array(
            'total_users'=>$total_users,
            'data_users'=>$data_users
        );

        return $users;

    }

    public function get_search_users_raffle($start,$user,$dni,$country,$campaign,$status)
    {
        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI."  LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%'";
            }
        }

        if ($user != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $user . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $user . "%' ";
            }
        }

        if ($campaign != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            } else
            {
                $where = CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            }
        }

        if ($country != "" )
        {

            if ($where != "")
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  " AND ". USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where .= " AND ".USERS_COUNTRY." = '" . $country . "' ";
                }

            } else
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where = USERS_COUNTRY." = '" . $country . "' ";
                }

            }
        }


        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".RAFFLE_STATUS." = " . $status . " ";
            } else
            {
                $where = RAFFLE_STATUS." = " . $status . "";
            }
        }


        if ($where !="")
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_STATUS,
                'join'=>array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". USERS_CAMPAIGN_ID
                ),

                'where'=>$where,
                'order'=> RAFFLE_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". USERS_CAMPAIGN_ID
                ),

                'order'=> RAFFLE_CREATED_AT .' desc '
            ];
        }


        $total_users = $this->total_records($query);

        $data_users = $this->search_data($query, $start,PAGINATION_LIST);

        $users = array(
            'total_users'=>$total_users,
            'data_users'=>$data_users
        );

        return $users;
    }

    public function export_list_user_raffle($user,$dni,$country,$campaign,$status)
    {
        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI."  LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%'";
            }
        }

        if ($user != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $user . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $user . "%' ";
            }
        }

        if ($country != "" )
        {

            if ($where != "")
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  " AND ". USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where .= " AND ".USERS_COUNTRY." = '" . $country . "' ";
                }

            } else
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where = USERS_COUNTRY." = '" . $country . "' ";
                }

            }
        }

        if ($campaign != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            } else
            {
                $where = CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            }
        }

        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".RAFFLE_STATUS." = " . $status . " ";
            } else
            {
                $where = RAFFLE_STATUS." = " . $status . "";
            }
        }


        if ($where !="")
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". USERS_CAMPAIGN_ID
                ),
                'where'=>$where,
                'order'=> RAFFLE_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". USERS_CAMPAIGN_ID
                ),
                'order'=> RAFFLE_CREATED_AT .' desc '
            ];
        }

        $users = $this->search($query);

        return $users;
    }



    public function get_total_participants($campaign)
    {
        $query = [
            'select' =>
                RAFFLE_USER_ID,
            'join'=> array(
                USERS_TABLE .', '.USERS_ID ."=".RAFFLE_USER_ID
            ),
            'where'=> USERS_CAMPAIGN_ID ."=".$campaign ." and ".
                    RAFFLE_STATUS ."=".ACTIVE

        ];

        $total = $this->total_records($query);

        return $total;
    }

    public function update_user_winner_raffle($id_user)
    {
            $parameters = [
                'status' => STATUS_INACTIVE,
                'updated_at'=>date('Y-m-d H:i:s')
            ];

            $where = RAFFLE_USER_ID."= $id_user";
        $raffle =   $this->update_where($where,$parameters);
        return $raffle;
    }

    public function get_user_winner_raffle($campaign)
    {
        $query = [
            'select' => USERS_ID.', '.
                USERS_NAME.', '.
                USERS_EMAIL.', '.
                USERS_PHONE.', '.
                USERS_DNI.', '.
                USERS_POINTS.', '.
                USERS_CAMPAIGN_ID.', '.
                USERS_STATUS.' as status_user , '.
                RAFFLE_STATUS,
                'join'=> array(
                    USERS_TABLE .', '. USERS_ID ."=".RAFFLE_USER_ID
                ),
            'where' => USERS_CAMPAIGN_ID."= '".$campaign."'  and ".
                        RAFFLE_STATUS ."=".ACTIVE,
            'order'=> 'RAND()',
            'limit'=> '1'

        ];

        $user = $this->get_search_row($query);
        return $user;
    }


}