<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Prizes_Sale_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = 'prizes_sale';
        $this->table_id = 'Id';
    }


    public function get_prizes_sale()
    {
      $tiempo = date('Y-m-d H:i:s');

      $join = array(
          'prizes as  P, P.id = prizes_sale.prize_id'
      );
      $query = [
        'select'=> 'prizes_sale.id,prizes_sale.prize_id,prizes_sale.description, prizes_sale.start,prizes_sale.end, P.puntos, P.photo',
        'join' => $join,
        'where'=>   "prizes_sale.start < '".$tiempo."' and prizes_sale.end > '".$tiempo."' and prizes_sale.status = 1"
      ];

      $prizes_sale = $this->search_array($query);
      return $prizes_sale;
    }

    public function get_prizes_sale_by_id($id)
    {
      $tiempo = date('Y-m-d H:i:s');

      $join = array(
          'prizes as  P, P.id = prizes_sale.prize_id'
      );
      $query = [
        'select'=> 'prizes_sale.id,prizes_sale.prize_id,prizes_sale.description, prizes_sale.start,prizes_sale.end, P.puntos, P.photo',
        'join' => $join,
        'where'=>   "prizes_sale.start < '".$tiempo."' and prizes_sale.end > '".$tiempo."' and prizes_sale.status = 1 and prizes_sale.id = ".$id
      ];

      $prizes_sale = $this->get_search_row($query);
      return $prizes_sale;
    }
}
