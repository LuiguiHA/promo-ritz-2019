<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require APPPATH."/libraries/Uuid.php";

class Token_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = TOKENS_TABLE;
        $this->table_id = TOKENS_ID;
    }

    public function save_token($token, $user_id)
    {
        $parameters = [
            'user_id' => $user_id,
            'token' => $token,
            'created_at' => date('Y-m-d H:i:s'),
            'status' => ACTIVE
        ];
        $id_token = $this->insert($parameters,TRUE);
        return $id_token;


    }

    public function get_user_with_token($token)
    {
        $query = [
            'select' => TOKENS_USER_ID,
            'where' => TOKENS_TOKEN. " = '".$token."' and ".
                TOKENS_STATUS.'='.ACTIVE
        ];

        $id_user = $this->get_search_row($query);
        return $id_user;
    }

    public function generate_token()
    {
        $token = Uuid::generate();
        return (string)$token;
    }

    public function update_token_close_session($token)
    {
        $parameters = [
            TOKENS_STATUS => INACTIVE,
              TOKENS_UPDATED_AT => date("Y-m-d H:i:s")
        ];

        $where = TOKENS_TOKEN." = '$token'";

        return $this->update_where($where ,$parameters);

    }

    public function validation_token($token){
        $query = [
            'select' => TOKENS_TOKEN,
            'where' => TOKENS_TOKEN. " = '".$token."' and ".
                TOKENS_STATUS.'='.ACTIVE
        ];

        return $this->get_search_row($query);
    }

    public function get_user_country_with_token($token)
    {
        $query = [
            'select' => USERS_ID.", ".USERS_POINTS.", ".TOKENS_USER_ID.", ".
                        USERS_COUNTRY." , win, ".
                        USERS_CAMPAIGN_ID,
            'join'=> array(
                USERS_TABLE. ", ".  USERS_ID."=".TOKENS_USER_ID
            ),
            'where' => TOKENS_TOKEN. " = '".$token."' and ".
                TOKENS_STATUS.'='.ACTIVE
        ];

        $user = $this->get_search_row($query);
        return $user;
    }
}
