<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Admin_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = ADMIN_TABLE;
        $this->table_id = ADMIN_ID;
    }


    public function login($username)
    {
        $query = [
            'select' => ADMIN_ID.', '.
                CAMPAIGNS_ID.' as campaign_id,'.
                CAMPAIGNS_COUNTRY_CODE .' ,'.
                CAMPAIGNS_COUNTRY_NAME .' ,'.
                ADMIN_SUPER_ADMIN .' ,'.
                ADMIN_USERNAME.', '.
                ADMIN_PASSWORD.', '.
                ADMIN_COUNTRY.', '.
                ADMIN_STATUS,
            'left'=>array(
                CAMPAIGNS_TABLE.' , '. CAMPAIGNS_ID .'='. ADMIN_CAMPAIGN_ID .' , left'
            ),
            'where' => ADMIN_USERNAME."='".$username. "'"
        ];

        $admin = $this->get_search_row($query);
        return $admin;
    }

    public function get_list_admins($start)
    {

        $query = [
            'select' =>
                ADMIN_ID.', '.
                ADMIN_USERNAME.', '.
                ADMIN_COUNTRY.', '.
                ADMIN_SUPER_ADMIN.', '.
                ADMIN_CREATED_AT.', '.
                CAMPAIGNS_COUNTRY_CODE.', '.
                ADMIN_STATUS,
            'left'=> array(
                CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".ADMIN_CAMPAIGN_ID .' , left'
            ),
            'order'=> ADMIN_CREATED_AT .' desc '
        ];

        $total_admins = $this->total_records($query);

        $data_admins = $this->search_data($query, $start,PAGINATION_LIST);

        $admins = array(
            'total_admins'=>$total_admins,
            'data_admins'=>$data_admins
        );

        return $admins;

    }

    public function get_search_admins($start,$user,$campaign,$status)
    {
        $where = "";

        if ($user != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".ADMIN_USERNAME." LIKE '%" . $user . "%' ";
            } else
            {
                $where = ADMIN_USERNAME." LIKE '%" . $user . "%' ";
            }
        }

        if ($campaign != "" )
        {
            if ($campaign == COUNTRY_ADMIN)
            {
                if ($where != "")
                {
                    $where .= " AND ".ADMIN_SUPER_ADMIN." = " . SUPER_ADMIN;
                } else
                {
                    $where = ADMIN_SUPER_ADMIN." = " . SUPER_ADMIN ;
                }
            } else
            {
                if ($where != "")
                {
                    $where .= " AND ".CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "' ";
                } else
                {
                    $where = CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "' ";
                }
            }

        }

        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".ADMIN_STATUS." = " . $status . " ";
            } else
            {
                $where = ADMIN_STATUS." = " . $status . "";
            }
        }


        if ($where !="")
        {
            $query = [
                'select' =>
                    ADMIN_ID.', '.
                    ADMIN_USERNAME.', '.
                    ADMIN_COUNTRY.', '.
                    ADMIN_SUPER_ADMIN.', '.
                    ADMIN_CREATED_AT.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    ADMIN_STATUS,
                'left'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".ADMIN_CAMPAIGN_ID .' , left'
                ),
                'where'=> $where,
                'order'=> ADMIN_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    ADMIN_ID.', '.
                    ADMIN_USERNAME.', '.
                    ADMIN_COUNTRY.', '.
                    ADMIN_SUPER_ADMIN.', '.
                    ADMIN_CREATED_AT.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    ADMIN_STATUS,
                'left'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".ADMIN_CAMPAIGN_ID .' , left'
                ),
                'order'=> ADMIN_CREATED_AT .' desc '
            ];
        }


        $total_admins = $this->total_records($query);

        $data_admins = $this->search_data($query, $start,PAGINATION_LIST);

        $admins = array(
            'total_admins'=>$total_admins,
            'data_admins'=>$data_admins
        );

        return $admins;
    }

    public function get_export_admins($user,$campaign,$status)
    {
        $where = "";

        if ($user != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".ADMIN_USERNAME." LIKE '%" . $user . "%' ";
            } else
            {
                $where = ADMIN_USERNAME." LIKE '%" . $user . "%' ";
            }
        }

        if ($campaign != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".CAMPAIGNS_ID." = '" . $campaign . "' ";
            } else
            {
                $where = CAMPAIGNS_ID." = '" . $campaign . "' ";
            }
        }

        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".ADMIN_STATUS." = " . $status . " ";
            } else
            {
                $where = ADMIN_STATUS." = " . $status . "";
            }
        }


        if ($where !="")
        {
            $query = [
                'select' =>
                    ADMIN_ID.', '.
                    ADMIN_USERNAME.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    ADMIN_SUPER_ADMIN.', '.
                    ADMIN_CREATED_AT.', '.
                    ADMIN_STATUS,
                'left'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".ADMIN_CAMPAIGN_ID .' , left'
                ),
                'where'=> $where,
                'order'=> ADMIN_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    ADMIN_ID.', '.
                    ADMIN_USERNAME.', '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    ADMIN_SUPER_ADMIN.' , '.
                    ADMIN_COUNTRY.', '.
                    ADMIN_CREATED_AT.', '.
                    ADMIN_STATUS,
                'left'=> array(
                    CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".ADMIN_CAMPAIGN_ID .' , left'
                ),
                'order'=> ADMIN_CREATED_AT .' desc '
            ];
        }

        $admins = $this->search($query);

        return $admins;
    }

    public function get_data_admin($id_admin)
    {

        $query = [
            'select' =>
                ADMIN_ID.', '.
                ADMIN_USERNAME.', '.
                ADMIN_COUNTRY.', '.
                ADMIN_CAMPAIGN_ID.', '.
                CAMPAIGNS_COUNTRY_CODE.', '.
                ADMIN_SUPER_ADMIN.', '.
                ADMIN_STATUS,
            'left'=> array(
                CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".ADMIN_CAMPAIGN_ID .' , left'
            ),
            'where'=> ADMIN_ID .'= '.$id_admin
        ];

        $admin = $this->get_search_row($query);
        return $admin;

    }
    
    public function add_admin($user,$password,$campaign,$super_admin,$status)
    {
        $admin = [
            'username' => $user,
            'password' => $password,
            'campaign_id' => $campaign,
            'super_admin' => $super_admin,
            'status' => $status,
            'created_at' => date('Y-m-d H:i:s')
        ];
        $id = $this->insert($admin, TRUE);
       
        return $id;
    }

    public function update_admin($id_admin,$user,$password,$campaign,$super_admin,$status)
    {

        $parameters = [
            'username' => $user,
            'campaign_id' => $campaign,
            'status' => $status,
            'super_admin'=>$super_admin,
            'updated_at'=>date('Y-m-d H:i:s')
        ];

        if ($password !="" )
        {
            $parameters = [
                'username' => $user,
                'password' => $password,
                'campaign_id' => $campaign,
                'super_admin'=>$super_admin,
                'status' => $status,
                'updated_at'=>date('Y-m-d H:i:s')
            ];
        }
        
        $admin = $this->update($id_admin,$parameters);
        return $admin;

    }




}