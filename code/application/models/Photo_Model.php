<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Photo_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = PHOTOS_TABLE;
        $this->table_id = PHOTOS_ID;
    }

    public function get_list_photos($id_challenge)
    {
        $query = [
            'select'=>  PHOTOS_ID.', '.
                        PHOTOS_CHALLENGE_ID.', 
                        CONCAT("'.URL_HOST_IMAGES.'",ifnull('.PHOTOS_URL.', "'.DEFAULT_IMAGE.'")) as photo ,'.
                        PHOTOS_POSITION.', '.
                        PHOTOS_COORDINATE_X.', '.
                        PHOTOS_COORDINATE_Y.', '.
                        PHOTOS_RADIUS,
            'where'=>   PHOTOS_CHALLENGE_ID . "='". $id_challenge . "' and ".
                        PHOTOS_STATUS ."=".ACTIVE,
            'order'=> 'RAND()',
            'limit'=> RANDOM_PHOTOS
        ];

        $photos = $this->search($query);
        return $photos;

    }

    public function get_photos($id_challenge,$limits,$photos_id)
    {
        $limit="";

        for ($i=0;$i<sizeof($limits);$i++){
            if ($i == sizeof($limits)- 1){
                if (isset($photos_id)>0){
                    $limit .= "'".$limits[$i]."',";
                }else{
                    $limit .= "'".$limits[$i]."'";
                }
            }else{
                $limit .= "'".$limits[$i]."'".",";
            }
        }

        for ($i=0;$i<sizeof($photos_id);$i++){

            if ($i == sizeof($photos_id)- 1){
                $limit .= "'".$photos_id[$i]->id."'";
            }else{
                    $limit .= "'".$photos_id[$i]->id."'".",";
            }
        }

        $where = PHOTOS_ID." not in (".$limit.")";

        $query = [
            'select'=>  PHOTOS_ID.', '.
                PHOTOS_CHALLENGE_ID.', 
                        CONCAT("'.URL_HOST_IMAGES.'",ifnull('.PHOTOS_URL.', "'.DEFAULT_IMAGE.'")) as photo ,'.
                PHOTOS_POSITION.', '.
                PHOTOS_COORDINATE_X.', '.
                PHOTOS_COORDINATE_Y.', '.
                PHOTOS_RADIUS,
            'where'=>   PHOTOS_CHALLENGE_ID . "='". $id_challenge . "' and ".
                PHOTOS_STATUS ."=".ACTIVE ." and ".$where,
            'order'=> 'RAND()',
            'limit'=> '1'
        ];

        $photo = $this->get_search_row($query);
        return $photo;

    }

}
