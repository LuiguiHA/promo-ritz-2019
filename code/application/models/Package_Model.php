<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Package_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = 'package';
        $this->table_id = 'Id';
    }


    public function get_points($id_package)
    {
        $query = [
          'select'=> 'points',
          'where'=>   "id =".$id_package." and status = 1"
        ];

      
        $package = $this->get_search_row($query);
        return $package;
    }
}
