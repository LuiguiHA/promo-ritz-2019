<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Raffle_Prize_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = RAFFLE_PRIZES_TABLE;
        $this->table_id = RAFFLE_PRIZES_ID;
    }

    public function get_list_prizes($campaign){
        $query = [
            'select'=>
                RAFFLE_PRIZES_ID.', '.
                RAFFLE_PRIZES_NAME,
            'join'=>array(
                CAMPAIGNS_TABLE .' , '. CAMPAIGNS_ID ."=".RAFFLE_PRIZES_CAMPAIGN_ID
            ),
            'where'=>   CAMPAIGNS_ID ."='".$campaign."'  and ".
                        RAFFLE_PRIZES_STATUS."=".ACTIVE
        ];

        $prizes = $this->search($query);
        return $prizes;
    }

    public function update_status_prize($id_prize)
    {
        $parameters = [
            'status' => INACTIVE,
            'updated_at'=>date('Y-m-d H:i:s')
        ];

        $prize = $this->update($id_prize,$parameters);
        return $prize;
    }

    public function get_list_raffle_prizes_menu($id_campaign)
    {
        $query = [
            'select'=>
                    RAFFLE_PRIZES_TITLE .', '.
                RAFFLE_PRIZES_NAME.', '.
                'CONCAT("'.URL_HOST_IMAGES.'",ifnull('.RAFFLE_PRIZES_PHOTO.', "'.DEFAULT_IMAGE.'")) as photo ',
            'where'=>   RAFFLE_PRIZES_CAMPAIGN_ID ."=".$id_campaign,
        ];

        $prizes = $this->search_array($query);
        return $prizes;
    }

   /* public function get_prize_by_raffle($id_prize,$campaign)
    {
        $query = [
            'select' =>
                RAFFLE_PRIZES_ID.', '.
                RAFFLE_PRIZES_NAME,

            'where' => RAFFLE_PRIZES_ID."= '".$id_prize."' and ".
                RAFFLE_PRIZES_COUNTRY ."='".$country."'",

        ];

        $prize = $this->get_search_row($query);
        return $prize;
    }*/
    
}