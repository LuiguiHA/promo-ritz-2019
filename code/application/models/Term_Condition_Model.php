<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Term_Condition_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = TERMS_CONDITIONS_TABLE;
        $this->table_id = TERMS_CONDITIONS_ID;
    }

    public function get_terms_by_campaign($id_campaign)
    {
        $query = [
            'select' =>
                TERMS_CONDITIONS_TITLE.', '.
                TERMS_CONDITIONS_DESCRIPTION,
            'where'=> TERMS_CONDITIONS_STATUS .'= '.ACTIVE .' and '.
                    TERMS_CONDITIONS_CAMPAIGN_ID .'='.$id_campaign
        ];

        $terms = $this->get_search_row($query);
        return $terms;

    }

}