<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Prize_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = PRIZES_TABLE;
        $this->table_id = PRIZES_ID;
    }


    public function get_list_prizes($id_campaign)
    {
        $query_active = [
                'select'=>
                            PRIZES_NAME.', '.
                            'CONCAT("'.URL_HOST_IMAGES.'",ifnull('.PRIZES_PHOTO.', "'.DEFAULT_IMAGE.'")) as photo ,'.
                            PRIZES_DESCRIPTION.', '.
                            PRIZES_TYPE.',
                1 as allow',
                'where'=>   PRIZES_CAMPAIGN_ID ."=".$id_campaign." and ".
                            PRIZES_STATUS."=".ACTIVE,
            'group'=>  PRIZES_TYPE.','.PRIZES_PHOTO.','.PRIZES_DESCRIPTION.','.PRIZES_NAME

        ];

        $prizes_active = $this->search_array($query_active);

        $query_inactive = [
            'select'=>
                PRIZES_NAME.', '.
                'CONCAT("'.URL_HOST_IMAGES.'",ifnull('.PRIZES_PHOTO_OFF.', "'.DEFAULT_IMAGE.'")) as photo ,'.
                PRIZES_DESCRIPTION.', '.
                PRIZES_TYPE.',
                0 as allow',
            'where'=>    PRIZES_CAMPAIGN_ID ."=".$id_campaign." and ".
                        PRIZES_STATUS."=".INACTIVE ." and ".
                        PRIZES_TYPE . ' not in
                        ( select '.PRIZES_TYPE .'
                         from '.PRIZES_TABLE .'
                         where '.PRIZES_CAMPAIGN_ID ."= ".$id_campaign ." and ".
                        PRIZES_STATUS ."=".ACTIVE . " group by '".PRIZES_TYPE ."','".PRIZES_PHOTO ."','".PRIZES_DESCRIPTION ."','".PRIZES_NAME ."')",
            'group'=>  PRIZES_TYPE.','.PRIZES_PHOTO.','.PRIZES_DESCRIPTION.','.PRIZES_NAME


        ];

        $prizes_inactive = $this->search_array($query_inactive);

        $prizes = null;

        if (isset($prizes_active) && isset($prizes_inactive))
        {
            $prizes = array_merge($prizes_active,$prizes_inactive);
        } else
        {
            if (isset($prizes_active))
            {
                $prizes = $prizes_active;
            } else if(isset($prizes_inactive))
            {
                $prizes = $prizes_inactive;
            }
        }

        return $prizes;
    }

    public function update_status_prize($id_prize)
    {
        $parameters = [
            'status' => INACTIVE ,
            'updated_at'=>date('Y-m-d H:i:s')
        ];

        $prize = $this->update($id_prize,$parameters);
        return $prize;
    }

    public function update_status($id_prize, $status)
    {
        $parameters = [
            'status' => $status ,
          //  'updated_at'=>date('Y-m-d H:i:s')
        ];

        $prize = $this->update($id_prize,$parameters);
        return $prize;
    }

    public function get_amount_prize($type,$id_campaign)
    {
        $query = [
            'select'=>
                PRIZES_ID.', '.
                PRIZES_NAME,
            'where'=>
                PRIZES_TYPE ."='".$type."' and ".
                PRIZES_CAMPAIGN_ID ."=".$id_campaign." and ".
                PRIZES_STATUS."=".ACTIVE,
        ];

        $prizes = $this->search_array($query);
        return $prizes;
    }

    public function get_name_by_id($id_prize,$country)
    {
        $query = [
            'select'=>
                PRIZES_NAME,
            'where'=>
                PRIZES_ID ."=".$id_prize." and ".
                PRIZES_COUNTRY ."='".$country."' and ".
                PRIZES_STATUS."=".ACTIVE,
        ];

        $prize = $this->get_search_row($query);
        return $prize;
    }

    public function get_all_list_prizes($start)
    {
        $query = [
            'select' =>
                PRIZES_ID.', '.
                PRIZES_NAME.', '.
                PRIZES_TYPE.', created_at, updated_at, '.
                PRIZES_STATUS,
            'order'=>  'status desc, updated_at asc'
        ];

        $total_prizes = $this->total_records($query);

        $data_prizes = $this->search_data($query, $start,1000);

        $prizes = array(
            'total_prizes'=>$total_prizes,
            'data_prizes'=>$data_prizes
        );

        return $prizes;

    }

    public function get_list_prizes_by_type(){
        $query = [
            'select'=>
                PRIZES_NAME.', '.
                PRIZES_TYPE,
            'group'=>   PRIZES_TYPE
        ];

        $prizes = $this->search($query);
        return $prizes;
    }

    public function get_list_prizes_menu($id_campaign)
    {
        $query = [
            'select'=>
                PRIZES_NAME.', '.
                'CONCAT("'.URL_HOST_IMAGES.'",ifnull('.PRIZES_PHOTO_MENU.', "'.DEFAULT_IMAGE.'")) as photo ,'.
                PRIZES_TYPE,
            'where'=>   PRIZES_CAMPAIGN_ID ."=".$id_campaign,
            'group'=>   PRIZES_TYPE.','.PRIZES_PHOTO_MENU.','.PRIZES_NAME
        ];

        $prizes = $this->search_array($query);
        return $prizes;
    }

    public function get_total_prizes_active($id_campaign)
    {
        $query = [
            'select'=>
                PRIZES_ID.', '.
                PRIZES_NAME,
            'where'=>
                PRIZES_CAMPAIGN_ID ."=".$id_campaign." and ".
                PRIZES_STATUS."=".ACTIVE,
        ];

        $prizes = $this->total_records($query);
        return $prizes;
    }

    public function get_total_prizes_instantwin($id_campaign)
    {
        $query = [
            'select'=>
                PRIZES_ID.', photo, type, '.
                PRIZES_NAME,
            'where'=>
                PRIZES_CAMPAIGN_ID ."=".$id_campaign." and ".
                //PRIZES_STATUS."=".ACTIVE ." and type = 2",
                PRIZES_STATUS."=".ACTIVE ."",
            'order' => 'rand()'
        ];

        // $prizes = $this->search($query);
        $prizes = $this->get_search_row($query);
        // $prizes = $this->total_records($query);
        return $prizes;
    }


}
