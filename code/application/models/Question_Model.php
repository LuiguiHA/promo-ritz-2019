<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Question_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = QUESTIONS_TABLE;
        $this->table_id = QUESTIONS_ID;
    }

    public function get_questions_by_campaign($id_campaign)
    {
        $query = [
            'select' =>
                QUESTIONS_TITLE.', '.
                QUESTIONS_DESCRIPTION,
            'where'=> QUESTIONS_STATUS .'= '.ACTIVE .' and '.
                    QUESTIONS_CAMPAIGN_ID .'='.$id_campaign
        ];

        $terms = $this->get_search_row($query);
        return $terms;

    }

}