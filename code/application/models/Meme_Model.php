<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Meme_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "meme";
        $this->table_id = "id";
    }

    public function save($data){
         $this->category_id = $data["category_id"];
        $this->admin_id = $data["admin_id"];
        $this->user_id = $data["user_id"];
        $this->register_at = $data["register_at"];
        $this->name = $data["name"];
        $this->photo = $data["photo"];
        $this->status = $data["status"];

        $this->db->insert('meme', $this);
        return $this->db->insert_id();
    }


    public function getUrlById($id){
        $query = $this->db->query("SELECT photo FROM meme where id=$id LIMIT 1;");
        $row = $query->row(0, 'Meme_Model');
        return $row->photo;
    }

    public function get_list_memes($pagination,$id_campaign)
    {
        $query = [
            'select' => "id, name, CONCAT('".base_url()."',photo) as photo",
            'where' => "status = '1' and  admin='1'"
        ];

        // var_dump($query);
        // die('--->');

        $pagination_next="0";
        $users_amount="0";

        if (is_numeric($pagination) && $pagination > 0)
        {
            $start = PAGINATION_USERS * ($pagination - 1);
            $data_users = $this->search_data_array($query, $start, PAGINATION_USERS);

            $users_amount = $this->total_records($query);
            if($users_amount/PAGINATION_USERS>$pagination)
                $pagination_next="1";
        } else
        {
            $data_users = $this->search_array($query);
        }
        $users = array(
            "data_users" => $data_users,
            "pagination_next" => $pagination_next,
            "users_total" => (string)ceil($users_amount/PAGINATION_USERS)
        );

        return $users;


    }

    public function get_list_memes_by_category($category_id)
    {
        $query = [
            'select' => "meme.id, meme.name, CONCAT('".base_url()."',meme.photo) as photo",
            'join' => array("meme_category, meme_category.id = meme.category_id"),
            'where' => "meme.status = '1' and  meme.admin_id='1' and meme.category_id='".$category_id."'"
        ];

        $memes = $this->search_array($query);;

        return $memes;

    }

    public function get_list_memes_by_user($pagination, $id_user)
    {
        $totalPorPagina = 4;

        $query = [
            'select' => "COALESCE(meme.id,0) as id, COALESCE(meme.name,'') as name, COALESCE(CONCAT('".base_url()."',meme.photo),'') as photo, COALESCE(meme_category.title,'') as category, raffle.numero_lote as lote, raffle.created_at as date",
            'join' => array(
                "meme_category, meme_category.id = meme.category_id"
                ),
            'left' => array(
                "raffle, raffle.meme_id = meme.id, left"
            ),
            'where' => "raffle.user_id='".$id_user."'"
        ];

         

        $pagination_next="0";
        $users_amount="0";

        if (is_numeric($pagination) && $pagination > 0)
        {
            $start = $totalPorPagina * ($pagination - 1);
            $data_users = $this->search_data_array($query, $start, $totalPorPagina);

            $users_amount = $this->total_records($query);
            if($users_amount/$totalPorPagina>$pagination)
                $pagination_next="1";
        } else
        {
            $data_users = $this->search_array($query);
        }
        $users = array(
            "data_users" => $data_users,
            "pagination_next" => $pagination_next,
            "users_total" => (string)ceil($users_amount/$totalPorPagina)
        );

        return $users;
    }

    public function get_list_memes_by_user_and_category($pagination, $id_user, $category_id)
    {
        $query = [
            'select' => "meme.id, meme.name, CONCAT('".base_url()."',meme.photo) as photo",
            'join' => array("meme_category, meme_category.id = meme.category_id"),
            'where' => "meme.status = '1' and meme.user_id='".$id_user."' and meme.category_id='".$category_id."'"
        ];

        // var_dump($query);
        // die('--->');

        $pagination_next="0";
        $users_amount="0";

        if (is_numeric($pagination) && $pagination > 0)
        {
            $start = PAGINATION_USERS * ($pagination - 1);
            $data_users = $this->search_data_array($query, $start, PAGINATION_USERS);

            $users_amount = $this->total_records($query);
            if($users_amount/PAGINATION_USERS>$pagination)
                $pagination_next="1";
        } else
        {
            $data_users = $this->search_array($query);
        }
        $users = array(
            "data_users" => $data_users,
            "pagination_next" => $pagination_next,
            "users_total" => (string)ceil($users_amount/PAGINATION_USERS)
        );

        return $users;


    }

    //

    

}