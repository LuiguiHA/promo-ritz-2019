<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require APPPATH."/libraries/Uuid.php";

class Token_Admin_Model extends MY_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = TOKEN_ADMIN_TABLE;
        $this->table_id = TOKEN_ADMIN_ID;
    }

    public function save_token($token, $admin_id)
    {
        $parameters = [
            'admin_id' => $admin_id,
            'token' => $token,
            'created_at' => date('Y-m-d H:i:s'),
            'status' => ACTIVE
        ];
        $id_token = $this->insert($parameters,TRUE);
        return $id_token;


    }

    public function generate_token()
    {
        $token = Uuid::generate();
        return (string)$token;
    }

    public function update_token_close_session($token)
    {
        $parameters = [
            TOKEN_ADMIN_STATUS => INACTIVE,
            TOKEN_ADMIN_UPDATED_AT=> date("Y-m-d H:i:s")
        ];

        $where = TOKEN_ADMIN_TOKEN." = '$token'";

        return $this->update_where($where ,$parameters);

    }

}
