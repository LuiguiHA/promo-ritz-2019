<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Campaign_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = CAMPAIGNS_TABLE;
        $this->table_id = CAMPAIGNS_ID;
    }


    public function get_campaign($id_campaign)
    {
        $query = [
            'select' => CAMPAIGNS_ID.', '.
                CAMPAIGNS_COUNTRY_CODE.', '.
                CAMPAIGNS_COUNTRY_NAME.', '.
                CAMPAIGNS_PRIZE_DAY.', '.
                CAMPAIGNS_RANDOM.', '.
                CAMPAIGNS_STATUS,
            'where'=> CAMPAIGNS_ID ."=".$id_campaign
        ];

        $campaign = $this->get_search_row($query);
        return $campaign;
    }

    public function get_list_campaign()
    {
        $query = [
            'select' => CAMPAIGNS_ID.', '.
                CAMPAIGNS_COUNTRY_CODE.', '.
                CAMPAIGNS_COUNTRY_NAME.', '.
                CAMPAIGNS_STATUS,

        ];

        $campaign = $this->search($query);
        return $campaign;
    }

    public function get_configuration($id_campaign)
    {
        $query = [
            'select' => CAMPAIGNS_ID . ', ' .
                CAMPAIGNS_PRIZE_DAY . ', ' .
                CAMPAIGNS_RANDOM . ', ' .
                CAMPAIGNS_STATUS,
            'where' => CAMPAIGNS_ID . "=" . $id_campaign
        ];

        $campaign = $this->get_search_row($query);
        return $campaign;

    }

    public function update_configuration($id_campaign,$prize_day,$random)
    {
        $paramters = [
           'prize_day'=>$prize_day,
           'random'=>$random,
           'updated_at'=>date('Y-m-d H:i:s')
        ];

        $campaign = $this->update($id_campaign,$paramters);
        return $campaign;
    }

}