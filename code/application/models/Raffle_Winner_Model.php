<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Raffle_Winner_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = RAFFLE_WINNERS_TABLE;
        $this->table_id = RAFFLE_WINNERS_ID;
    }

    public function register_user_raffle_winner($prize,$id_user,$campaign)
    {
        $query = [
            'raffle_prize_id'=>$prize,
            'user_id'=>$id_user,
            'campaign_id'=>$campaign,
            'created_at'=>date('Y-m-d H:i:s'),
            'status'=>ACTIVE
        ];

        $winner= $this->insert($query);

        return $winner;
    }

    public function get_prize_by_raffle($id_prize,$campaign)
    {
        $query = [
            'select' =>
                RAFFLE_WINNERS_ID.', '.
                RAFFLE_WINNERS_USER_ID,
            'where' => RAFFLE_WINNERS_PRIZE_ID."= '".$id_prize."' and ".
                RAFFLE_WINNERS_CAMPAIGN_ID ."='".$campaign."'",

        ];

        $prize = $this->get_search_row($query);
        return $prize;
    }

    public function get_list_user_winners($start,$campaign,$super_admin)
    {
        if ($super_admin != SUPER_ADMIN)
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    PRIZES_NAME.' as prize_name , '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_WINNERS_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_WINNERS_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". RAFFLE_WINNERS_CAMPAIGN_ID,
                    PRIZES_TABLE . ' , ' . PRIZES_ID."=".RAFFLE_WINNERS_PRIZE_ID
                ),
                'where'=> CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'",
                'order'=> RAFFLE_WINNERS_CREATED_AT .' desc '
            ];
        }else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    PRIZES_NAME.' as prize_name , '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_WINNERS_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_WINNERS_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". RAFFLE_WINNERS_CAMPAIGN_ID,
                    PRIZES_TABLE . ' , ' . PRIZES_ID."=".RAFFLE_WINNERS_PRIZE_ID
                ),
                'order'=> RAFFLE_WINNERS_CREATED_AT .' desc '
            ];
        }


        $total_users = $this->total_records($query);

        $data_users = $this->search_data($query, $start,PAGINATION_LIST);

        $users = array(
            'total_users'=>$total_users,
            'data_users'=>$data_users
        );

        return $users;

    }

    public function get_search_users_winners_raffle($start,$user,$dni,$country,$campaign,$status)
    {
        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI."  LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%'";
            }
        }

        if ($user != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $user . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $user . "%' ";
            }
        }

        if ($campaign != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            } else
            {
                $where = CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            }
        }

        if ($country != "" )
        {

            if ($where != "")
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  " AND ". USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where .= " AND ".USERS_COUNTRY." = '" . $country . "' ";
                }

            } else
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where = USERS_COUNTRY." = '" . $country . "' ";
                }

            }
        }


        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".RAFFLE_WINNERS_STATUS." = " . $status . " ";
            } else
            {
                $where = RAFFLE_WINNERS_STATUS." = " . $status . "";
            }
        }


        if ($where !="")
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    PRIZES_NAME.' as prize_name , '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_WINNERS_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_WINNERS_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". RAFFLE_WINNERS_CAMPAIGN_ID,
                    PRIZES_TABLE . ' , ' . PRIZES_ID."=".RAFFLE_WINNERS_PRIZE_ID
                ),
                'where'=>$where,
                'order'=> RAFFLE_WINNERS_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    PRIZES_NAME.' as prize_name , '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_WINNERS_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_WINNERS_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". RAFFLE_WINNERS_CAMPAIGN_ID,
                    PRIZES_TABLE . ' , ' . PRIZES_ID."=".RAFFLE_WINNERS_PRIZE_ID
                ),
                'order'=> RAFFLE_WINNERS_CREATED_AT .' desc '
            ];
        }


        $total_users = $this->total_records($query);

        $data_users = $this->search_data($query, $start,PAGINATION_LIST);

        $users = array(
            'total_users'=>$total_users,
            'data_users'=>$data_users
        );

        return $users;
    }

    public function export_list_user_winners_raffle($user,$dni,$country,$campaign,$status)
    {
        $where = "";

        if ($dni != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_DNI."  LIKE '%" . $dni . "%' ";
            } else
            {
                $where = USERS_DNI." LIKE '%" . $dni . "%'";
            }
        }

        if ($user != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".USERS_NAME." LIKE '%" . $user . "%' ";
            } else
            {
                $where = USERS_NAME." LIKE '%" . $user . "%' ";
            }
        }

        if ($country != "" )
        {

            if ($where != "")
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  " AND ". USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where .= " AND ".USERS_COUNTRY." = '" . $country . "' ";
                }

            } else
            {
                if ($country == COUNTRY_OTHERS)
                {
                    $where .=  USERS_COUNTRY . " not in ('PE','EC','PR','DO')";
                }else
                {
                    $where = USERS_COUNTRY." = '" . $country . "' ";
                }

            }
        }

        if ($campaign != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            } else
            {
                $where = CAMPAIGNS_COUNTRY_CODE." = '" . $campaign . "'";
            }
        }

        if ($status != "" )
        {
            if ($where != "")
            {
                $where .= " AND ".RAFFLE_WINNERS_STATUS." = " . $status . " ";
            } else
            {
                $where = RAFFLE_WINNERS_STATUS." = " . $status . "";
            }
        }


        if ($where !="")
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    PRIZES_NAME.' as prize_name , '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_WINNERS_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_WINNERS_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". RAFFLE_WINNERS_CAMPAIGN_ID,
                    PRIZES_TABLE . ' , ' . PRIZES_ID."=".RAFFLE_WINNERS_PRIZE_ID
                ),
                'where'=>$where,
                'order'=> RAFFLE_WINNERS_CREATED_AT .' desc '
            ];
        } else
        {
            $query = [
                'select' =>
                    USERS_ID.', '.
                    USERS_NAME.', '.
                    USERS_DNI.', '.
                    USERS_EMAIL.', '.
                    USERS_PHONE.', '.
                    USERS_POINTS.', '.
                    USERS_COUNTRY.', '.
                    PRIZES_NAME.' as prize_name , '.
                    CAMPAIGNS_COUNTRY_CODE.', '.
                    RAFFLE_WINNERS_STATUS,
                'join' => array(
                    USERS_TABLE.', '. USERS_ID ."=".RAFFLE_WINNERS_USER_ID,
                    CAMPAIGNS_TABLE . ' , '. CAMPAIGNS_ID ."=". RAFFLE_WINNERS_CAMPAIGN_ID,
                    PRIZES_TABLE . ' , ' . PRIZES_ID."=".RAFFLE_WINNERS_PRIZE_ID
                ),

                'order'=> RAFFLE_WINNERS_CREATED_AT .' desc '
            ];
        }

        $users = $this->search($query);

        return $users;
    }



}