<?php

class MY_Controller extends CI_Controller
{

    protected $arrayVista;
    protected $elementoPorPagina = PAGINATION_LIST;

    function __construct()
    {
        parent::__construct();
        $this->arrayVista = array("nombreWeb" => "Ritz","mostrarMenuLateral"=>true,"tituloPagina"=>"Promo Rtiz");
    }

    protected function cargarVistaLoginCMS()
    {
        $this->load->view("cms/layout/plantilla_login_view", $this->arrayVista);
    }

    protected function cargarVistaCMS()
    {
        $this->load->view("cms/layout/plantilla_view", $this->arrayVista);
    }

    protected function cargarVistaListaAjax($vista)
    {
        $this->load->view($vista, $this->arrayVista);
    }

    protected function checkLoginBackEnd($controlador)
    {

        $array = $this->session->userdata();

        if ($this->session->userdata('logged_in') !== TRUE)
            redirect(site_url($this->config->item('path_backend')));
        else {
            if ($this->session->userdata('usuario_sistema')->tipo == TIPO_USUARIO_ADMINISTRADOR_CMS) {
                if ($controlador == NOMBRE_CONTROLADOR_USUARIO)
                    redirect(site_url($this->config->item('path_backend')));
            }
        }

    }


    protected function obtenerPaginadoListado($baseUrl, $totalRegistros,$segmentoURI = 0)
    {
        $this->load->library('pagination');

        $config = array();
        $config["base_url"] = $baseUrl;
        $config["total_rows"] = $totalRegistros;

        $config["uri_segment"] = $segmentoURI;



        $config["per_page"] = PAGINATION_LIST;
        //$config["num_links"] = $cantidadLinks;


        $config['full_tag_open'] = '<div id="paginador"><ul class="pagination pagination-sm pull-right push-down-20">';
        $config['full_tag_close'] = '</ul></div>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a  href="#" class="disabled">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['last_link'] = 'Último';
        $config['first_link'] = 'Inicio';

        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }


    protected function subirArchivo($nombre_file = "", $path_file = "")
    {

        $config['upload_path'] = $path_file;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        $array_resultado = array();

        if (!$this->upload->do_upload($nombre_file))
            $array_resultado = array('error' => $this->upload->display_errors());
        else
            $array_resultado = array('upload_data' => $this->upload->data());


        return $array_resultado;
    }

    protected function alert($message, $url = null)
    {

        print '<script language="JavaScript">';
        print 'alert("' . $message . '");';
        if (isset($url)) print 'window.location.assign("' . $url . '");';
        print '</script>';
    }

    protected function fechaImagen($foto)
    {

        $dir = "./upload/categoria_promocion/";
        $files = scandir($dir);
        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                $file1 = $dir . "./$file/";
                $file1 = scandir($file1);
                foreach ($file1 as $imagen) {
                    if ($imagen == $foto) {
                        $fecha = $file;
                        return $fecha;
                        exit;
                    }else{
                        return null;
                    }
                }

            }
        }
    }

    protected function json_url($token,$url,$type,$data){


        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        $CI = &get_instance();
        if ($type == "POST")
        {
            curl_setopt($ch, CURLOPT_POST, true);
        }else
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $CI->input->user_agent());

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'token:'.$token,
            'Content-Type: application/json'
        ));

        $result = curl_exec($ch);
        $pos = strpos($result, "{");
        $result=  substr($result,$pos);
        $result = json_decode($result);
        $result = json_decode(json_encode($result), True);
        return $result;
    }

    function validar_formato_fecha($fecha){
        //YYYY-MM-DD
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$fecha))
        {
            return true;
        }else{
            return false;
        }
    }

    function validation_admin()
    {
        
        $country = "";
        if ($this->session->userdata("country") == COUNTRY_ADMIN)
        {
            $country = COUNTRY_ADMIN;
        } elseif($this->session->userdata("country") == COUNTRY_PERU)
        {
            $country = COUNTRY_PERU;
        } elseif($this->session->userdata("country") == COUNTRY_PUERTO_RICO)
        {
            $country = COUNTRY_PUERTO_RICO;
        } elseif($this->session->userdata("country") == COUNTRY_DOMINICAN_REPUBLIC)
        {
            $country = COUNTRY_DOMINICAN_REPUBLIC;
        }
        
        return $country;
    }


}

