<?php
/* $lang[module_*]*/

$lang['user_success_created'] = 'Usuario creado de manera exitosa.';
$lang['user_success_login'] = 'Ingreso correcto.';
$lang['user_success_close_session'] = 'Se cerró la sesión de manera exitisa.';
$lang['user_success_detail'] = 'Se obtuvo el detalle del usuario de manera exitosa.';
$lang['user_success_points'] = 'Se obtuvo los puntos del usuario de manera exitosa.';
$lang['user_success_accumulated'] = 'Se acumulo de manera exitosa los puntos.';
$lang['user_success_list_winners'] = 'Se obtuvo la lista de ganadores de manera exitosa.';
$lang['user_success_list_memes'] = 'Se obtuvo la lista de memes de manera exitosa.';
$lang['user_success_recover'] = 'Se obtuvo los datos del usuario de manera exitosa.';

$lang['challenge_success_list'] = 'Se obtuvo la lista de desafios de manera exitosa.';

$lang['photo_success_list'] = 'Se obtuvo la lista de las fotos de manera exitosa.';

$lang['concourse_success_create'] = 'Se registro de manera exitosa la participación.';

$lang['prize_success_register_prize_user'] = 'Se registro el premio al usuario manera exitosa.';

$lang['prize_success_list_menu'] = 'Se obtuvo la lista de premios de manera exitosa.';

$lang['term_condition_success_list'] = 'Se obtuvo los terminos y condiciones de manera exitosa.';

$lang['question_success_list'] = 'Se obtuvo las preguntas de manera exitosa.';

$lang['prize_success_list_winner'] = 'Se obtuvo la lista de premios de manera exitosa.';

