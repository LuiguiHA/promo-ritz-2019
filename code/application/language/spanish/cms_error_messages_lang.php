<?php


$lang['login_form_user_required'] = 'Debe ingresar un usuario';
$lang['login_form_password_required'] = 'Debe ingresar su password';

$lang['login_error_user_incorrect'] = 'El usuario ingresado es incorrecto';
$lang['login_error_password_incorrect'] = 'La contraseña ingresada es incorrecta';
$lang['login_error_user_inactive'] = 'El usuario ingresado se encuentra inactivo';
$lang['login_error_token'] = 'No se ha podido iniciar sesion';

$lang['admin_error_update'] = 'No se pudo actualizar los dato del administrador';
$lang['admin_error_add'] = 'No se pudo agregar al administrador';

$lang['raffle_prizes_error_list'] = 'No se pudo obtener los premios';

$lang['raffle_error_user'] = 'No se pudo obtener al usuario';

$lang['raffle_error_prize_cero'] = 'El premio ya se agoto';
$lang['raffle_error_total_user'] = 'No se pudo obtener el numero de participantes';
$lang['raffle_error_total_user_cero'] = 'No hay participantes';
$lang['raffle_error_update_user'] = 'No se pudo actualizar al usuario';
$lang['raffle_error_update_raffle_prize'] = 'No se pudo actualizar el premio';
$lang['raffle_error_user_inactive'] = 'El participante esta inactivo , intente nuevamente';
$lang['raffle_error_user_campaign'] = 'El participante no participa en esta campaña , intente nuevamente ';
$lang['raffle_error_register_winner'] = 'No se pudo registrar al ganador';
$lang['configuration_error_get_detail'] = 'No se pudo obtener la configuración de la campaña';
$lang['configuration_error_update'] = 'No se pudo actualizar la configuración';
