<?php
/* $lang[module_*]*/

$lang['constant_title_default'] = 'Ritz';
/* MENU */
$lang['constant_menu_user'] = 'Usuarios';
$lang['constant_menu_user_list'] = 'Lista';
$lang['constant_menu_user_list_winners'] = 'Lista de Ganadores';

$lang['constant_menu_admin'] = 'Administradores';
$lang['constant_menu_subasta'] = 'Subastas';

$lang['constant_menu_prize'] = 'Premios';

$lang['constant_menu_raffle'] = 'Sorteo';
$lang['constant_menu_raffle_list'] = 'Lista';
$lang['constant_menu_raffle_generate'] = 'Generar';
$lang['constant_menu_raffle_winners'] = 'Lista de Ganadores';

$lang['constant_menu_configuration'] = 'Configuración';


/* GENERAL */

$lang['constant_list_actions'] = 'Acciones';
$lang['constant_select'] = 'Seleccione';
$lang['constant_btn_search'] = 'Buscar';
$lang['constant_btn_save'] = 'Guardar';
$lang['constant_btn_add'] = 'Agregar';
$lang['constant_btn_cancel'] = 'Cancelar';
$lang['constant_btn_accept'] = 'Aceptar';
$lang['constant_status_active'] = 'Activo';
$lang['constant_status_inactive'] = 'Inactivo';
$lang['constant_country_peru'] = 'Perú';
$lang['constant_country_dominican_republic'] = 'República Dominicana';
$lang['constant_country_ecuador'] = 'Ecuador';
$lang['constant_country_puerto_rico'] = 'Puerto Rico';
$lang['constant_country_admin'] = 'Administrador';
$lang['constant_country_others'] = 'Otros';

$lang['constant_export_excel_list'] = 'Exportar excel';

$lang['constant_yes'] = 'Si';
$lang['constant_no'] = 'No';

$lang['constant_error_service']= "No se pudo conectar con el servidor";
/* USER*/

$lang['constant_form_title_user'] = 'Usuarios';
$lang['constant_list_total_user'] = 'Total de usuarios : ';
$lang['constant_excel_user'] = 'Usuarios';
$lang['constant_excel_user_winner'] = 'Usuarios_ganadores';


$lang['constant_form_search_dni_user'] = 'DNI';
$lang['constant_form_search_name_user'] = 'Nombre';
$lang['constant_form_search_country_user'] = 'País';
$lang['constant_form_search_campaign_user'] = 'Campaña';
$lang['constant_form_search_status_user'] = 'Estado';

$lang['constant_list_name_user'] = 'Nombre';
$lang['constant_list_dni_user'] = 'DNI';
$lang['constant_list_email_user'] = 'Email';
$lang['constant_list_phone_user'] = 'Telefono';
$lang['constant_list_country_user'] = 'País';
$lang['constant_list_campaign_user'] = 'Campaña';
$lang['constant_list_points_user'] = 'Puntos';
$lang['constant_list_status_user'] = 'Estado';
$lang['constant_list_prize_user'] = 'Premio';
$lang['constant_list_created_at_user'] = 'Fecha de registro';
$lang['constant_list_winner_at_user'] = 'Fecha ganada';
$lang['constant_list_share_user'] = 'Compartir facebook';


$lang['constant_form_title_user_winner'] = 'Usuarios Ganadores';

$lang['constant_form_search_prize_user'] = 'Premio';

/*ADMIN*/

$lang['constant_excel_admin'] = 'Administradores';

$lang['constant_country_super_admin'] = 'Todos';

$lang['constant_form_search_super_admin'] = 'Todos (Super admin)';

$lang['constant_form_title_admin'] = 'Administradores';
$lang['constant_list_total_admins'] = 'Total de administradores : ';

$lang['constant_form_search_user_admin'] = 'Usuario';
$lang['constant_form_search_status_admin'] = 'Estado';
$lang['constant_form_search_country_admin'] = 'País';
$lang['constant_form_search_campaign_admin'] = 'Campaña';



$lang['constant_list_name_admin'] = 'Usuario';
$lang['constant_list_country_admin'] = 'País';
$lang['constant_list_super_admin'] = 'Super admin';
$lang['constant_list_campaign_admin'] = 'Campaña';
$lang['constant_list_created_at_admin'] = 'Fecha de creación';
$lang['constant_list_status_admin'] = 'Estado';

$lang['constant_form_title_admin'] = 'Administrador';
$lang['constant_form_user_admin'] = 'Usuario';
$lang['constant_form_password_admin'] = 'Contraseña';
$lang['constant_form_country_admin'] = 'País';
$lang['constant_form_campaign_admin'] = 'Campaña';
$lang['constant_form_super_admin'] = 'Super Admin';
$lang['constant_form_status_admin'] = 'Estado';

$lang['constant_form_required_user_admin'] = 'Debe ingresar uno nombre de usuario';
$lang['constant_form_required_password_admin'] = 'Debe ingresar una contraseña';
$lang['constant_form_required_campaign_admin'] = 'Debe seleccionar un campaña';
$lang['constant_form_required_status_admin'] = 'Debe seleccionar un estado';

/*RAFFLE*/

$lang['constant_form_search_title_raffle'] = 'Participantes';
$lang['constant_form_winner_raffle'] = 'Ganador';

$lang['constant_list_winner_raffle'] = 'Ganador';
$lang['constant_list_prize_raffle'] = 'Premio';

$lang['constant_form_title_raffle'] = 'Sorteo';
$lang['constant_form_prize_raffle'] = 'Premio';

$lang['constant_btn_raffle'] = 'Sortear';

$lang['constant_form_required_country_raffle'] = 'Debe seleccionar un país';
$lang['constant_form_required_prize_raffle'] = 'Debe seleccionar un premio';

$lang['constant_excel_raffle'] = 'Participantes';

$lang['constant_title_detail'] = 'Datos del Ganador';
$lang['constant_form_name_detail'] = 'Nombre';
$lang['constant_form_dni_detail'] = 'DNI';
$lang['constant_form_phone_detail'] = 'Telefono';
$lang['constant_form_email_detail'] = 'Email';
$lang['constant_form_points_detail'] = 'Puntos';
$lang['constant_form_prize_detail'] = 'Premio';
$lang['constant_form_status_detail'] = 'Estado';

/* CONFIGURATION */

$lang['constant_form_title_configuration'] = 'Configuración';

$lang['constant_form_prizes_day_configuration'] = 'Premios por dia';
$lang['constant_form_random_configuration'] = 'Random';
$lang['constant_form_probability_configuration'] = 'Probabilidad';

$lang['constant_form_required_campaign_configuration'] = 'Debe seleccionar una campaña';
$lang['constant_form_required_prize_day_configuration'] = 'Debe ingresar una cantidad en premios por dia';
$lang['constant_form_required_random_configuration'] = 'Debe ingresar una cantidad en random';
$lang['constant_form_required_random_cero_configuration'] = 'Debe ingresar una cantidad mayor que cero en random';






