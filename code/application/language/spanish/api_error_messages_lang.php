<?php

//Form validation
$lang['user_form_name_required'] = 'El campo nombre no puede ser vacío.';
$lang['user_form_name_alphabet'] = 'Ingresar un nombre correcto.';
$lang['user_form_dni_required'] = 'El campo DNI no puede ser vacío.';
$lang['user_form_dni_numeric'] = 'Ingrese un DNI correcto.';
$lang['user_form_dni_length'] = 'Ingrese un DNI con 8 digitos.';
$lang['user_form_dni_size'] = 'Ingrese un DNI correcto.';
$lang['user_form_phone_required'] = 'El campo teléfono no puede ser vacío.';
$lang['user_form_phone_size'] = 'Ingrese un teléfono correcto.';
$lang['user_form_phone_length'] = 'Ingrese un teléfono de 9 digitos.';
$lang['user_form_phone_numeric'] = 'Ingrese un teléfono correcto.';
$lang['user_form_email_required'] = 'El campo correo no puede ser vacío.';
$lang['user_form_email_required'] = 'El campo correo no puede ser vacío.';
$lang['user_form_email_valid'] = 'Ingrese un correo correcto.';
$lang['user_error_created'] = 'No se pudo registrar al usuario.';
$lang['user_error_exist'] = 'El usuario ya existe.';
$lang['user_error_exist_dni'] = 'El DNI ya ha sido registrado anteriormente.';
$lang['user_error_exist_email'] = 'El email ya ha sido registrado anteriormente.';
$lang['user_error_found'] = 'No se pudo encontrar al usuario.';
$lang['user_error_send_email'] = 'No se pudo enviar el email.';
$lang['campaign_error_get'] = 'No hay campaña activa.';
$lang['campaign_error_get_campaign'] = 'No se pudo obtener la campaña.';
$lang['campaign_error_inactive'] = 'La campaña para su pais no esta habilitada.';


$lang['user_error_token'] = 'Usuario no autorizado, Token inválido.';
$lang['user_error_close_session'] = 'No se puedo cerrar la sesión.';

$lang['user_error_get_detail'] = 'No se puedo obtener el detalle del usuario.';
$lang['user_error_get_points'] = 'No se puedo obtener los puntos del usuario.';

$lang['challenge_error_list'] = 'No se puedo obtener la lista de los desafios.';
$lang['challenge_error_detail'] = 'No se puedo obtener la foto del desafio.';
$lang['challenge_error_list_empty'] = 'En este momento no hay desafios habilitados.';
$lang['challenge_error_validation_user_win'] = 'No se ha podido validar los desafios ganados por el usuario.';
$lang['challenge_error_validation_user'] = 'No se ha podido validar el desafio del usuario.';
$lang['challenge_error_user_challenge'] = 'El usuario paso el limite de triunfos en el desafio.';

$lang['photo_error_list'] = 'No se puedo obtener la lista de las fotos.';
$lang['photo_error_list_empty'] = 'En este momento no hay fotos habilitados.';
$lang['photo_error_list_challenge_empty'] = 'El desafio es incorrecto.';

$lang['concourse_error_user_required'] = 'El id del usuario no puede ser vacío.';

$lang['concourse_error_challenge_required'] = 'El id del desafio no puede ser vacío.';
$lang['concourse_error_challenge_numeric'] = 'El id del desafio debe ser entero.';
$lang['concourse_error_challenge_natural'] = 'El id del desafio debe ser un numero positivo.';
$lang['concourse_error_time_required'] = 'El tiempo no puede ser vacío.';
$lang['concourse_error_time_numeric'] = 'El tiempo debe ser entero.';
$lang['concourse_error_time_natural'] = 'El tiempo debe ser un numero positivo.';
$lang['concourse_error_win_required'] = 'El campo win no puede ser vacío.';
$lang['concourse_error_win_numeric'] = 'El campo win debe ser entero.';
$lang['concourse_error_win_natural'] = 'El campo win debe ser un numero positivo.';
$lang['concourse_error_create'] = 'No se pudo registrar la participación.';

$lang['concourse_error_max_won_per_challenge'] = 'Usted ya sobrepaso el limite de participaciones ganadas por desafio.';
$lang['concourse_error_max_won_per_day'] = 'Usted ya sobrepaso el limite de participaciones ganadas por dia.';
$lang['concourse_error_user_prize'] = 'Usted ya gano un premio anteriormente.';
$lang['concourse_error_list_prize'] = 'No se puedo obtener el listado de los premios.';
$lang['concourse_error_list_prize_empty'] = 'En este momento no hay premios habilitados.';
$lang['concourse_error_user'] = 'No se puedo encontrar al usuario.';
$lang['concourse_error_raffle'] = 'No se pudo registrar para el sorteo.';
$lang['concourse_error_user_win'] = 'No se pudo actualizar al usuario.';


$lang['concourse_error_points'] = 'No se puedo acumular los puntos.';

$lang['user_error_get_points'] = 'No se pudo obtener los datos del usuario.';
$lang['user_error_shared_facebook'] = 'Usted ya no puedo acumular puntos mediante facebook.';

$lang['user_error_list_winners'] = 'No se puedo obtener la lista de ganadores.';
$lang['user_error_list_winners_empty'] = 'En estos momentos no hay ningún ganador.';
$lang['user_error_list_memes'] = 'No se puedo obtener la lista de memes.';
$lang['user_error_list_memes_empty'] = 'En estos momentos no hay ningún meme.';

$lang['prize_error_user_required'] = 'El id del usuario no puede ser vacío.';
$lang['prize_error_prize_required'] = 'El id del premio no puede ser vacío.';
$lang['prize_error_type_prize_required'] = 'El tipo del premio no puede ser vacío.';
$lang['prize_error_register_prize_user'] = 'No se pudo registrar el premio al usuario.';
$lang['prize_error_update_prize_user'] = 'No se pudo actualizar el estado del premio.';
$lang['prize_error_amount_prize'] = 'No se pudo obtener la cantidad del premio.';

$lang['prize_error_amount_prize_cero'] = 'El premio ya se ha agotado.';
$lang['prize_error_get_country_user'] = 'No se pudo obtener el pais del usuario.';
$lang['prize_error_get_name_prize'] = 'No se pudo obtener el nombre del premio.';

$lang['prize_error_user_win'] = 'El usuario todavia no ha ganado.';
$lang['prize_error_user_win_prize'] = 'El usuario ya gano un premio.';

$lang['prize_error_get_list_id_prizes'] = 'No se pudo obtener los premios.';
$lang['prize_error_get_list_menu'] = 'No se pudo obtener el listado de los premios.';
$lang['prize_error_get_list_raffle_menu'] = 'No se pudo obtener el listado de los premios de sorteo.';

$lang['term_condition_error_list'] = 'No se pudo obtener los terminos y condiciones.';

$lang['question_error_list'] = 'No se pudo obtener las preguntas.';

$lang['winner_error_get_users'] = 'No se puedo obtener los usuarios de cada premio.';

$lang['country_error_find'] = 'País no encontrado.';




