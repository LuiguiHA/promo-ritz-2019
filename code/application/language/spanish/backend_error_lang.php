<?php

$lang['error_banner_agregar'] = 'Error al agregar banner';
$lang['error_beneficio_agregar'] = 'Error al agregar beneficio';
$lang['error_carta_agregar'] = 'Error al agregar carta';
$lang['error_categoria_agregar'] = 'Error al agregar categoria';
$lang['error_contacto_agregar'] = 'Error al agregar contacto';
$lang['error_empresa_agregar'] = 'Error al agregar empresa';
$lang['error_formula_agregar'] = 'Error al agregar formula';
$lang['error_foto_agregar'] = 'Error al agregar foto';
$lang['error_foto_eliminar'] = 'Error al eliminar la foto';
$lang['error_informacion_adicional_agregar'] = 'Error al agregar la información adicional';
$lang['error_informacion_adicional_eliminar'] = 'Error al eliminar la información adicional';
$lang['error_local_agregar'] = 'Error al agregar local';
$lang['error_local_promocion_eliminar'] = 'Error al quitar el  local';
$lang['error_promocion_agregar'] = 'Error al agregar promoción';
$lang['error_usuario_agregar'] = 'Error al agregar usuario';
$lang['error_tamaño_imagen'] = 'Imagen no válida, verifica que el peso máximo sea de 1 MB y tenga el formato correcto (*.jpg, *.png)';
$lang['error_password'] = 'Password Incorrecto';
$lang['error_usuario'] = 'Este usuario está desactivado.';
$lang['error_usuario_incorrecto'] = 'Usuario incorrecto';
$lang['error_local_ubicacion_agregar'] = 'Error al agregar ubicación del local';
$lang['error_local_horario_agregar'] = 'Error al agregar horario del local';
$lang['error_local_empresa_empty'] = 'La empresa no cuenta con locales';
$lang['error_local_beneficio_minimo'] = 'El beneficio tiene que tener un local seleccionado';
