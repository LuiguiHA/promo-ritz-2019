var memeList = new Vue({
    el: "#meme-list",
    delimiters: ['${', '}'],
    props:{},
    data: {
        memes: [],
        memesFiltered: [],
        dialogVisibleShare: false,
        dialogVisibleShareSuccess: false,
        memeSelected: '',
        type: '',
        lote: '',
        selectedNetwork: 0,
        memeSelectedId: 0
    },
    validations: {
        lote:{
            required: requiredValidator,
            regex: loteValidator,
        }
    },
    components: {
    },
    watch: {
          dialogVisibleShare: function(newValue){
              if(newValue == false){
                  this.$v.lote.$reset()
              }
          },
        type: function(value){
            this.memesFiltered = _.find(this.memes, function(m){ return m.category == value})
            this.memesFiltered = this.memesFiltered.memes
        }
    },
    created: function(){
        if(localStorage.getItem('token') == null){
          window.location = "/home"
        }
        document.getElementById('meme-list').style.display = 'block';
    },
    mounted: function(){
        this.loadMemes()
    },
    computed: {
    },
    methods:{
        selectNetwork: function(network){
          if(this.selectedNetwork == network){
            this.selectedNetwork = 0
          } else {
            this.selectedNetwork = network  
          }
        },
        shareMeme: function(){

            var popupShare = null

            var formData = new FormData();
            formData.append("token",  localStorage.getItem('token'))
            formData.append("option", this.selectedNetwork)
            formData.append("lote", this.lote)
            formData.append("meme", this.memeSelectedId)

            var url = window.siteUrl+"meme/"+this.memeSelectedId

            if(this.selectedNetwork == 1){
                FB.ui({
                  method: 'share',
                  href: url,
                }, function(responseShare){
                  if(responseShare == undefined){
                    // formData.append("option", 0)
                    // this.saveShare(1, formData, null, null)
                  } else {
                    formData.append("option", this.selectedNetwork)
                    this.saveShare(1, formData, null, null)
                  }
                }.bind(this));
            }

            if(this.selectedNetwork == 3){
                popupShare = window.open("", "_blank")
                formData.append("option", this.selectedNetwork)
                this.saveShare(1, formData, url, popupShare)
            }

            if(this.selectedNetwork == 0){
                // formData.append("option", 0)
                // this.saveShare(0, formData, url, null)
            }

            

        },
        saveShare: function(optionsEarned, formData, url, popupShare){
            axios.post("/api/users/share_meme", formData)
            .then(function (responseShare) {
              
              if(this.selectedNetwork == 3){
                this.shareUrl = "https://api.whatsapp.com/send?text=" + url  
                popupShare.location = this.shareUrl 
              }

              if(responseShare.data.prize){
                menu.showWinner(responseShare.data.prize.type)
              } else {
                menu.showShareSuccess(optionsEarned)
              }

              menu.loadMe()
              this.resetShare()
              

            }.bind(this))
            .catch(function (error) {
                this.resetShare()
                menu.showShareError()
            }.bind(this));
        },
        resetShare: function(){
            this.dialogVisibleShare = false
            this.lote = ""
            this.memeSelectedId = 0
            this.selectedNetwork = 0
        },
        loadMemes: function(){
            axios.get('api/users/list_memes/1')
            .then(function(response){
                if(response.data.result == 1){
                    this.memes = response.data.memes
                    this.type = 'Animales'
                }
            }.bind(this))
        },
        removeLote: function(){
            this.lote = ''
        },
        share: function(id){
            if(localStorage.getItem('token') != undefined){
                var meme = _.find(this.memesFiltered, function(m){ return m.id == id})
                this.memeSelected = meme.photo
                this.memeSelectedId = meme.id
                this.dialogVisibleShare = true
            } else {
                menu.dialogVisibleLogin = true
            }
        }
    }
  })