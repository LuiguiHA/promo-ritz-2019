var BeatLoader = VueSpinner.BeatLoader
var awards = new Vue({
    el: "#awards",
    delimiters: ['${', '}'],
    props:{},
    data: {
      desktop: true
    },
    validations: {
    },
    watch: {
    },
    created: function(){
    },
    mounted: function(){
        document.getElementById('awards').style.display = 'block';
        if(window.innerWidth <= 992){
            this.desktop = false
          }
    },
    computed: {
    },
    methods:{
    }
  })