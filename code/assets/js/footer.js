var footer = new Vue({
    el: "#footer",
    delimiters: ['${', '}'],
    props:{},
    data: {
        dialogVisibleQuestions: false,
        activeQuestion: 1
    },
    validations: {
    },
    watch: {
    },
    created: function(){
        document.getElementById('footer').style.display = 'block';
    },
    mounted: function(){
    },
    computed: {
    },
    methods:{
    }
  })