var BeatLoader = VueSpinner.BeatLoader
var profile = new Vue({
    el: "#profile",
    delimiters: ['${', '}'],
    props:{},
    data: {
        memes: [],
        dialogVisibleEnterLote: false,
        name: '',
        document: '',
        email: '',
        points: 0,
        phone: '',
        memesTotal: 0,
        lote: '',
        loading: false,
        page: 1,
    },
    validations: {
      lote:{
          required: requiredValidator,
          regex: loteValidator,
      }
    },
    watch: {
        dialogVisibleEnterLote: function(newValue){
            if(newValue == false){
                this.$v.lote.$reset()
            }
        }
    },
    created: function(){
    },
    mounted: function(){
        if(localStorage.getItem('token')  != undefined){
            this.loadMe(localStorage.getItem('token'))
            this.loadMemes()
        }
        document.getElementById('profile').style.display = 'block';
    },
    computed: {
    },
    methods:{
        removeLote: function(){
            this.lote = ''
        },
        loadMe: function(token){
            var formData = new FormData()
            formData.append('token', token)
            axios.post('/api/users/detail', formData)
            .then(function(response){
              if(response.data.result == 1){
                this.name = response.data.user.name
                this.points = response.data.user.points
                this.email = response.data.user.email;
                this.phone = response.data.user.phone;
                this.document = response.data.user.dni;
              }
            }.bind(this))
        },
        loadMemes: function(){
            var formData = new FormData()
            formData.append('token', localStorage.getItem('token'))
            var url = '/api/users/list_memes_by_user/' + this.page
            axios.post(url, formData)
            .then(function(response){
              if(response.data.result == 1){
                this.memes = response.data.memes
                this.memesTotal = parseInt(response.data.pagination_total) * 4
              }
            }.bind(this))
        },
        pageList: function(page){
            this.page = page
            this.loadMemes()
        },
        shareLote: function(){
            this.loading = true
            var formData = new FormData()
            formData.append('token', localStorage.getItem('token'))
            formData.append('lote', this.lote)
            axios.post('/api/users/share_lote', formData)
            .then(function(response){
                this.loading = false
                this.lote = ''
                this.dialogVisibleEnterLote = false
                
                if(response.data.result == 1){
                    this.points = response.data.points
                    menu.points = response.data.points
                    this.loadMemes(1)
                    if(response.data.prize){
                        menu.showWinner(response.data.prize.type)
                    } else {
                        menu.showShareSuccess(1)
                    }
                }
            }.bind(this))
            .catch(function(fail){
                this.loading = false
                this.dialogVisibleEnterLote = false
                menu.showShareError()
            }.bind(this))
        }
    }
  })