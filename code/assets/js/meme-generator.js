Vue.component('input-autofit', {
  delimiters: ['${', '}'],
  props: {
    fontSize: {
      type: Number,
      default: 13
    },
    fontFamily: {
      type: String,
      default: "helvetica-bold"
    },
    width: {
      type: Number,
      default: 100
    },
    text: {
      type: String,
      default: "sdsdsd"
    },
    fontColor: {
      type: String,
      default: "#ffffff"
    }
  },
  data: function () {
    return {
      calculatedFontSize: ""
    }
  },
  created: function(){
    if(localStorage.getItem('token') == null){
      window.location = "/home"
    }
  },
  mounted: function(){
    this.calculatedFontSize = this.fontSize
  },
  watch: {
    text: function(newValue){
        this.$nextTick(function(){
            this.resizeContents()
        }.bind(this))

    }
  },
  methods:{
    resizeContents: function(){

        var input = this.$refs.realInput
        var inputWidth = Math.round(input.getBoundingClientRect().width);
        var percent = this.width / inputWidth

        if(percent < 1){
            this.calculatedFontSize = this.fontSize * percent
        } else {
            this.calculatedFontSize = this.fontSize
            percent = 1
        }

        this.$emit("input", percent)

    }
  },
  template: "#input-autofit"
})

var memeGenerator = new Vue({
  el: "#meme-generator",
  delimiters: ['${', '}'],
  props:{
    finalMemeWidth: {
      type: Number,
      default: 500
    },
    finalMemeHeight: {
      type: Number,
      default: 500
    },
    memeFontSize: {
      type: Number,
      default: 48
    }
  },
  data: {
    imageUrl: null,
    colorBottom: "#f6d02e",
    textBottom: "",
    bottomFontSize: 0,
    colorTop: "#f6d02e",
    textTop: "",
    topFontSize: 0,
    previewImageWidth: 0,
    previewImageHeight: 0,
    finalImageWidth: 0,
    finalImageHeight: 0,
    dialogVisibleShare: false,
    fontFamily: "helvetica-bold",
    formData: null,
    loteNumber: "",
    selectedNetwork: 0,
    shareUrl: null,
    previewMemeWidth: 380,
    previewMemeHeight:400,
    processing: false,
    memeName:""
  },
  validations: {
      loteNumber:{
          required: requiredValidator,
          regex: loteValidator,
      }
  },
  components: {
  },
  watch: {
      dialogVisibleShare: function(newValue){
          if(newValue == false){
              this.$v.loteNumber.$reset()
          }
      }
  },
  created: function(){
    if(localStorage.getItem('token') == null){
      window.location = "/home"
    }
    document.getElementById('meme-generator').style.display = 'block';
  },
  mounted: function(){
    this.bottomFontSize = this.finalFontSize

    // EJEMPLO DE COMO LLAMAR A LOS MODALES DE BASE
    menu.dialogVisibleRegister = false

    var w = window.innerWidth;
    var h = window.innerHeight;

    if(w < 400){
      this.previewMemeWidth = 300;
      this.previewMemeHeight = 300;
    }
  },
  computed: {
    finalFontSize: function(){
        return (this.memeFontSize * ((this.finalMemeWidth / this.previewMemeWidth)))
    },
  },
  methods:{
    removeLote: function(){
      this.loteNumber = ''
    },
    goMemeList: function(){
      location.href = '/meme-list'
    },
    selectNetwork: function(network){
      if(this.selectedNetwork == network){
        this.selectedNetwork = 0
      } else {
        this.selectedNetwork = network
      }
    },
    handleTextBottom: function(percent){
        this.textBottomPercent = percent
        this.bottomFontSize = this.finalFontSize * percent
    },
    handleTextTop: function(percent){
        this.textTopPercent = percent
        this.topFontSize = this.finalFontSize * percent
    },
    b64ToUint8Array: function(b64Image) {
       var img = atob(b64Image.split(',')[1]);
       var img_buffer = [];
       var i = 0;
       while (i < img.length) {
          img_buffer.push(img.charCodeAt(i));
          i++;
       }
       return new Uint8Array(img_buffer);
    },
    generateMeme: function(){
      if(this.imageUrl){
        html2canvas(document.querySelector("#meme-image-for-screenshot"), {width:this.finalMemeWidth, height:this.finalMemeHeight, backgroundColor:"#DA1F27"}).then(function(canvas){
          this.dialogVisibleShare = true
          this.processing = true

          this.$nextTick(function(){

            document.getElementById("share-image-container").innerHTML = ""
            document.getElementById("share-image-container").appendChild(canvas)

            canvas.style.width = "200px";
            canvas.style.height = "200px";

            var b64Image = canvas.toDataURL();
            var u8Image  = this.b64ToUint8Array(b64Image);
            var blob = new Blob([ u8Image ], {type: "image/png", name:'file.png'});

            var formData = new FormData();
            formData.append("image", blob);
            formData.append("token", localStorage.getItem('token'));

            this.processing = true

            this.saveMemeImageOnServer(formData, function(response){

              this.memeName = response.data.name
              this.processing = false

              this.formData = new FormData();
              this.formData.append("path", response.data.path);
              this.formData.append("token", localStorage.getItem('token'));

            }.bind(this))

          }.bind(this))
        }.bind(this));
      } else {
        this.$message.error('Opps! Debes seleccionar una imagen.');
      }
    },
    saveMeme: function(){

      var popupShare = null

      this.validateLote(function(){

        if(this.selectedNetwork == 3){
            popupShare = window.open("", "_blank")
        }

        this.saveMemeOnServer(this.formData, function(response){

          var formData = new FormData();
          formData.append("token",  localStorage.getItem('token'))
          formData.append("lote", this.loteNumber)
          formData.append("meme", response.data.meme)

          var url = window.siteUrl+"meme/"+this.memeName

          if(this.selectedNetwork == 1){
            FB.ui({
              method: 'share',
              href: url,
            }, function(responseShare){
              if(responseShare == undefined){
                // formData.append("option", 0)
                // this.saveShare(1, formData, null, null)
              } else {
                formData.append("option", this.selectedNetwork)
                this.saveShare(2, formData, null, null)
              }
            }.bind(this));
          }

          if(this.selectedNetwork == 3){
            formData.append("option", this.selectedNetwork)
            this.saveShare(2, formData, url, popupShare)
          }

          if(this.selectedNetwork == 0){
            // formData.append("option", 0)
            // this.saveShare(0, formData, url, null)
          }

        }.bind(this))

      }.bind(this))



      // if(this.selectedNetwork == 1){
      //   FB.ui({
      //     method: 'share',
      //     href: 'https://developers.facebook.com/docs/',
      //   }, function(response){
      //     this.validateLote()
      //   }.bind(this));
      // } else {
      //   // if(this.selectedNetwork != 0){
      //     // var popupShare = window.open("", "_blank")
      //   // }
      // }





      // var fd = new FormData();
      // fd.append("token", localStorage.getItem('token'))
      // fd.append("lote", this.loteNumber)

      // axios.post("/api/meme/validate-lote", fd)
      // .then(function (responseValidate) {

      //   if(responseValidate.data.valid == false){
      //     menu.dialogVisibleTwelve = true
      //     this.dialogVisibleShare = false
      //     this.loteNumber = ""
      //     return
      //   }

      //   axios.post("/api/meme/save-meme", this.formData)
      //   .then(function (responseSave) {

      //     var formData = new FormData();
      //     formData.append("token",  localStorage.getItem('token'))
      //     formData.append("option", this.selectedNetwork)
      //     formData.append("lote", this.loteNumber)
      //     formData.append("meme", responseSave.data.meme)


      //   }.bind(this))
      //   .catch(function (error) {
      //     this.dialogVisibleShare = false
      //     menu.dialogVisibleTwelve = true
      //   }.bind(this));

      // }.bind(this))
      // .catch(function (error) {
      //     console.log(error);
      // }.bind(this));

    },
    validateLote: function(callback){
      var fd = new FormData();
      fd.append("token", localStorage.getItem('token'))
      fd.append("lote", this.loteNumber)

      axios.post("/api/meme/validate-lote", fd)
      .then(function (responseValidate) {

        if(responseValidate.data.valid == false){
          menu.dialogVisibleTwelve = true
          this.dialogVisibleShare = false
          this.loteNumber = ""
          this.selectedNetwork = 0
        } else {
          callback()
        }

      }.bind(this))
      .catch(function (error) {
          console.log(error);
      }.bind(this));
    },
    saveMemeImageOnServer: function(formData, callback){
      axios.post("/api/meme/save-meme-image", formData)
      .then(function (responseSave) {
        callback(responseSave)
      }.bind(this))
      .catch(function (error) {
        this.$message.error("Hubo un error guardando la imagen, por favor inténtelo nuevamente");
      }.bind(this));
    },
    saveMemeOnServer: function(formData, callback){
      axios.post("/api/meme/save-meme", formData)
      .then(function (responseSave) {
        callback(responseSave)
      }.bind(this))
      .catch(function (error) {
        this.$message.error("Hubo un error guardando el meme, por favor inténtelo nuevamente");
      }.bind(this));
    },
    saveShare: function(optionsEarned, formData, url, popupShare){
       axios.post("/api/users/share_meme", formData)
      .then(function (responseShare) {

        var win = false

        if(responseShare.data.prize){
          win = true
          menu.showWinner(responseShare.data.prize.type)
        } else {
          menu.showShareSuccess(optionsEarned)
        }

        this.dialogVisibleShare = false
        this.loteNumber = ""


        if(this.selectedNetwork == 3){
          this.shareUrl = "https://api.whatsapp.com/send?text=" + url
        }


        setTimeout(function(){
          if(this.selectedNetwork == 3){
            popupShare.location = this.shareUrl
          }
          this.resetForm()
        }.bind(this), 100)

        menu.loadMe()

      }.bind(this))
      .catch(function (error) {
        console.log(error)
        this.resetForm()
        menu.showShareError()
      }.bind(this));
    },
    resetForm: function(){
      this.shareUrl = null
      this.imageUrl = null
      this.textTop = ""
      this.textBottom = ""
      this.colorTop = "#f6d02e"
      this.colorBottom = "#f6d02e"
      this.selectedNetwork = 0
      this.memeName = ""
    },
      handleRemove: function(file, fileList) {
        console.log(file, fileList);
      },
      handleChange: function(file) {

        var fr = new FileReader;

        fr.onload = function() { // file is loaded
            var img = new Image;

            img.onload = function() {
                // console.log(img.width, img.height); // image is loaded; sizes are available
                var percentPreview = 1
                var percentFinal = 1

                if(img.width > this.previewMemeWidth || img.height > this.previewMemeHeight){

                    if(img.width > img.height){
                        percentPreview = this.previewMemeWidth / img.width
                        percentFinal = this.finalMemeWidth / img.width
                    } else {
                        percentPreview = this.previewMemeHeight / img.height
                        percentFinal = this.finalMemeHeight / img.height
                    }
                }

                this.previewImageWidth = img.width * percentPreview
                this.previewImageHeight = img.height * percentPreview

                this.finalImageWidth = img.width * percentFinal
                this.finalImageHeight = img.height * percentFinal


                this.imageUrl = URL.createObjectURL(file.raw);
            }.bind(this);

            img.src = fr.result; // is the data URL because called with readAsDataURL
        }.bind(this);

        fr.readAsDataURL(file.raw);
      }
    }
})
