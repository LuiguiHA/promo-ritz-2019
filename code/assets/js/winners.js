var BeatLoader = VueSpinner.BeatLoader
var profile = new Vue({
    el: "#winners",
    delimiters: ['${', '}'],
    props:{},
    data: {
      winners: [],
      winnersPartitions: [],
      winnersTotal: 0,
      page: 0,
      itemsPerPage: 9
    },
    validations: {
    },
    watch: {
    },
    created: function(){
    },
    mounted: function(){
        this.loadWinners(1)
        document.getElementById('winners').style.display = 'block';
    },
    computed: {
    },
    methods:{
      loadWinners: function(page){
        this.page= page
        var url = '/api/winners/list_winners/' + page
        axios.get(url)
        .then(function(response){
          var data = response.data
          if(data.result == 1){
            this.winners = data.users
            this.winnersTotal = parseInt(data.pagination_total) * this.itemsPerPage
          }
        }.bind(this))
      },
      pageList: function(page){
        this.loadWinners(page)
      }
    }
  })