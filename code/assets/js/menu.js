var BeatLoader = VueSpinner.BeatLoader
var menu = new Vue({
    el: "#menu",
    delimiters: ['${', '}'],
    props:{},
    data: {
      dialogVisibleLogin: false,
      dialogVisibleRegister: false,
      dialogVisibleAwardGift: false,
      dialogVisibleAwardSpotify: false,
      dialogVisibleSuccessRegister: false,
      dialogVisibleShareSuccess: false,
      dialogVisibleTwelve: false,
      dialogTermsVisible: false,
      optionsEarned: 0,
      loading: false,
      document: '',
      user: {
        documentRegister: '',
        name: '',
        email: '',
        phone: ''
      },
      isAdult: false,
      acceptedTerms: false,
      acceptedDates: false,
      marginMenu: true,
      logued: false,
      username: '',
      points: 0
    },
    validations: {
      document: {
        required: requiredValidator,
        numeric: numericValidator,
        minLength: minLengthValidator(8),
        maxLength: maxLengthValidator(8)
      },
      user: {
        documentRegister: {
          required: requiredValidator,
          numeric: numericValidator,
          minLength: minLengthValidator(8),
          maxLength: maxLengthValidator(8)
        },
        name: {
          required: requiredValidator,
        },
        email: {
          email: emailValidator,
          required: requiredValidator
        },
        phone: {
          required: requiredValidator,
          numeric: numericValidator,
          minLength: minLengthValidator(7),
          maxLength: maxLengthValidator(9)
        }
      }
    },
    watch: {
    },
    created: function(){
      document.getElementById('menu').style.display = 'block';
    },
    mounted: function(){
      this.loadMe()
      if(location.pathname == '/home'){
        this.marginMenu = false
      }
      var hash = window.location.hash
      if(hash){
        hash = hash.split('-')[0]
        setTimeout(function(){
          $('html,body').scrollTo($(hash).offset().top - 60, {duration: 1000});
        }, 500)
        
        return false
      }
    },
    computed: {
    },
    methods:{
      showShareSuccess: function(optionsEarned){
        this.optionsEarned = optionsEarned
        this.dialogVisibleShareSuccess = true
      },
      showShareError: function(){
        this.dialogVisibleTwelve = true
      },
      showWinner: function(t){
        if(parseInt(t) == 1){
          menu.dialogVisibleAwardGift = true
        }
        if(parseInt(t) == 2){
          menu.dialogVisibleAwardSpotify = true
        }
      },
      loadMe: function(){
        if(localStorage.getItem('token')  != undefined){
          var token = localStorage.getItem('token')
          var formData = new FormData()
          formData.append('token', token)
          axios.post('/api/users/detail', formData)
          .then(function(response){
            if(response.data.result == 1){
              this.username = response.data.user.name_short
              this.points = response.data.user.points
              this.logued = true;
            }
          }.bind(this))
        }
      },
      scrollSection: function(section){
        if(location.pathname == '/home'){
          var headerHeight = 0 
          if(window.innerWidth > 992){
            headerHeight = 10
          } else {
              headerHeight = 0
          }
          var section = $(section).offset().top - 60
          $('html,body').scrollTo(section, {duration: 1000});
        } else {
          location.href='/home' + section + '-redirect'
        }
      },
      loginDialog: function(){
        this.dialogVisibleLogin = true
      },
      registerDialog: function(){
        this.dialogVisibleLogin = false
        this.dialogVisibleRegister = true
      },
      login: function(){
        this.$v.document.$touch()
        if (this.$v.document.$invalid == false){
          var data = null
          this.loading = true
          var formData = new FormData()
          formData.append('dni', this.document)
          axios.post('/api/users/login', formData)
          .then(function(response){
            this.loading = false
            data = response.data
            if(data.result == 1){
              this.dialogVisibleLogin = false
              this.$message.success(data.message);
              localStorage.setItem('token', data.token)
              window.location = "/perfil"
            }
            return
          }.bind(this))
          .catch(function(fail){
            this.loading = false
            data = fail.response.data
            this.$message.error(data.message);
          }.bind(this))
        }
      },
      register: function(){
        this.$v.user.$touch()
        if (this.$v.user.$invalid == false){
          this.loading = true
          var formData = new FormData()
          formData.append('dni', this.user.documentRegister)
          formData.append('name', this.user.name)
          formData.append('email', this.user.email)
          formData.append('phone', this.user.phone)
          axios.post('/api/users', formData)
          .then(function(response){
            this.loading = false
            var data = response.data
            if(data.result == 1){
              this.dialogVisibleRegister = false
              this.dialogVisibleSuccessRegister = true
              localStorage.setItem('token', data.token)
              location.reload();
            } else{
              this.$message.error(data.message);
            }
          }.bind(this))
        }
      },
      logout: function(){
        localStorage.removeItem('token')
        location.href= '/home'
      }
    }
  })