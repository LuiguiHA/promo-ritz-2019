/**
* Slot machine
* Author: Saurabh Odhyan | http://odhyan.com
*
* Licensed under the Creative Commons Attribution-ShareAlike License, Version 3.0 (the "License")
* You may obtain a copy of the License at
* http://creativecommons.org/licenses/by-sa/3.0/
*
* Date: May 23, 2011
*/
$(document).ready(function() {
    /**
    * Global variables
    */
    var completed = 0,
        imgHeight = 960,
        posArr = [
            0, //empaque1
            80, //empaque2
            160, //empaque3
            240, //empaque4
            320, //empaque1
            400, //empaque2
            480, //empaque3
            560, //empaque4
            640, //empaque1
            720, //empaque2
            800, //empaque3
            880 //empaque4
        ];

    var win = [];
    win[0] = win[320] = win[640] = 1;
    win[80] = win[400] = win[720] = 4;
    win[160] = win[480] = win[800] = 2;
    win[240] = win[560] = win[880] = 3;

    /**
    * @class Slot
    * @constructor
    */
    function Slot(el, max, step) {
        this.speed = 0; //speed of the slot at any point of time
        this.step = step; //speed will increase at this rate
        this.si = null; //holds setInterval object for the given slot
        this.el = el; //dom element of the slot
        this.maxSpeed = max; //max speed this slot can have
        this.pos = null; //final position of the slot

        $(el).pan({
            fps:30,
            dir:'down'
        });
        $(el).spStop();
    }

    /**
    * @method start
    * Starts a slot
    */
    Slot.prototype.start = function() {
        var _this = this;
        $(_this.el).addClass('motion');
        $(_this.el).spStart();
        _this.si = window.setInterval(function() {
            if(_this.speed < _this.maxSpeed) {
                _this.speed += _this.step;
                $(_this.el).spSpeed(_this.speed);
            }
        }, 100);
    };

    /**
    * @method stop
    * Stops a slot
    */
    Slot.prototype.stop = function() {
        var _this = this,
            limit = 30;
        clearInterval(_this.si);
        _this.si = window.setInterval(function() {
            if(_this.speed > limit) {
                _this.speed -= _this.step;
                $(_this.el).spSpeed(_this.speed);
            }
            if(_this.speed <= limit) {
                _this.finalPos(_this.el);
                $(_this.el).spSpeed(0);
                $(_this.el).spStop();
                clearInterval(_this.si);
                $(_this.el).removeClass('motion');
                _this.speed = 0;
            }
        }, 100);
    };

    /**
    * @method finalPos
    * Finds the final position of the slot
    */
    Slot.prototype.finalPos = function() {
        var el = this.el,
            el_id,
            pos,
            posMin = 2000000000,
            best,
            bgPos,
            i,
            j,
            k;

        el_id = $(el).attr('id');
        //pos = $(el).css('background-position'); //for some unknown reason, this does not work in IE
        pos = document.getElementById(el_id).style.backgroundPosition;
        pos = pos.split(' ')[1];
        pos = parseInt(pos, 10);

        for(i = 0; i < posArr.length; i++) {
            for(j = 0;;j++) {
                k = posArr[i] + (imgHeight * j);
                if(k > pos) {
                    if((k - pos) < posMin) {
                        posMin = k - pos;
                        best = k;
                        this.pos = posArr[i]; //update the final position of the slot
                    }
                    break;
                }
            }
        }

        best += imgHeight + 4;
        bgPos = "0 " + best + "px";
        $(el).animate({
            backgroundPosition:"(" + bgPos + ")"
        }, {
            duration: 200,
            complete: function() {
                completed ++;
            }
        });
    };

    /**
    * @method reset
    * Reset a slot to initial state
    */
    Slot.prototype.reset = function() {
        var el_id = $(this.el).attr('id');
        $._spritely.instances[el_id].t = 0;
        $(this.el).css('background-position', '0px 0px');
        this.speed = 0;
        completed = 0;
        $('#result').html('');
    };

    function enableControl() {
        $('#control').attr("disabled", false);
    }

    function disableControl() {
        $('#control').attr("disabled", true);
    }

    function printResult() {
        var res;
        if(win[a.pos] === win[b.pos] && win[a.pos] === win[c.pos] && win[a.pos] === win[d.pos]) {
            res = "cargando...";

            $.ajax({
                type:"POST",
                url: PATH_GATEWAY + 'concourses',
                data: { point : win[a.pos]},
                dataType: "json",
                success: function (response) {

                  console.log(response);

                  if(response.code == 1){
                    $('#result').html('Ganaste '+response.data.points+' puntos');
                    mostrarPuntos(response.data.total);
                    $('.modUser p span').html(response.data.total + ' Ptos.');

                    if (!(typeof response.data.win_prize === 'undefined' || response.data.win_prize === null)) {
                      if(response.data.win_prize == 1) {
                        $('#modal-premio .premio').html('<h3>¡Ganaste!</h3>' +
                        '<img src="'+PATH_BASE+'assets/web/images/premios/'+response.data.prize.photo+'" />' +
                        '<div class="nombre">' +response.data.prize.name +'</div>');

                        $('#modal-premio').addClass('md-show');

                        $(".btnAceptarPremio").click(function(event){
                          event.preventDefault();
                          $('#modal-premio').removeClass('md-show');
                        });
                      }
                    }

                  }else{

                  }
                },
                error: function (err) {

                }
            });

        } else {
            res = "¡Anímate! Vuelve a intentarlo";
        }
        $('#result').html(res);
    }

    //create slot objects
    // var a = new Slot('#slot1', 30, 1),
    //     b = new Slot('#slot2', 45, 2),
    //     c = new Slot('#slot3', 70, 3);
    //     d = new Slot('#slot4', 70, 4);

    var a = new Slot('#slot1', 30, 1),
        b = new Slot('#slot2', 45, 2),
        c = new Slot('#slot3', 70, 3);
        d = new Slot('#slot4', 85, 4);

        // var a = new Slot('#slot1', 30, 1),
        //     b = new Slot('#slot2', 30, 1),
        //     c = new Slot('#slot3', 30, 1);

    /**
    * Slot machine controller
    */
    $('#control').click(function() {
        var x;
        if(this.innerHTML == "¡SUERTE!") {
          //$('.blockParticipa .wrap .slot-machine').css('background-image','url('+PATH_BASE+'assets/web/css/images/participa/slot_machine2.png)');
            a.start();
            b.start();
            c.start();
            d.start();
            this.innerHTML = "DETENER";

            disableControl(); //disable control until the slots reach max speed

            //check every 100ms if slots have reached max speed
            //if so, enable the control
            x = window.setInterval(function() {
                if(a.speed >= a.maxSpeed && b.speed >= b.maxSpeed && c.speed >= c.maxSpeed && d.speed >= d.maxSpeed) {
                    enableControl();
                    window.clearInterval(x);
                }
            }, 100);
        } else if(this.innerHTML == "DETENER") {
            a.stop();
            b.stop();
            c.stop();
            d.stop();
            this.innerHTML = "VOLVER A GIRAR";

            //$('.blockParticipa .wrap .slot-machine').css('background-image','url('+PATH_BASE+'assets/web/css/images/participa/slot_machine.png)');

            disableControl(); //disable control until the slots stop

            //check every 100ms if slots have stopped
            //if so, enable the control
            x = window.setInterval(function() {
                if(a.speed === 0 && b.speed === 0 && c.speed === 0 && d.speed === 0 && completed === 4) {
                    enableControl();
                    window.clearInterval(x);
                    printResult();
                }
            }, 100);
        } else { //reset
            a.reset();
            b.reset();
            c.reset();
            d.reset();
            this.innerHTML = "¡SUERTE!";
        }
    });
});
