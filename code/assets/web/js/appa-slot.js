$(document).ready(function(){

  $('.movMenu ul li a,.movMenu .btnSesion').click(function(){
    $('.movMenu').fadeOut();
  });

  $('.btnMenu').click(function(){
    $('.movMenu').fadeIn();
  });

  $('.movMenu .btnClose').click(function(){
    $('.movMenu').fadeOut();
  });

  $('.footer .menubottom ul li a').click(function(event) {
    event.preventDefault();

    var dataModel = $(this).attr('data-modal');
    $('#'+dataModel).addClass('md-show');
  });

  $('.md-modal .md-close').click(function(event) {
    event.preventDefault();

    $('.md-modal').removeClass('md-show');
  });

});

function mostrarPuntos(puntos){

  $('.modPuntos').html('');

  puntos = puntos + '';

  puntos = puntos.split("");

  if(puntos.length < 8){

    for(var i=0;i < (8 - puntos.length); i++){
      $('.modPuntos').append('<div class="numero">0</div>');
    }

    $.each(puntos, function(index, value) {
        $('.modPuntos').append('<div class="numero">'+value+'</div>');
    });
  }else{
    $.each(puntos, function(index, value) {
        $('.modPuntos').append('<div class="numero">'+value+'</div>');
    });
  }
}
