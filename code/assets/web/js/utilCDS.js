function isAlpha(e) {
    var onkeypress  = (document.all) ? e.keyCode : e.which;
    if(onkeypress == 8 || onkeypress == 46)
        return true;
    var pattern = /[Ã¡Ã©Ã­Ã³ÃºÃÃ‰ÃÃ“ÃšÃ¼ÃœabcdefghijklmnÃ±opqrstuvwxyzABCDEFGHIJKLMNÃ‘OPQRSTUVWXYZ ]/
    var key = String.fromCharCode(onkeypress);
    return pattern.test(key);
}

function isAplhaNumeric(e) {
    var onkeypress  = (document.all) ? e.keyCode : e.which;
    if(onkeypress == 8 || onkeypress == 46)
        return true;
    var pattern = /[Ã¡Ã©Ã­Ã³ÃºÃÃ‰ÃÃ“ÃšÃ¼ÃœabcdefghijklmnÃ±opqrstuvwxyzABCDEFGHIJKLMNÃ‘OPQRSTUVWXYZ 0123456789#_+,.;Â°()/-]/
    var key = String.fromCharCode(onkeypress);
    return pattern.test(key);
}

function isNumeric(e) {
    var onkeypress  = (document.all) ? e.keyCode : e.which;
    if(onkeypress == 8 || onkeypress == 46)
        return true;
    var pattern = /[0123456789]/
    var key = String.fromCharCode(onkeypress);
    return pattern.test(key);
}
