var destino = '';

$(window).on('load',function(){
  if(window.location.hash) {
    $("html,body").animate({scrollTop: $(window.location.hash).offset().top}, 200);
  }
});

$(document).ready(function(){

  $.datepicker.regional['es'] = {

		 closeText: 'Cerrar',

		 prevText: '< Ant',

		 nextText: 'Sig >',

		 currentText: 'Hoy',

		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],

		 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],

		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],

		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],

		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],

		 weekHeader: 'Sm',

		 dateFormat: 'dd/mm/yy',

		 firstDay: 1,

		 isRTL: false,

		 showMonthAfterYear: false,

		 yearSuffix: ''

		 };

		 $.datepicker.setDefaults(jQuery.datepicker.regional['es']);



		 $( "#fecha_compra" ).datepicker({ dateFormat: 'yy-mm-dd' });



  $('.movMenu ul li a,.movMenu .btnSesion').click(function(){
    $('.movMenu').fadeOut();
  });

  $('.btnMenu').click(function(){
    $('.movMenu').fadeIn();
  });

  $('.movMenu .btnClose').click(function(){
    $('.movMenu').fadeOut();
  });

  $('#botonSubastar').click(function(event){
    event.preventDefault();

    var puntos = $('#puntos_subasta').val();
    var psId = $('#prizes_sale_id').val();
    var token = $('#token').val();

    if(puntos != ''){
      $('.lblErrorSubasta').html('');

      $.ajax({
          type:"POST",
          url: PATH_GATEWAY + 'api/prizes/sale',
          data: { token : token, puntos : puntos, prize_sales_id : psId },
          dataType: "json",
          success: function (res) {
            if(res.resultado == 1){
              $('#botonSubastar').html('¡Aumentar!');
              $('#puntos_subasta').val('');
            }else{
              $('.lblErrorSubasta').html(res.mensaje);
            }
          },
          error: function (err) {

          }
      });

    }else {
      $('.lblErrorSubasta').html('Ingrese una cantidad de puntos');
    }

  });

  $('.blockParticipa a.subastar').click(function(event) {
    event.preventDefault();

    var href = $(this).attr('href');

    $('.btnIniciarSubasta').click(function(event){
      location.href = href;
    });
  });

  $('.modVideo .videoMenu a, .movMenu .btnSesion.md-trigger').click(function(event) {
    event.preventDefault();
  });

  $('.blockMecanicaTest .wrap .pasos .paso3 a').click(function(event) {
    event.preventDefault();
  });

  $('#verSubasta').click(function(event) {

    var psId = $(this).attr('psId');

    mostrarStatus(psId);
  });

  $('.footer .menubottom ul li a').click(function(event) {
    event.preventDefault();
  });

  $('.formModal .btnRegistro').click(function(event) {
    event.preventDefault();
  });

  $('.modUser .btnSesion').click(function(event) {
    event.preventDefault();
    destino = '';
  });

  $('.btnLogin').click(function(event){

    event.preventDefault();
    var email = $('#formLogin #emailLogin').val();
    var passwordLogin = $('#formLogin #passwordLogin').val();

    if (validate.login()) {

      $('.lblError').html('');

      $.ajax({
          type:"POST",
          url: PATH_GATEWAY + 'login',
          data: { password : passwordLogin, email : email },
          dataType: "json",
          success: function (response) {
            if(response.code == 1){
              location.href = PATH_GATEWAY + destino;
            }else{
              $('.lblError').html(response.message);
            }
          },
          error: function (err) {

          }
      });

    }else{
      $('.inError').html('Debe de ingresar los datos');
    }
  });


  $('.btnRegistrar').click(function(event){

    event.preventDefault();

    var params = {
        name : $('#formRegistro #name').val(),
        email : $('#formRegistro #email').val(),
        password : $('#formRegistro #password').val(),
        phone : $('#formRegistro #phone').val(),
        fecha_compra : $('#formRegistro #fecha_compra').val(),
        tienda_compra : $('#formRegistro #tienda_compra').val(),
        terminos : $('#formRegistro #terminos').val(),
        datos : $('#formRegistro #datos').val(),
        older : $('#formRegistro #older').val()
    };

    if (validate.register()) {

      $('.lblError').html('');

      $.ajax({
          type:"POST",
          url: PATH_GATEWAY + 'api/users',
          data: params,
          dataType: "json",
          success: function (res) {
            if(res.result == 1){
              $("#formRegistro")[0].reset();

              listarErrores(res.message);

              setTimeout(function(){
                $("#modal-1").removeClass("md-show");
                $("#modal-2").removeClass("md-show");

                location.href = PATH_GATEWAY + destino;

              }, 3000);

            }else{
              listarErrores(res.message);
            }
          },
          error: function (err) {

          }
      });

    }else{
      $('.inError').html('Debe de ingresar los datos correctamente.');
    }
  });

  $('.modHomePremios .sliderPremios').slick(
    {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      infinite: true,
      prevArrow: $('.modHomePremios .flechaIzq'),
      nextArrow: $('.modHomePremios .flechaDer')
    }
  );

  $('.blockParticipa .sliderPremios').slick(
    {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      infinite: true,
      prevArrow: $('.blockParticipa .flechaIzq'),
      nextArrow: $('.blockParticipa .flechaDer')
    }
  );

  $('.modHomePremios a.btnAcumular.md-trigger').on('click', function(event){
    event.preventDefault();
    destino = $(this).attr('destino');
  });

  $('.modNuevoHome .box a.md-trigger').on('click', function(event){
    event.preventDefault();
    destino = $(this).attr('destino');
  });

  $('.boxVideoGeneral__boton').on('click', function(event){
    event.preventDefault();
    var iframe = $(this).data('video');
    $('#boxVideoGeneral__video').attr('src','https://www.youtube.com/embed/'+iframe+'?rel=0&amp;showinfo=0&amp;autoplay=1');
    $('.boxVideoGeneral').fadeIn(250);
  });

  $('.boxVideoGeneral__cerrar').on('click', function(event){
    event.preventDefault();
    $('.boxVideoGeneral').fadeOut(250, function(){
      $('#boxVideoGeneral__video').attr('src','');
    });
  });

  // $('.menu ul li a, .btnAcumular').click(function(e){
  //   e.preventDefault();
  //   $("html,body").animate({scrollTop: $(this.hash).offset().top}, 500);
  //
  // });

  $('.logoRitz').click(function(e){
    e.preventDefault();
    $("html,body").animate({scrollTop: $('#top').offset().top}, 1000);

  });

});

function listarErrores(obj){

  if(typeof(obj) == 'string'){
    $('.lblError').html(obj);
  }else{
    var  mensaje = '';
    $.map(obj, function(value, index) {
        mensaje += value + '<br>';
    });
    $('.lblError').html(mensaje);
  }
}

function mostrarStatus(psId){
  $.ajax({
      type:"GET",
      url: PATH_GATEWAY + "api/prizes/status?prize_sales_id="+psId,
      dataType: "json",
      success: function (res) {
        if(res.result == 1){
          if(res.usuarios.length > 0){
            var userTop = res.usuarios[0];
            $('.intPuntos').html(userTop.points);

            $('#modal-subasta .listado').html('<div class="row header">' +
              '<div class="col one">NOMBRE COMPLETO</div>' +
              '<div class="col two">PUNTOS</div>' +
            '</div>');

            $.map(res.usuarios, function(user, index) {
              $('#modal-subasta .listado').append('<div class="row"><div class="col one">'+user.name+'</div><div class="col two">'+user.points+'</div></div>');
            });
          } else {
            $('#modal-subasta .listado').html('Aún no hay personas que hayan subastado.');
          }

        }
      },
      error: function (err) {

      }
  });
}


var validate = {
    register: function () {
        var Valid = {
            name: true,
            password: true,
            email: true,
            phone: true,
            fecha_compra: true,
            tienda_compra: true,
            terminos: true,
            datos: true,
            older: true
        };
        var name = $('#name'),
            password = $('#password'),
            email = $('#email'),
            phone = $('#phone'),
            fecha_compra = $('#fecha_compra'),
            tienda_compra = $('#tienda_compra'),
            terminos = $('#terminos'),
            datos = $('#datos'),
            older = $('#older');

        if (!older.is(':checked')) {
            $('.lblError').html('Debes de ser mayor de edad para participar');
            Valid.older = false;
        }

        if (!terminos.is(':checked')) {
            $('.lblError').html('Debe de aceptar los términos y condiciones');
            Valid.terminos = false;
        }

        if (tienda_compra.val() == undefined || tienda_compra.val() == '') {
            $('.lblError').html('Ingrese la tienda de la compra');
            tienda_compra.addClass('error');
            Valid.tienda_compra = false;
        } else {
            fecha_compra.removeClass('error');
        }

        if (fecha_compra.val() == undefined || fecha_compra.val() == '') {
            $('.lblError').html('Ingrese la fecha de la compra');
            fecha_compra.addClass('error');
            Valid.fecha_compra = false;
        } else {
            fecha_compra.removeClass('error');
        }

        if (phone.val() == undefined || phone.val() == '') {
            $('.lblError').html('Debe de ingresar el nro. de su celular');
            phone.addClass('error');
            Valid.phone = false;
        } else {
            phone.removeClass('error');
        }

        if (password.val() == undefined || password.val() == '') {
            $('.lblError').html('Debe de ingresar un password');
            password.addClass('error');
            Valid.password = false;
        } else {
            password.removeClass('error');
        }

        if (email.val() == undefined || email.val() == '') {
            $('.lblError').html('Debe de ingresar su correo electrónico');
            email.addClass('error');
            Valid.email = false;

        } else {
            var expression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

            if (expression.test(email.val())) {
              email.removeClass('error');
            }
            else {
              $('.lblError').html('Ingrese un correo electrónico válido');
              email.addClass('error');
              Valid.email = false;
            }
        }

        if ((name.val() == undefined || name.val() == '')) {
            $('.lblError').html('Debe de ingresar su nombre completo');
            name.addClass('error');
            Valid.name = false;
        } else {
            name.removeClass('error');
        }

        return (Valid.name && Valid.email && Valid.password && Valid.phone && Valid.fecha_compra && Valid.tienda_compra && Valid.terminos && Valid.older);
    },
    login: function () {
        var Valid = {
            passwordLogin: true,
            emailLogin: true
        };

        var passwordLogin = $('#passwordLogin'),
            emailLogin = $('#emailLogin');

        if (emailLogin.val() == undefined || emailLogin.val() == '') {
            $('.lblError').html('Debe de ingresar su correo electrónico');
            emailLogin.addClass('error');
            Valid.emailLogin = false;

        } else {
            var expression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

            if (expression.test(emailLogin.val())) {
              emailLogin.removeClass('error');
            }
            else {
              $('.lblError').html('Ingrese un correo electrónico válido');
              emailLogin.addClass('error');
              Valid.emailLogin = false;
            }
        }

        if ((passwordLogin.val() == undefined || passwordLogin.val() == '')) {
            $('.lblError').html('Debe de ingresar su password');
            passwordLogin.addClass('error');
            Valid.passwordLogin = false;
        } else {
            passwordLogin.removeClass('error');
        }

        return (Valid.passwordLogin && Valid.emailLogin);
    }
};

function countdown(start,final){

  var finicio = start+"";
  var ffin = final+"";

  var fechaHoraArray = ffin.split(' ');
  var fechaArray = fechaHoraArray[0].split('-');

  var horaArray = fechaHoraArray[1].split(':');
    var fecha = new Date(fechaArray[0],fechaArray[1],fechaArray[2],horaArray[0],horaArray[1],horaArray[2])
    var hoy = new Date()
    var dias = 0
    var horas = 0
    var minutos = 0
    var segundos = 0

    if (fecha>hoy){
      var diferencia=(fecha.getTime()-hoy.getTime())/1000
      dias=Math.floor(diferencia/86400)
      diferencia=diferencia-(86400*dias)
      horas=Math.floor(diferencia/3600)
      diferencia=diferencia-(3600*horas)
      minutos=Math.floor(diferencia/60)
      diferencia=diferencia-(60*minutos)
      segundos=Math.floor(diferencia)

      $('.container_prize_sale .prize_crono .crono').html(horas + ':' + minutos + ':' + segundos);
      $('.blockParticipa .sliderPremios .item .crono').html(horas + ':' + minutos + ':' + segundos);

      if (horas>0 || minutos>0 || segundos>0){
          setTimeout("countdown('"+start+"','"+final+"')",1000)
      }
    }
    else{
      // $("#subastar").hide();
      // $("#modSubasta").show();
    }
}
