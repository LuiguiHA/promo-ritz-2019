/**
 * Created by Mediabyte on 27/01/2017.
 */
$(document).ready(function(){

    $("#div_search").click(function () {
        show_hidden_div_search();
    });

    function show_hidden_div_search(){
        $('#img_search').removeAttr('class');
        if ($('#div_content_search').is(':visible')){
            $('#div_content_search').css({"visibility":"hidden"});
            $('#div_content_search').css({"display":"none"});
            $('#img_search').addClass( 'zmdi zmdi-chevron-down' );
        }else{
            $('#div_content_search').css({"visibility":"visible"});
            $('#div_content_search').css({"display":"block"});
            $('#img_search').addClass( 'zmdi zmdi-chevron-up' );
        }
    }

   function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    }

    $('#prize_day').keypress(validateNumber);
    $('#random').keypress(validateNumber);
    $('#search_user_dni').keypress(validateNumber);
    $('#search_user_winner_dni').keypress(validateNumber);
    $('#search_raffle_dni').keypress(validateNumber);


})
