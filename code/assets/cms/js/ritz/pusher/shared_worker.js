/**
 * Created by Mediabyte on 12/12/2016.
 */

importScripts("pusher.worker.js");



// Connect to Pusher
var pusher = new Pusher('d5143f48561b03e9c95d', {
    encrypted: true
});
var $window = $(window);
var local = $window.localStorage.getItem('local');

var pusherChannel = pusher.subscribe('channel_' + 1);

var clients = [];

self.addEventListener("connect", function(evt){
    var client = evt.ports[0];
    clients.push(client);
    client.start();
});

pusherChannel.bind('my_event', function(data) {

    // Relay the payload on to each client
        clients.forEach(function(client){
        client.postMessage(data);
    });
});
